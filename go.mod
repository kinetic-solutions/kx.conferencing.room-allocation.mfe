module bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe

go 1.20

require (
	bitbucket.org/kinetic-solutions/kx.conferencing.audit-records v0.12.99
	bitbucket.org/kinetic-solutions/kx.pulse.krn v0.9.0
	bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe v0.4.1
	bitbucket.org/kinetic-solutions/kx.testing.go-germ v0.0.4
	github.com/aws/aws-lambda-go v1.41.0
	github.com/aws/aws-sdk-go v1.46.3
	github.com/aws/aws-xray-sdk-go v1.8.2
	github.com/aws/smithy-go v1.13.5
	github.com/go-playground/validator/v10 v10.15.5
	github.com/jmoiron/sqlx v1.3.5
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.4
	go.uber.org/zap v1.26.0
)

require (
	bitbucket.org/kinetic-solutions/kx.go.krn v0.0.6 // indirect
	github.com/andybalholm/brotli v1.0.6 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.12.3 // indirect
	github.com/gabriel-vasile/mimetype v1.4.3 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.50.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231016165738-49dd2c1f3d0b // indirect
	google.golang.org/grpc v1.59.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

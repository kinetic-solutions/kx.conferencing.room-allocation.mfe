import React from 'react';
import { getTenantCode } from "./core/axios"

export default ({ children }) => {
  const tenantCode = getTenantCode()
  return (
    <>
      {tenantCode === "paytest2" && children}
    </>
  )
}
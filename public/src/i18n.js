import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import enGB from "./locales/en-GB.json";
import format from "./i18n-format";
import getLocale from "./core/locales";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      "en-GB": {
        translation: enGB,
      },
    },
    lng: getLocale(),
    fallbackLng: "en-GB",
    interpolation: {
      escapeValue: false,
      format,
    },
  });

export default i18n;

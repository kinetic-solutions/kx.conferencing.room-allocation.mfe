import "./i18n";
import "./set-public-path";
import { initializeIcons } from "@uifabric/icons";
import { loadTheme } from "@fluentui/react";
import AllocateRoomModal from "./features/allocate-room-modal/AllocateRoomModal"
import { deallocateRoom } from "./core/api";
export let authService;
export let basePath;

const initialize = ({ theme, authService: _authService, basePath: _basePath }) => {
  initializeIcons("/cdn/")
  loadTheme(theme)
  authService = _authService
  basePath = _basePath
}

export {
  initialize,
  deallocateRoom,
  AllocateRoomModal
};

import React, { useState } from "react";
import { PrimaryButton, Spinner } from "@fluentui/react";

const SpinnerButton = ({
  id,
  disabled,
  text,
  ariaLabel,
  spinnerAriaLabel,
  onClick,
  styles,
}) => {
  const [spinnerIsVisible, setSpinnerIsVisible] = useState(false);

  const showSpinner = async () => {
    setSpinnerIsVisible(true);
    const success = await onClick();
    if (success) return;

    setSpinnerIsVisible(false);
  };

  return (
    <PrimaryButton
      id={id}
      disabled={disabled}
      onClick={showSpinner}
      ariaLabel={ariaLabel}
      styles={styles}
    >
      {spinnerIsVisible ? (
        <Spinner
          ariaLabel={spinnerAriaLabel}
          styles={{ root: { position: "absolute" } }}
        />
      ) : (
        <span>{text}</span>
      )}
    </PrimaryButton>
  );
};

export default SpinnerButton;

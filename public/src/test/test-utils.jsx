import { initializeIcons } from "@uifabric/icons";
import "../i18n"

jest.setTimeout(60000)

jest.mock("../core/utils", () => ({
  getEventId: () => 5
}))

jest.mock("../kinetic-room-allocation", () => ({
  authService: {
    getToken: jest.fn().mockResolvedValue("fake-token")
  }
}))

jest.mock("systemjs-webpack-interop", () => ({
  setPublicPath: jest.fn(),
}));

initializeIcons();

import axios from "./axios";
import { getEventId } from "./utils";

export const getAttendee = (attendeeId) => {
  return axios.get(`/room-allocation/events/${getEventId()}/attendees/${attendeeId}`);
};

// #region bedrooms
export const getPaginatedRoomingListBedrooms = (params) => {
  return axios.get(`/room-allocation/events/${getEventId()}/roominglist`, { params })
}

export const getPaginatedReservationRooms = (params) => {
  return axios.get(`/room-allocation/events/${getEventId()}/reservation-rooms`, { params })
}

export const getPaginatedBedrooms = (params) => {
  return axios.get(`/room-allocation/events/${getEventId()}/rooms`, { params })
}
// #endregion bedrooms

export const allocateRoom = (attendeeId, roomId, payload) =>{
  return axios.post(`/room-allocation/events/${getEventId()}/attendees/${attendeeId}/rooms/${roomId}`, payload)
}

export const deallocateRoom = (attendeeId, roomId, payload) => {
  return axios.delete(`/room-allocation/events/${ getEventId() }/attendees/${ attendeeId }/rooms/${ roomId }`, { data: payload })
}

export const getSiteOptions = (scope, eventID) => {
  const params = { scope, eventID }
  return axios.get(`/room-allocation/sites`, { params })
}

export const getAreaOptions = (siteId, scope, eventID) => {
  const params = { siteId, scope, eventID }
  return axios.get(`/room-allocation/areas`, { params })
}

export const getBlockOptions = (siteId, areaId, scope, eventID) => {
  const params = { siteId, areaId, scope, eventID }
  return axios.get(`/room-allocation/blocks`, { params })
}

export const getSubBlockOptions = (masterBlockId, scope, eventID) => {
  const params = { scope, eventID, masterBlockId }
  return axios.get(`/room-allocation/subblocks`, { params })
}

export const getBedroomTypeOptions = (scope, eventID) => {
  const params = { scope, eventID }
  return axios.get(`/room-allocation/bedroomtypes`, { params })
}

export const getAttributeOptions = () => {
  return axios.get("/room-allocation/attributes")
}
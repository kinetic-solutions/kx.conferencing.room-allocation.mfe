const supportedLocales = ["en-GB", "en-US", "en-us"];
export default function getLocale() {
  const locale =
    navigator.languages !== undefined
      ? navigator.languages[0]
      : navigator.language;

  if (supportedLocales.includes(locale)) {
    return locale;
  }

  const fallbackLocale = "en-GB";
  return fallbackLocale;
}

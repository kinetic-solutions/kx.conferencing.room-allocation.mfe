const getTenantCode = () => window.location.host.split(".")[0];
export const getEventId = () => window.location.pathname.split("/")[3];
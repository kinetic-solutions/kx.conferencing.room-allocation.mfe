import axios from "axios";
import { authService } from "../kinetic-room-allocation";

export const getTenantCode = () => window.location.host.split(".")[0];
const baseURL = () => `https://conferencing.api.kxcloud.net/${getTenantCode()}`;

axios.defaults.adapter = require("axios/lib/adapters/http");
axios.defaults.baseURL = baseURL();
axios.interceptors.request.use(function (config) {
  return authService.getToken().then((accessToken) => {
    config.headers.Authorization = `Bearer ${accessToken}`;
    return config;
  });
});

export default axios;

import {
  parse,
  lightFormat
} from "date-fns";

const DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

export function parseDate(date) {
  return parse(date, DATE_FORMAT, new Date());
}
export const formatDate = (date) => {
 return lightFormat(date, DATE_FORMAT)
};
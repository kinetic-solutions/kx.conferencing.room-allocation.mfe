import React from 'react';
import { Text, CommandBarButton, Stack, getTheme, Shimmer, ShimmerElementType } from '@fluentui/react';
import { useTranslation } from 'react-i18next';
import EmptyItemsList from './EmptyItemsList';
import CurrentAllocation from './CurrentAllocation';
import ValidationError from './ValidationError';

const useStyles = () => {
  const theme = getTheme();

  return {
    root: {
      padding: theme.spacing.m,
    },
  };
};

export default ({
  allocation,
  shimmer,
  validationMessage,
  onRemoveAllocation,
  onChangeAllocation,
}) => {
  const { t } = useTranslation()
  const styles = useStyles();

  if (shimmer) {
    return (
      <Stack style={styles.root} tokens={{ childrenGap: "8px" }}>
        <Shimmer shimmerElements={[{ type: ShimmerElementType.line, height: 32 }]} />
        <Shimmer shimmerElements={[{ type: ShimmerElementType.gap, height: 8, width: "100%" }]} />
        <Shimmer
          shimmerElements={[
            { type: ShimmerElementType.line, height: 16, width: 40 },
            { type: ShimmerElementType.gap, height: 16, width: 16 },
            { type: ShimmerElementType.line, height: 16 }
          ]}
        />
      </Stack>
    )
  }

  if (validationMessage) {
    return (
      <Stack style={styles.root} tokens={{ childrenGap: "8px" }}>
        <ValidationError message={validationMessage} />
      </Stack>
    )
  }

  return (
    <Stack style={styles.root} tokens={{ childrenGap: "8px" }}>
      <Stack horizontal >
        <Text variant="large" as="h2">{t("allocationOverview.title")}</Text>
        {allocation != null &&
          <>
            <CommandBarButton
              iconProps={{ iconName: "Delete" }}
              ariaLabel={t("allocationOverview.removeAllocationButton.ariaLabel")}
              text={t("allocationOverview.removeAllocationButton.label")}
              onClick={onRemoveAllocation}
            />

            <CommandBarButton
              iconProps={{ iconName: "Edit" }}
              ariaLabel={t("allocationOverview.changeAllocationButton.ariaLabel")}
              text={t("allocationOverview.changeAllocationButton.label")}
              onClick={onChangeAllocation}
            />
          </>
        }
      </Stack>
      {allocation == null ?
        <EmptyItemsList /> :
        <CurrentAllocation allocation={allocation} />
      }
    </Stack>
  )
}
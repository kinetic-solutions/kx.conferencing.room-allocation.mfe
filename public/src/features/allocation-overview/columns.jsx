import React from 'react';
import { Text, Stack, FontIcon, getTheme, mergeStyles } from "@fluentui/react"

import { Attribute } from '../allocate-room-modal/Attributes';
import { RoomStatus } from '../allocate-room-modal/RoomStatus'

const theme = getTheme()

export default ({ t }) => {
  return [
    {
      key: "status",
      name: t("detailsList.columns.status.label"),
      minWidth: 100,
      maxWidth: 150,
      onRender: ({ status }) => {
        return (
          <RoomStatus roomStatus={status} />
        )
      }
    },
    {
      key: "bedroom",
      name: t("detailsList.columns.bedroom.label"),
      minWidth: 150,
      maxWidth: 150,
      fieldName: "bedroomName"
    },
    {
      key: "bedroomType",
      name: t("detailsList.columns.bedroomType.label"),
      minWidth: 150,
      maxWidth: 150,
      fieldName: "bedroomType"
    },
    {
      key: "block",
      name: t("detailsList.columns.block.label"),
      minWidth: 150,
      maxWidth: 150,
      fieldName: "blockName"
    },
    {
      key: "subBlock",
      name: t("detailsList.columns.subBlock.label"),
      minWidth: 150,
      maxWidth: 150,
      fieldName: "subBlockName"
    },
    {
      key: "typicalCapacity",
      name: t("detailsList.columns.typicalCapacity.label"),
      minWidth: 125,
      maxWidth: 125,
      fieldName: "typicalCapacity"
    },
    {
      key: "actualCapacity",
      name: t("detailsList.columns.actualCapacity.label"),
      minWidth: 125,
      maxWidth: 125,
      fieldName: "actualCapacity"
    },
    {
      key: "occupants",
      name: t("detailsList.columns.occupants.label"),
      minWidth: 125,
      maxWidth: 125,
      onRender: (item, i, column) => {
        if (item.occupied > 0 && item.occupied >= item.typicalCapacity) {
          column.className = mergeStyles({
            backgroundColor: theme.semanticColors.errorBackground,
          })
          const iconStyle = mergeStyles({ color: theme.palette.redDark })
          return (
            <Text><FontIcon className={iconStyle} aria-label={t("detailsList.columns.occupants.iconAriaLabel")} iconName="Warning" />{item.occupied}</Text>
          )
        }

        if (item.occupied > 0 && item.occupied < item.typicalCapacity) {
          column.className = mergeStyles({
            backgroundColor: theme.semanticColors.warningBackground,
          })
          const iconStyle = mergeStyles({ color: theme.palette.yellowDark })
          return (
            <Text><FontIcon className={iconStyle} aria-label={t("detailsList.columns.occupants.iconAriaLabel")} iconName="Warning" />{item.occupied}</Text>
          )
        }

        column.className = mergeStyles({})
        return (
          <Text>{item.occupied}</Text>
        )
      }
    },
    {
      key: "attributes",
      name: t("detailsList.columns.attributes.label"),
      minWidth: 150,
      maxWidth: 200,
      onRender: ({ attributes }) => {
        if (attributes.length == 0) {
          return (
            <Text>{t("detailsList.columns.attributes.noAttributes.ariaLabel")}</Text>
          )
        }
        return (
          <Stack horizontal tokens={{ childrenGap: "8px" }}>
            {attributes.map((attribute) => {
              return <Attribute key={attribute.key} attribute={attribute} />
            })}
          </Stack>
        )
      }
    }
  ]
}
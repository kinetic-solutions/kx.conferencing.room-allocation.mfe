import React from "react";
import { Text, FontWeights, getTheme } from "@fluentui/react";
import { useTranslation } from "react-i18next";

function EmptyItemsList() {
  const { t } = useTranslation();
  const theme = getTheme();

  return (
    <div
      style={{
        border: "2px dashed #8A8886",
        padding: "48px",
        fontWeight: 700,
        textAlign: "center",
        borderRadius: "6px",
      }}
    >
      <Text
        block
        styles={{
          root: {
            fontWeight: FontWeights.bold,
            marginBottom: theme.spacing.s1,
          },
        }}
      >
        {t("allocationOverview.noAllocationMessage")}
      </Text>
    </div>
  );
}

export default EmptyItemsList;
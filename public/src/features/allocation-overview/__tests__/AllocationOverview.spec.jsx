import React from 'react'
import { render, screen } from '@testing-library/react'
import AllocationOverview from "../AllocationOverview"
import "../../../test/test-utils"
import t from "../../../locales/en-GB"
import userEvent from '@testing-library/user-event'

const allocation = {
  bedroomID: 576,
  status: "D",
  bedroomName: "BT000",

  siteID: 2,
  areaID: 1,

  blockID: 2,
  blockName: "Bed Block 2",

  subBlockID: 3,
  subBlockName: "Sub Block 3",

  bedroomTypeID: 3,
  bedroomType: "Bedroom Type 3",

  typicalCapacity: 1,
  actualCapacity: 1,
  occupied: 0,
  attributes: [],
}

describe("no allocation message", () => {
  test("If there isn't an allocation, a message is displayed", () => {
    render(<AllocationOverview />)

    const message = screen.queryByText(t.allocationOverview.noAllocationMessage)
    expect(message).toBeInTheDocument()
  })

  test("If there is an allocation, a message isn't displayed", () => {
    render(<AllocationOverview allocation={allocation} />)

    const message = screen.queryByText(t.allocationOverview.noAllocationMessage)
    expect(message).not.toBeInTheDocument()
  })

  test("If there is a validation message, a message isn't displayed", () => {
    render(<AllocationOverview validationMessage="Validation error" />)

    const message = screen.queryByText(t.allocationOverview.noAllocationMessage)
    expect(message).not.toBeInTheDocument()
  })
})

describe("remove allocation button", () => {
  test("If there is an allocation, a remove allocation button is displayed which calls the callback", () => {
    const removeAllocation = jest.fn()
    render(<AllocationOverview allocation={allocation} onRemoveAllocation={removeAllocation} />)

    const button = screen.queryByRole('button', { name: t.allocationOverview.removeAllocationButton.ariaLabel })
    userEvent.click(button)
    expect(button).toBeInTheDocument()
    expect(removeAllocation).toHaveBeenCalled()
  })

  test("If there isn't an allocation, the button to remove allocation is not displayed", () => {
    render(<AllocationOverview />)

    const button = screen.queryByRole('button', { name: t.allocationOverview.removeAllocationButton.ariaLabel })
    expect(button).not.toBeInTheDocument()
  })

  test("If there is a validation message, the button isn't displayed", () => {
    const removeAllocation = jest.fn()
    render(<AllocationOverview allocation={allocation} onRemoveAllocation={removeAllocation} validationMessage="validation error" />)

    const button = screen.queryByRole('button', { name: t.allocationOverview.removeAllocationButton.ariaLabel })
    expect(button).not.toBeInTheDocument()
  })
})

describe("change allocation button", () => {
  const getChangeAllocationButton = () => {
    return screen.queryByRole("button", {
      name: t.allocationOverview.changeAllocationButton.ariaLabel
    })
  }

  test("if there is a room allocation, the change allocation button is shown", () => {
    const onChangeAllocation = jest.fn()
    render(
      <AllocationOverview
        allocation={allocation}
        onChangeAllocation={onChangeAllocation}
      />
    )

    const changeAllocationButton = getChangeAllocationButton()

    userEvent.click(changeAllocationButton)

    expect(onChangeAllocation).toHaveBeenCalled()
  })

  test("if there isn't a room allocation, the change allocation button isn't shown", () => {
    render(
      <AllocationOverview
        allocation={null}
      />
    )

    expect(getChangeAllocationButton()).not.toBeInTheDocument()
  })
})

describe("current allocation", () => {
  test("If there is an allocation, we should see a table containing the allocation information", () => {

    render(<AllocationOverview allocation={allocation} />)

    const rows = screen.getAllByRole('row')
    expect(rows).toHaveLength(2)
    expect(rows[0]).toHaveTextContent("StatusRoomBedroom TypeBlockSub BlockTypical CapacityActual CapacityOccupantsAttributes")
    expect(rows[1]).toHaveTextContent("BT000Bedroom Type 3Bed Block 2Sub Block 3110None")
  })

  test("If there is no allocation, we should not render any grid rows", () => {

    render(<AllocationOverview />)

    const rows = screen.queryByRole('row')
    expect(rows).not.toBeInTheDocument()
  })

  test("If there is a validation message, we should not render any grid rows", () => {
    render(<AllocationOverview allocation={allocation} validationMessage="Validation error" />)

    const rows = screen.queryByRole('row')
    expect(rows).not.toBeInTheDocument()
  })
})


import { DetailsList, SelectionMode } from '@fluentui/react'
import { useTranslation } from 'react-i18next'
import React from 'react'
import getColumns from './columns'


export default function ({ allocation }) {
    const { t } = useTranslation()
    const columns = getColumns({ t })
    const items = [allocation]
    return (
        <DetailsList
            id="allocate-room-overview-current-allocation"
            ariaLabel={t("allocationOverview.detailsList.ariaLabel")}
            getRowAriaLabel={(item) => getRowAriaLabel(t, item)}
            items={items}
            columns={columns}
            selectionMode={SelectionMode.none}
        />
    )
}

const getRowAriaLabel = (
    t,
    row
) => {
    const {
        status,
        attributes,
        ...rest
    } = row

    const mappedStatus = {
        C: t("detailsList.columns.status.statusTypes.clean"),
        I: t("detailsList.columns.status.statusTypes.inspected"),
        R: t("detailsList.columns.status.statusTypes.rejected"),
        D: t("detailsList.columns.status.statusTypes.dirty")
    }[status]

    return t("detailsList.rowAriaLabel", {
        status: mappedStatus,
        attributes: attributes.map(x => x.value).join(", ") || t("detailsList.columns.attributes.noAttributes.ariaLabel"),
        ...rest
    })
}
import React from "react";
import { MessageBar, MessageBarType } from "@fluentui/react";


export default function ({ message }) {
    return (<MessageBar
        messageBarType={MessageBarType.warning}
        isMultiline={false}
    >
        {message}
    </MessageBar>)
}

import React from "react";
import { Text, FontWeights, getTheme } from "@fluentui/react";
import { useTranslation } from "react-i18next";

export default () => {
  const { t } = useTranslation();
  const theme = getTheme();

  return (
    <div
      style={{
        border: "2px dashed #8A8886",
        padding: "48px",
        fontWeight: 700,
        textAlign: "center",
        borderRadius: "6px",
        margin: theme.spacing.s1
      }}
    >
      <Text
        block
        styles={{ root: { fontWeight: FontWeights.bold } }}
      >
        {t("detailsList.noBedrooms.label")}
      </Text>
    </div >
  );
}
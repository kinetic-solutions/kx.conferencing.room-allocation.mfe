import React, { useState } from 'react';
import {
  ShimmeredDetailsList,
  SelectionMode,
  Selection,
  Text,
  ScrollablePane,
  ScrollbarVisibility,
  CheckboxVisibility,
  StickyPositionType,
  ConstrainMode,
  SelectAllVisibility,
  Sticky,
} from "@fluentui/react";
import { useTranslation } from "react-i18next"
import getColumns from "./columns"
import NoRooms from "./NoRooms"

const getRowAriaLabel = (
  t,
  row
) => {
  const {
    status,
    attributes,
    ...rest
  } = row

  const mappedStatus = {
    C: t("detailsList.columns.status.statusTypes.clean"),
    I: t("detailsList.columns.status.statusTypes.inspected"),
    R: t("detailsList.columns.status.statusTypes.rejected"),
    D: t("detailsList.columns.status.statusTypes.dirty")
  }[status]

  return t("detailsList.rowAriaLabel", {
    status: mappedStatus,
    attributes: attributes.map(x => x.value).join(", ") || t("detailsList.columns.attributes.noAttributes.ariaLabel"),
    ...rest
  })
}

//TODO: Pass down a onSelection function instead of passing down the setSelectedBedroom function
//It's not good practice to let a child edit the data of a parent itself
//It's better for the child to notify the parent what's happend and let the parent decide what to do
export default ({ fetchNext, bedrooms, setSelectedBedroom, isBedroomsLoading }) => {
  const { t } = useTranslation()

  const [columns] = useState(getColumns({ t }))
  const [selection] = useState(
    new Selection({
      onSelectionChanged: () => {
        setSelectedBedroom(selection.getSelection()[0])
      }
    })
  )

  const onRenderDetailsHeader = (props, DefaultRender) => {
    return (
      <Sticky stickyPosition={StickyPositionType.Header} isScrollSynced>
        <DefaultRender
          {...props}
          styles={{ root: { paddingTop: "0px" } }}
          selectAllVisibility={SelectAllVisibility.hidden}
        />
      </Sticky>
    );
  };

  const onItemInvoked = (_, index) => {
    selection.setAllSelected(false)
    selection.setIndexSelected(index, true)
  }

  //onRenderCustomPlaceholder is called by the details list when there is a null row
  //We use this to implement out infinite scrolling logic as described here: https://github.com/microsoft/fluentui/issues/9568
  const onRenderCustomPlaceholder = (rowProps, index, defaultRender) => {
    // custom logic execution
    if (!isBedroomsLoading) {
      fetchNext()
    }

    return defaultRender(rowProps);
  }

  return isBedroomsLoading || bedrooms.length > 0 ? (
    <div style={{ position: "relative", height: "60vh" }}>
      <ScrollablePane scrollbarVisibility={ScrollbarVisibility.always}>
        <ShimmeredDetailsList
          id="allocate-room-details-list"
          ariaLabel={t("detailsList.ariaLabel")}
          getRowAriaLabel={(item) => getRowAriaLabel(t, item)}
          enableShimmer={isBedroomsLoading}
          onItemInvoked={onItemInvoked}
          styles={{
            root: {
              overflow: "unset",
            },
          }}
          selection={selection}
          items={bedrooms}
          columns={columns}
          compact={true}
          selectionZoneProps={{
            isSelectedOnFocus: false,
            selection: selection,
          }}
          selectionMode={SelectionMode.single}
          constrainMode={ConstrainMode.unconstrained}
          checkboxVisibility={CheckboxVisibility.always}
          selectionPreservedOnEmptyClick={true}
          onRenderDetailsHeader={onRenderDetailsHeader}
          onRenderCustomPlaceholder={onRenderCustomPlaceholder}
        />
      </ScrollablePane>
    </div>
  ) : (
    <NoRooms />
  );
}
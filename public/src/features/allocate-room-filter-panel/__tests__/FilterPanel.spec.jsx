import React from 'react'
import { render, screen, waitFor, act } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import "../../../test/test-utils"
import {
  getAttendee,
  getPaginatedBedrooms,
  getPaginatedRoomingListBedrooms,
  getPaginatedReservationRooms,
  getSiteOptions,
  getAreaOptions,
  getBlockOptions,
  getSubBlockOptions,
  getBedroomTypeOptions,
  getAttributeOptions,
} from '../../../core/api'

import AllocateRoomModal from "../../allocate-room-modal/AllocateRoomModal"
import t from "../../../locales/en-GB"

jest.mock("../../../core/api", () => ({
  getAttendee: jest.fn(),
  getPaginatedBedrooms: jest.fn(),
  getPaginatedRoomingListBedrooms: jest.fn(),
  getPaginatedReservationRooms: jest.fn(),
  getSiteOptions: jest.fn(),
  getAreaOptions: jest.fn(),
  getBlockOptions: jest.fn(),
  getSubBlockOptions: jest.fn(),
  getBedroomTypeOptions: jest.fn(),
  getAttributeOptions: jest.fn(),
  deallocateRoom: jest.fn()
}))

const bedroomsStub = Promise.resolve({
  data: {
    rooms: [
      {
        bedroomID: 576,
        status: "D",
        bedroomName: "BT000",

        siteID: 2,
        areaID: 1,

        blockID: 2,
        blockName: "Bed Block 2",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 3,
        bedroomType: "Bedroom Type 3",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT001",

        siteID: 2,
        areaID: 1,

        blockID: 2,
        blockName: "Bed Block 2",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 3,
        bedroomType: "Bedroom Type 3",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT002",

        siteID: 2,
        areaID: 2,

        blockID: 2,
        blockName: "Bed Block 2",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 3,
        bedroomType: "Bedroom Type 3",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT003",

        siteID: 2,
        areaID: 1,

        blockID: 1,
        blockName: "Bed Block 1",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 1,
        bedroomType: "Bedroom Type 1",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT004",

        siteID: 3,
        areaID: 3,

        blockID: 2,
        blockName: "Bed Block 2",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 3,
        bedroomType: "Bedroom Type 3",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT005",

        siteID: 2,
        areaID: 1,

        blockID: 1,
        blockName: "Bed Block 1",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 2,
        bedroomType: "Bedroom Type 2",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT006",

        siteID: 2,
        areaID: 1,

        blockID: 1,
        blockName: "Bed Block 1",

        subBlockID: 1,
        subBlockName: "Sub Block 1",

        bedroomTypeID: 3,
        bedroomType: "Bedroom Type 2",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [{ key: "smoking", value: "Smoking" }],
      },
      {
        bedroomID: 576,
        status: "I",
        bedroomName: "BT007",

        siteID: 2,
        areaID: 1,

        blockID: 1,
        blockName: "Bed Block 1",

        subBlockID: 3,
        subBlockName: "Sub Block 3",

        bedroomTypeID: 2,
        bedroomType: "Bedroom Type 2",

        typicalCapacity: 1,
        actualCapacity: 1,
        occupied: 0,
        attributes: [{ key: "lastLet", value: "Last Let" }],
      },
    ]
  }
})

const sitesStub = Promise.resolve({
  data: {
    sites: [
      { id: 1, name: "Site 1" },
      { id: 2, name: "Site 2" },
      { id: 3, name: "Site 3" },
    ]
  }
})

const areasStub = Promise.resolve({
  data: {
    areas: [
      { id: 1, name: "Area 1" },
      { id: 2, name: "Area 2" },
      { id: 3, name: "Area 3" },
    ]
  }
})

const blocksStub = Promise.resolve({
  data: {
    blocks: [
      { id: 1, name: "Block 1" },
      { id: 2, name: "Block 2" },
      { id: 3, name: "Block 3" },
    ]
  }
})

const subBlocksStub = Promise.resolve({
  data: {
    subblocks: [
      { id: 1, name: "Sub Block 1" },
      { id: 2, name: "Sub Block 2" },
      { id: 3, name: "Sub Block 3" },
    ]
  }
})

const bedroomTypeStub = Promise.resolve({
  data: {
    bedroomTypes: [
      { id: 1, name: "Bedroom Type 1" },
      { id: 2, name: "Bedroom Type 2" },
      { id: 3, name: "Bedroom Type 3" },
      { id: 4, name: "Bedroom Type 4" },
      { id: 5, name: "Bedroom Type 5" }
    ]
  }
})

const attributesStub = Promise.resolve({
  data: {
    attributes: {
      disabled: "Disabled",
      lastLet: "Last Let",
      vip: "VIP",
      smoking: "Smoking"
    }
  }
})

beforeEach(() => {
  // mock attendee info
  getAttendee.mockResolvedValue({
    data: {
      forename: "Paul",
      surname: "Smith",
      block: null,
      bedroomType: null,
      event: { id: 48885, siteId: 1, hasRoomingList: true },
      arrival: '2020-01-01T09:00:00',
      departure: '2020-01-03T17:00:00',
      attributes: []
    }
  })


  // mock list of bedrooms
  getPaginatedBedrooms.mockResolvedValue(bedroomsStub)
  getPaginatedRoomingListBedrooms.mockResolvedValue(bedroomsStub)
  getPaginatedReservationRooms.mockResolvedValue(bedroomsStub)

  // mock sites, areas, blocks, sub blocks, bedroom types
  getSiteOptions.mockResolvedValue(sitesStub)
  getAreaOptions.mockResolvedValue(areasStub)
  getBlockOptions.mockResolvedValue(blocksStub)
  getSubBlockOptions.mockResolvedValue(subBlocksStub)
  getBedroomTypeOptions.mockResolvedValue(bedroomTypeStub)
  getAttributeOptions.mockResolvedValue(attributesStub)
})

const renderModal = async () => {
  await waitFor(() => {
    render(
      <AllocateRoomModal
        isOpen={true}
        onDismiss={jest.fn()}
        attendeeId={10}
      />
    )
  })
}

const openFilterPanel = async () => {
  const openFilterPanelButton = screen.getByRole("button", {
    name: t.toolbar.openFilterPanelButton.ariaLabel
  })

  await waitFor(() => {
    userEvent.click(openFilterPanelButton)
  })
}

const clearFilters = async () => {
  const clearFiltersButton = screen.getByRole("button", {
    name: t.filterPanel.clearFiltersButtonAriaLabel
  })

  await waitFor(() => {
    userEvent.click(clearFiltersButton)
  })
}

const closeFilterPanel = async () => {
  const closePanelButton = screen.getByRole("button", {
    name: t.filterPanel.closeButtonAriaLabel
  })

  await waitFor(() => {
    userEvent.click(closePanelButton)
  })
}

const waitForDebounce = async () => {
  await new Promise((resolve) => {
    setTimeout(resolve, 1500)
  })
}

// get filters 
const getSiteFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.siteFilter.ariaLabel
  })
}

const getAreaFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.areaFilter.ariaLabel
  })
}

const getBlockFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.blockFilter.ariaLabel
  })
}

const getSubBlockFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.subBlockFilter.ariaLabel
  })
}

const getBedroomTypeFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.bedroomTypeFilter.ariaLabel
  })
}

const getRoomStatusFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.roomStatusFilter.ariaLabel
  })
}

const getAttributesFilter = () => {
  return screen.getByRole("combobox", {
    name: t.filterPanel.attributesFilter.ariaLabel
  })
}

// select filter options
const selectSite = async (name) => {
  await waitFor(() => userEvent.click(getSiteFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectArea = async (name) => {
  await waitFor(() => userEvent.click(getAreaFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectBlock = async (name) => {
  await waitFor(() => userEvent.click(getBlockFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectSubBlock = async (name) => {
  await waitFor(() => userEvent.click(getSubBlockFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectBedroomType = async (name) => {
  await waitFor(() => userEvent.click(getBedroomTypeFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectRoomStatus = async (name) => {
  await waitFor(() => userEvent.click(getRoomStatusFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

const selectAttribute = async (name) => {
  await waitFor(() => userEvent.click(getAttributesFilter()))

  const option = screen.getByRole("option", { name })

  await waitFor(() => userEvent.click(option))
}

test("filters make the correct query", async () => {
  await act(async () => {
    await renderModal()
    await openFilterPanel()
    await selectSite("Site 2")
    await waitForDebounce()
    await selectArea("Area 1")
    await waitForDebounce()
    await selectBlock("Block 1")
    await waitForDebounce()
    await selectSubBlock("Sub Block 3")
    await waitForDebounce()
    await selectBedroomType("Bedroom Type 3")
    await waitForDebounce()
    await selectRoomStatus("Inspected")
    await waitForDebounce()
    await selectAttribute("Last Let")
    await waitForDebounce()
  })


  expect(getPaginatedRoomingListBedrooms).toHaveBeenLastCalledWith({
    arrival: "2020-01-01",
    departure: "2020-01-03",
    includeOccupied: false,
    roomName: undefined,
    siteID: 2,
    areaID: 1,
    blockID: 1,
    subBlockID: 3,
    bedroomTypeID: 3,
    maidStatus: "I",
    lastLet: true
  })
})


describe("attendee preferences", () => {
  test("if the attendee has no preferences, site filter should default to event site and others should be blank", async () => {
    await act(async () => {
      await renderModal()
      await openFilterPanel()
    })

    expect(getSiteFilter()).toHaveTextContent("Site 1")
    expect(getAreaFilter()).toHaveTextContent("Please select")
    expect(getBlockFilter()).toHaveTextContent("Please select")
    expect(getBedroomTypeFilter()).toHaveTextContent("Please select")
    expect(getAttributesFilter()).toHaveTextContent("Please select")

  })

  test("if the attendee has preferences, the filters should default to them", async () => {
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        block: { id: 2, siteId: 1, areaId: 3 },
        bedroomType: { id: 2 },
        event: { id: 48885, siteId: 1 },
        arrival: '2020-01-01T09:00:00',
        departure: '2020-01-03T17:00:00',
        attributes: [{ key: "vip", value: "VIP" }, { key: "disabled", value: "Disabled" }]
      }
    })

    await act(async () => {
      await renderModal()
      await openFilterPanel()
    })

    expect(getSiteFilter()).toHaveTextContent("Site 1")
    expect(getAreaFilter()).toHaveTextContent("Area 3")
    expect(getBlockFilter()).toHaveTextContent("Block 2")
    expect(getBedroomTypeFilter()).toHaveTextContent("Bedroom Type 2")
    expect(getAttributesFilter()).toHaveTextContent("VIP, Disabled")
  })
})

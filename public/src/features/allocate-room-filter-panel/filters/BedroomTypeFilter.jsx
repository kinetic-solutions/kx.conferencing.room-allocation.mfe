import React from 'react';
import { useTranslation } from 'react-i18next';
import SpinnerDropdown from './SpinnerDropdown';

export default ({
  onChange,
  selectedBedroomType,
  bedroomTypeOptions,
  loading
}) => {
  const { t } = useTranslation()

  return (
    <SpinnerDropdown
      label={t("filterPanel.bedroomTypeFilter.label")}
      ariaLabel={t("filterPanel.bedroomTypeFilter.ariaLabel")}
      placeholder={t("filterPanel.bedroomTypeFilter.placeholder")}
      id={t("filterPanel.bedroomTypeFilter.id")}
      onChange={(_, bedroomType) => onChange(bedroomType)}
      selectedKey={selectedBedroomType?.key ?? null}
      options={bedroomTypeOptions}
      loading={loading}
    />
  );
}
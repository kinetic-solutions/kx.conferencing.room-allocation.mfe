import React from 'react';
import { Dropdown } from '@fluentui/react';
import { useTranslation } from 'react-i18next';

export default ({
  onChange,
  selectedRoomStatus,
  roomStatusOptions
}) => {
  const { t } = useTranslation()

  return (
    <Dropdown
      label={t("filterPanel.roomStatusFilter.label")}
      ariaLabel={t("filterPanel.roomStatusFilter.ariaLabel")}
      placeholder={t("filterPanel.roomStatusFilter.placeholder")}
      id={t("filterPanel.roomStatusFilter.id")}
      onChange={(_, roomStatus) => onChange(roomStatus)}
      selectedKey={selectedRoomStatus?.key ?? null}
      options={roomStatusOptions}
    />
  );
}
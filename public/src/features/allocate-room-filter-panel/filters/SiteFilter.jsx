import React from 'react';
import { useTranslation } from 'react-i18next';
import SpinnerDropdown from './SpinnerDropdown';

export default ({
  onChange,
  selectedSite,
  siteOptions,
  loading
}) => {
  const { t } = useTranslation()

  return (
    <SpinnerDropdown
      label={t("filterPanel.siteFilter.label")}
      ariaLabel={t("filterPanel.siteFilter.ariaLabel")}
      placeholder={t("filterPanel.siteFilter.placeholder")}
      id={t("filterPanel.siteFilter.id")}
      onChange={(_, site) => onChange(site)}
      selectedKey={selectedSite?.key ?? null}
      options={siteOptions}
      loading={loading}
    />
  );
}
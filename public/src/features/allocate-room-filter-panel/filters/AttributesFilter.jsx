import React from 'react';
import { Dropdown } from '@fluentui/react';
import { useTranslation } from 'react-i18next';

export default ({ onChange, selectedAttributes, attributeOptions }) => {
  const { t } = useTranslation()

  return (
    <Dropdown
      label={t("filterPanel.attributesFilter.label")}
      ariaLabel={t("filterPanel.attributesFilter.ariaLabel")}
      placeholder={t("filterPanel.attributesFilter.placeholder")}
      id={t("filterPanel.attributesFilter.id")}
      onChange={(_, attribute) => onChange(attribute)}
      selectedKeys={selectedAttributes}
      options={attributeOptions}
      multiSelect
    />
  );
}
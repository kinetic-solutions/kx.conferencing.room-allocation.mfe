import React from 'react';
import { useTranslation } from 'react-i18next';
import SpinnerDropdown from './SpinnerDropdown';

export default ({
  onChange,
  selectedArea,
  areaOptions,
  disabled,
  loading
}) => {
  const { t } = useTranslation()

  return (
    <SpinnerDropdown
      label={t("filterPanel.areaFilter.label")}
      ariaLabel={t("filterPanel.areaFilter.ariaLabel")}
      placeholder={t("filterPanel.areaFilter.placeholder")}
      id={t("filterPanel.areaFilter.id")}
      onChange={(_, area) => onChange(area)}
      selectedKey={selectedArea?.key ?? null}
      options={areaOptions}
      disabled={disabled}
      loading={loading}
    />
  );
}
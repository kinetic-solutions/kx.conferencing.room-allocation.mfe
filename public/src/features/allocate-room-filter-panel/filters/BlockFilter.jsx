import React from 'react';
import { useTranslation } from 'react-i18next';
import SpinnerDropdown from './SpinnerDropdown';

export default ({
  onChange,
  selectedBlock,
  blockOptions,
  disabled,
  loading
}) => {
  const { t } = useTranslation()

  return (
    <SpinnerDropdown
      label={t("filterPanel.blockFilter.label")}
      ariaLabel={t("filterPanel.blockFilter.ariaLabel")}
      placeholder={t("filterPanel.blockFilter.placeholder")}
      id={t("filterPanel.blockFilter.id")}
      onChange={(_, block) => onChange(block)}
      selectedKey={selectedBlock?.key ?? null}
      options={blockOptions}
      disabled={disabled}
      loading={loading}
    />
  )
}
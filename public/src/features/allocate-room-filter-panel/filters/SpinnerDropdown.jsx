import React from 'react';
import { Dropdown, Spinner, SpinnerSize, Stack } from '@fluentui/react';
import { useTranslation } from 'react-i18next';

export default ({
    label,
    ariaLabel,
    placeholder,
    id,
    onChange,
    options,
    selectedKey,
    disabled,
    loading
}) => {
    const { t } = useTranslation()

    const onRenderTitle = (options) => {
        const option = options[0];

        return (
            <Stack horizontal verticalAlign='center' tokens={{ childrenGap: 10 }}>
                {loading && <Spinner size={SpinnerSize.small} />}
                <span>{option.text}</span>
            </Stack>
        );
    };

    const onRenderPlaceholder = (props) => {
        return (
            <Stack horizontal verticalAlign='center' tokens={{ childrenGap: 10 }}>
                {loading && <Spinner size={SpinnerSize.small} />}
                <span>{props.placeholder}</span>
            </Stack>
        );
    };

    return (
        <Dropdown
            label={label}
            ariaLabel={ariaLabel}
            placeholder={placeholder}
            id={id}
            onChange={onChange}
            selectedKey={selectedKey}
            options={options}
            disabled={loading || disabled}
            onRenderTitle={onRenderTitle}
            onRenderPlaceholder={onRenderPlaceholder}
        />
    );
}
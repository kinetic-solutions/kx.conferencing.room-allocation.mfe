import React from 'react';
import { useTranslation } from 'react-i18next';
import SpinnerDropdown from './SpinnerDropdown';

export default ({
  onChange,
  selectedSubBlock,
  subBlockOptions,
  disabled,
  loading
}) => {
  const { t } = useTranslation()

  return (
    <SpinnerDropdown
      label={t("filterPanel.subBlockFilter.label")}
      ariaLabel={t("filterPanel.subBlockFilter.ariaLabel")}
      placeholder={t("filterPanel.subBlockFilter.placeholder")}
      id={t("filterPanel.subBlockFilter.id")}
      onChange={(_, subBlock) => onChange(subBlock)}
      selectedKey={selectedSubBlock?.key ?? null}
      options={subBlockOptions}
      disabled={disabled}
      loading={loading}
    />
  );
}
import React from 'react';
import { IconButton, Panel, Stack, Toggle, getTheme } from "@fluentui/react"
import { useTranslation } from 'react-i18next';

import SiteFilter from "./filters/SiteFilter"
import AreaFilter from "./filters/AreaFilter"
import BlockFilter from "./filters/BlockFilter"
import SubBlockFilter from './filters/SubBlockFilter';
import BedroomTypeFilter from './filters/BedroomTypeFilter';
import RoomStatusFilter from './filters/RoomStatusFilter';
import AttributesFilter from './filters/AttributesFilter';

const Header = ({ onClearFilters }) => {
  const { t } = useTranslation()

  return (
    <Stack
      horizontal
      horizontalAlign="space-between"
    >
      {t("filterPanel.headerText")}

      <IconButton
        iconProps={{ iconName: "ClearFilter" }}
        ariaLabel={t("filterPanel.clearFiltersButtonAriaLabel")}
        onClick={onClearFilters}
      />
    </Stack>
  )
}

export default ({
  isOpen,
  onDismiss,
  options,
  selectedFilters,
  onChangeFilters,
  onClearFilters,
  loading
}) => {
  const { t } = useTranslation()
  const theme = getTheme()

  return (
    <Panel
      headerText={<Header onClearFilters={onClearFilters} />}
      isOpen={isOpen}
      isBlocking={false}
      onDismiss={onDismiss}
      closeButtonAriaLabel={t("filterPanel.closeButtonAriaLabel")}
    >
      <Stack tokens={{ childrenGap: "8px" }}>
        <Toggle
          id="allocate-room-filter-panel-show-all-rooms-toggle"
          ariaLabel={t("filterPanel.showAllRoomsToggle.ariaLabel")}
          label={t("filterPanel.showAllRoomsToggle.label")}
          inlineLabel
          styles={{ root: { margin: 0, marginTop: theme.spacing.s1 } }}
          checked={selectedFilters.showAllRooms}
          onChange={(_, showAllRooms) => onChangeFilters({ showAllRooms })}
        />

        <SiteFilter
          onChange={(site) => onChangeFilters({ site })}
          selectedSite={selectedFilters.site}
          siteOptions={options.site}
          loading={loading.site}
        />

        <AreaFilter
          onChange={(area) => onChangeFilters({ area })}
          selectedArea={selectedFilters.area}
          areaOptions={options.area}
          disabled={!selectedFilters.site?.key}
          loading={loading.area}
        />

        <BlockFilter
          onChange={(block) => onChangeFilters({ block })}
          selectedBlock={selectedFilters.block}
          blockOptions={options.block}
          disabled={!selectedFilters.area?.key}
          loading={loading.block}
        />

        <SubBlockFilter
          onChange={(subBlock) => onChangeFilters({ subBlock })}
          selectedSubBlock={selectedFilters.subBlock}
          subBlockOptions={options.subBlock}
          disabled={!selectedFilters.block?.key}
          loading={loading.subBlock}
        />

        <BedroomTypeFilter
          onChange={(bedroomType) => onChangeFilters({ bedroomType })}
          selectedBedroomType={selectedFilters.bedroomType}
          bedroomTypeOptions={options.bedroomType}
          loading={loading.bedroomType}
        />

        <RoomStatusFilter
          onChange={(roomStatus) => onChangeFilters({ roomStatus })}
          selectedRoomStatus={selectedFilters.roomStatus}
          roomStatusOptions={options.roomStatus}
        />

        <AttributesFilter
          onChange={(attribute) => onChangeFilters({ attribute })}
          selectedAttributes={selectedFilters.attributes}
          attributeOptions={options.attribute}
        />
      </Stack>
    </Panel>
  );
}
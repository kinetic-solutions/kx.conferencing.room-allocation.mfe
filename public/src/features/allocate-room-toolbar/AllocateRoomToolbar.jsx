import React from 'react';
import { IconButton, SearchBox, Stack, Toggle } from '@fluentui/react';
import { useTranslation } from 'react-i18next';
import CurrentFilters from "./CurrentFilters"

export default ({
  scope,
  roomName,
  showOccupied,
  selectedFilters,
  options,
  onChangeRoomName,
  onChangeFilters,
  onChangeShowOccupied,
  onOpenFilterPanel
}) => {
  const { t } = useTranslation()

  return (
    <Stack
      id="allocate-room-toolbar"
      vertical
      style={{ padding: "16px 16px 0px 16px" }}
    >
      <Stack
        horizontal
        horizontalAlign="space-between"
        tokens={{ childrenGap: "32px" }}
      >
        <Stack horizontal tokens={{ childrenGap: "32px" }}>
          <SearchBox
            id="allocate-room-toolbar-room-name-text-field"
            ariaLabel={t("toolbar.roomNameSearchBox.ariaLabel")}
            placeholder={t("toolbar.roomNameSearchBox.placeholder")}
            styles={{ root: { width: "240px" } }}
            value={roomName}
            onChange={(_, roomName) => {
              onChangeRoomName(roomName)
            }}
            onClear={(e) => {
              e.preventDefault() // stop onChange also being called
              onChangeRoomName("")
            }}
          />

          <Toggle
            id="allocate-room-toolbar-show-occupied-toggle"
            label={t("toolbar.showOccupiedToggle.label")}
            ariaLabel={t("toolbar.showOccupiedToggle.ariaLabel")}
            checked={showOccupied}
            inlineLabel={true}
            onChange={(_, checked) => onChangeShowOccupied(checked)}
          />
        </Stack>

        <IconButton
          id="allocate-room-toolbar-open-filter-panel-button"
          ariaLabel={t("toolbar.openFilterPanelButton.ariaLabel")}
          iconProps={{ iconName: "Filter" }}
          onClick={onOpenFilterPanel}
        />
      </Stack>

      <CurrentFilters
        scope={scope}
        selectedFilters={selectedFilters}
        onChangeFilters={onChangeFilters}
        options={options}
      />
    </Stack>
  )
}
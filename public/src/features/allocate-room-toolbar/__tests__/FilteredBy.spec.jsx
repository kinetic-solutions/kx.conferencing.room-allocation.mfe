import React from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import "../../../test/test-utils"

import AllocateRoomToolbar from '../AllocateRoomToolbar'

describe("filtered by", () => {
  test("shows the correct pills when filters change", async () => {
    const options = {
      site: [
        {
          key: 9,
          text: "Ashby House"
        },
        {
          key: 13,
          text: "Derwent House"
        },
        {
          key: 7,
          text: "Innovation Centre"
        },
      ],
      area: [
        {
          key: 6,
          text: "Student Union"
        },
        {
          key: 8,
          text: "Sullivan Quad"
        },
        {
          key: 7,
          text: "West Campus"
        }
      ],
      block: [
        {
          key: 39,
          text: "Kx Temporary Block"
        },
        {
          key: 23,
          text: "Bed Block 3"
        },
        {
          key: 27,
          text: "Bed Block 5"
        },
        {
          key: 29,
          text: "Bed Block 8"
        }
      ],
      subBlock: [
        {
          key: 24,
          text: "Sub Block 3"
        },
        {
          key: 25,
          text: "Sub Block 4"
        },
      ],
      bedroomType: [
        {
          key: 1,
          text: "Double-Premium"
        },
        {
          key: 2,
          text: "En-Suite Single"
        },
        {
          key: 3,
          text: "En-Suite Twin"
        },
      ],
      roomStatus: [
        {
          key: "C",
          text: "Clean"
        },
        {
          key: "I",
          text: "Inspected"
        },
        {
          key: "R",
          text: "Rejected"
        },
        {
          key: "D",
          text: "Dirty"
        }
      ],
      attribute: [
        {
          key: "disabled",
          text: "Disabled"
        },
        {
          key: "smoking",
          text: "Smoking"
        },
        {
          key: "vip",
          text: "VIP"
        },
        {
          key: "lastLet",
          text: "Last Let"
        }
      ]
    }

    const selectedFilters = {
      site: { key: 9 },
      area: { key: 8 },
      block: { key: 23 },
      subBlock: { key: 25 },
      bedroomType: { key: 1 },
      roomStatus: { key: "C" },
      attributes: ["vip", "disabled"]
    }

    render(
      <AllocateRoomToolbar
        roomName={"test 123"}
        showOccupied={false}
        selectedFilters={selectedFilters}
        options={options}
        onChangeRoomName={jest.fn()}
        onChangeShowOccupied={jest.fn()}
        onOpenFilterPanel={jest.fn()}
      />
    )

    // this will fail if not found so no need for assertion
    screen.getByText("Ashby House")
    screen.getByText("Sullivan Quad")
    screen.getByText("Bed Block 3")
    screen.getByText("Sub Block 4")
    screen.getByText("Double-Premium")
    screen.getByText("Clean")
    screen.getByText("VIP")
    screen.getByText("Disabled")
  })
})
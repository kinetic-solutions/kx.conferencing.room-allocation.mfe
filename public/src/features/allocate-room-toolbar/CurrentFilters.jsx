import React from 'react';
import { getTheme, Stack, Text, ActionButton } from "@fluentui/react"
import { useTranslation } from 'react-i18next';

const Pill = ({ text, onClear }) => {
  const { t } = useTranslation()
  const theme = getTheme()

  return (
    <Stack
      horizontal
      style={{
        height: "32px",
        borderRadius: theme.effects.roundedCorner6,
        backgroundColor: theme.palette.neutralLight,
        alignItems: "center",
        paddingLeft: theme.spacing.s1
      }}
    >
      <Text
        style={{ fontSize: "14px" }}
        nowrap
      >
        {text}
      </Text>

      <ActionButton
        iconProps={{ iconName: "Cancel", style: { fontSize: "12px !important" } }}
        ariaLabel={t("toolbar.filteredBy.clearFilter.ariaLabel", {
          text
        })}
        onClick={onClear}
      />
    </Stack>
  )
}

const getOptionName = ({ key, text }, options) => {
  // if no options are present then return nothing
  if (!options.length) return undefined

  // if the option already has text then return it
  if (text) return text


  // if no option is selected for this filter then return nothing
  if (!key) return undefined

  // return the text of the option with a matching key
  const option = options.find((option) => option.key === key)
  if (option) {
    return option.text
  }

  return null;
}

export default ({
  scope,
  selectedFilters,
  onChangeFilters,
  options
}) => {
  const { t } = useTranslation()
  const theme = getTheme()

  const locationPillProps = [
    "site",
    "area",
    "block",
    "subBlock",
    "bedroomType",
    "roomStatus",
  ].map((filterKey) => {
    if (!selectedFilters[filterKey]?.key) return

    const text = getOptionName(selectedFilters[filterKey], options[filterKey]);
    if (!text) {
      return null;
    }
    const onClear = () => onChangeFilters({ [filterKey]: null })

    return [text, onClear, filterKey]
  })

  const { attributes } = selectedFilters
  const attributePillProps = attributes.length ? attributes.map((attributeKey) => {
    const text = getOptionName({ key: attributeKey }, options.attribute)
    const onClear = () => onChangeFilters({ attribute: { key: attributeKey } })

    return [text, onClear, attributeKey]
  }) : []

  const pillProps = [...locationPillProps, ...attributePillProps].filter(pill => !!pill)

  return (
    <Stack
      id="allocate-room-toolbar-pill-container"
      horizontal
      tokens={{ childrenGap: theme.spacing.s1 }}
      style={{
        overflowX: "auto",
        padding: theme.spacing.s1,
        backgroundColor: theme.palette.themeLighterAlt,
        alignItems: "center"
      }}
    >
      <Stack.Item>
        <Text nowrap>
          {t("toolbar.currentlyShowing.label", {
            scope: t(`toolbar.currentlyShowing.${scope}`)
          })}{", "}

          {pillProps.length ? t("toolbar.filteredBy.label") : t("toolbar.noFiltersSelected.label")}
        </Text>
      </Stack.Item>

      {pillProps.map(([text, onClear, key]) => {
        return <Pill key={key} text={text} onClear={onClear} />
      })}
    </Stack>
  )
}
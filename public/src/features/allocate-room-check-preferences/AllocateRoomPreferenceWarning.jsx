import React, { } from "react"
import { MessageBar, MessageBarType } from '@fluentui/react';
import { useTranslation } from "react-i18next";

export default ({ onDismiss }) => {
  const { t } = useTranslation()
  return (
    <MessageBar
      messageBarType={MessageBarType.warning}
      isMultiline={false}
      onDismiss={onDismiss}
      dismissButtonAriaLabel={t("preferenceWarning.ariaLabel")}
    >
      {t("preferenceWarning.mismatchedPreferences")}
    </MessageBar>
  )
}
import {useState, useEffect} from "react"

export default (attendee, selectedFilters) => {
  const [scope, setScope] = useState(null)

  // update the scope when the attendee or the filters change
  useEffect(() => {
    if (!attendee) return
    
    if (selectedFilters.showAllRooms) {
      setScope("all")
      return
    }

    if (attendee?.event?.hasRoomingList) {
      setScope("roominglist")
      return
    }
    
    setScope("reservations")
  }, [attendee, selectedFilters])

  return scope
}
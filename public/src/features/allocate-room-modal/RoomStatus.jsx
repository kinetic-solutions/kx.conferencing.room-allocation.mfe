import React from 'react'
import { getTheme } from "@fluentui/react"
import { useRoomStatusDefinitions } from "./useRoomStatusDefinitions";

const Pill = ({ text, color }) => {
  const theme = getTheme()

  const { border, backgroundColor } =
    {
      green: {
        border: "#25936f",
        backgroundColor: "#c3e5da",
      },
      red: {
        border: "#f48331",
        backgroundColor: "#f8d7da",
      },
      orange: {
        border: "#f48331",
        backgroundColor: "#fad5c8",
      },
      grey: {
        border: "#d9d9d9",
        backgroundColor: theme.palette.neutralLight,
      },
    }[color] || {};

  return (
    <div
      style={{
        border: `1px solid ${border}`,
        backgroundColor,
        padding: "4px",
        borderRadius: "4px",
        color: "#282b2c",
        width: "min-content"
      }}
    >
      {text}
    </div>
  )
}

const definitions = useRoomStatusDefinitions();

export const RoomStatus = ({ roomStatus }) => {
  const { text, color } = definitions[roomStatus]

  return (
    <Pill text={text} color={color} />
  )
}
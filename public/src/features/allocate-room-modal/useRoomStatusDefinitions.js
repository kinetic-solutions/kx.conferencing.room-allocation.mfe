import i18n from 'i18next';

export function useRoomStatusDefinitions() {
    return ({
        C: {
            text: i18n.t("detailsList.columns.status.statusTypes.clean"),
            color: "green",
        },
        I: {
            text: i18n.t("detailsList.columns.status.statusTypes.inspected"),
            color: "orange",
        },
        R: {
            text: i18n.t("detailsList.columns.status.statusTypes.rejected"),
            color: "orange",
        },
        D: {
            text: i18n.t("detailsList.columns.status.statusTypes.dirty"),
            color: "red",
        },
    })
}
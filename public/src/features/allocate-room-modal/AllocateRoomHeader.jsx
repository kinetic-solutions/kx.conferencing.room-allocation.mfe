import React, { useEffect } from "react"
import { Shimmer, ShimmerElementType, Stack, Text, getTheme, DefaultButton } from '@fluentui/react';
import { useTranslation } from "react-i18next";
import { parseDate } from "../../core/dates";
import { Attribute } from './Attributes';
import SpinnerButton from "../../shared/SpinnerButton";

const Key = ({ children }) => {
  const theme = getTheme();
  return (
    <Text
      styles={{
        root: {
          color: theme.palette.neutralSecondary
        }
      }}
    >
      {children}:
    </Text>
  )
}

const AttendeeName = ({ attendee }) => {
  const { t } = useTranslation();

  return (
    <Stack horizontal verticalAlign="center" tokens={{ childrenGap: "8px" }}>
      <Key>{t("header.name")}</Key>
      <Text id="allocate-bedroom-header-attendee-name">{attendee.forename} {attendee.surname}</Text>
    </Stack>
  )
}

const AttendeeDates = ({ attendee }) => {
  const { t } = useTranslation();

  return (
    <Stack horizontal verticalAlign="center" tokens={{ childrenGap: "8px" }}>
      <Key>{t("header.dates")}</Key>
      <Text id="allocate-bedroom-header-dates">
        {t("date", { date: parseDate(attendee.arrival) })} - {t("date", { date: parseDate(attendee.departure) })}
      </Text>
    </Stack>
  )
}

const NoPreference = () => {
  const { t } = useTranslation();

  return (<Text>{t("header.noPreference")}</Text>)
}

const Preference = ({ value }) => {
  const { t } = useTranslation();

  if (!value) {
    return (
      <NoPreference />
    )
  }

  return (
    <Text>{value}</Text>
  )
}

const PreferredBedroom = ({ bedroom }) => {
  const { t } = useTranslation();


  return (
    <Stack horizontal verticalAlign="center" tokens={{ childrenGap: "8px" }}>
      <Key>{t("header.preferredBedroom")}</Key>
      <Text id="allocate-bedroom-header-preferred-bedroom-type"><Preference value={bedroom?.name} /></Text>
    </Stack>
  )
}

const PreferredBlock = ({ block }) => {
  const { t } = useTranslation();

  return (
    <Stack horizontal verticalAlign="center" tokens={{ childrenGap: "8px" }}>
      <Key>{t("header.preferredBlock")}</Key>
      <Text id="allocate-bedroom-header-preferred-block"><Preference value={block?.name} /></Text>
    </Stack>
  )
}

const PreferredAttributes = ({ attributes }) => {
  const { t } = useTranslation();

  return (
    <Stack horizontal verticalAlign="center" tokens={{ childrenGap: "8px" }}>
      <Key>{t("header.preferredAttributes")}</Key>
      {attributes.length != 0 ? <Stack id="allocate-bedroom-header-preferred-attributes" horizontal tokens={{ childrenGap: "8px" }}>
        {attributes.map((attribute) => {
          return <Attribute key={attribute.key} attribute={attribute} />
        })}
      </Stack> : <NoPreference />}
    </Stack>
  )
}

const useStyles = () => {
  const theme = getTheme()

  return {
    header: {
      backgroundColor: theme.palette.white,
      padding: theme.spacing.m,
      boxShadow: theme.effects.elevation4,
    }
  }
}

export default ({
  saveDisabled,
  isAttendeeLoading,
  attendee,
  onSave,
  onDismiss
}) => {
  const { t } = useTranslation();
  const theme = getTheme()
  const styles = useStyles()

  // set user focus on "Cancel" button on load
  useEffect(() => {
    if (!isAttendeeLoading) {
      const closeButton = document.querySelector("#allocate-room-header-close-button")
      closeButton.focus()
    }
  }, [isAttendeeLoading])

  return (
    <Stack
      id="allocate-room-header"
    >
      {!isAttendeeLoading ? (
        <Stack
          vertical
          style={styles.header}
          tokens={{ childrenGap: theme.spacing.s1 }}
        >
          <Stack tokens={{ childrenGap: "8px" }}>
            <Text variant="xxLargePlus" as="h1">
              {t("header.title")}
            </Text>

            <Stack horizontal tokens={{ childrenGap: "24px" }}>
              <AttendeeName attendee={attendee} />
              <AttendeeDates attendee={attendee} />
            </Stack>

            <Stack
              horizontal
              verticalAlign="center"
              tokens={{ childrenGap: "24px" }}
            >
              <PreferredBedroom bedroom={attendee.bedroomType} />
              <PreferredBlock block={attendee.block} />
            </Stack>
          </Stack>

          <Stack horizontal horizontalAlign="space-between">
            <PreferredAttributes attributes={attendee.attributes} />

            <Stack
              horizontal
              tokens={{
                childrenGap: theme.spacing.s1
              }}
            >
              <SpinnerButton
                id="allocate-room-header-save-button"
                disabled={saveDisabled}
                text={t("header.saveButton.label")}
                ariaLabel={t("header.saveButton.ariaLabel")}
                spinnerAriaLabel={t("header.saveButton.spinnerAriaLabel")}
                onClick={onSave}
              />
              <DefaultButton
                id="allocate-room-header-close-button"
                ariaLabel={t("header.closeButton.ariaLabel")}
                text={t("header.closeButton.label")}
                onClick={onDismiss}
              />
            </Stack>
          </Stack>
        </Stack>
      ) : (
        <>
          <Shimmer
            shimmerElements={[
              { type: ShimmerElementType.line, height: 32 }
            ]}
          />
          <Shimmer
            shimmerElements={[
              { type: ShimmerElementType.gap, height: 8, width: "100%" }
            ]}
          />
          <Shimmer
            shimmerElements={[
              { type: ShimmerElementType.line, height: 16, width: 40 },
              { type: ShimmerElementType.gap, height: 16, width: 16 },
              { type: ShimmerElementType.line, height: 16 }
            ]}
          />
        </>
      )}
    </Stack >
  )
}
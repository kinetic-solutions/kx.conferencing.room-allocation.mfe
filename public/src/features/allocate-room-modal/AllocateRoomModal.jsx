import React, { useState, useEffect } from "react";
import { Panel, Stack, getTheme, Text, Announced, PanelType, DatePicker, MessageBar, MessageBarType, StackItem } from '@fluentui/react';
import { useTranslation } from "react-i18next";
import { allocateRoom, deallocateRoom } from "../../core/api"
import { publishSuccessToast } from "@kinetic/notification-system";

// components
import AllocateRoomHeader from "./AllocateRoomHeader"
import AllocateRoomToolbar from "../allocate-room-toolbar/AllocateRoomToolbar"
import AllocateRoomPreferenceWarning from "../allocate-room-check-preferences/AllocateRoomPreferenceWarning";
import AllocateRoomDetailsList from "../allocate-room-details-list/AllocateRoomDetailsList"
import AllocateRoomFilterPanel from "../allocate-room-filter-panel/AllocateRoomFilterPanel";
import AllocationOverview from "../allocation-overview/AllocationOverview";

// hooks
import usePreferenceCheck from "./usePreferenceCheck";
import useAttendee from "./useAttendee";
import useBedrooms from "./useBedrooms";
import useFilters from "./useFilters";
import useFilterOptions from "./useFilterOptions"
import useScope from "./useScope";
import useCurrentRoomAllocation from "./useCurrentRoomAllocation";
import { formatDate } from "../../core/dates";

const useStyles = () => {
  const theme = getTheme();

  return {
    panel: {
      main: {
        backgroundColor: theme.palette.neutralLighterAlt,
      },
      content: {
        overflowY: "hidden",
        height: "100%",
        padding: 0,
      }
    },
    header: {
      backgroundColor: theme.palette.white,
      padding: theme.spacing.m,
      paddingTop: theme.spacing.s2,
      boxShadow: theme.effects.elevation4,
    },
    detailsContainer: {
      margin: theme.spacing.m,
      backgroundColor: theme.palette.white,
      borderRadius: theme.effects.roundedCorner4,
      boxShadow: theme.effects.elevation4,
      itemsListWrapper: {
        root: {
          width: "100%",
          backgroundColor: theme.palette.white,
          padding: theme.spacing.m,
          borderRadius: theme.effects.roundedCorner4,
          boxShadow: theme.effects.elevation4,
        },
      },
    },
  };
};

const ErrorMessage = ({ msg }) => {
  const theme = getTheme()

  return (
    <Stack
      horizontal
      horizontalAlign="end"
      styles={{
        root: {
          padding: "0px 16px 8px 16px",
        },
      }}
    >
      <Stack.Item>
        <Text
          styles={{
            root: {
              color: theme.semanticColors.errorText,
              fontSize: "12px",
            },
          }}
        >
          {msg}
        </Text>
      </Stack.Item>
    </Stack>
  )
}
const ShowCheckedInPicker = ({ msg, firstNight, onChangeFirstNight, arrivalDate, departureDate }) => {

  const minDate = new Date(arrivalDate);
  const maxDate = new Date(Date.now());
  const theme = getTheme();
  const { t } = useTranslation();

  return (
    <MessageBar messageBarType={MessageBarType.warning} isMultiline={false} >
      <Stack horizontal tokens={{ childrenGap: "10px" }}>
        {msg}
        <StackItem style={{ width: "150px", marginLeft: '10px' }} >
          <DatePicker
            value={firstNight}
            formatDate={(date) => t("date", { date })}
            ariaLabel={t('allocationOverview.changeAllocationDatePicker.ariaLabel')}
            minDate={minDate}
            maxDate={maxDate}
            onSelectDate={(value) => { onChangeFirstNight(value) }}
            textField={{
              styles: {
                fieldGroup: {
                  borderColor: theme.semanticColors.inputBorder,
                },
              },
            }}

          />
        </StackItem>
      </Stack>
    </MessageBar>

  )

}

export default ({ isOpen, onDismiss, attendeeId }) => {
  const { t } = useTranslation()
  const styles = useStyles();
  const today = new Date(Date.now())
  today.setHours(0, 0, 0, 0);

  const [isFilterPanelOpen, setIsFilterPanelOpen] = useState(false)
  const [selectedBedroom, setSelectedBedroom] = useState()
  const [errorMessage, setErrorMessage] = useState("")
  const [isRoomFinderOpen, setIsRoomFinderOpen] = useState(false)
  const [roomName, setRoomName] = useState("")
  const [showOccupied, setShowOccupied] = useState(false)
  const [isSaving, setIsSaving] = useState(false)
  const [firstNight, setFirstNight] = useState(today);

  const { attendee, isAttendeeLoading, isAttendeeValid, attendeeValidationMessage } = useAttendee(attendeeId)
  const [currentRoomAllocation, setCurrentRoomAllocation] = useCurrentRoomAllocation(attendee, isAttendeeValid, setIsRoomFinderOpen)
  const { selectedFilters, setSelectedFilters, clearFilters } = useFilters(attendee)
  //The scope of the rooms and filter options
  //Possible values are 'all', 'roominglist' or 'reservations'
  const scope = useScope(attendee, selectedFilters)

  const { bedrooms, isBedroomsLoading, fetchNext } = useBedrooms(attendee, selectedFilters, showOccupied, roomName, scope) // all bedrooms from API

  const { options, optionsLoading } = useFilterOptions(bedrooms, selectedFilters, setSelectedFilters, scope, attendee?.event?.id);

  const { isMismatchedPreferences, resetPreferenceCheck } = usePreferenceCheck(attendee, selectedBedroom);

  const showAttendeeCheckedInPicker = () => {
    //Only show if the attendee is checked in
    if (!attendee?.isCheckedIn) {
      return false
    }
    //Only show if there aren't any validation errors
    if (attendeeValidationMessage.length !== 0) {
      return false
    }

    //If we're deleting the room allocation, show the picker
    if (currentRoomAllocation == null && attendee?.roomAllocation != null){
      return true
    }
    //Only show if we are changing an existing allocation's room
    if (currentRoomAllocation == null && attendee?.roomAllocation == null) {
      return false
    }

    return isRoomFinderOpen
  }

  useEffect(() => {
    // disable the page scrollbar when modal is open
    document.querySelector("html").style.overflowY = "hidden"

    // enable the page scrollbar when modal closes
    return () => document.querySelector("html").style.overflowY = "auto"
  }, [])

  const saveDisabled = () => {
    if (isSaving || !attendee) {
      return true;
    }
    if (!isRoomFinderOpen && currentRoomAllocation !== attendee.roomAllocation) {
      return false
    }
    if (selectedBedroom) {
      return false
    }

    return true
  }

  const handleSave = async () => {
    setIsSaving(true);
    try {
      const payload = {}
      if (showAttendeeCheckedInPicker()) {
        payload.firstNight = formatDate(firstNight).split("T")[0]
      }
      if (isRoomFinderOpen) {
        // user is just allocating a new room

        await allocateRoom(attendee.id, selectedBedroom.bedroomID, payload)
      } else {
        // user is just deallocating a room
        await deallocateRoom(attendee.id, attendee.roomAllocation.bedroomID, payload)
      }

      onDismiss(true);
      publishSuccessToast(t("successToast.label"))
    } catch (e) {
      console.error(e);

      if (e.isAxiosError) {
        const errArr = e.response.data?.fields?.map(x => t(`header.saveButton.errorMessages.${x.error}`)) || [];
        const err = errArr.join(", ")
        const msg = err || t("header.saveButton.errorMessages.unknown");
        setErrorMessage(msg);
      }
    } finally {
      setIsSaving(false);
    }
  };

  return (
    <Panel
      isOpen={isOpen}
      onDismiss={onDismiss}
      type={PanelType.smallFluid}
      onRenderNavigation={() => null}
      styles={styles.panel}
      allowTouchBodyScroll={true}
    >
      <Announced message={t("modalOpened")} />

      <Stack>
        <AllocateRoomHeader
          saveDisabled={saveDisabled()}
          isAttendeeLoading={isAttendeeLoading}
          attendee={attendee}
          onDismiss={onDismiss}
          onSave={handleSave}
        />

        {errorMessage?.length && (
          <ErrorMessage msg={errorMessage} />
        )}

        {showAttendeeCheckedInPicker() && (
          <ShowCheckedInPicker
            msg={t("allocationOverview.changeAllocationMessage")}
            firstNight={firstNight}
            onChangeFirstNight={setFirstNight}
            arrivalDate={attendee.arrival}
            departureDate={attendee.departure}
          />
        )}

        {isMismatchedPreferences && (
          <AllocateRoomPreferenceWarning
            onDismiss={resetPreferenceCheck}
          />
        )}

        <Stack
          style={styles.detailsContainer}
          tokens={{ childrenGap: "16px" }}
        >
          {isRoomFinderOpen ? (
            <>
              <AllocateRoomToolbar
                scope={scope}
                roomName={roomName}
                showOccupied={showOccupied}
                selectedFilters={selectedFilters}
                options={options}
                onChangeRoomName={setRoomName}
                onChangeShowOccupied={setShowOccupied}
                onChangeFilters={setSelectedFilters}
                onOpenFilterPanel={() => setIsFilterPanelOpen(true)}
              />

              <AllocateRoomDetailsList
                fetchNext={fetchNext}
                isBedroomsLoading={isBedroomsLoading}
                setSelectedBedroom={setSelectedBedroom}
                bedrooms={bedrooms}
              />
            </>
          ) : (
            <AllocationOverview
              shimmer={isAttendeeLoading}
              allocation={currentRoomAllocation}
              validationMessage={attendeeValidationMessage}
              onRemoveAllocation={() => setCurrentRoomAllocation(null)}
              onChangeAllocation={() => setIsRoomFinderOpen(true)}
            />
          )}
        </Stack>
      </Stack>

      <AllocateRoomFilterPanel
        isOpen={isFilterPanelOpen}
        onDismiss={() => setIsFilterPanelOpen(false)}
        options={options}
        selectedFilters={selectedFilters}
        onChangeFilters={setSelectedFilters}
        onClearFilters={clearFilters}
        loading={optionsLoading}
      />
    </Panel>
  )
}
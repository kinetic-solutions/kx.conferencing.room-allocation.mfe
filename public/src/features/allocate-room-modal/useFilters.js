import { useState, useEffect } from "react"

const useSiteFilter = (attendee) => {
  const [selectedSite, setSelectedSite] = useState(null)

 // use event's site as default of site dropdown filter
  useEffect(() => {
    if (!attendee) return

    const { block, event } = attendee

    // if the attendee has a site preference,
    // then set that as the default site filter

    // if it matches the event default site,
    // then it doesn't matter which one we use
    // OR
    // if it doesn't match the event default site
    // we need to use this one anyway
    setSelectedSite({ key: block?.siteId ?? event.siteId })
  }, [attendee])

  return {
    selectedSite,
    setSelectedSite
  }
}

const useAreaFilter = (attendee, selectedSite) => {
  const [selectedArea, setSelectedArea] = useState(null)
  const [isAreaFilterDisabled, setIsAreaFilterDisabled] = useState(true)
  const [defaultAreaDone, setDefaultAreaDone] = useState(false)

  useEffect(() => {
    setIsAreaFilterDisabled(!!selectedSite)

    if (attendee && selectedSite && !defaultAreaDone) {
      // if the default area hasn't been set yet, then set it
      setSelectedArea({ key: attendee?.block?.areaId })
      setDefaultAreaDone(true)
      return
    }

    setSelectedArea(null)
  }, [attendee, selectedSite])

  return {
    selectedArea,
    setSelectedArea,
    isAreaFilterDisabled
  }
}

const useBlockFilter = (attendee, selectedArea) => {
  const [selectedBlock, setSelectedBlock] = useState(null)
  const [isBlockFilterDisabled, setIsBlockFilterDisabled] = useState(true)
  const [defaultBlockDone, setDefaultBlockDone] = useState(false)

  useEffect(() => {
    setIsBlockFilterDisabled(!!selectedArea)

    if (attendee && selectedArea && !defaultBlockDone) {
      // if the default block hasn't been set yet, then set it
      setSelectedBlock({ key: attendee.block?.id })
      setDefaultBlockDone(true)
      return
    }

    setSelectedBlock(null)
  }, [attendee, selectedArea])

  return {
    selectedBlock,
    setSelectedBlock,
    isBlockFilterDisabled
  }
}

const useSubBlockFilter = (selectedBlock) => {
  const [selectedSubBlock, setSelectedSubBlock] = useState(null)
  const [isSubBlockFilterDisabled, setIsSubBlockFilterDisabled] = useState(true)

  useEffect(() => {
    setIsSubBlockFilterDisabled(!!selectedBlock)

    setSelectedSubBlock(null)
  }, [selectedBlock])

  return {
    selectedSubBlock,
    setSelectedSubBlock,
    isSubBlockFilterDisabled
  }
}

const useBedroomTypeFilter = (attendee) => {
  const [selectedBedroomType, setSelectedBedroomType] = useState(null)

   // set default bedroom type
  useEffect(() => {
    if (!attendee?.bedroomType) return

    setSelectedBedroomType({ key: attendee.bedroomType?.id });
  }, [attendee])

  return {
    selectedBedroomType,
    setSelectedBedroomType,
  }
}

const useRoomStatusFilter = () => {
  const [selectedRoomStatus, setSelectedRoomStatus] = useState(null)

  return {
    selectedRoomStatus,
    setSelectedRoomStatus,
  }
}

const useAttributesFilter = (attendee) => {
  const [selectedAttributes, setSelectedAttributes] = useState([])

  // set default attributes
  useEffect(() => {
    if (!attendee) return

    const attr = attendee.attributes.map(({ key }) => key);
    setSelectedAttributes(attr);
  }, [attendee])

  return {
    selectedAttributes,
    setSelectedAttributes: ({ key: newKey }) => {
      const newAttributes = selectedAttributes.includes(newKey)
        // if attribute is already in selectedAttributes then remove it
        ? selectedAttributes.filter((selectedKey) => selectedKey !== newKey)
        // otherwise, append it to the array
        : [...selectedAttributes, newKey]

      setSelectedAttributes(newAttributes)
    },
    clearSelectedAttributes: () => setSelectedAttributes([])
  }
}

export default (attendee) => {
  const [showAllRooms, setShowAllRooms] = useState(false)
  const { selectedSite, setSelectedSite } = useSiteFilter(attendee)
  const { selectedArea, setSelectedArea } = useAreaFilter(attendee, selectedSite)
  const { selectedBlock, setSelectedBlock } = useBlockFilter(attendee, selectedArea)
  const { selectedSubBlock, setSelectedSubBlock } = useSubBlockFilter(selectedBlock)
  const { selectedBedroomType, setSelectedBedroomType } = useBedroomTypeFilter(attendee)
  const { selectedRoomStatus, setSelectedRoomStatus } = useRoomStatusFilter()
  const { selectedAttributes, setSelectedAttributes, clearSelectedAttributes } = useAttributesFilter(attendee)

  return {
    selectedFilters: {
      showAllRooms,
      site: selectedSite,
      area: selectedArea,
      block: selectedBlock,
      subBlock: selectedSubBlock,
      bedroomType: selectedBedroomType,
      roomStatus: selectedRoomStatus,
      attributes: selectedAttributes
    },
    setSelectedFilters: ({ showAllRooms, site, area, block, subBlock, bedroomType, roomStatus, attribute }) => {
      if(showAllRooms !== undefined) {
        setShowAllRooms(showAllRooms)
        return
      }

      if (site !== undefined) {
        setSelectedSite(site)
        return
      }

      if (area !== undefined) {
        setSelectedArea(area)
        return
      }

      if (block !== undefined) {
        setSelectedBlock(block)
        return
      }

      if (subBlock !== undefined) {
        setSelectedSubBlock(subBlock)
        return
      }

      if (bedroomType !== undefined) {
        setSelectedBedroomType(bedroomType)
        return
      }

      if (roomStatus !== undefined) {
        setSelectedRoomStatus(roomStatus)
        return
      }

      if (attribute !== undefined) {
        setSelectedAttributes(attribute)
        return
      }
    },
    clearFilters: () => {
      setSelectedSite(null)
      setSelectedArea(null)
      setSelectedBlock(null)
      setSelectedSubBlock(null)
      setSelectedBedroomType(null)
      setSelectedRoomStatus(null)
      clearSelectedAttributes()
    }
  }
}
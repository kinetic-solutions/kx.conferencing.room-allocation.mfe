import { useState, useEffect } from 'react';

export default (attendee, selectedBedroom) => {
    const [isMismatchedPreferences, setIsMismatchedPreferences] = useState(false)

    const resetPreferenceCheck = () => {
        return setIsMismatchedPreferences(false)
    }

    useEffect(() => {
        if (!attendee || !selectedBedroom) {
            return setIsMismatchedPreferences(false)
        }
        //Check attributes
        if (
            attributeMismatch(attendee, selectedBedroom) ||
            blockMismatch(attendee, selectedBedroom) ||
            bedroomTypeMismatch(attendee, selectedBedroom)
        ) {
            return setIsMismatchedPreferences(true)
        }

        return setIsMismatchedPreferences(false)
    }, [attendee, selectedBedroom])

    return { isMismatchedPreferences, resetPreferenceCheck }
}

const attributeMismatch = (attendee, bedroom) => {
    const roomAttributes = {
        smoking: false,
        vip: false,
        disabled: false
    }
    for (const attr of bedroom.attributes) {
        switch (attr.key) {
            case "smoking":
                roomAttributes.smoking = true
                break;
            case "vip":
                roomAttributes.vip = true
                break;
            case "disabled":
                roomAttributes.disabled = true
                break;
        }
    }

    const attendeeAttributes = {
        smoking: false,
        vip: false,
        disabled: false
    }
    for (const attr of attendee.attributes) {
        switch (attr.key) {
            case "smoking":
                attendeeAttributes.smoking = true
                break;
            case "vip":
                attendeeAttributes.vip = true
                break;
            case "disabled":
                attendeeAttributes.disabled = true
                break;
        }
    }

    return roomAttributes.disabled != attendeeAttributes.disabled
        || roomAttributes.smoking != attendeeAttributes.smoking
        || roomAttributes.vip != attendeeAttributes.vip
}

const blockMismatch = (attendee, bedroom) => {
    //If the attendee doesn't have any preference, then we don't need to check the block
    if (!attendee.block) {
        return false
    }
    return attendee.block?.id !== bedroom.blockID
}

const bedroomTypeMismatch = (attendee, bedroom) => {
    //If the attendee doesn't have any preference, then we don't need to check the bedroom
    if (!attendee.bedroomType) {
        return false
    }
    return attendee.bedroomType?.id !== bedroom.bedroomTypeID
}
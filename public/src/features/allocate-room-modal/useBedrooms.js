import { useState, useEffect, useRef } from 'react';
import {
  getPaginatedBedrooms,
  getPaginatedRoomingListBedrooms,
  getPaginatedReservationRooms
} from "../../core/api"

let globalDebounceToken

export default (
  attendee,
  {
    site,
    area,
    block,
    subBlock,
    bedroomType,
    roomStatus,
    attributes,
  },
  showOccupied,
  roomName,
  scope
) => {
  const [bedrooms, setBedrooms] = useState([])
  const [isBedroomsLoading, setIsBedroomsLoading] = useState(true)
  const [isFetchingNextPage, setIsFetchingNextPage] = useState(false)
  const [cursor, setCursor] = useState(null)

  const mounted = useRef(false)

  const getEndpoint = () => {
    switch (scope) {
      case "all":
        return getPaginatedBedrooms
      case "roominglist":
        return getPaginatedRoomingListBedrooms
      case "reservations":
        return getPaginatedReservationRooms
    }
  }

  const getParams = () => {
    const removeDate = (date) => date.substring(0, 10)

    const attrs = attributes.reduce((prev, next) => ({ ...prev, [next]: true }), [])

    return {
      arrival: removeDate(attendee.arrival),
      departure: removeDate(attendee.departure),
      includeOccupied: showOccupied,
      roomName: roomName || undefined,
      siteID: site?.key,
      areaID: area?.key,
      blockID: block?.key,
      subBlockID: subBlock?.key,
      bedroomTypeID: bedroomType?.key,
      maidStatus: roomStatus?.key,
      ...attrs
    }
  }

  const fetchNext = async () => {
    const endpoint = getEndpoint()

    setIsFetchingNextPage(true)

    if (isFetchingNextPage) return

    const { data: { rooms: newRooms, next } } = await endpoint({ cursor })

    // the last element in the array will be null due to the pagination implementation
    // so first we need to remove that null entry
    const oldRooms = bedrooms
    oldRooms.pop()
    const rooms = [...oldRooms, ...newRooms]

    // for pagination to work, we need to add a null entry to the end of the array
    if (next) rooms.push(null)

    setCursor(next)
    setBedrooms(rooms)
    setIsFetchingNextPage(false)
  }

  const fetchData = async () => {
    const debounceToken = Date.now()
    globalDebounceToken = debounceToken

    await new Promise((resolve) => setTimeout(resolve, 1500))

    try {
      const endpoint = getEndpoint()
      const params = getParams()

      if (globalDebounceToken !== debounceToken) return

      const { data: { rooms, next } } = await endpoint(params)

      //for pagination to work, we need to add a null entry to the end of the array if there is more data available
      if (next) rooms.push(null)

      setCursor(next)
      setBedrooms(rooms)
    } catch (e) {
      console.error(e)
    }

    // if the tokens don't match as above then this line won't run
    // this stops the loading state being changed incorrectly
    setIsBedroomsLoading(false)
  }

  useEffect(() => {
    mounted.current = true
    // don't try to fetch until the scope has been determined
    if (scope) {
      if (attendee && mounted.current) {
        setIsBedroomsLoading(true)
        fetchData();
      }
    }

    return () => { mounted.current = false }
  }, [
    attendee,
    site,
    area,
    block,
    subBlock,
    bedroomType,
    roomStatus,
    attributes,
    showOccupied,
    roomName,
    scope
  ]);

  return { bedrooms, isBedroomsLoading, fetchNext }
}
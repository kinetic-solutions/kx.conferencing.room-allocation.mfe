import React from "react"
import { render, screen, waitFor, within, act } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import "@testing-library/jest-dom"
import "../../../test/test-utils"
import {
  getAttendee,
  getPaginatedBedrooms,
  getPaginatedRoomingListBedrooms,
  getPaginatedReservationRooms,
  getSiteOptions,
  getAreaOptions,
  getBlockOptions,
  getSubBlockOptions,
  getBedroomTypeOptions,
  getAttributeOptions,
} from "../../../core/api"
import AllocateRoomModal from "../AllocateRoomModal"
import t from "../../../locales/en-GB"

jest.mock("../../../core/api", () => ({
  getAttendee: jest.fn(),
  getPaginatedBedrooms: jest.fn(),
  getPaginatedRoomingListBedrooms: jest.fn(),
  getPaginatedReservationRooms: jest.fn(),
  getSiteOptions: jest.fn(),
  getAreaOptions: jest.fn(),
  getBlockOptions: jest.fn(),
  getSubBlockOptions: jest.fn(),
  getBedroomTypeOptions: jest.fn(),
  getAttributeOptions: jest.fn(),
  allocateRoom: jest.fn(),
  deallocateRoom: jest.fn(),
}));

const bedroomsStub = Promise.resolve({
  data: {
    rooms: [{
      actualCapacity: 1,
      attributes: [],
      bedroomID: 576,
      bedroomName: "BT125A",
      bedroomType: "Single-Standard",
      bedroomTypeID: 7,
      blockID: 11,
      blockName: "Bed Block 1",
      occupied: 0,
      status: "I",
      siteID: 1,
      subBlockID: 12,
      subBlockName: "Sub Block 1",
      typicalCapacity: 1,
    }]
  }
})

beforeEach(() => {
  getAttendee.mockResolvedValue({
    data: {
      forename: "Paul",
      surname: "Smith",
      block: null,
      bedroomType: null,
      event: { id: 48885, siteId: 1, hasRoomingList: true },
      arrival: "2020-01-01T09:00:00",
      departure: "2020-01-03T17:00:00",
      attributes: []
    }
  })

  getPaginatedBedrooms.mockResolvedValue(bedroomsStub)
  getPaginatedRoomingListBedrooms.mockResolvedValue(bedroomsStub)
  getPaginatedReservationRooms.mockResolvedValue(bedroomsStub)

  getSiteOptions.mockResolvedValue({ data: { sites: [{ id: 1, name: 'site 1' }] } });
  getAreaOptions.mockResolvedValue({ data: { areas: [] } })
  getBlockOptions.mockResolvedValue({ data: { blocks: [] } })
  getSubBlockOptions.mockResolvedValue({ data: { subblocks: [] } })
  getBedroomTypeOptions.mockResolvedValue({ data: { bedroomTypes: [] } })
  getAttributeOptions.mockResolvedValue({ data: { attributes: [] } })
});

const renderModal = async () => {
  await waitFor(() => {
    render(
      <AllocateRoomModal
        onDismiss={jest.fn()}
        isOpen={true}
        attendeeId={10}
      />
    )
  })

  await waitForDebounce()
}

const openFilterPanel = async () => {
  const openFilterPanelButton = screen.getByRole("button", {
    name: t.toolbar.openFilterPanelButton.ariaLabel
  })

  await waitFor(() => {
    userEvent.click(openFilterPanelButton)
  })
}

const clearFilters = async () => {
  const clearFiltersButton = screen.getByRole("button", {
    name: t.filterPanel.clearFiltersButtonAriaLabel
  })

  await waitFor(() => {
    userEvent.click(clearFiltersButton)
  })

  await waitForDebounce()
}

const closeFilterPanel = async () => {
  const closePanelButton = screen.getByRole("button", {
    name: t.filterPanel.closeButtonAriaLabel
  })

  await waitFor(() => {
    userEvent.click(closePanelButton)
  })
}

const waitForDebounce = async () => {
  await new Promise((resolve) => {
    setTimeout(resolve, 1500)
  })
}

describe("grid", () => {
  test("loads and displays attendee and the room data", async () => {
    await act(async () => await renderModal())

    //Loading message should appear
    await screen.findByRole("heading")

    //Name and date should be displayed
    const name = await screen.findByText("Paul Smith")
    expect(name).toBeInTheDocument()

    const date = await screen.findByText(/Jan 01, 2020 \- Jan 03, 2020/i)
    expect(date).toBeInTheDocument()

    const [_, ...rows] = screen.getAllByRole("row")
    expect(rows).toHaveLength(1)
    expect(rows[0]).toHaveTextContent("InspectedBT125ASingle-StandardBed Block 1Sub Block 1110None")
  })

  test("displays a message if no bedrooms were found", async () => {
    getPaginatedRoomingListBedrooms.mockResolvedValue({ data: { rooms: [] } })

    await act(async () => await renderModal())

    //Should show a message about bedrooms
    const noRoomsMessage = await screen.findByText(t.detailsList.noBedrooms.label);
    expect(noRoomsMessage).toBeInTheDocument()
  })
})

describe("mismatched preferences message", () => {
  test("doesn't display a message bar if the selected room matches the attendee's preferences completely", async () => {
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        arrival: "2020-01-01T09:00:00",
        departure: "2020-01-03T17:00:00",
        attributes: [
          {
            key: "smoking",
            value: "Smoking"
          },
          {
            key: "disabled",
            value: "Disabled"
          },
          {
            key: "vip",
            value: "VIP"
          }
        ],
        block: {
          id: 12,
          name: "Block 12",
          areaId: 1,
          siteId: 2
        },
        event: {
          siteId: 2,
          hasRoomingList: true
        },
        bedroomType: {
          id: 12,
          name: "Single En-Suite"
        }
      }
    })

    getPaginatedRoomingListBedrooms.mockResolvedValue({
      data: {
        rooms: [{
          actualCapacity: 1,
          attributes: [{ key: "smoking", value: "Smoking" }, { key: "disabled", value: "Disabled" }, { key: "vip", value: "VIP" }],
          bedroomID: 576,
          bedroomName: "BT125A",
          bedroomType: "Single En-Suite",
          bedroomTypeID: 12,
          blockID: 12,
          blockName: "Block 12",
          occupied: 0,
          status: "I",
          subBlockID: 12,
          subBlockName: "Sub Block 1",
          typicalCapacity: 1,
        }]
      }
    })

    await act(async () => {
      await renderModal()
      await openFilterPanel()
      await clearFilters()
      await closeFilterPanel()
    })

    const [_, ...rows] = screen.getAllByRole("row")

    expect(rows).toHaveLength(1)

    await waitFor(() => {
      userEvent.click(rows[0])
    })

    await waitForDebounce()

    //Since the selected bedroom block doesn't conflict with the attendees preferences, an error isn"t returned
    //The status bar is usually contained in within an element that has a region role
    expect(screen.queryByRole("region")).toBeNull()
  })

  test("displays a message bar if the preferences don't match", async () => {
    const attendee = {
      forename: "Paul",
      surname: "Smith",
      arrival: "2020-01-01T09:00:00",
      departure: "2020-01-03T17:00:00",
      attributes: [{ key: "smoking", value: "Smoking" }, { key: "disabled", value: "Disabled" }, { key: "vip", value: "VIP" }],
      block: { id: 12, name: "Block 12", siteId: 1 },
      event: { hasRoomingList: true },
      bedroomType: { id: 12, name: "Single En-Suite" }
    }

    const room = (bedroomName, { block, bedroomType, attributes } = {}) => ({
      actualCapacity: 1,
      attributes: attributes ?? attendee.attributes,
      bedroomID: 576,
      bedroomName,
      bedroomType: bedroomType?.name ?? attendee.bedroomType.name,
      bedroomTypeID: bedroomType?.id ?? attendee.bedroomType.id,
      blockID: block?.id ?? attendee.block.id,
      blockName: block?.name ?? attendee.block.name,
      occupied: 0,
      status: "I",
      subBlockID: 12,
      subBlockName: "Sub Block 1",
      typicalCapacity: 1,
    })

    //The returned attendee has a preferences for block, bedroomType and attributes
    getAttendee.mockResolvedValue({
      data: attendee
    })

    getPaginatedRoomingListBedrooms.mockResolvedValue({
      data: {
        rooms: [
          room("room with wrong block", { block: { id: 100, name: "Wrong block" } }),
          room("room with wrong bedroom type", { bedroomType: { id: 100, name: "Wrong bedroom type" } }),
          room("room without smoking", { attributes: [{ key: "vip", value: "vip" }, { key: "disabled", value: "disabled" }] }),
          room("room without vip", { attributes: [{ key: "smoking", value: "smoking" }, { key: "disabled", value: "disabled" }] }),
          room("room without disabled", { attributes: [{ key: "vip", value: "vip" }, { key: "smoking", value: "smoking" }] }),
          room("room without attributes", { attributes: [] })
        ]
      }
    })

    await act(async () => {
      await renderModal()
      await openFilterPanel()
      await clearFilters()
      await closeFilterPanel()
    })

    //The first room doesn't have the right block
    userEvent.click(screen.getByText("room with wrong block"))
    const region = screen.getByRole("region")
    expect(within(region).getByRole("status")).toBeInTheDocument()

    //The second room doesn't have the right bedroom type
    userEvent.click(screen.getByText("room with wrong bedroom type"))
    expect(within(region).getByRole("status")).toBeInTheDocument()

    //The third room doesn't have the smoking attribute
    userEvent.click(screen.getByText("room without smoking"))
    expect(within(region).getByRole("status")).toBeInTheDocument()

    //The fourth room doesn't have the vip attribute
    userEvent.click(screen.getByText("room without vip"))
    expect(within(region).getByRole("status")).toBeInTheDocument()

    //The fifth room doesn't have the disabled attribute
    userEvent.click(screen.getByText("room without disabled"))
    expect(within(region).getByRole("status")).toBeInTheDocument()

    //The sixth room doesn't have any attributes
    userEvent.click(screen.getByText("room without attributes"))
    expect(within(region).getByRole("status")).toBeInTheDocument()
  })

  test("doesn't display a message bar if the attendee has no preferences", async () => {
    await act(async () => {
      await renderModal()
      await openFilterPanel()
      await clearFilters()
      await closeFilterPanel()
    })

    await waitFor(() => {
      userEvent.click(screen.getByText("BT125A"))
    })

    expect(screen.queryByRole("region")).toBeNull()
  })
})

describe("attendee that already has an allocation", () => {
  test("the user can see the current allocation screen", async () => {
    //The returned attendee has a room allocation
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        arrival: "2020-01-01T09:00:00",
        departure: "2020-01-03T17:00:00",
        attributes: [],
        event: { siteId: 1, hasRoomingList: true },
        roomAllocation: {
          actualCapacity: 1,
          attributes: [{ key: "smoking", value: "Smoking" }, { key: "disabled", value: "Disabled" }, { key: "vip", value: "VIP" }],
          bedroomID: 576,
          bedroomName: "BT125A",
          bedroomType: "Single En-Suite",
          bedroomTypeID: 12,
          blockID: 12,
          blockName: "Block 12",
          occupied: 0,
          status: "C",
          subBlockID: 12,
          subBlockName: "Sub Block 1",
          typicalCapacity: 1,
        }
      }
    })

    await act(async () => await renderModal())

    const currentAllocation = screen.getByText(t.allocationOverview.title)

    expect(currentAllocation).toBeInTheDocument()
  })

  test("if the user chooses to select a new allocation, the current room allocation isn't visible in the grid", async () => {
    //The returned attendee has a room allocation
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        arrival: "2020-01-01T09:00:00",
        departure: "2020-01-03T17:00:00",
        attributes: [],
        event: { siteId: 1, hasRoomingList: true },
        roomAllocation: {
          actualCapacity: 1,
          attributes: [{ key: "smoking", value: "Smoking" }, { key: "disabled", value: "Disabled" }, { key: "vip", value: "VIP" }],
          bedroomID: 576,
          bedroomName: "BT125A",
          bedroomType: "Single En-Suite",
          bedroomTypeID: 12,
          blockID: 12,
          blockName: "Block 12",
          occupied: 0,
          status: "C",
          subBlockID: 12,
          subBlockName: "Sub Block 1",
          typicalCapacity: 1,
        }
      }
    })

    await act(async () => await renderModal())

    const changeAllocation = screen.getByRole('button', { name: t.allocationOverview.changeAllocationButton.ariaLabel })

    await waitFor(() => {
      userEvent.click(changeAllocation)
    })

    expect(screen.queryByText(t.allocationOverview.title)).not.toBeInTheDocument()
  })
})

describe("unable to edit allocation screen", () => {
  test("if the attendee is allocated to a flat, a message is visible", async () => {
    //The returned attendee is allocated to a flat
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        arrival: "2020-01-01T09:00:00",
        departure: "2020-01-03T17:00:00",
        attributes: [],
        event: { siteId: 1, hasRoomingList: true },
        roomAllocation: {
          actualCapacity: 1,
          attributes: [{ key: "smoking", value: "Smoking" }, { key: "disabled", value: "Disabled" }, { key: "vip", value: "VIP" }],
          bedroomID: 576,
          bedroomName: "BT125A",
          bedroomType: "Single En-Suite",
          bedroomTypeID: 12,
          blockID: 12,
          blockName: "Block 12",
          occupied: 0,
          status: "C",
          subBlockID: 12,
          subBlockName: "Sub Block 1",
          typicalCapacity: 1,
          isFlat: true
        }
      }
    })

    await act(async () => await renderModal())

    const errorMessage = await screen.findByText(t.allocationOverview.errors.isFlat)
    expect(errorMessage).toBeInTheDocument()
  })

  test("if the attendee is arriving and departing on the same day, a message is visible", async () => {
    //The returned attendee is allocated to a flat
    getAttendee.mockResolvedValue({
      data: {
        forename: "Paul",
        surname: "Smith",
        arrival: "2020-01-01T09:00:00",
        departure: "2020-01-01T17:00:00",
        event: { siteId: 1, hasRoomingList: true },
        attributes: []
      }
    })

    await act(async () => await renderModal())

    const errorMessage = await screen.findByText(t.allocationOverview.errors.sameDay)
    expect(errorMessage).toBeInTheDocument()
  })
})
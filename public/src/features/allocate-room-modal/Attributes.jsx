import React from 'react'
import { getTheme } from "@fluentui/react"

const Pill = ({ text, color }) => {
  const theme = getTheme()

  const { border, backgroundColor } =
    {
      green: {
        border: "#25936f",
        backgroundColor: "#c3e5da",
      },
      red: {
        border: "#f48331",
        backgroundColor: "#f8d7da",
      },
      orange: {
        border: "#f48331",
        backgroundColor: "#fad5c8",
      },
      grey: {
        border: "#d9d9d9",
        backgroundColor: theme.palette.neutralLight,
      },
    }[color] || {};

  return (
    <span
      style={{
        border: `1px solid ${border}`,
        backgroundColor,
        padding: "4px",
        borderRadius: "4px",
        color: "#282b2c",
        width: "min-content"
      }}
    >
      {text}
    </span>
  )
}

export const Attribute = ({ attribute }) => {
  const attributesColorMap = {
    vip: "grey",
    smoking: "orange",
    lastLet: "green",
    disabled: "green"
  }
  const color = attributesColorMap[attribute.key]
  return (<Pill text={attribute.value} color={color} />)
}
import { useState, useEffect } from 'react';
import { getAttendee } from "../../core/api"
import { useTranslation } from 'react-i18next';
import { getTenantCode } from '../../core/axios';

export default (attendeeID) => {
  const [attendee, setAttendee] = useState(null)
  const [isAttendeeValid, setIsAttendeeValid] = useState(true)
  const [attendeeValidationMessage, setAttendeeValidationMessage] = useState("")
  const [isAttendeeLoading, setIsAttendeeLoading] = useState(true)
  const { t } = useTranslation()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: attendeeData } = await getAttendee(attendeeID)
        setAttendee(attendeeData)
      } catch (e) {
        console.error(e)
      }

      setIsAttendeeLoading(false)
    }

    if (attendeeID) {
      fetchData();
    }

  }, [attendeeID]);

  //when the attendee is loaded, check if it's valid
  useEffect(() => {
    if (!attendee) {
      return
    }
    if (attendee.roomAllocation?.isFlat) {
      setIsAttendeeValid(false)
      return setAttendeeValidationMessage(t("allocationOverview.errors.isFlat"))
    }
    //We need to check if the arrival date and departure date at the time, ignoring the time
    //Date is a string with format of 2020-01-01T09:00:00
    if (attendee.arrival.split("T")[0] === attendee.departure.split("T")[0]) {
      setIsAttendeeValid(false)
      return setAttendeeValidationMessage(t("allocationOverview.errors.sameDay"))
    }

    setIsAttendeeValid(true)
    }, [attendee]);

  return { attendee, isAttendeeLoading, isAttendeeValid, attendeeValidationMessage }
}
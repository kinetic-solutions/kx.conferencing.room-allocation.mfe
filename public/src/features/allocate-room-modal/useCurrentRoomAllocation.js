import {useState, useEffect} from "react"

export default (attendee, isAttendeeValid, setIsRoomFinderOpen) => {
  const [currentRoomAllocation, setCurrentRoomAllocation] = useState(null)
    // Set the current room allocation
  useEffect(() => {
    if (!attendee) return;

    setCurrentRoomAllocation(attendee.roomAllocation);
    setIsRoomFinderOpen(isAttendeeValid && attendee.roomAllocation == null)
  }, [attendee, isAttendeeValid]);

  return [currentRoomAllocation, setCurrentRoomAllocation]
}
import { useEffect, useState, useRef } from "react"
import {
  getSiteOptions,
  getAreaOptions,
  getBlockOptions,
  getSubBlockOptions,
  getBedroomTypeOptions,
  getAttributeOptions
} from "../../core/api"
import { useRoomStatusDefinitions } from "./useRoomStatusDefinitions"

const mapToDropdownOptions = (options) => {
  return options.map(({ id, name }) => ({ key: id, text: name }))
}

export default (bedrooms, selectedFilters, setSelectedFilters, scope, eventId) => {
  const [siteLoading, setSiteLoading] = useState(false)
  const [areaLoading, setAreaLoading] = useState(false)
  const [blockLoading, setBlockLoading] = useState(false)
  const [subBlockLoading, setSubBlockLoading] = useState(false)
  const [bedroomTypeLoading, setBedroomTypeLoading] = useState(false)

  const [siteOptions, setSiteOptions] = useState([])
  const [areaOptions, setAreaOptions] = useState([])
  const [blockOptions, setBlockOptions] = useState([])
  const [subBlockOptions, setSubBlockOptions] = useState([])
  const [bedroomTypeOptions, setBedroomTypeOptions] = useState([])
  const [roomStatusOptions, setRoomStatusOptions] = useState([])
  const [attributeOptions, setAttributeOptions] = useState([])
  const mounted = useRef(false)
  useEffect(() => {
    // fetch options that only need to be re-fetched when the scope changes
    // sites, room statuses, attributes
    if (!scope || !eventId) return

    (async () => {
      setSiteLoading(true)
      const { data: { sites } } = await getSiteOptions(scope, eventId)
      setSiteLoading(false)
      setSiteOptions(mapToDropdownOptions(sites))
    })();

    (async () => {
      setBedroomTypeLoading(true)
      const { data: { bedroomTypes } } = await getBedroomTypeOptions(scope, eventId)
      setBedroomTypeLoading(false)

      setBedroomTypeOptions(mapToDropdownOptions(bedroomTypes))
    })();

    (async () => {
      const { data: { attributes } } = await getAttributeOptions()

      const options = Object.entries(attributes).map(([key, text]) => ({ key, text }))
      setAttributeOptions(options)
    })();
  }, [scope, eventId])

  useEffect(() => {
    mounted.current = true
    // Check if the "default" site exists in siteOptions
    if (!!selectedFilters) {
      const notExist = !siteOptions.find(x => {
        return x.key === selectedFilters.site?.key
      })

      if (notExist && mounted.current) {
          setSelectedFilters({ "site": null })
      }
    }

    return () => { mounted.current = false }
  }, [siteOptions])

  useEffect(() => {
    // if no site is selected then return
    if (!selectedFilters.site?.key || !scope) {
      setAreaOptions([])
      return
    }

    // refetch area options
    (async () => {
      setAreaLoading(true)
      const { data: { areas } } = await getAreaOptions(selectedFilters.site.key, scope, eventId)
      setAreaLoading(false)
      setAreaOptions(mapToDropdownOptions(areas))
    })();
  }, [selectedFilters.site, scope])

  useEffect(() => {
    // if no area is selected then return
    if (!selectedFilters.area?.key || !scope) {
      setBlockOptions([])
      return
    }

    // refetch block options
    (async () => {
      setBlockLoading(true)
      const { data: { blocks } } = await getBlockOptions(selectedFilters.site.key, selectedFilters.area.key, scope, eventId)
      setBlockLoading(false)

      setBlockOptions(mapToDropdownOptions(blocks))
    })();
  }, [selectedFilters.area, scope])

  useEffect(() => {
    // if no block is selected then return
    if (!selectedFilters.block?.key || !scope) {
      setSubBlockOptions([])
      return
    }

    // refetch sub block options
    (async () => {
      setSubBlockLoading(true)
      const { data: { subblocks } } = await getSubBlockOptions(selectedFilters.block.key, scope, eventId)
      setSubBlockLoading(false)
      setSubBlockOptions(mapToDropdownOptions(subblocks))
    })();
  }, [selectedFilters.block, scope])

  useEffect(() => {
    if (!bedrooms?.length) return

    const definitions = useRoomStatusDefinitions()

    const options = Object.entries(definitions).map(
      ([key, { text }]) => ({ key, text })
    )

    setRoomStatusOptions(options)
  }, [bedrooms])

  return {
    options: {
      site: siteOptions,
      area: areaOptions,
      block: blockOptions,
      subBlock: subBlockOptions,
      bedroomType: bedroomTypeOptions,
      roomStatus: roomStatusOptions,
      attribute: attributeOptions,
    },
    optionsLoading: {
      site: siteLoading,
      area: areaLoading,
      block: blockLoading,
      subBlock: subBlockLoading,
      bedroomType: bedroomTypeLoading,
    }
  }
}
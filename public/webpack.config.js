const fs = require("fs");
const webpackMerge = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react");

module.exports = (webpackConfigEnv, argv) => {
  const projectName = "room-allocation";

  const defaultConfig = singleSpaDefaults({
    orgName: "kinetic",
    projectName,
    webpackConfigEnv,
    argv,
    orgPackagesAsExternal: false,
  });

  const filenamePattern =
    argv.mode === "production" ? "[name]/[name].[contenthash].js" : "[name].js";

  const config = webpackMerge.smart(defaultConfig, {
    output: {
      filename: filenamePattern,
    },
    entry: {
      [projectName]: `./src/kinetic-${projectName}.js`,
    },
    externals: [/^@kinetic\/.+/], //this let's us use components exported by other MFEs
  });

  return config;
};

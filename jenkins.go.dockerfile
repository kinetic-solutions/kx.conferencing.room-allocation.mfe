FROM golang:1.20-buster

RUN apt-get update
RUN apt-get -y install software-properties-common
RUN apt-get -y install python3 python3-venv python3-pip
RUN apt-get -y install unzip
RUN curl -L "https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip" -o "aws-sam-cli-linux-x86_64.zip"
RUN unzip aws-sam-cli-linux-x86_64.zip -d sam-installation
RUN ./sam-installation/install
RUN sam --version

// Package getsubblocks gets a list of subblocks for a site and area
package getsubblocks

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getSubBlocks", func(ctx context.Context) error {
		query := &query{}
		if err := parseQuery(lc.Request.QueryStringParameters, query); err != nil {
			return err
		}

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		rows := []subblock{}
		switch query.Scope {
		case RoomingListScope:
			if err := getRoomingListSubBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		case ReservationsScope:
			if err := getReservedRoomsSubBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		default:
			if err := getAllSubBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		}

		return lc.JSON(http.StatusOK, apiResponse{SubBlocks: rows})
	})
}

func getAllSubBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]subblock, query *query) error {
	return tx.SelectContext(ctx, dest, `
			SELECT
				SB.BlockID,
				RTRIM(SB.Name) AS Name
			FROM SiteBlocks SB
			WHERE SB.MasterBlockID=@MasterBlockID
			ORDER BY SB.SequenceNo`,
		sql.Named("MasterBlockID", query.MasterBlockID),
	)
}

func getRoomingListSubBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]subblock, query *query) error {
	return tx.SelectContext(ctx, dest, `
			SELECT DISTINCT
				SB.BlockID,
				RTRIM(SB.Name) AS Name,
				SB.SequenceNo
			FROM EventModuleResidentialRoomAllocation EMRRA
			JOIN ResidentialRooms RR ON RR.ResidentialRoomID = EMRRA.ResidentialRoomID
			JOIN SiteBlocks SB ON SB.BlockID = RR.SubBlockID
			JOIN EventModules EM ON EM.EventModuleID = EMRRA.EventModuleID
			WHERE SB.MasterBlockID=@MasterBlockID
			AND EM.EventID = @EventID
			ORDER BY SB.SequenceNo`,
		sql.Named("MasterBlockID", query.MasterBlockID),
		sql.Named("EventID", query.EventID),
	)
}

func getReservedRoomsSubBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]subblock, query *query) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT
		SB.BlockID,
		RTRIM(SB.Name) AS Name
	FROM EventModuleBlockBedroomAllocation EMBBA
	JOIN SiteBlocks SB ON SB.MasterBlockID = EMBBA.BlockID
	JOIN EventModules EM ON EM.EventModuleID = EMBBA.EventModuleID
	WHERE EMBBA.BlockID=@MasterBlockID
	AND EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID),
		sql.Named("MasterBlockID", query.MasterBlockID))
}

//parseQuery parses and validates the query string parameters into a struct
func parseQuery(queryParams map[string]string, dest *query) error {
	scope, ok := queryParams["scope"]
	if ok {
		if scope != RoomingListScope && scope != ReservationsScope && scope != AllScope {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "scope", Error: "scope must be either roominglist, reservations, or all"}},
			}
		}
		dest.Scope = scope
	} else {
		//default to all
		dest.Scope = AllScope
	}

	eventID, ok := queryParams["eventID"]
	if ok {
		id, err := strconv.Atoi(eventID)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID must be an integer"}},
			}
		}
		dest.EventID = &id
	}

	if dest.Scope == RoomingListScope || dest.Scope == ReservationsScope {
		if dest.EventID == nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID is required if scope is set"}},
			}
		}
	}

	masterBlockIdString, ok := queryParams["masterBlockId"]
	if ok {
		masterBlockID, err := strconv.Atoi(masterBlockIdString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "masterBlockId", Error: "masterBlockId must be a number"}},
			}
		}

		dest.MasterBlockID = masterBlockID
	} else {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "masterBlockId", Error: "masterBlockId is required"}},
		}
	}

	return nil
}

package getsubblocks

const (
	RoomingListScope  string = "roominglist"
	ReservationsScope string = "reservations"
	AllScope          string = "all"
)

type query struct {
	EventID *int
	//oneof: roominglist, reservations, all - defaults to all
	Scope         string
	MasterBlockID int
}

//the data retured to the API
type apiResponse struct {
	SubBlocks []subblock `json:"subblocks"`
}

//the data returned from the database
type subblock struct {
	ID         int    `db:"BlockID" json:"id"`
	Name       string `db:"Name" json:"name"`
	SequenceNo int    `db:"SequenceNo" json:"-"`
}

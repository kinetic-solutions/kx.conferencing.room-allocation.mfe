// Package getsites gets a list of sties for an event.
package getsites

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getSites", func(ctx context.Context) error {
		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		query := getSitesQuery{}
		if err := parseQuery(lc.Request.QueryStringParameters, &query); err != nil {
			return err
		}

		rows := []dbRow{}
		switch query.Scope {
		case RoomingListScope:
			if err := getRoomingListSites(ctx, tx, &rows, &query); err != nil {
				return err
			}
		case ReservationsScope:
			if err := getReservedRoomsSites(ctx, tx, &rows, &query); err != nil {
				return err
			}
		default:
			if err := getAllSites(ctx, tx, &rows); err != nil {
				return err
			}
		}

		//If there aren't any rows, get the default site for the event
		if len(rows) == 0 {
			if err := getDefaultSite(ctx, tx, &rows, &query); err != nil {
				return err
			}
		}

		uiRows := make([]uiRow, 0, len(rows))
		for _, r := range rows {
			uiRows = append(uiRows, r.toUI())
		}
		return lc.JSON(http.StatusOK, apiResponse{Sites: uiRows})
	})
}

// getAllSites gets all of the tenant's sites
func getAllSites(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow) error {
	return tx.SelectContext(ctx, dest, `SELECT SiteID, Name FROM Sites S`)
}

func getDefaultSite(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *getSitesQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT S.SiteID, S.Name
	FROM Sites S
	JOIN EventModules EM ON S.SiteID = EM.SiteID
	WHERE EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID))
}

// getRoomingListSites gets the sites for the rooms that are on the rooming list for the event.
func getRoomingListSites(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *getSitesQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT S.SiteID, S.Name
	FROM EventModuleResidentialRoomAllocation EMRRA
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = EMRRA.ResidentialRoomID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID
	JOIN Sites S on S.SiteID = B.SiteID
	JOIN EventModules EM ON EM.EventModuleID = EMRRA.EventModuleID
	WHERE EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID))
}

// getReservedRoomsSites gets the sites for the rooms that are reserved for the event.
func getReservedRoomsSites(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *getSitesQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT S.SiteID, S.Name
	FROM EventModuleBlockBedroomAllocation EMBBA
	JOIN SiteBlocks B ON B.BlockID = EMBBA.BlockID
	JOIN Sites S on S.SiteID = B.SiteID
	JOIN EventModules EM ON EM.EventModuleID = EMBBA.EventModuleID
	WHERE EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID))
}

// parseQuery parses the query string parameters into a getSitesQuery
func parseQuery(queryParams map[string]string, dest *getSitesQuery) error {
	scope, ok := queryParams["scope"]
	if ok {
		if scope != RoomingListScope && scope != ReservationsScope && scope != AllScope {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "scope", Error: "scope must be either roominglist, reservations, or all"}},
			}
		}
		dest.Scope = scope
	} else {
		//default to all
		dest.Scope = AllScope
	}

	eventID, ok := queryParams["eventID"]
	if ok {
		id, err := strconv.Atoi(eventID)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID must be an integer"}},
			}
		}
		dest.EventID = &id
	}

	if dest.Scope == RoomingListScope || dest.Scope == ReservationsScope {
		if dest.EventID == nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID is required if scope is set"}},
			}
		}
	}

	return nil
}

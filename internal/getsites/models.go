package getsites

const (
	RoomingListScope  string = "roominglist"
	ReservationsScope string = "reservations"
	AllScope          string = "all"
)

//the data retured to the API
type apiResponse struct {
	Sites []uiRow `json:"sites"`
}

type uiRow struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

//the data returned from the database
type dbRow struct {
	ID   int    `db:"SiteID"`
	Name string `db:"Name"`
}

type getSitesQuery struct {
	EventID *int
	//oneof: roominglist, reservations, all - defaults to all
	Scope string
}

// toUI converts a database row to a UI Row
func (r *dbRow) toUI() uiRow {

	row := uiRow{
		ID:   r.ID,
		Name: r.Name,
	}

	return row
}

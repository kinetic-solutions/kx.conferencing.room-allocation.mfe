// Package validate adds custom functionality to the go validator
package validate

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/go-playground/validator/v10"
)

type AttendeeRequestValidator struct {
	*validator.Validate
}

func New() *AttendeeRequestValidator {
	validate := validator.New()
	// we want the error messages to use the json tag name instead of the struct property name
	// so we can override this behaviour here
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	return &AttendeeRequestValidator{
		Validate: validate,
	}
}

//Uses the validator library to valdiate a given struct
//It uses the struct validate tags to know what is expected
func (vld *AttendeeRequestValidator) ValidateModel(out interface{}) ([]lambdawrapper.FieldError, error) {
	switch v := vld.Struct(out).(type) {
	case nil:
		return nil, nil
	case validator.ValidationErrors:
		fields := make([]lambdawrapper.FieldError, len(v))

		for i, e := range v {
			fields[i] = lambdawrapper.FieldError{Field: e.Field(), Error: e.ActualTag()}
		}

		return fields, nil
	default:
		return nil, fmt.Errorf("error validating model: %w", v)
	}
}

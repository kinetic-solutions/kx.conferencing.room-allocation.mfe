package deallocateroom

import (
	"context"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog"
	krn "bitbucket.org/kinetic-solutions/kx.pulse.krn"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"
)

//publish publishes an audit record
func publishAudit(lc *lambdawrapper.LambdaContext, pub auditlog.Publisher, eventID int, userID int, msg []string) error {
	return xray.Capture(lc.Context(), "publish audit record",
		func(ctx context.Context) error {
			t := lc.Tenancy()
			o := "Room Un-Allocated"
			messageID, err := pub.Publish(ctx, t.ID, auditlog.Message{
				Records: []auditlog.Record{
					{
						AuditLog:  buildKRN(lc.Tenancy(), 0), // IDValue is 0 for these audits
						UserID:    userID,
						Operation: o,
						Message:   msg,
					},
				},
				Category:  "Bedrooms",
				Publisher: "kx.conferencing.room-allocation-mfe",
				EventID:   eventID,
				LogType:   auditlog.EventModuleAuditLogs,
			})
			if err != nil {
				return errors.Wrap(err, "failed to publish audit record")
			}
			lc.Log.Infow("published audit record", "messageID", messageID)
			return nil
		})
}

func buildKRN(t lambdawrapper.Tenancy, id int) krn.KRN {
	var (
		tenantID = t.ID
		table    = "ResidentialRoomOccupants"
		key      = strconv.Itoa(id)
		emal     = auditlog.EventModuleAuditLogs
	)

	return auditlog.KRN(tenantID, emal, table, key)
}

package deallocateroom

import (
	"context"
	"database/sql"
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/date"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type dbEventModuleDelegate struct {
	EventModuleDelegateID    int       `db:"EventModuleDelegateID"`
	PersonID                 int       `db:"PersonID"`
	Forename                 string    `db:"Forename"`
	Surname                  string    `db:"Surname"`
	PersonSequenceID         int       `db:"PersonSequenceID"`
	ArrivalDate              time.Time `db:"ArrivalDate"`
	DepartureDate            time.Time `db:"DepartureDate"`
	ResidentialRoomOccupants []roomOccupant
}
type roomOccupant struct {
	ResidentialRoomID int           `db:"ResidentialRoomID"`
	Date              date.Date     `db:"Date"`
	Capacity          int           `db:"Capacity"`
	RoomName          string        `db:"Name"`
	BedroomType       dbBedroomType `db:"BT"`
	BlockID           int           `db:"BlockID"`
}

// getAttendee queries the database for the list of attendees
func getAttendee(ctx context.Context, tx *sqlx.Tx, eventModuleID int, attendeeId int, row *dbEventModuleDelegate) error {
	return xray.Capture(ctx, "query.getEventAttendee", func(c context.Context) error {
		if err := tx.GetContext(c, row, `
		SELECT
			EMD.EventModuleDelegateID,
			EMD.PersonID,
			P.Forename,
			P.Surname,
			EMD.PersonSequenceID,
			EMD.ArrivalDate,
			EMD.DepartureDate
		FROM EventModuleDelegates EMD
			LEFT JOIN People P ON P.PersonID = EMD.PersonID
		WHERE
			EMD.EventModuleID = @EventModuleID
			AND EMD.EventModuleDelegateID = @AttendeeID
		`, sql.Named("EventModuleID", eventModuleID), sql.Named("AttendeeID", attendeeId)); err != nil {
			return err
		}
		err := tx.SelectContext(ctx, &row.ResidentialRoomOccupants, `
			SELECT 
				RRO.ResidentialRoomID,
				RRO.Date, 
				RR.Capacity,
				RR.BlockID,
				RR.Name,
				BT.BedroomTypeID  AS [BT.BedroomTypeID],
				BT.Description AS [BT.Description],
				BT.BookByRoom AS [BT.BookByRoom],
				BT.RoomInFlat AS [BT.RoomInFlat]
			FROM ResidentialRoomOccupants RRO 
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
			LEFT JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomtypeID
			WHERE 
			PersonID = @PersonID AND PersonSequenceID = @PersonSequenceID AND EventModuleID = @EventModuleID
			ORDER BY RRO.Date ASC
		`, sql.Named("PersonID", row.PersonID), sql.Named("PersonSequenceID", row.PersonSequenceID),
			sql.Named("EventModuleID", eventModuleID))
		return err
	})

}

func DeleteFromRoomingList(ctx context.Context, log *zap.SugaredLogger, tx *sqlx.Tx, allocationIDs []int) error {
	return xray.Capture(ctx, "query.deleteFromRoomingList", func(ctx context.Context) error {
		sql := `
	DELETE RRO
	FROM ResidentialRoomOccupants RRO
	JOIN EventModuleResidentialRoomAllocation EMRRA
	 	ON RRO.EventModuleID = EMRRA.EventModuleID
		 AND RRO.ResidentialRoomID = EMRRA.ResidentialRoomID
		 AND RRO.Date BETWEEN EMRRA.FirstNight and EMRRA.LastNight
	WHERE EMRRA.EMRRAID IN (?)

	DELETE FROM EventModuleResidentialRoomAllocation
	WHERE EMRRAID IN (?)
	`

		query, args, err := sqlx.In(sql, allocationIDs, allocationIDs)
		if err != nil {
			return err
		}
		query = tx.Rebind(query)

		result, err := tx.ExecContext(ctx, query, args...)
		if err != nil {
			return err
		}
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			return err
		}
		log.Infow("finished deleting rooms from rooming list", "rowsAffected", rowsAffected)
		return err
	})
}

type dbBedroomType struct {
	BedroomTypeID int    `db:"BedroomTypeID"`
	Description   string `db:"Description"`
	BookByRoom    bool   `db:"BookByRoom"`
	RoomInFlat    bool   `db:"RoomInFlat"`
}
type dbResidentialRoom struct {
	ResidentialRoomID int           `db:"ResidentialRoomID"`
	Capacity          int           `db:"Capacity"`
	RoomName          string        `db:"Name"`
	BedroomType       dbBedroomType `db:"BT"`
	BlockID           int           `db:"BlockID"`
}

// GetRoom gets the room for the given room id
func GetRoom(c context.Context, tx *sqlx.Tx, roomID int, ra *dbResidentialRoom) error {
	return xray.Capture(c, "attendees.getRooms", func(ctx context.Context) error {
		return tx.GetContext(ctx, ra, `
	SELECT
		RR.ResidentialRoomID,
		RR.Capacity,
		RR.BlockID,
		RR.Name,
		BT.BedroomTypeID  AS [BT.BedroomTypeID],
		BT.Description AS [BT.Description],
		BT.BookByRoom AS [BT.BookByRoom],
		BT.RoomInFlat AS [BT.RoomInFlat]
	FROM ResidentialRooms RR
	LEFT JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomtypeID
	WHERE RR.ResidentialRoomID = @ResidentialRoomID
`, sql.Named("ResidentialRoomID", roomID))
	})
}

// Package deallocateroom deallocates a attendee from their rooms
package deallocateroom

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/date"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/eventmodule"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/lambdacontext"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/roomoccupancy"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/usertokenhelper"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"
)

const attendeeIDPathParam = "attendeeID"

// Handler creates a lambda handler.
// It will use the given auditlog.Publisher to publish audit records.
func Handler(pub auditlog.Publisher) func(lc *lambdawrapper.LambdaContext) error {
	return func(lc *lambdawrapper.LambdaContext) error {
		log, db := lc.Log, lc.DB
		lambdaCtx := &lambdacontext.RoomAllocationLambdaContext{
			LambdaContext: lc,
		}

		return xray.Capture(lc.Context(), "roomAllocation.allocateRoom", func(ctx context.Context) error {
			log.Infow("parsing request")
			attendeeID, err := parseAttendeeID(lc.Request.PathParameters)
			if err != nil {
				return err
			}

			var requestBody = &requestBody{}
			if len(lc.Request.Body) > 0 {
				log.Infow("parsing JSON request Body")
				if err := lambdawrapper.ParseJSON(lc.Request.Body, requestBody); err != nil {
					switch v := err.(type) {
					case lambdawrapper.RequestError:
						return err
					default:
						return lambdawrapper.RequestError{
							Status:  http.StatusBadRequest,
							Message: v.Error(),
						}
					}
				}
			}

			//Getting event module
			eventID, err := lambdaCtx.GetEventID()
			if err != nil {
				return invalidEventID(err.Error())
			}
			userID, err := usertokenhelper.ExtractUserId(lc)
			if err != nil {
				return lambdawrapper.RequestError{
					Status:  http.StatusBadRequest,
					Message: fmt.Sprintf("Failed to retrieve user id from token: %v", err),
				}
			}

			log.Infow("getting the request time adjusted for the tenants timezone")
			zone, now, creationDate, err := lambdaCtx.GetTenantRequestTime()
			if err != nil {
				return fmt.Errorf("getting request time in tenant's local timezone: %w", err)
			}
			log.Infow("got tenant local time",
				"timestamp", now,
				"creationDate", creationDate.Format("2006-01-02T15:04:05"),
				"zone", zone)

			tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
			if err != nil {
				return fmt.Errorf("cannot start read transaction: %w", err)
			}
			defer tx.Rollback()

			eventModule, err := eventmodule.FetchEventModule(ctx, eventID, tx)
			if err != nil {
				if err == sql.ErrNoRows {
					return eventNotFound(eventID)
				}

				return fmt.Errorf("getting event module: %w", err)
			}
			log.Debugw("found event module", "eventModuleID", eventModule.EventID)

			log.Debugw("getting attendee", "attendeeID", eventModule.EventID)
			ro, err := roomoccupancy.NewFromDatabase(ctx, tx, eventModule.EventModuleID)
			if err != nil {
				return fmt.Errorf("getting room occupancy: %w", err)
			}

			attendee := &dbEventModuleDelegate{}
			if err := getAttendee(ctx, tx, eventModule.EventModuleID, attendeeID, attendee); err != nil {
				if err.Error() == "sql: no rows in result set" {
					return attendeeNotFound(attendeeID)
				}
				return err
			}
			if err := validateRequestBody(requestBody, attendee); err != nil {
				return err
			}

			deletedRooms := map[int]roomOccupant{}

			//For each date between the first and last day, delete the corresponding room occupants
			firstNight := getDateWithoutTime(attendee.ArrivalDate)
			if requestBody.FirstNight != nil {
				firstNight = requestBody.FirstNight.Time
			}
			lastNight := getDateWithoutTime(attendee.DepartureDate).AddDate(0, 0, -1)
			for d := firstNight; !d.After(lastNight); d = d.AddDate(0, 0, 1) {
				for _, r := range attendee.ResidentialRoomOccupants {
					if !r.Date.Equal(d) {
						continue
					}
					if r.BedroomType.BookByRoom {
						return lambdawrapper.RequestError{
							Status:  http.StatusUnprocessableEntity,
							Message: fmt.Sprintf("room %d is a flat", r.ResidentialRoomID),
						}
					}
					if r.BedroomType.RoomInFlat {
						return lambdawrapper.RequestError{
							Status:  http.StatusUnprocessableEntity,
							Message: fmt.Sprintf("room %d is a room in a flat", r.ResidentialRoomID),
						}
					}

					if err := ro.DeleteRoomOccupant(r.ResidentialRoomID, attendee.PersonID, attendee.PersonSequenceID, d); err != nil {
						return err
					}
					deletedRooms[r.ResidentialRoomID] = r
					break

				}
			}
			//Persist occupants
			if err := ro.PersistRoomOccupants(ctx, tx); err != nil {
				return err
			}

			log.Debugw("comitting transaction")
			if err := tx.Commit(); err != nil {
				return err
			}

			log.Debugw("transaction committed")

			if len(deletedRooms) > 0 {
				auditMessage := []string{}
				for _, dr := range deletedRooms {
					auditMessage = append(auditMessage, fmt.Sprintf("Reservation for %s %s removed from room %s", attendee.Forename, attendee.Surname, dr.RoomName))
				}

				if err := publishAudit(lc, pub, eventID, userID, auditMessage); err != nil {
					// log the error, but allow the handler to succeed
					cause := errors.Cause(err)
					log.Warnw("Failed to publish audit record", "cause", cause)
				}
			}

			return lc.NoContent()
		})
	}
}

// eventNotFound returns a request error with a not found message and status code 404
func eventNotFound(id int) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: fmt.Sprintf("event %d not found", id),
	}
}

// parseAttendeeID parses the path parameters to get the attendee ID
func parseAttendeeID(params map[string]string) (int, error) {
	raw, ok := params[attendeeIDPathParam]
	if !ok {
		return 0, invalidAttendeeID("attendee id is required")
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, invalidAttendeeID("attendee id must be an integer")
	}

	if id < 1 {
		return 0, invalidAttendeeID("attendee id must be greater than 0")
	}

	return id, nil
}

// invalidAttendeeID returns a request error with a bad request message and status code 400
func invalidAttendeeID(msg string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusBadRequest,
		Message: "Bad Request",
		Fields:  []lambdawrapper.FieldError{{Field: attendeeIDPathParam, Error: msg}},
	}
}

// invalidEventID returns a request error with a bad request message and status code 400
func invalidEventID(msg string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusBadRequest,
		Message: "Bad Request",
		Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: msg}},
	}
}

// attendeeNotFound returns a request error with a not found message and status code 404
func attendeeNotFound(id int) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: fmt.Sprintf("attendee %d not found", id),
	}
}

func getDateWithoutTime(d time.Time) time.Time {
	return time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, d.Nanosecond(), d.Location())
}

func validateRequestBody(requestBody *requestBody, a *dbEventModuleDelegate) error {
	attendeeFirstNight := getDateWithoutTime(a.ArrivalDate)
	currentDate := getDateWithoutTime(time.Now())

	if requestBody.FirstNight != nil {
		if requestBody.FirstNight.Time.Before(attendeeFirstNight) || requestBody.FirstNight.Time.After(currentDate) {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "firstNight parameter is before arrival date or todays date",
			}
		}
	}

	return nil
}

//requestBody is the incoming request body
type requestBody struct {
	//FirstNight is the date from which to apply the changes to. Optional.
	FirstNight *date.Date `json:"firstNight"`
}

// Package getblocks gets a list of blocks for an event.
package getblocks

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getBlocks", func(ctx context.Context) error {
		query := &getBlocksQuery{}
		if err := parseQuery(lc.Request.QueryStringParameters, query); err != nil {
			return err
		}

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		rows := []block{}
		switch query.Scope {
		case RoomingListScope:
			if err := getRoomingListBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		case ReservationsScope:
			if err := getReservedRoomsBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		default:
			if err := getAllBlocks(ctx, tx, &rows, query); err != nil {
				return err
			}
		}

		return lc.JSON(http.StatusOK, apiResponse{Blocks: rows})
	})
}

func getAllBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]block, query *getBlocksQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT
		SB.BlockID,
		RTRIM(SB.Name) AS Name,
		SB.SiteID,
		SB.AreaID
	FROM SiteBlocks SB
	WHERE (@SiteID IS NULL OR SB.SiteID = @SiteID)
	AND (@AreaID IS NULL OR SB.AreaID = @AreaID)
	AND (@MasterBlockID IS NULL OR SB.MasterBlockID = @MasterBlockID)
	ORDER BY SB.SequenceNo, SB.Name`,
		sql.Named("SiteID", query.SiteID),
		sql.Named("AreaID", query.AreaID),
		sql.Named("MasterBlockID", query.MasterBlockID),
	)
}

func getRoomingListBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]block, query *getBlocksQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT 
		B.BlockID,
		RTRIM(B.Name) AS Name,
		B.SiteID,
		B.AreaID
	FROM EventModuleResidentialRoomAllocation EMRRA
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = EMRRA.ResidentialRoomID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID
	JOIN Sites S on S.SiteID = B.SiteID
	JOIN EventModules EM ON EM.EventModuleID = EMRRA.EventModuleID
	WHERE (@SiteID IS NULL OR B.SiteID = @SiteID)
	AND (@AreaID IS NULL OR B.AreaID = @AreaID)
	AND (@MasterBlockID IS NULL OR B.MasterBlockID = @MasterBlockID)
	AND EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID),
		sql.Named("SiteID", query.SiteID),
		sql.Named("AreaID", query.AreaID),
		sql.Named("MasterBlockID", query.MasterBlockID))
}

func getReservedRoomsBlocks(ctx context.Context, tx *sqlx.Tx, dest *[]block, query *getBlocksQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT 	
		B.BlockID,
		RTRIM(B.Name) AS Name,
		B.SiteID,
		B.AreaID
	FROM EventModuleBlockBedroomAllocation EMBBA
	JOIN SiteBlocks B ON B.BlockID = EMBBA.BlockID
	JOIN Sites S on S.SiteID = B.SiteID
	JOIN EventModules EM ON EM.EventModuleID = EMBBA.EventModuleID
	WHERE (@SiteID IS NULL OR B.SiteID = @SiteID)
	AND (@AreaID IS NULL OR B.AreaID = @AreaID)
	AND (@MasterBlockID IS NULL OR B.MasterBlockID = @MasterBlockID)
	AND EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID),
		sql.Named("SiteID", query.SiteID),
		sql.Named("AreaID", query.AreaID),
		sql.Named("MasterBlockID", query.MasterBlockID))
}

//parseQuery parses and validates the query string parameters into a struct
func parseQuery(queryParams map[string]string, dest *getBlocksQuery) error {
	scope, ok := queryParams["scope"]
	if ok {
		if scope != RoomingListScope && scope != ReservationsScope && scope != AllScope {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "scope", Error: "scope must be either roominglist, reservations, or all"}},
			}
		}
		dest.Scope = scope
	} else {
		//default to all
		dest.Scope = AllScope
	}

	eventID, ok := queryParams["eventID"]
	if ok {
		id, err := strconv.Atoi(eventID)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID must be an integer"}},
			}
		}
		dest.EventID = &id
	}

	if dest.Scope == RoomingListScope || dest.Scope == ReservationsScope {
		if dest.EventID == nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID is required if scope is set"}},
			}
		}
	}

	siteIdString, ok := queryParams["siteId"]
	if ok {
		siteId, err := strconv.Atoi(siteIdString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "siteId", Error: "siteId must be a number"}},
			}
		}

		dest.SiteID = &siteId
	}

	areaIdString, ok := queryParams["areaId"]
	if ok {
		areaId, err := strconv.Atoi(areaIdString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "areaId", Error: "areaId must be a number"}},
			}
		}

		dest.AreaID = &areaId
	}

	masterBlockIDString, ok := queryParams["masterBlockId"]
	if ok {
		masterBlockID, err := strconv.Atoi(masterBlockIDString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "masterBlockId", Error: "masterBlockId must be a number"}},
			}
		}

		dest.MasterBlockID = &masterBlockID
	}

	return nil
}

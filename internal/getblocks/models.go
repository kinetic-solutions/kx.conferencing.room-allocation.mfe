package getblocks

const (
	RoomingListScope  string = "roominglist"
	ReservationsScope string = "reservations"
	AllScope          string = "all"
)

type getBlocksQuery struct {
	EventID *int
	//oneof: roominglist, reservations, all - defaults to all
	Scope         string
	SiteID        *int
	AreaID        *int
	MasterBlockID *int
}

//the data retured to the API
type apiResponse struct {
	Blocks []block `json:"blocks"`
}

//the data returned from the database
type block struct {
	ID     int    `db:"BlockID" json:"id"`
	Name   string `db:"Name" json:"name"`
	SiteID int    `db:"SiteID" json:"-"`
	AreaID int    `db:"AreaID" json:"-"`
}

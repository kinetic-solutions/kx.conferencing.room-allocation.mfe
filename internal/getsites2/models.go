package getsites2

//the data retured to the API
type apiResponse struct {
	Sites []Site `json:"sites"`
}

type Site struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Areas []Area `json:"areas"`
}

type Area struct {
	AreaID      int    `json:"id"`
	Description string `json:"name"`
}

//the data returned from the database
type dbSite struct {
	ID   int    `db:"SiteID"`
	Name string `db:"Name"`
}

//the data returned from the database
type dbArea struct {
	ID     int    `db:"AreaID"`
	Name   string `db:"Description"`
	SiteID int    `db:"SiteID"`
}

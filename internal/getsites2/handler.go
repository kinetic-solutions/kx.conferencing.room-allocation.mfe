// Package getsites2 gets a list of sites for an event.
package getsites2

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	db := lc.DB

	return xray.Capture(lc.Context(), "roomAllocation.getSites2", func(ctx context.Context) error {
		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		// get sites from db
		dbSites := []dbSite{}
		err = tx.SelectContext(ctx, &dbSites, `SELECT SiteID, Name FROM Sites`)
		if err != nil {
			return err
		}

		// get areas from db
		dbAreas := []dbArea{}
		err = tx.SelectContext(ctx, &dbAreas, `SELECT AreaID, Description, SiteID FROM Areas`)
		if err != nil {
			return err
		}

		sites := []Site{}
		for _, s := range dbSites {
			site := Site{ID: s.ID, Name: s.Name}
			areas := []Area{}
			for _, a := range dbAreas {
				if a.SiteID == s.ID {
					area := Area{AreaID: a.ID, Description: a.Name}
					areas = append(areas, area)
				}
			}

			site.Areas = areas
			sites = append(sites, site)
		}

		return lc.JSON(http.StatusOK, apiResponse{Sites: sites})
	})
}

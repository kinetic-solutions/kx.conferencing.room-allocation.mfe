// Package allocateroom allocates an attendee to a room
package allocateroom

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/eventmodule"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/httpclient"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/roomoccupancy"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/roomreservations"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/usertokenhelper"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const eventIDPathParam = "eventID"
const attendeeIDPathParam = "attendeeID"
const roomIDPathParam = "roomID"

// Handler creates a lambda handler.
// It will use the given auditlog.Publisher to publish audit records.
func Handler(pub auditlog.Publisher, httpClient httpclient.HttpClient) func(lc *lambdawrapper.LambdaContext) error {
	return func(lc *lambdawrapper.LambdaContext) error {
		log, db := lc.Log, lc.DB

		return xray.Capture(lc.Context(), "roomAllocation.allocateRoom", func(ctx context.Context) error {
			log.Infow("parsing request")
			eventID, err := parseEventID(lc.Request.PathParameters)
			if err != nil {
				return err
			}

			attendeeID, err := parseAttendeeID(lc.Request.PathParameters)
			if err != nil {
				return err
			}

			roomID, err := parseRoomID(lc.Request.PathParameters)
			if err != nil {
				return err
			}

			var requestBody = &requestBody{}

			if len(lc.Request.Body) > 0 {
				log.Infow("parsing JSON request Body")
				if err := lambdawrapper.ParseJSON(lc.Request.Body, requestBody); err != nil {
					switch v := err.(type) {
					case lambdawrapper.RequestError:
						return err
					default:
						return lambdawrapper.RequestError{
							Status:  http.StatusBadRequest,
							Message: v.Error(),
						}
					}
				}
			}

			tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
			if err != nil {
				return fmt.Errorf("cannot start read transaction: %w", err)
			}
			defer tx.Rollback()

			em, err := eventmodule.FetchEventModule(ctx, eventID, tx)
			if err != nil {
				if err == sql.ErrNoRows {
					return notFound(fmt.Sprintf("event %d not found", eventID))
				}

				return fmt.Errorf("getting event module: %w", err)
			}

			attendee := attendee{}
			err = getAttendee(lc.Context(), tx, attendeeID, em, &attendee)
			if err != nil {
				if err.Error() == "sql: no rows in result set" {
					return notFound(fmt.Sprintf("attendee %d not found", attendeeID))
				}
				return err
			}

			room := &dbResidentialRoom{
				BedroomType: dbBedroomType{},
			}
			if err := getRoom(ctx, tx, roomID, room); err != nil {
				if err == sql.ErrNoRows {
					return notFound(fmt.Sprintf("room %d not found", roomID))
				}

				return fmt.Errorf("getting room: %w", err)
			}

			dbRoomOccupants := []dbRoomOccupant{}
			if err := getBookedOnDifferentEvent(ctx, tx, roomID, &dbRoomOccupants, em, &attendee); err != nil {
				return fmt.Errorf("getting room: %w", err)
			}
			if len(dbRoomOccupants) > 0 {
				field := "roomId"
				errCode := "4"
				msg := "this room is occupied by an attendee from another event"
				return unprocessableEntity(msg, field, errCode)
			}

			availabilityRestrictedRooms := []dbAvailabilityRestrictedRoom{}
			if err := getAvailabilityRestrictedRooms(ctx, tx, attendee.Arrival, attendee.Departure, roomID, &availabilityRestrictedRooms); err != nil {
				return fmt.Errorf("reading availability restricted rooms from database: %w", err)
			}

			if len(availabilityRestrictedRooms) > 0 {
				field := "roomId"
				errCode := "5"
				msg := "this room is has its availability restricted"
				return unprocessableEntity(msg, field, errCode)
			}

			allowDifferentAllocationOverBooking := false
			customSettings := []dbCustomSetting{}
			if err := tx.SelectContext(ctx, &customSettings, sqlGetAllowDifferentAllocationOverBooking); err != nil {
				if err != sql.ErrNoRows {
					return err
				}
			}
			if len(customSettings) > 0 {
				allowDifferentAllocationOverBooking = true
			}

			if !allowDifferentAllocationOverBooking {
				outOfServiceRooms := []dbOutOfServiceRoom{}
				if err := getOutOfServiceRooms(ctx, tx, attendee.Arrival, attendee.Departure, &outOfServiceRooms); err != nil {
					return fmt.Errorf("reading out of service rooms from database: %w", err)
				}

				for _, r := range outOfServiceRooms {
					if r.ResidentialRoomID == roomID {
						field := "roomId"
						errCode := "2"
						msg := "this room is out of service"
						return unprocessableEntity(msg, field, errCode)
					}
				}

				// this will be a list of allocations which are full based on block and bedroomtypes
				bedroomTypeSiteLimits := []dbBedroomTypeSiteLimits{}
				if err := getBedroomTypeSiteLimits(ctx, tx, attendee.Arrival, attendee.Departure, em, room, &bedroomTypeSiteLimits); err != nil {
					return fmt.Errorf("reading fully allocated rooms from database: %w", err)
				}
				if len(bedroomTypeSiteLimits) > 0 {
					field := "roomId"
					errCode := "3"
					msg := "this room's allocation is full"
					return unprocessableEntity(msg, field, errCode)
				}
			}

			reservations, err := roomreservations.NewFromDatabase(ctx, tx, em.EventModuleID)
			if err != nil {
				return fmt.Errorf("creating new reservations object: %w", err)
			}
			err = validateRequestBody(requestBody, &attendee)
			if err != nil {
				return err
			}

			firstNight := attendee.FirstNight()
			lastNight := attendee.LastNight()

			// If attendee has already checked in AND they have some rooms allocated, allocate more rooms from end of existing allocation
			// We need to also check if the checked in date is NOT a "germ null date"
			t, _ := time.Parse("2006-01-02", "1970-01-01")
			if attendee.CheckedInDate != nil && attendee.CheckedInDate.After(t) {
				if len(attendee.ResidentialRoomOccupants) > 0 {
					existingLastNight := getExistingLastNight(attendee.ResidentialRoomOccupants)
					firstNight = existingLastNight.AddDate(0, 0, 1)
				}
			}

			if requestBody.FirstNight != nil {
				firstNight = requestBody.FirstNight.Time
			}

			//roomoccupancy is a helper object that handles the business logic for editing room occupants
			ro, err := roomoccupancy.NewFromDatabase(ctx, tx, em.EventModuleID)
			if err != nil {
				return fmt.Errorf("getting new room occupancy object: %w", err)
			}
			//index by roomID so we can prevent duplicates
			deletedRooms := map[int]roomOccupant{}

			for d := firstNight; !d.After(lastNight); d = d.AddDate(0, 0, 1) {
				occupants, err := ro.GetRoomOccupants(roomID, d)
				if err != nil {
					return fmt.Errorf("getting occupants: %w", err)
				}

				//Check whether the room has enough capacity for a new attendee
				if occupants.Allocated >= room.Capacity {
					field := "roomId"
					errCode := "1"
					return unprocessableEntity("this room is full or already occupied", field, errCode)
				}

				//If the attendee already has a room allocation, then delete it
				for _, r := range attendee.ResidentialRoomOccupants {
					if !r.Date.Equal(d) {
						continue
					}

					if err := ro.DeleteRoomOccupant(r.ResidentialRoomID, attendee.PersonID, attendee.PersonSequenceID, d); err != nil {
						return fmt.Errorf("deleting room occupant: %w", err)
					}
					deletedRooms[r.ResidentialRoomID] = r
					break
				}

				//Add the attendee to the room
				if err := ro.AddRoomOccupant(roomID, attendee.PersonID, attendee.PersonSequenceID, d); err != nil {
					return fmt.Errorf("adding room occupant: %w", err)
				}
			}

			//Persist reservations
			if err := reservations.PersistRoomReservations(ctx, tx); err != nil {
				return err
			}
			//Persist occupants
			if err := ro.PersistRoomOccupants(ctx, tx); err != nil {
				return fmt.Errorf("persisting room occupants: %w", err)
			}

			//Update EventModuleDays
			if err := updateEventModuleDays(ctx, tx, em, firstNight, lastNight); err != nil {
				return fmt.Errorf("updating event module days: %w", err)
			}

			log.Debugw("comitting transaction")
			if err := tx.Commit(); err != nil {
				return err
			}

			log.Infow("parsing user")
			userID, err := usertokenhelper.ExtractUserId(lc)
			if err != nil {
				return lambdawrapper.RequestError{
					Status:  http.StatusBadRequest,
					Message: fmt.Sprintf("Failed to retrieve user id from token: %v", err),
				}
			}
			allocationAuditMessage := []string{
				fmt.Sprintf("Reservation for %s %s in room %s Added", attendee.Forename, attendee.Surname, room.RoomName),
			}
			if err := publishAudit(lc, pub, eventID, userID, allocationAuditMessage, "Room Allocated"); err != nil {
				// log the error, but allow the handler to succeed
				cause := errors.Cause(err)
				log.Warnw("Failed to publish allocate room audit record", "cause", cause)
			}

			//If any rooms were deleted, then publish an audit
			if len(deletedRooms) > 0 {
				auditMessage := []string{}
				for _, dr := range deletedRooms {
					auditMessage = append(auditMessage, fmt.Sprintf("Reservation for %s %s removed from room %s", attendee.Forename, attendee.Surname, dr.RoomName))
				}

				if err := publishAudit(lc, pub, eventID, userID, auditMessage, "Room Un-Allocated"); err != nil {
					// log the error, but allow the handler to succeed
					cause := errors.Cause(err)
					log.Warnw("Failed to publish de-allocate room audit record", "cause", cause)
				}
			}

			res, err := callback(lc, httpClient, eventID, roomID, attendee)
			if err != nil {
				// log the error, but allow the handler to succeed
				cause := errors.Cause(err)
				log.Warnw("Failed to make callback", "cause", cause)
			}
			statusOK := res.StatusCode >= 200 && res.StatusCode < 300
			if !statusOK {
				body, err := ioutil.ReadAll(res.Body)
				if err != nil {
					// log the error, but allow the handler to succeed
					cause := errors.Cause(err)
					log.Warnw("Failed to make callback", "cause", cause)
				}
				log.Warnw("callback received non-200 status code", "statusCode", res.StatusCode, "body", string(body[:]))
			}
			return lc.NoContent()
		})
	}
}

func callback(lc *lambdawrapper.LambdaContext, httpClient httpclient.HttpClient, eventID, roomID int, attendee attendee) (*http.Response, error) {
	tenantCode := lc.Tenancy().Code
	callbackURL := fmt.Sprintf("https://conferencing.api.kxcloud.net/%s/bedroom-event-handlers/handle-allocate-room", tenantCode)

	firstNight := attendee.FirstNight()
	lastNight := attendee.LastNight()
	callbackPayload, err := json.Marshal(map[string]interface{}{
		"eventID":    eventID,
		"attendeeID": attendee.Id,
		"roomID":     roomID,
		"firstNight": firstNight,
		"lastNight":  lastNight,
	})
	if err != nil {
		return nil, fmt.Errorf("marshalling callback payload: %w", err)
	}
	req, err := http.NewRequestWithContext(lc.Context(), "POST", callbackURL, bytes.NewBuffer(callbackPayload))
	if err != nil {
		return nil, fmt.Errorf("creating callback request: %w", err)
	}
	req.Header.Add("authorization", lc.Request.Headers["authorization"])
	req.Header.Set("content-type", "application/json")

	res, err := httpClient.Do(req)

	return res, err
}

// getOutOfServiceRooms gets the list of room ids that are out of service
func getOutOfServiceRooms(ctx context.Context, tx *sqlx.Tx, arrival time.Time, departure time.Time, dest *[]dbOutOfServiceRoom) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := arrival
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := departure.AddDate(0, 0, -1)

	err := tx.SelectContext(ctx, dest, sqlGetOutOfServiceRooms,
		sql.Named("firstNight", firstNight),
		sql.Named("lastNight", lastNight))

	return err
}

// getAvailabilityRestrictedRooms gets the list of room ids that are availability restricted
func getAvailabilityRestrictedRooms(ctx context.Context, tx *sqlx.Tx, arrival time.Time, departure time.Time, roomID int, dest *[]dbAvailabilityRestrictedRoom) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := getDateWithoutTime(arrival)
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := getDateWithoutTime(departure).AddDate(0, 0, -1)

	err := tx.SelectContext(ctx, dest, sqlAvailabilityRestrictedRooms,
		sql.Named("RoomID", roomID),
		sql.Named("FirstNight", firstNight),
		sql.Named("LastNight", lastNight))

	return err
}

// getBedroomTypeSiteLimits gets the list of full allocations
func getBedroomTypeSiteLimits(ctx context.Context, tx *sqlx.Tx, arrival time.Time, departure time.Time, em *eventmodule.EventModule, room *dbResidentialRoom, dest *[]dbBedroomTypeSiteLimits) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := getDateWithoutTime(arrival)
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := getDateWithoutTime(departure).AddDate(0, 0, -1)

	err := tx.SelectContext(ctx, dest, sqlViewBedroomTypeSiteLimits,
		sql.Named("EventModuleID", em.EventModuleID),
		sql.Named("BlockID", room.BlockID),
		sql.Named("BedroomTypeID", room.BedroomType.BedroomTypeID),
		sql.Named("FirstNight", firstNight),
		sql.Named("LastNight", lastNight))

	return err
}

// getRoom gets the room for the given room id
func getRoom(c context.Context, tx *sqlx.Tx, roomID int, dest *dbResidentialRoom) error {
	return xray.Capture(c, "attendees.getRooms", func(ctx context.Context) error {
		return tx.GetContext(ctx, dest, `
			SELECT
				RR.ResidentialRoomID,
				RR.Capacity,
				RR.BlockID,
				RR.Name,
				BT.BedroomTypeID  AS [BT.BedroomTypeID],
				BT.Description AS [BT.Description],
				BT.BookByRoom AS [BT.BookByRoom],
				BT.RoomInFlat AS [BT.RoomInFlat]
			FROM ResidentialRooms RR
			LEFT JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomtypeID
			WHERE RR.ResidentialRoomID = @ResidentialRoomID
		`, sql.Named("ResidentialRoomID", roomID))
	})
}

// getBookedOnDifferentEvent gets booked rooms from a different event with the given room id
func getBookedOnDifferentEvent(c context.Context, tx *sqlx.Tx, roomID int, dest *[]dbRoomOccupant, em *eventmodule.EventModule, a *attendee) error {
	firstNight := getDateWithoutTime(a.Arrival)
	lastNight := getDateWithoutTime(a.Departure).AddDate(0, 0, -1)

	return xray.Capture(c, "attendees.", func(ctx context.Context) error {
		return tx.SelectContext(ctx, dest, `
		SELECT ResidentialRoomID FROM ResidentialRoomOccupants
		WHERE ResidentialRoomID = @ResidentialRoomID
		AND EventModuleID <> @EventModuleID
		AND Date BETWEEN @FirstNight AND @LastNight
		AND PersonID > 0
`,
			sql.Named("ResidentialRoomID", roomID),
			sql.Named("EventModuleID", em.EventModuleID),
			sql.Named("FirstNight", firstNight),
			sql.Named("LastNight", lastNight),
		)
	})
}

//updateEventModuleDays syncs the event module days table with the reservations
//Paul Bedford: EventModuleDays is a link that was originally used to link catering days to an event but is also used for holding bedrooms daily figures (that are currently that I know of only used in one report!!)
func updateEventModuleDays(ctx context.Context, tx *sqlx.Tx, em *eventmodule.EventModule, firstNight, lastNight time.Time) error {
	return xray.Capture(ctx, "query.updateEventModuleDays", func(c context.Context) error {
		_, err := tx.ExecContext(ctx, sqlUpdateEventModuleDays,
			sql.Named("EventModuleID", em.EventModuleID),
			sql.Named("FirstNight", firstNight),
			sql.Named("LastNight", lastNight),
		)
		return err
	})
}

func getAttendee(ctx context.Context, tx *sqlx.Tx, id int, em *eventmodule.EventModule, a *attendee) error {
	return xray.Capture(ctx, "query.getEventModuleDelegate", func(c context.Context) error {
		if err := tx.GetContext(ctx, a, sqlGetEventModuleDelegate,
			sql.Named("EventModuleDelegateID", id),
			sql.Named("EventModuleID", em.EventModuleID),
		); err != nil {
			return err
		}
		err :=
			tx.SelectContext(ctx, &a.ResidentialRoomOccupants, sqlGetRoomOccupants,
				sql.Named("PersonID", a.PersonID),
				sql.Named("PersonSequenceID", a.PersonSequenceID),
				sql.Named("EventModuleID", em.EventModuleID),
			)
		return err
	})
}

// badRequest returns a bad request error
func badRequest(msg, field string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusBadRequest,
		Message: "Bad Request",
		Fields:  []lambdawrapper.FieldError{{Field: field, Error: msg}},
	}
}

// notFound returns a not found request error
func notFound(msg string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: msg,
	}
}

// unprocessable entity returns a request error with a not found message and status code 404
func unprocessableEntity(message string, field string, errCode string) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusUnprocessableEntity,
		Message: message,
		Fields: []lambdawrapper.FieldError{
			{Field: field, Error: errCode},
		},
	}
}

// parseEventID parses the path parameters to get the event ID
func parseEventID(params map[string]string) (int, error) {
	raw, ok := params[eventIDPathParam]
	if !ok {
		return 0, badRequest("event id is required", eventIDPathParam)
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, badRequest("event id must be an integer", eventIDPathParam)
	}

	if id < 1 {
		return 0, badRequest("event id must be greater than 0", eventIDPathParam)
	}

	return id, nil
}

// parseAttendeeID parses the path parameters to get the attendee ID
func parseAttendeeID(params map[string]string) (int, error) {
	raw, ok := params[attendeeIDPathParam]
	if !ok {
		return 0, badRequest("attendee id is required", attendeeIDPathParam)
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, badRequest("attendee id must be an integer", attendeeIDPathParam)
	}

	if id < 1 {
		return 0, badRequest("attendee id must be greater than 0", attendeeIDPathParam)
	}

	return id, nil
}

// parseRoomID parses the path parameters to get the attendee ID
func parseRoomID(params map[string]string) (int, error) {
	raw, ok := params[roomIDPathParam]
	if !ok {
		return 0, badRequest("room id is required", roomIDPathParam)
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, badRequest("room id must be an integer", roomIDPathParam)
	}

	if id < 1 {
		return 0, badRequest("room id must be greater than 0", roomIDPathParam)
	}

	return id, nil
}

func getDateWithoutTime(d time.Time) time.Time {
	return time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, d.Nanosecond(), d.Location())
}

// check for validation between request body first night and attendee arival
func validateRequestBody(requestBody *requestBody, a *attendee) error {
	attendeeArrival := getDateWithoutTime(a.Arrival)
	currentDate := getDateWithoutTime(time.Now())

	if requestBody.FirstNight != nil {
		if requestBody.FirstNight.Time.Before(attendeeArrival) || requestBody.FirstNight.Time.After(currentDate) {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "firstNight parameter is before arrival date or todays date",
			}
		}
	}
	return nil

}

// finds latest night of attendee's stay in their currently allocated room
func getExistingLastNight(roomOccupants []roomOccupant) time.Time {
	var latestNight time.Time
	for _, v := range roomOccupants {
		if v.Date.Time.After(latestNight) {
			latestNight = v.Date.Time
		}
	}

	return latestNight
}

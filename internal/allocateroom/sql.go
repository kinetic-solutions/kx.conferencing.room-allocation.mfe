package allocateroom

const (
	sqlGetAllowDifferentAllocationOverBooking = `
		SELECT Value FROM CustomSettings WHERE CustomSetting='AllowDifferentAllocationOverBooking'
	`

	sqlGetEventModuleDelegate = `
		SELECT
			EMD.EventModuleDelegateID,
			EMD.PersonID,
			EMD.PersonSequenceID,
			EMD.ArrivalDate,
			EMD.DepartureDate,
			EMD.CheckedInDate,
			ISNULL(P.Forename, '') AS Forename,
			ISNULL(P.Surname, '') AS Surname
		FROM EventModuleDelegates EMD
		JOIN People P ON P.PersonID = EMD.PersonID
		WHERE EventModuleDelegateID = @EventModuleDelegateID
		AND EMD.EventModuleID = @EventModuleID
	`

	sqlGetRoomOccupants = `
	SELECT 
		RRO.ResidentialRoomID,
		RRO.Date, 
		RR.Capacity,
		RR.BlockID,
		RR.Name,
		BT.BedroomTypeID  AS [BT.BedroomTypeID],
		BT.Description AS [BT.Description],
		BT.BookByRoom AS [BT.BookByRoom],
		BT.RoomInFlat AS [BT.RoomInFlat]
	FROM ResidentialRoomOccupants RRO 
    JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomtypeID
	WHERE 
	PersonID = @PersonID AND PersonSequenceID = @PersonSequenceID AND EventModuleID = @EventModuleID
	ORDER BY RRO.Date ASC`

	sqlUpdateEventModuleDays = `
	UPDATE EMD
	SET Beds = (SELECT SUM(EMBBA.Allocation) FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = @EventModuleID AND EMBBA.Date = EMD.Date)
	FROM EventModuleDays EMD
	WHERE EventModuleID = @EventModuleID and Date BETWEEN @FirstNight AND @LastNight
	`

	sqlGetOutOfServiceRooms = `
		SELECT	DISTINCT RRR.ResidentialRoomID
		from	ResidentialRoomRestrictions rrr
		join	ResidentialRoomRestrictionheader rrrh on rrrh.residentialroomrestrictionid =  rrr.residentialroomrestrictionid
		join	ResidentialRooms RR ON RR.ResidentialRoomID = RRR.ResidentialRoomID
		WHERE	enddate >= @FirstNight AND startdate <= @LastNight
	`

	sqlAvailabilityRestrictedRooms = `
		SELECT ResidentialRoomID
		FROM ViewResidentialRoomAvailabilityRestrictions
		WHERE ResidentialRoomID = @RoomID
		AND FromDate <= @LastNight
		AND ToDate >= @FirstNight
		AND Commercial = 1
	`

	sqlViewBedroomTypeSiteLimits = `
	SELECT	DISTINCT BTSL.BlockID, BTSL.BedroomTypeID
	FROM	ViewBedroomTypeSiteLimits BTSL
	JOIN	(
		SELECT	BlockID,
			BedroomTypeID,
			SUM(Allocation) as Allocation,
			Date
		FROM	(
			SELECT	BlockID,
				BedroomTypeID,
				SUM(Allocation) as Allocation,
				Date
			FROM
				EventModuleBlockBedroomAllocation
			WHERE	EventModuleID <> @EventModuleID
			AND Date BETWEEN @FirstNight AND @LastNight
			AND	EventModuleID <> -99
			GROUP BY
				BlockID, BedroomTypeID, Date
			UNION ALL
			SELECT	rr.BlockID,
				BedroomTypeID,
				1,
				Date
			from	ResidentialRoomRestrictions rrr
			join	ResidentialRoomRestrictionheader rrrh on rrrh.residentialroomrestrictionid =  rrr.residentialroomrestrictionid
			join	ResidentialRooms RR ON RR.ResidentialRoomID = RRR.ResidentialRoomID
			JOIN	Dates D ON D.Date BETWEEN StartDate AND ENDDATE
			WHERE	enddate >= @FirstNight AND startdate <= @LastNight
			UNION ALL
			SELECT	ASRT.BlockID, ASRT.BedroomTypeID, ASDQ.CurrentRoomQuantity, DayDate
			FROM	AllocationSets AA
			JOIN	AllocationSetRoomTypes ASRT ON ASRT.AllocationSetID = AA.AllocationSetID
			JOIN	AllocationSetStatuses S ON S.StatusID = AA.StatusID
			JOIN	AllocationSetDayQuantities ASDQ ON ASRT.AllocationSetRoomTypeID = ASDQ.AllocationSetRoomTypeID
			JOIN	AllocationCompanies AC ON AC.AllocationSetID = AA.AllocationSetID
			WHERE	AA.enddate >= @FirstNight AND AA.startdate <= @LastNight
			AND	DayDate BETWEEN @FirstNight AND @LastNight
			AND	S.Active = 1
			AND	NOT EXISTS(SELECT NULL FROM EventModules EM JOIN EventCompanies EC ON EM.EventID = EC.EventID AND EC.CompanyID = AC.CompanyID WHERE EM.EventModuleID = @EventModuleID)
		) R
 		GROUP BY
			BlockID,
			BedroomTypeID,
			DATE
		) BB ON BB.BlockID = BTSL.BlockID and BTSL.BedroomTypeID = BB.BedroomTypeID
	WHERE	BB.Allocation >= BTSL.BedroomCount
	AND BTSL.BlockID = @BlockID AND BTSL.BedroomTypeID = @BedroomTypeID
	`
)

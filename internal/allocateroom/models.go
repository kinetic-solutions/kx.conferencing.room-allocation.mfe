package allocateroom

import (
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/date"
)

type dbOutOfServiceRoom struct {
	ResidentialRoomID int `db:"ResidentialRoomID"`
}

type dbAvailabilityRestrictedRoom struct {
	ResidentialRoomID int `db:"ResidentialRoomID"`
}

type dbBedroomTypeSiteLimits struct {
	BlockID       int `db:"BlockID"`
	BedroomTypeID int `db:"BedroomTypeID"`
}

type dbCustomSetting struct {
	Value string `db:"Value"`
}

type attendee struct {
	Id                       int       `db:"EventModuleDelegateID"`
	PersonID                 int       `db:"PersonID"`
	PersonSequenceID         int       `db:"PersonSequenceID"`
	Forename                 string    `db:"Forename"`
	Surname                  string    `db:"Surname"`
	Arrival                  time.Time `db:"ArrivalDate"`
	Departure                time.Time `db:"DepartureDate"`
	ResidentialRoomOccupants []roomOccupant
	CheckedInDate            *time.Time `db:"CheckedInDate"`
}

//FirstNight is the first night the attendee is staying
func (a *attendee) FirstNight() time.Time {
	return getDateWithoutTime(a.Arrival)
}

//LastNight is the last night the attendee is staying
func (a *attendee) LastNight() time.Time {
	return getDateWithoutTime(a.Departure).AddDate(0, 0, -1)
}

type roomOccupant struct {
	ResidentialRoomID int           `db:"ResidentialRoomID"`
	Date              date.Date     `db:"Date"`
	Capacity          int           `db:"Capacity"`
	RoomName          string        `db:"Name"`
	BedroomType       dbBedroomType `db:"BT"`
	BlockID           int           `db:"BlockID"`
}

type dbBedroomType struct {
	BedroomTypeID int    `db:"BedroomTypeID"`
	Description   string `db:"Description"`
	BookByRoom    bool   `db:"BookByRoom"`
	RoomInFlat    bool   `db:"RoomInFlat"`
}
type dbResidentialRoom struct {
	ResidentialRoomID int           `db:"ResidentialRoomID"`
	Capacity          int           `db:"Capacity"`
	RoomName          string        `db:"Name"`
	BedroomType       dbBedroomType `db:"BT"`
	BlockID           int           `db:"BlockID"`
}

type dbRoomOccupant struct {
	ResidentialRoomID int `db:"ResidentialRoomID"`
}
type requestBody struct {
	FirstNight *date.Date `json:"firstNight"`
}

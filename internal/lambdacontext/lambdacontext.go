package lambdacontext

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
)

type RoomAllocationLambdaContext struct {
	*lambdawrapper.LambdaContext
}

func (lc *RoomAllocationLambdaContext) GetEventID() (int, error) {
	raw, ok := lc.Request.PathParameters["eventID"]
	if !ok {
		return 0, errors.New("event id is required")
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, errors.New("event id must be an integer")
	}

	if id < 1 {
		return 0, errors.New("event id must be greater than 0")
	}

	return id, nil
}

func (lc *RoomAllocationLambdaContext) GetTenantRequestTime() (*string, *time.Time, *time.Time, error) {
	timeZone := lc.Tenancy().TimeZone
	loc, err := time.LoadLocation(timeZone)
	if err != nil {
		return nil, nil, nil, err
	}

	millis := lc.Request.RequestContext.TimeEpoch
	now := time.Unix(0, millis*int64(time.Millisecond))
	zone, modifiedDate := toLocal(&now, loc)
	return &zone, &now, &modifiedDate, nil
}

func toLocal(t *time.Time, loc *time.Location) (string, time.Time) {
	abbrev, offset := t.In(loc).Zone()
	t2 := t.Add(time.Second * time.Duration(offset)).In(time.UTC)
	return abbrev, t2
}

// ParseJSONBody parses the request body returning an error if the
// body isn't valid JSON or if the passed JSON is invalid.
func (lc *RoomAllocationLambdaContext) ParseJSONBody(body string, out interface{}) ([]lambdawrapper.FieldError, error) {
	err := json.Unmarshal([]byte(body), out)

	if err != nil {
		switch v := err.(type) {
		case *json.UnmarshalTypeError:
			return []lambdawrapper.FieldError{
				{Field: v.Field, Error: fmt.Sprintf("expected %s; got %s", v.Type.Name(), v.Value)},
			}, nil
		default:
			return []lambdawrapper.FieldError{}, v
		}
	}

	return nil, nil
}

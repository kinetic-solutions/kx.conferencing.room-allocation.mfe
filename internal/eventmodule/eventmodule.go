package eventmodule

import (
	"context"
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
)

type EventModule struct {
	EventID       int       `db:"EventID"`
	EventModuleID int       `db:"EventModuleID"`
	Arrival       time.Time `db:"ArrivalDate"`
	Departure     time.Time `db:"DepartureDate"`
}

// fetchEventModule gets the event module from the database
func FetchEventModule(ctx context.Context, eventID int, tx *sqlx.Tx) (*EventModule, error) {
	em := &EventModule{}
	err := tx.GetContext(ctx, em, "SELECT EventModuleID, EventID, ArrivalDate, DepartureDate FROM EventModules WHERE EventID = @eventID", sql.Named("eventID", eventID))
	return em, err
}

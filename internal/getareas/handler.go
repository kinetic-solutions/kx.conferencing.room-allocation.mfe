// Package getareas gets a list of areas for a site
package getareas

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getAreas", func(ctx context.Context) error {
		query := query{}
		if err := parseQuery(lc.Request.QueryStringParameters, &query); err != nil {
			return err
		}

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		rows := []dbRow{}
		switch query.Scope {
		case RoomingListScope:
			if err := getRoomingListAreas(ctx, tx, &rows, &query); err != nil {
				return err
			}
		case ReservationsScope:
			if err := getAreasWithReservations(ctx, tx, &rows, &query); err != nil {
				return err
			}
		default:
			if err := getAllAreas(ctx, tx, &rows, &query); err != nil {
				return err
			}
		}

		return lc.JSON(http.StatusOK, apiResponse{Areas: rows})
	})
}

func getAreasWithReservations(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *query) error {
	return tx.SelectContext(ctx, dest, `
		SELECT DISTINCT A.AreaID, A.Description, A.DisplayOrder
		FROM EventModuleBlockBedroomAllocation EMBBA
		JOIN SiteBlocks B ON B.BlockID = EMBBA.BlockID
		JOIN Areas A on A.AreaID = B.AreaID
		WHERE B.SiteID = @SiteID`,
		sql.Named("SiteID", query.SiteID))
}

func getAllAreas(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *query) error {
	return tx.SelectContext(ctx, dest, `
		SELECT A.AreaID, RTRIM(A.Description) AS Description, A.DisplayOrder
		FROM Areas A
		WHERE A.SiteID=@SiteID
		ORDER BY A.DisplayOrder, Description`,
		sql.Named("SiteID", *query.SiteID),
	)
}

func getRoomingListAreas(ctx context.Context, tx *sqlx.Tx, dest *[]dbRow, query *query) error {
	return tx.SelectContext(ctx, dest, `
		SELECT DISTINCT A.AreaID, RTRIM(A.Description) AS Description, A.DisplayOrder
		FROM Areas A
		JOIN SiteBlocks SB ON SB.AreaID = A.AreaID
		JOIN ResidentialRooms RR ON RR.BlockID = SB.BlockID
		WHERE A.SiteID=@SiteID
		ORDER BY A.DisplayOrder, Description`,
		sql.Named("SiteID", *query.SiteID),
	)
}

//parseQuery parses and validates the query string parameters into a struct
func parseQuery(queryParams map[string]string, dest *query) error {
	siteIdString, ok := queryParams["siteId"]
	if ok {
		siteId, err := strconv.Atoi(siteIdString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "siteId", Error: "siteId must be a number"}},
			}
		}

		dest.SiteID = &siteId
	} else {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "siteId", Error: "siteId is required"}},
		}
	}

	scope, ok := queryParams["scope"]
	if ok {
		if scope != RoomingListScope && scope != ReservationsScope && scope != AllScope {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "scope", Error: "scope must be either roominglist, reservations, or all"}},
			}
		}
		dest.Scope = scope
	} else {
		//default to all
		dest.Scope = AllScope
	}

	return nil
}

package getareas

const (
	RoomingListScope  string = "roominglist"
	ReservationsScope string = "reservations"
	AllScope          string = "all"
)

type query struct {
	SiteID *int

	//oneof: roominglist, reservations, all - defaults to all
	Scope string
}

//the data retured to the API
type apiResponse struct {
	Areas []dbRow `json:"areas"`
}

//the data returned from the database
type dbRow struct {
	ID           int    `db:"AreaID" json:"id"`
	Name         string `db:"Description" json:"name"`
	DisplayOrder string `db:"DisplayOrder" json:"-"`
}

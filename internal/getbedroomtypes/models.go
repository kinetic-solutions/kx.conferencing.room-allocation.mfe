package getbedroomtypes

const (
	RoomingListScope  string = "roominglist"
	ReservationsScope string = "reservations"
	AllScope          string = "all"
)

//the data retured to the API
type apiResponse struct {
	BedroomTypes []bedroomType `json:"bedroomTypes"`
}

type getBedroomTypesQuery struct {
	EventID *int
	//oneof: roominglist, reservations, all - defaults to all
	Scope string
}

//the data returned from the database
type bedroomType struct {
	ID   int    `db:"BedroomTypeID" json:"id"`
	Name string `db:"Description" json:"name"`
}

// Package getsubblocks gets a list of subblocks for a masterBlockId
package getbedroomtypes

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getBedroomTypes", func(ctx context.Context) error {
		query := &getBedroomTypesQuery{}
		if err := parseQuery(lc.Request.QueryStringParameters, query); err != nil {
			return err
		}

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		rows := []bedroomType{}
		switch query.Scope {
		case RoomingListScope:
			if err := getRoomingListBedroomTypes(ctx, tx, &rows, query); err != nil {
				return err
			}
		case ReservationsScope:
			if err := getReservedRoomsBedroomTypes(ctx, tx, &rows, query); err != nil {
				return err
			}
		default:
			if err := getAllBedroomTypes(ctx, tx, &rows, query); err != nil {
				return err
			}
		}

		return lc.JSON(http.StatusOK, apiResponse{BedroomTypes: rows})
	})
}

func getAllBedroomTypes(ctx context.Context, tx *sqlx.Tx, dest *[]bedroomType, query *getBedroomTypesQuery) error {
	return tx.SelectContext(ctx, dest, `
		SELECT
			BT.BedroomTypeID,
			RTRIM(BT.Description) AS Description
		FROM BedroomTypes BT`,
	)
}

func getRoomingListBedroomTypes(ctx context.Context, tx *sqlx.Tx, dest *[]bedroomType, query *getBedroomTypesQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT 
		BT.BedroomTypeID,
		RTRIM(BT.Description) AS Description
	FROM EventModuleResidentialRoomAllocation EMRRA
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = EMRRA.ResidentialRoomID
	JOIN BedroomTypes BT ON BT.BedroomTypeID = RR.BedroomTypeID
	JOIN EventModules EM ON EM.EventModuleID = EMRRA.EventModuleID
	WHERE EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID))
}

func getReservedRoomsBedroomTypes(ctx context.Context, tx *sqlx.Tx, dest *[]bedroomType, query *getBedroomTypesQuery) error {
	return tx.SelectContext(ctx, dest, `
	SELECT DISTINCT 
		BT.BedroomTypeID,
		RTRIM(BT.Description) AS Description
	FROM EventModuleBlockBedroomAllocation EMBBA
	JOIN BedroomTypes BT ON BT.BedroomTypeID = EMBBA.BedroomTypeID
	JOIN EventModules EM ON EM.EventModuleID = EMBBA.EventModuleID
	WHERE EM.EventID = @EventID`,
		sql.Named("EventID", query.EventID))
}

//parseQuery parses and validates the query string parameters into a struct
func parseQuery(queryParams map[string]string, dest *getBedroomTypesQuery) error {
	scope, ok := queryParams["scope"]
	if ok {
		if scope != RoomingListScope && scope != ReservationsScope && scope != AllScope {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "scope", Error: "scope must be either roominglist, reservations, or all"}},
			}
		}
		dest.Scope = scope
	} else {
		//default to all
		dest.Scope = AllScope
	}

	eventID, ok := queryParams["eventID"]
	if ok {
		id, err := strconv.Atoi(eventID)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID must be an integer"}},
			}
		}
		dest.EventID = &id
	}

	if dest.Scope == RoomingListScope || dest.Scope == ReservationsScope {
		if dest.EventID == nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: "eventID is required if scope is set"}},
			}
		}
	}

	return nil
}

// Package date implements a serializable date without any timezone or time part.
// It supports reading and writing to SQL, JSON, or text.
package date

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

// layout is the time format string that Date uses.
const layout = "2006-01-02"

// Date is a date with no time part that implements JSON and text marshalling,
// as well as reading from and writing to SQL.
type Date struct {
	time.Time
}

var (
	_ fmt.Stringer             = Date{}
	_ sql.Scanner              = new(Date)
	_ driver.Valuer            = Date{}
	_ json.Unmarshaler         = new(Date)
	_ json.Marshaler           = Date{}
	_ encoding.TextUnmarshaler = new(Date)
	_ encoding.TextMarshaler   = Date{}
)

// Must parses the given string as a Date.
// It assumes that the format is correct, and will panic if not.
func Must(text string) Date {
	var d Date
	if err := d.UnmarshalText([]byte(text)); err != nil {
		panic(err)
	}
	return d
}

// String implements fmt.Stringer.
func (d Date) String() string {
	return d.Format(layout)
}

// MarshalJSON implements json.Marshaller.
// MarshalJSON never returns an error.
func (d Date) MarshalJSON() ([]byte, error) {
	if d.Time.IsZero() {
		return []byte("null"), nil
	}

	b, _ := d.MarshalText()
	b = append([]byte{'"'}, b...)
	b = append(b, '"')
	return b, nil
}

// UnmarshalJSON implements json.Unmarshaller.
func (d *Date) UnmarshalJSON(b []byte) (err error) {
	return d.UnmarshalText(bytes.Trim(b, `"`))
}

// MarshalText implements encoding.TextMarshaller.
// MarshalText never returns an error.
func (d Date) MarshalText() ([]byte, error) {
	return []byte(d.Format(layout)), nil
}

// UnmarshalText implements encoding.TextUnmarshaller.
func (d *Date) UnmarshalText(text []byte) error {
	t, err := time.Parse(layout, string(text))
	if err != nil {
		return errors.New("date format must be yyyy-mm-dd")
	}

	d.Time = t
	return nil
}

// Scan implements sql.Scanner.
func (d *Date) Scan(src interface{}) (err error) {
	switch src := src.(type) {
	case string:
		err = d.UnmarshalText([]byte(src))
	case []byte:
		err = d.UnmarshalText(src)
	case time.Time:
		if src.Location() != time.UTC {
			return errors.New("src contains location information")
		}
		if !src.Equal(src.Truncate(24 * time.Hour)) {
			return errors.New("src contains greater precision than Date")
		}
		d.Time = src
	default:
		err = errors.New("scan: incompatible type for Date")
	}
	return
}

// Value implements driver.Valuer.
func (d Date) Value() (driver.Value, error) {
	return d.Time, nil
}

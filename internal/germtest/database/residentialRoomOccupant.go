package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type ResidentialRoomOccupant struct {
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	EventModuleID     int       `db:"EventModuleID"`
	PersonID          int       `db:"PersonID"`
	PersonSequenceID  int       `db:"PersonSequenceID"`
	Date              time.Time `db:"Date"`
}

func (rro *ResidentialRoomOccupant) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	INSERT INTO ResidentialRoomOccupants
		(ResidentialRoomID,		EventModuleID,	PersonID,	Date,	PersonSequenceID,	BedIndex,	PaidDeposit,	SingleOccupant) VALUES
		(:ResidentialRoomID,	:EventModuleID,	:PersonID,	:Date,	:PersonSequenceID,	0,			0,				0             )
	`
	_, eventErr := db.NamedExecContext(ctx, query, rro)
	if eventErr != nil {
		return fmt.Errorf("persisting residential room occupant: %w", eventErr)
	}

	return nil
}

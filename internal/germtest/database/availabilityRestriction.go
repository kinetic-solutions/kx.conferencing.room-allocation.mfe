package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type AvailabilityRestriction struct {
	AvailabilityRestrictionID int       `db:"AvailabilityRestrictionID"`
	EventBusinessTypeID       int       `db:"EventBusinessTypeID"`
	BlockID                   int       `db:"BlockID"`
	BedroomTypeID             int       `db:"BedroomTypeID"`
	FromDate                  time.Time `db:"FromDate"`
	ToDate                    time.Time `db:"ToDate"`
	Notes                     string    `db:"Notes"`
}

func (rra *AvailabilityRestriction) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AvailabilityRestrictions ON
      INSERT INTO AvailabilityRestrictions
	  (AvailabilityRestrictionID, EventBusinessTypeID, BlockID, BedroomTypeID, FromDate, ToDate, Notes) VALUES
	  (:AvailabilityRestrictionID, :EventBusinessTypeID, :BlockID, :BedroomTypeID, :FromDate, :ToDate, :Notes)
    SET IDENTITY_INSERT AvailabilityRestrictions OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, rra)
	if eventErr != nil {
		return fmt.Errorf("persisting availability restrictions: %w", eventErr)
	}

	return nil
}

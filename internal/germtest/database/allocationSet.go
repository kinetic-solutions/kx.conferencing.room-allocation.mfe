package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type AllocationSet struct {
	AllocationSetID int       `db:"AllocationSetID"`
	Name            string    `db:"Name"`
	SalesTypeID     int       `db:"SalesTypeID"`
	StartDate       time.Time `db:"StartDate"`
	EndDate         time.Time `db:"EndDate"`
	CreationUserID  int       `db:"CreationUserID"`
	CreationDate    time.Time `db:"CreationDate"`
	StatusID        int       `db:"StatusID"`
	ReleaseStyleID  int       `db:"ReleaseStyleID"`
}

func (as *AllocationSet) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationSets ON
      INSERT INTO AllocationSets
	  (AllocationSetID, Name, SalesTypeID, StartDate, EndDate, CreationUserID, CreationDate, StatusID, ReleaseStyleID) VALUES
	  (:AllocationSetID, :Name, :SalesTypeID, :StartDate, :EndDate, :CreationUserID, :CreationDate, :StatusID, :ReleaseStyleID)
    SET IDENTITY_INSERT AllocationSets OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, as)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation sets: %w", eventErr)
	}

	return nil
}

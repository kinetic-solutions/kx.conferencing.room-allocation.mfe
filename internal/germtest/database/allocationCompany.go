package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type AllocationCompany struct {
	AllocationCompanyID int `db:"AllocationCompanyID"`
	AllocationSetID     int `db:"AllocationSetID"`
	CompanyID           int `db:"CompanyID"`
}

func (ac *AllocationCompany) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationCompanies ON
      INSERT INTO AllocationCompanies
	  (AllocationCompanyID, AllocationSetID, CompanyID) VALUES
	  (:AllocationCompanyID, :AllocationSetID, :CompanyID)
    SET IDENTITY_INSERT AllocationCompanies OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, ac)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation companies: %w", eventErr)
	}

	return nil
}

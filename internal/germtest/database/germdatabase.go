package database

import (
	"context"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func DefaultDate() time.Time {
	return time.Date(2021, 10, 1, 12, 0, 0, 0, time.UTC)
}

type GermDatabase struct {
	events                                  []Event
	eventModules                            []EventModule
	people                                  []Person
	sites                                   []Site
	areas                                   []Area
	siteBlocks                              []SiteBlock
	bedroomTypes                            []BedroomType
	residentialRooms                        []ResidentialRoom
	residentialRoomAllocations              []ResidentialRoomAllocation
	residentialRoomOccupants                []ResidentialRoomOccupant
	eventModuleDelegates                    []EventModuleDelegate
	eventModuleBlockBedroomAllocation       []EventModuleBlockBedroomAllocation
	availabilityRestrictions                []AvailabilityRestriction
	availabilityRestrictionResidentialRooms []AvailabilityRestrictionResidentialRoom
	control                                 []Control
	residentialRoomRestrictions             []ResidentialRoomRestriction
	residentialRoomRestrictionHeaders       []ResidentialRoomRestrictionHeader
	allocationSetDayQuantities              []AllocationSetDayQuantity
	allocationSets                          []AllocationSet
	allocationSetRoomTypes                  []AllocationSetRoomType
	allocationSetStatuses                   []AllocationSetStatus
	allocationCompanies                     []AllocationCompany
	allocationReleaseStyles                 []AllocationReleaseStyle
	customSettings                          []CustomSetting
}

func New() *GermDatabase {
	return &GermDatabase{}
}

func (g *GermDatabase) Reset(ctx context.Context, db *sqlx.DB) error {
	query := `
		DELETE FROM [EventModuleDelegates];
		DELETE FROM [ResidentialRoomOccupants]
		DELETE FROM [People];
		DELETE FROM [EventModuleResidentialRoomAllocation];
		DELETE FROM [EventModuleBlockBedroomAllocation];
		DELETE FROM [ResidentialRooms];
		DELETE FROM [BedroomTypes];
		DELETE FROM [SiteBlocks];
		DELETE FROM [Sites];
		DELETE FROM [Areas];
		DELETE FROM [Events];
		DELETE FROM [EventModules];
		DELETE FROM [AvailabilityRestrictions];
		DELETE FROM [Control];
		DELETE FROM [ResidentialRoomRestrictions];
		DELETE FROM [ResidentialRoomRestrictionHeader];
		DELETE FROM [CustomSettings];
		DELETE FROM [AllocationSetDayQuantities];
		DELETE FROM [AllocationCompanies];
		DELETE FROM [AllocationSetRoomTypes];
		DELETE FROM [AllocationSets];
		DELETE FROM [AllocationSetStatuses];
		DELETE FROM [AllocationReleaseStyles];
		DBCC CHECKIDENT ('[EventModuleBlockBedroomAllocation]', RESEED, 0);
	`
	_, err := db.ExecContext(ctx, query)
	return err
}

func (g *GermDatabase) Persist(ctx context.Context, db *sqlx.DB) error {
	if err := g.Reset(ctx, db); err != nil {
		return err
	}
	for _, e := range g.events {
		if err := e.Persist(ctx, db); err != nil {
			return err
		}
	}

	for _, em := range g.eventModules {
		if err := em.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, p := range g.people {
		if err := p.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, bt := range g.bedroomTypes {
		if err := bt.Persist(ctx, db); err != nil {
			return err
		}
	}

	for _, s := range g.sites {
		if err := s.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, a := range g.areas {
		if err := a.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, sb := range g.siteBlocks {
		if err := sb.Persist(ctx, db); err != nil {
			return err
		}
	}

	for _, rr := range g.residentialRooms {
		if err := rr.Persist(ctx, db); err != nil {
			return err
		}
	}

	for _, rra := range g.residentialRoomAllocations {
		if err := rra.Persist(ctx, db); err != nil {
			return err
		}
	}

	for _, rro := range g.residentialRoomOccupants {
		if err := rro.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, emd := range g.eventModuleDelegates {
		if err := emd.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, embba := range g.eventModuleBlockBedroomAllocation {
		if err := embba.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, ar := range g.availabilityRestrictions {
		if err := ar.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, arrr := range g.availabilityRestrictionResidentialRooms {
		if err := arrr.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, c := range g.control {
		if err := c.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, rrr := range g.residentialRoomRestrictions {
		if err := rrr.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, rrrh := range g.residentialRoomRestrictionHeaders {
		if err := rrrh.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, ars := range g.allocationReleaseStyles {
		if err := ars.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, ass := range g.allocationSetStatuses {
		if err := ass.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, as := range g.allocationSets {
		if err := as.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, asrt := range g.allocationSetRoomTypes {
		if err := asrt.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, asdq := range g.allocationSetDayQuantities {
		if err := asdq.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, ac := range g.allocationCompanies {
		if err := ac.Persist(ctx, db); err != nil {
			return err
		}
	}
	for _, cs := range g.customSettings {
		if err := cs.Persist(ctx, db); err != nil {
			return err
		}
	}

	return nil
}

func (g *GermDatabase) Assert(ctx context.Context, db *sqlx.DB, t *testing.T) {
	a := assert.New(t)
	r := require.New(t)
	if g.events != nil {
		dest := []Event{}
		a.NoError(db.SelectContext(ctx, &dest, `SELECT EventID, EventTypeID, EventTitle, CreationDate FROM Events`))
		a.Equal(g.events, dest)
	}

	if g.eventModules != nil {
		dest := []EventModule{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT EventID, EventModuleID, SiteID, Description, ArrivalDate, DepartureDate, CreationDate, ModifiedDate FROM EventModules`))
		a.Equal(g.eventModules, dest)
	}

	if g.people != nil {
		dest := []Person{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT PersonID, Forename, Surname, Title, TRIM(Sex) AS Sex, Email, VIP, Disabled, Smoker, CreationDate, ModifiedDate FROM People`))
		a.Equal(g.people, dest)
	}

	if g.siteBlocks != nil {
		dest := []SiteBlock{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT SiteID, BlockID, AreaID, TRIM(Name) AS Name, TRIM(BlockType) AS BlockType, MasterBlockID FROM SiteBlocks`))
		a.Equal(g.siteBlocks, dest)
	}

	if g.sites != nil {
		dest := []Site{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT SiteID, OffSite, TRIM(Name) AS Name FROM Sites`))
		a.Equal(g.sites, dest)
	}
	if g.areas != nil {
		dest := []Area{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT AreaID, TRIM(Name) AS Name FROM Areas`))
		a.Equal(g.sites, dest)
	}
	if g.bedroomTypes != nil {
		dest := []BedroomType{}
		r.NoError(db.SelectContext(ctx, &dest, `BedroomTypeID,Description, Capacity, RoomInFlat, BookByRoom`))
		a.Equal(g.bedroomTypes, dest)
	}

	if g.residentialRooms != nil {
		dest := []ResidentialRoom{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT ResidentialRoomID, BedroomTypeID, BlockID, SubBlockID, Capacity, TypicalCapacity, MaidStatus, TRIM(Name) AS Name, CreationDate, Smoking, Disabled, VIP, LastLet FROM ResidentialRooms ORDER BY ResidentialRoomID`))
		a.Equal(g.residentialRooms, dest)
	}
	if g.residentialRoomAllocations != nil {
		dest := []ResidentialRoomAllocation{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT EMRRAID, EventModuleID, ResidentialRoomID, FirstNight, LastNight, CreationUserID, CreationDate FROM EventModuleResidentialRoomAllocation`))
		a.Equal(g.residentialRoomAllocations, dest)
	}
	if g.residentialRoomOccupants != nil {
		dest := []ResidentialRoomOccupant{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT ResidentialRoomID, EventModuleID, PersonID, PersonSequenceID, Date FROM ResidentialRoomOccupants ORDER BY ResidentialRoomID, Date`))
		a.Equal(g.residentialRoomOccupants, dest)
	}

	if g.eventModuleDelegates != nil {
		dest := []EventModuleDelegate{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT EventModuleDelegateID, EventModuleID, PersonID,PersonSequenceID, StatusID, BlockID, BedroomTypeID, SpecialDietID, ModifiedUserID, ArrivalDate, DepartureDate, Resident, ReceivedMedical, CreationDate FROM EventModuleDelegates`))
		a.Equal(g.eventModuleDelegates, dest)
	}
	if g.eventModuleBlockBedroomAllocation != nil {
		dest := []EventModuleBlockBedroomAllocation{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT EventModuleID, BlockID, Allocation, 	 BedroomTypeID,    Date FROM EventModuleBlockBedroomAllocation ORDER BY Date`))
		a.Equal(g.eventModuleBlockBedroomAllocation, dest)
	}
	if g.availabilityRestrictions != nil {
		dest := []AvailabilityRestriction{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT AvailabilityRestrictionID, EventBusinessTypeID, BlockID, BedroomTypeID, FromDate, ToDate, Notes, FROM AvailabilityRestrictions`))
		a.Equal(g.availabilityRestrictions, dest)
	}
	if g.availabilityRestrictionResidentialRooms != nil {
		dest := []AvailabilityRestrictionResidentialRoom{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT AvailabilityRestrictionID, ResidentialRoomID FROM AvailabilityRestrictionResidentialRooms`))
		a.Equal(g.availabilityRestrictionResidentialRooms, dest)
	}
	if g.control != nil {
		dest := []Control{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT StudentEventBusinessTypeID, RollForward FROM Control`))
		a.Equal(g.control, dest)
	}
	if g.residentialRoomRestrictions != nil {
		dest := []ResidentialRoomRestriction{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT ResidentialRoomRestrictionID, ResidentialRoomID, BedIndex FROM ResidentialRoomRestrictions`))
		a.Equal(g.residentialRoomRestrictions, dest)
	}
	if g.residentialRoomRestrictionHeaders != nil {
		dest := []ResidentialRoomRestrictionHeader{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT ResidentialRoomRestrictionID, BlockID, StartDate, EndDate, Notes, CreationUserID, CreationDate FROM ResidentialRoomRestrictionHeader`))
		a.Equal(g.residentialRoomRestrictions, dest)
	}
	if g.customSettings != nil {
		dest := []ResidentialRoomRestrictionHeader{}
		r.NoError(db.SelectContext(ctx, &dest, `SELECT CustomSettingID,	CustomSetting, Value, String1, String2, String3, String4 FROM CustomSettings`))
		a.Equal(g.residentialRoomRestrictions, dest)
	}
}

func (g *GermDatabase) WithEvents(events ...Event) *GermDatabase {
	g.events = append(g.events, events...)
	return g
}

func (g *GermDatabase) WithEventModules(eventModules ...EventModule) *GermDatabase {
	g.eventModules = append(g.eventModules, eventModules...)
	return g
}
func (g *GermDatabase) WithSites(sites ...Site) *GermDatabase {
	g.sites = append(g.sites, sites...)
	return g
}
func (g *GermDatabase) WithAreas(areas ...Area) *GermDatabase {
	g.areas = append(g.areas, areas...)
	return g
}
func (g *GermDatabase) WithSiteBlocks(siteBlocks ...SiteBlock) *GermDatabase {
	g.siteBlocks = append(g.siteBlocks, siteBlocks...)
	return g
}

func (g *GermDatabase) WithPeople(people ...Person) *GermDatabase {
	g.people = append(g.people, people...)
	return g
}

func (g *GermDatabase) WithBedroomTypes(types ...BedroomType) *GermDatabase {
	g.bedroomTypes = append(g.bedroomTypes, types...)
	return g
}

func (g *GermDatabase) WithResidentialRooms(rooms ...ResidentialRoom) *GermDatabase {
	g.residentialRooms = append(g.residentialRooms, rooms...)
	return g
}

func (g *GermDatabase) WithResidentialRoomAllocations(allocations ...ResidentialRoomAllocation) *GermDatabase {
	g.residentialRoomAllocations = append(g.residentialRoomAllocations, allocations...)
	return g
}

func (g *GermDatabase) WithResidentialRoomOccupants(occupants ...ResidentialRoomOccupant) *GermDatabase {
	g.residentialRoomOccupants = append(g.residentialRoomOccupants, occupants...)
	return g
}

func (g *GermDatabase) WithEventModuleDelegates(eventModuleDelegates ...EventModuleDelegate) *GermDatabase {
	g.eventModuleDelegates = append(g.eventModuleDelegates, eventModuleDelegates...)
	return g
}

func (g *GermDatabase) WithEventModuleBlockBedroomAllocation(allocation ...EventModuleBlockBedroomAllocation) *GermDatabase {
	g.eventModuleBlockBedroomAllocation = append(g.eventModuleBlockBedroomAllocation, allocation...)
	return g
}

func (g *GermDatabase) WithAvailabilityRestrictions(availabilityRestrictions ...AvailabilityRestriction) *GermDatabase {
	g.availabilityRestrictions = append(g.availabilityRestrictions, availabilityRestrictions...)
	return g
}

func (g *GermDatabase) WithAvailabilityResidentialRooms(availabilityResidentialRooms ...AvailabilityRestrictionResidentialRoom) *GermDatabase {
	g.availabilityRestrictionResidentialRooms = append(g.availabilityRestrictionResidentialRooms, availabilityResidentialRooms...)
	return g
}

func (g *GermDatabase) WithCustomSettings(customSettings ...CustomSetting) *GermDatabase {
	g.customSettings = append(g.customSettings, customSettings...)
	return g
}

func (g *GermDatabase) WithControl(control ...Control) *GermDatabase {
	g.control = append(g.control, control...)
	return g
}
func (g *GermDatabase) WithResidentialRoomRestrictions(residentialRoomRestrictions ...ResidentialRoomRestriction) *GermDatabase {
	g.residentialRoomRestrictions = append(g.residentialRoomRestrictions, residentialRoomRestrictions...)
	return g
}
func (g *GermDatabase) WithResidentialRoomRestrictionHeader(header ...ResidentialRoomRestrictionHeader) *GermDatabase {
	g.residentialRoomRestrictionHeaders = append(g.residentialRoomRestrictionHeaders, header...)
	return g
}

func (g *GermDatabase) WithAllocationSetDayQuantities(asdq ...AllocationSetDayQuantity) *GermDatabase {
	g.allocationSetDayQuantities = append(g.allocationSetDayQuantities, asdq...)
	return g
}

func (g *GermDatabase) WithAllocationCompanies(ac ...AllocationCompany) *GermDatabase {
	g.allocationCompanies = append(g.allocationCompanies, ac...)
	return g
}

func (g *GermDatabase) WithAllocationSets(as ...AllocationSet) *GermDatabase {
	g.allocationSets = append(g.allocationSets, as...)
	return g
}

func (g *GermDatabase) WithAllocationReleaseStyles(ars ...AllocationReleaseStyle) *GermDatabase {
	g.allocationReleaseStyles = append(g.allocationReleaseStyles, ars...)
	return g
}

func (g *GermDatabase) WithAllocationSetRoomTypes(asrt ...AllocationSetRoomType) *GermDatabase {
	g.allocationSetRoomTypes = append(g.allocationSetRoomTypes, asrt...)
	return g
}

func (g *GermDatabase) WithAllocationSetStatuses(ass ...AllocationSetStatus) *GermDatabase {
	g.allocationSetStatuses = append(g.allocationSetStatuses, ass...)
	return g
}

package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type ResidentialRoomRestrictionHeader struct {
	ResidentialRoomRestrictionID int       `db:"ResidentialRoomRestrictionID"`
	BlockID                      int       `db:"BlockID"`
	StartDate                    time.Time `db:"StartDate"`
	EndDate                      time.Time `db:"EndDate"`
	Notes                        string    `db:"Notes"`
	CreationUserID               int       `db:"CreationUserID"`
	CreationDate                 time.Time `db:"CreationDate"`
}

func (rrr *ResidentialRoomRestrictionHeader) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT ResidentialRoomRestrictionHeader ON
      INSERT INTO ResidentialRoomRestrictionHeader

	  (ResidentialRoomRestrictionID, BlockID, StartDate, EndDate, Notes, CreationUserID, CreationDate) VALUES
	  (:ResidentialRoomRestrictionID, :BlockID, :StartDate, :EndDate, :Notes, :CreationUserID, :CreationDate)
    SET IDENTITY_INSERT ResidentialRoomRestrictionHeader OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, rrr)
	if eventErr != nil {
		return fmt.Errorf("persisting residential room restriction headers: %w", eventErr)
	}

	return nil
}

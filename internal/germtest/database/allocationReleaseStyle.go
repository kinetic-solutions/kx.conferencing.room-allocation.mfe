package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type AllocationReleaseStyle struct {
	ReleaseStyleID int    `db:"ReleaseStyleID"`
	Description    string `db:"Description"`
}

func (ass *AllocationReleaseStyle) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationReleaseStyles ON
      INSERT INTO AllocationReleaseStyles
	  (ReleaseStyleID, Description) VALUES
	  (:ReleaseStyleID, :Description)
    SET IDENTITY_INSERT AllocationReleaseStyles OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, ass)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation release styles: %w", eventErr)
	}

	return nil
}

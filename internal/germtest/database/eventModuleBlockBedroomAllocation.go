package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type EventModuleBlockBedroomAllocation struct {
	EMBBAID       int       `db:"EMBBAID"`
	EventModuleID int       `db:"EventModuleID"`
	BlockID       int       `db:"BlockID"`
	Allocation    int       `db:"Allocation"`
	BedroomTypeID int       `db:"BedroomTypeID"`
	Date          time.Time `db:"Date"`
}

func (rra *EventModuleBlockBedroomAllocation) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT EventModuleBlockBedroomAllocation ON
        INSERT INTO EventModuleBlockBedroomAllocation
        (EMBBAID, EventModuleID, BlockID, Allocation, 	 BedroomTypeID,    Date) VALUES
        (:EMBBAID, :EventModuleID, :BlockID, :Allocation,  :BedroomTypeID,   :Date)
    SET IDENTITY_INSERT EventModuleBlockBedroomAllocation OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, rra)
	if eventErr != nil {
		return fmt.Errorf("persisting block bedroom allocation: %w", eventErr)
	}

	return nil
}

package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type Event struct {
	EventID      int       `db:"EventID"`
	EventTypeID  int       `db:"EventTypeID"`
	EventTitle   string    `db:"EventTitle"`
	CreationDate time.Time `db:"CreationDate"`
}

func (e *Event) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT Events ON
	INSERT INTO Events
	(EventID,	EventTitle,  EventTypeID,	CreationDate, CreationUserID) VALUES
	(:EventID,  :EventTitle, :EventTypeID,	:CreationDate, 1)
	SET IDENTITY_INSERT Events OFF
`
	_, eventErr := db.NamedExecContext(ctx, query, e)
	if eventErr != nil {
		return fmt.Errorf("persisting events: %w", eventErr)
	}

	return nil
}

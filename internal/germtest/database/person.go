package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type Person struct {
	PersonID     int       `db:"PersonID"`
	Forename     string    `db:"Forename"`
	Surname      string    `db:"Surname"`
	Title        string    `db:"Title"`
	Sex          string    `db:"Sex"`
	Email        string    `db:"Email"`
	VIP          bool      `db:"VIP"`
	Disabled     bool      `db:"Disabled"`
	Smoker       bool      `db:"Smoker"`
	CreationDate time.Time `db:"CreationDate"`
	ModifiedDate time.Time `db:"ModifiedDate"`
}

func (p *Person) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT People ON
    INSERT INTO People
    (PersonID, Surname,   Forename, Title,  Sex, 	Email, VIP, Disabled, Smoker,             CreationDate, CreationUserID, ModifiedDate, ModifiedUserID) VALUES
    (:PersonID, :Surname, :Forename, :Title, :Sex, 	:Email, :VIP, :Disabled, :Smoker,       		:CreationDate,       1,       :ModifiedDate,       1 )
    SET IDENTITY_INSERT People OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, p)
	if eventErr != nil {
		return fmt.Errorf("persisting person: %w", eventErr)
	}

	return nil
}

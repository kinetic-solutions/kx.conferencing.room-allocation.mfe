package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type AllocationSetDayQuantity struct {
	AllocationSetDayQuantityID int       `db:"AllocationSetDayQuantityID"`
	AllocationSetRoomTypeID    int       `db:"AllocationSetRoomTypeID"`
	DayDate                    time.Time `db:"DayDate"`
	InitialRoomQuantity        int       `db:"InitialRoomQuantity"`
	CurrentRoomQuantity        int       `db:"CurrentRoomQuantity"`
}

func (ass *AllocationSetDayQuantity) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationSetDayQuantities ON
      INSERT INTO AllocationSetDayQuantities
	  (AllocationSetDayQuantityID, AllocationSetRoomTypeID, DayDate, InitialRoomQuantity, CurrentRoomQuantity) VALUES
	  (:AllocationSetDayQuantityID, :AllocationSetRoomTypeID, :DayDate, :InitialRoomQuantity, :CurrentRoomQuantity)
    SET IDENTITY_INSERT AllocationSetDayQuantities OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, ass)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation set day quantities: %w", eventErr)
	}

	return nil
}

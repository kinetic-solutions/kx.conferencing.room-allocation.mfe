package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type ResidentialRoomAllocation struct {
	EMRRAID           int       `db:"EMRRAID"`
	EventModuleID     int       `db:"EventModuleID"`
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	CreationUserID    int       `db:"CreationUserID"`
	FirstNight        time.Time `db:"FirstNight"`
	LastNight         time.Time `db:"LastNight"`
	CreationDate      time.Time `db:"CreationDate"`
}

func (rra *ResidentialRoomAllocation) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT EventModuleResidentialRoomAllocation ON
        INSERT INTO EventModuleResidentialRoomAllocation
        (EMRRAID, EventModuleID, ResidentialRoomID, FirstNight, 	 LastNight,    CreationUserID, CreationDate) VALUES
        (:EMRRAID, :EventModuleID, :ResidentialRoomID, :FirstNight,  :LastNight,   :CreationUserID, :CreationDate)
    SET IDENTITY_INSERT EventModuleResidentialRoomAllocation OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, rra)
	if eventErr != nil {
		return fmt.Errorf("persisting residential room: %w", eventErr)
	}

	return nil
}

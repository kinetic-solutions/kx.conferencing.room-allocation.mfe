package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type CustomSetting struct {
	CustomSettingID int    `db:"CustomSettingID"`
	CustomSetting   string `db:"CustomSetting"`
	Value           string `db:"Value"`
	String1         string `db:"String1"`
	String2         string `db:"String2"`
	String3         string `db:"String3"`
	String4         string `db:"String4"`
}

func (rra *CustomSetting) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
		SET IDENTITY_INSERT CustomSettings ON
        INSERT INTO CustomSettings
        (CustomSettingID, CustomSetting, Value, String1, String2, String3, String4) VALUES
		(:CustomSettingID, :CustomSetting, :Value, :String1, :String2, :String3, :String4)
		SET IDENTITY_INSERT CustomSettings OFF

	`
	_, eventErr := db.NamedExecContext(ctx, query, rra)
	if eventErr != nil {
		return fmt.Errorf("persisting custom settings: %w", eventErr)
	}

	return nil
}

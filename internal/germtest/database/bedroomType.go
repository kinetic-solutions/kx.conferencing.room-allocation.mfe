package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type BedroomType struct {
	BedroomTypeID int    `db:"BedroomTypeID"`
	Capacity      int    `db:"Capacity"`
	RoomInFlat    bool   `db:"RoomInFlat"`
	BookByRoom    bool   `db:"BookByRoom"`
	Description   string `db:"Description"`
}

func (bt *BedroomType) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT BedroomTypes ON
    INSERT INTO BedroomTypes
    (BedroomTypeID, Capacity, Description, RoomInFlat, BookByRoom) VALUES
    (:BedroomTypeID, :Capacity, :Description,  :RoomInFlat,:BookByRoom)
    SET IDENTITY_INSERT BedroomTypes OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, bt)
	if eventErr != nil {
		return fmt.Errorf("persisting bedroom types: %w", eventErr)
	}

	return nil
}

package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type AllocationSetStatus struct {
	StatusID    int    `db:"StatusID"`
	Description string `db:"Description"`
	Active      bool   `db:"Active"`
}

func (ass *AllocationSetStatus) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationSetStatuses ON
      INSERT INTO AllocationSetStatuses
	  (StatusID, Description, Active) VALUES
	  (:StatusID, :Description, :Active)
    SET IDENTITY_INSERT AllocationSetStatuses OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, ass)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation set statuses: %w", eventErr)
	}

	return nil
}

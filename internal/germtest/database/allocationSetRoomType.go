package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type AllocationSetRoomType struct {
	AllocationSetRoomTypeID int `db:"AllocationSetRoomTypeID"`
	AllocationSetID         int `db:"AllocationSetID"`
	BedroomTypeID           int `db:"BedroomTypeID"`
	SiteID                  int `db:"SiteID"`
	AreaID                  int `db:"AreaID"`
	BlockID                 int `db:"BlockID"`
	InitialRoomQuantity     int `db:"InitialRoomQuantity"`
}

func (asrt *AllocationSetRoomType) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
    SET IDENTITY_INSERT AllocationSetRoomTypes ON
      INSERT INTO AllocationSetRoomTypes
	  (AllocationSetRoomTypeID, AllocationSetID, BedroomTypeID, SiteID, AreaID, BlockID, InitialRoomQuantity) VALUES
	  (:AllocationSetRoomTypeID, :AllocationSetID, :BedroomTypeID, :SiteID, :AreaID, :BlockID, :InitialRoomQuantity)
    SET IDENTITY_INSERT AllocationSetRoomTypes OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, asrt)
	if eventErr != nil {
		return fmt.Errorf("persisting allocation set room types: %w", eventErr)
	}

	return nil
}

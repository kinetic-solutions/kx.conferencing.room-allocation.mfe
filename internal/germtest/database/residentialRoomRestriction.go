package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type ResidentialRoomRestriction struct {
	ResidentialRoomRestrictionID int `db:"ResidentialRoomRestrictionID"`
	ResidentialRoomID            int `db:"ResidentialRoomID"`
	BedIndex                     int `db:"BedIndex"`
}

func (rrr *ResidentialRoomRestriction) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
      INSERT INTO ResidentialRoomRestrictions
	  (ResidentialRoomRestrictionID, ResidentialRoomID, BedIndex) VALUES
	  (:ResidentialRoomRestrictionID, :ResidentialRoomID, :BedIndex)
	`
	_, eventErr := db.NamedExecContext(ctx, query, rrr)
	if eventErr != nil {
		return fmt.Errorf("persisting residential room restrictions: %w", eventErr)
	}

	return nil
}

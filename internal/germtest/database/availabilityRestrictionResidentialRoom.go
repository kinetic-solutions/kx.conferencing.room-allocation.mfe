package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type AvailabilityRestrictionResidentialRoom struct {
	AvailabilityRestrictionID int `db:"AvailabilityRestrictionID"`
	ResidentialRoomID         int `db:"ResidentialRoomID"`
}

func (rra *AvailabilityRestrictionResidentialRoom) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
        INSERT INTO AvailabilityRestrictionResidentialRooms
        (AvailabilityRestrictionID, ResidentialRoomID) VALUES
        (:AvailabilityRestrictionID, :ResidentialRoomID)
	`
	_, eventErr := db.NamedExecContext(ctx, query, rra)
	if eventErr != nil {
		return fmt.Errorf("persisting availability restriction residential rooms: %w", eventErr)
	}

	return nil
}

package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type Site struct {
	SiteID  int    `db:"SiteID"`
	OffSite bool   `db:"OffSite"`
	Name    string `db:"Name"`
}

func (s *Site) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT Sites ON
	INSERT INTO Sites
	(SiteID, Name,     OffSite) VALUES
	(:SiteID, :Name, :OffSite)
	SET IDENTITY_INSERT Sites OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, s)
	if eventErr != nil {
		return fmt.Errorf("persisting site: %w", eventErr)
	}

	return nil
}

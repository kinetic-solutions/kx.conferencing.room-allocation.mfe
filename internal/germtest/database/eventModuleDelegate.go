package database

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type EventModuleDelegate struct {
	EventModuleDelegateID int           `db:"EventModuleDelegateID"`
	EventModuleID         int           `db:"EventModuleID"`
	PersonID              int           `db:"PersonID"`
	PersonSequenceID      int           `db:"PersonSequenceID"`
	StatusID              int           `db:"StatusID"`
	BlockID               sql.NullInt64 `db:"BlockID"`
	BedroomTypeID         sql.NullInt64 `db:"BedroomTypeID"`
	SpecialDietID         int           `db:"SpecialDietID"`
	ModifiedUserID        int           `db:"ModifiedUserID"`
	Resident              bool          `db:"Resident"`
	ReceivedMedical       bool          `db:"ReceivedMedical"`
	ArrivalDate           time.Time     `db:"ArrivalDate"`
	DepartureDate         time.Time     `db:"DepartureDate"`
	CheckedInDate         sql.NullTime  `db:"CheckedInDate"`
	CreationDate          time.Time     `db:"CreationDate"`
}

func (emd *EventModuleDelegate) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT EventModuleDelegates ON
    INSERT INTO EventModuleDelegates
    (EventModuleDelegateID, EventModuleID, PersonID, PersonSequenceID, StatusID, BlockID, BedroomTypeID, SpecialDietID, ModifiedUserID, ArrivalDate, DepartureDate, Resident, ReceivedMedical, CreationDate, CheckedInDate) VALUES
    (:EventModuleDelegateID, :EventModuleID, :PersonID,  :PersonSequenceID, :StatusID, :BlockID, :BedroomTypeID,  :SpecialDietID, :ModifiedUserID, :ArrivalDate, :DepartureDate, :Resident, :ReceivedMedical, :CreationDate, :CheckedInDate)
    SET IDENTITY_INSERT EventModuleDelegates OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, emd)
	if eventErr != nil {
		return fmt.Errorf("persisting event module delegate: %w", eventErr)
	}

	return nil
}

package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type SiteBlock struct {
	SiteID        int    `db:"SiteID"`
	BlockID       int    `db:"BlockID"`
	AreaID        int    `db:"AreaID"`
	Name          string `db:"Name"`
	BlockType     string `db:"BlockType"`
	MasterBlockID int    `db:"MasterBlockID"`
}

func (sb *SiteBlock) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT SiteBlocks ON
	INSERT INTO SiteBlocks
	(BlockID,	SiteID,AreaID,	Name,              BlockType, MasterBlockID) VALUES
	(:BlockID, 	    :SiteID, :AreaID, :Name, :BlockType, :MasterBlockID)
SET IDENTITY_INSERT SiteBlocks OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, sb)
	if eventErr != nil {
		return fmt.Errorf("persisting site block: %w", eventErr)
	}

	return nil
}

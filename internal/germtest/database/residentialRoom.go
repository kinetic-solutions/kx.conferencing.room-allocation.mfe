package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type ResidentialRoom struct {
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	BedroomTypeID     int       `db:"BedroomTypeID"`
	BlockID           int       `db:"BlockID"`
	SubBlockID        int       `db:"SubBlockID"`
	Capacity          int       `db:"Capacity"`
	TypicalCapacity   int       `db:"TypicalCapacity"`
	Name              string    `db:"Name"`
	CreationDate      time.Time `db:"CreationDate"`
	MaidStatus        string    `db:"MaidStatus"`
	Inactive          int       `db:"Inactive"`
	Smoking           bool      `db:"Smoking"`
	Disabled          bool      `db:"Disabled"`
	VIP               bool      `db:"VIP"`
	LastLet           bool      `db:"LastLet"`
}

func (rr *ResidentialRoom) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT ResidentialRooms ON
    INSERT INTO ResidentialRooms
    (ResidentialRoomID, BedroomTypeID, BlockID, SubBlockID, Name, Capacity,TypicalCapacity, MaidStatus,CreationDate, Inactive, Smoking, Disabled, VIP, LastLet) VALUES
    (:ResidentialRoomID, :BedroomTypeID,:BlockID, :SubBlockID,  :Name, :Capacity,:TypicalCapacity, :MaidStatus, :CreationDate, :Inactive, :Smoking, :Disabled, :VIP, :LastLet)
    SET IDENTITY_INSERT ResidentialRooms OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, rr)
	if eventErr != nil {
		return fmt.Errorf("persisting residential room: %w", eventErr)
	}

	return nil
}

package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type EventModule struct {
	EventID       int       `db:"EventID"`
	EventModuleId int       `db:"EventModuleID"`
	SiteID        int       `db:"SiteID"`
	Description   string    `db:"Description"`
	ArrivalDate   time.Time `db:"ArrivalDate"`
	DepartureDate time.Time `db:"DepartureDate"`
	CreationDate  time.Time `db:"CreationDate"`
	ModifiedDate  time.Time `db:"ModifiedDate"`
}

func (e *EventModule) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT EventModules ON
	INSERT INTO EventModules
	(EventModuleID, 	EventID, 	SiteID, 	[Description],	ArrivalDate,	DepartureDate,	StatusID, DayPeople, NightPeople, CreationDate, CreationUserID, ModifiedDate, ModifiedUserID) VALUES
	(:EventModuleID,	:EventID,	:SiteID,	:Description,	:ArrivalDate,	:DepartureDate, 2,        50,        50,          :CreationDate,       1,       :ModifiedDate,       1)
	SET IDENTITY_INSERT EventModules OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, e)
	if eventErr != nil {
		return fmt.Errorf("persisting event modules: %w", eventErr)
	}

	return nil
}

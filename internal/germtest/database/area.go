package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type Area struct {
	AreaID      int    `db:"AreaID"`
	SiteID      int    `db:"SiteID"`
	Description string `db:"Description"`
}

func (s *Area) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	SET IDENTITY_INSERT Areas ON
		INSERT INTO Areas
		(AreaID,  Description, SiteID) VALUES
		(:AreaID, :Description, :SiteID)
	SET IDENTITY_INSERT Areas OFF
	`
	_, eventErr := db.NamedExecContext(ctx, query, s)
	if eventErr != nil {
		return fmt.Errorf("persisting area: %w", eventErr)
	}

	return nil
}

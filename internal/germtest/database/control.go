package database

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type Control struct {
	StudentEventBusinessTypeID int  `db:"StudentEventBusinessTypeID"`
	RollForward                bool `db:"RollForward"`
}

func (e *Control) Persist(ctx context.Context, db *sqlx.DB) error {
	query := `
	INSERT INTO Control
	(StudentEventBusinessTypeID, RollForward) VALUES
	(:StudentEventBusinessTypeID, :RollForward)
`
	_, eventErr := db.NamedExecContext(ctx, query, e)
	if eventErr != nil {
		return fmt.Errorf("persisting control: %w", eventErr)
	}

	return nil
}

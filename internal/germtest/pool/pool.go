package pool

// This package is used to create a database pool with configurable options.
// The database pool can then be used to isolate the test cases in one test
// function from another test function.
//
// This 'Functional Options' pattern is described in Uber's style guide for Go:
// https://github.com/uber-go/guide/blob/master/style.md#functional-options

import (
	"context"
	"time"

	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
)

// NewPool creates a pool of databases to use for testing.
// Omitting the options parameter will use the defaults. Individual options can
// be set by passing that option individually.
func NewPool(ctx context.Context, log germ.Log, s germ.Server, opts ...Option,
) (*germ.DatabasePool, error) {
	options := options{
		maxDatabases:      20,
		namePrefix:        "germ_",
		leasePollInterval: 100 * time.Millisecond,
	}

	for _, o := range opts {
		o.apply(&options)
	}

	germOpts := germ.PoolOptions{
		Server:            s,
		MaxDatabases:      options.maxDatabases,
		NamePrefix:        options.namePrefix,
		LeasePollInterval: options.leasePollInterval,
	}

	return germ.NewPool(ctx, log, germOpts)
}

// Option is used to override default behaviour when creating a DB Pool.
type Option interface {
	apply(*options)
}

type options struct {
	maxDatabases      int
	namePrefix        string
	leasePollInterval time.Duration
}

// WithMaxDatabases limits the number of new database created.
// Set to 0 for unlimited databases.
func WithMaxDatabases(n int) Option {
	return maxDbOption(n)
}

type maxDbOption int

func (max maxDbOption) apply(o *options) {
	o.maxDatabases = int(max)
}

// WithNamePrefix is used to group pooled databases together.
// This allows more than one pool of databases on a server.
func WithNamePrefix(prefix string) Option {
	return prefixOption(prefix)
}

type prefixOption string

func (p prefixOption) apply(o *options) {
	o.namePrefix = string(p)
}

// WithLeasePollInterval specifies how often Lease should check to
// see if a new database has been freed
func WithLeasePollInterval(t time.Duration) Option {
	return leasePollOption(t)
}

type leasePollOption time.Duration

func (t leasePollOption) apply(o *options) {
	o.leasePollInterval = time.Duration(t)
}

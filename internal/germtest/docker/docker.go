package docker

import (
	"context"
	"crypto/rand"
	"encoding/hex"

	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
)

// StartDocker re-uses or creates a docker server instance.
// use WithImage(), WithName() and WithEnv() to override the options.
func StartDocker(ctx context.Context, opts ...Option) (germ.Server, error) {
	options := options{
		name:  "GERM-testing-sql",
		image: "mcr.microsoft.com/mssql/server:latest",
		env: map[string]string{
			"SA_PASSWORD": "UPPER" + rndHex(8) + "CASE",
			"ACCEPT_EULA": "Y",
			"MSSQL_PID":   "Developer",
		},
	}

	for _, o := range opts {
		o.apply(&options)
	}

	dockerOpts := germ.DockerOptions{
		Image: options.image,
		Name:  options.name,
		Env:   options.env,
	}

	return germ.Docker(ctx, dockerOpts)
}

// Option is used to override default behaviour when starting a docker sql server.
type Option interface {
	apply(*options)
}

type options struct {
	image string
	name  string
	env   map[string]string
}

// WithImage specifies the image to use for the docker container.
// Default = mcr.microsoft.com/mssql/server:latest
func WithImage(image string) Option {
	return imageOption(image)
}

type imageOption string

func (image imageOption) apply(o *options) {
	o.image = string(image)
}

// WithName specifies the name to use for the docker image.
// Default = GERM-testing-sql
func WithName(name string) Option {
	return nameOption(name)
}

type nameOption string

func (name nameOption) apply(o *options) {
	o.name = string(name)
}

// WithEnv specifies environment variables to set on start-up
// Defaults:
//   "ACCEPT_EULA": "Y"
//   "SA_PASSWORD": (random)
//   "MSSQL_PID":   "Developer"
func WithEnv(env map[string]string) Option {
	return envOption{env}
}

type envOption struct {
	env map[string]string
}

func (env envOption) apply(o *options) {
	for k, v := range env.env {
		o.env[k] = v
	}
}

func rndHex(size int) string {
	b := make([]byte, size)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}

	return hex.EncodeToString(b)
}

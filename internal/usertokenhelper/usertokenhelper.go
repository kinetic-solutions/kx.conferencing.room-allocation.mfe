package usertokenhelper

import (
	"fmt"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/users"
)

type extendedUser struct {
	users.User
	UserIds map[string][]string `json:"https://kxcloud.net/germ/userids"`
}

func ExtractUserId(lc *lambdawrapper.LambdaContext) (int, error) {
	var user extendedUser

	switch err := lc.User(&user); err {
	case nil:
		break
	default:
		return 0, fmt.Errorf("failed to extract user from token: %w", err)
	}

	if user.UserIds == nil || len(user.UserIds) == 0 {
		return 0, fmt.Errorf("no tenant access is configured for this user")
	}

	tenantId := lc.Tenancy().ID
	if tenantId == "" {
		return 0, fmt.Errorf("cannot find tenant id header")
	}

	userIds := user.UserIds[tenantId]
	if len(userIds) == 0 {
		return 0, fmt.Errorf("the user doesn't have access to this tenant's data")
	}

	userID, err := strconv.Atoi(userIds[0])
	if err != nil {
		return 0, fmt.Errorf("parsing userID: %w", err)
	}

	return userID, nil
}

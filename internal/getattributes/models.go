package getattributes

//the data retured to the API
type apiResponse struct {
	Attributes attributes `json:"attributes"`
}

type dbAttr struct {
	Key   string `db:"CustomSetting"`
	Value string `db:"Value"`
}

type attributes struct {
	Disabled string `json:"disabled"`
	Smoking  string `json:"smoking"`
	Vip      string `json:"vip"`
	LastLet  string `json:"lastLet"`
}

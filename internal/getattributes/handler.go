// Package getattributes gets a list of attributes for an event.
package getattributes

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.getAttributes", func(ctx context.Context) error {
		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		dbAttrs, err := getDbAttrs(ctx, tx)
		if err != nil {
			return fmt.Errorf("cannot select room attributes: %w", err)
		}

		attributes := buildAttrs(dbAttrs)
		return lc.JSON(http.StatusOK, apiResponse{Attributes: attributes})
	})
}

func getDbAttrs(ctx context.Context, tx *sqlx.Tx) ([]dbAttr, error) {
	rows := []dbAttr{}
	err := xray.Capture(ctx, "roomAllocation.getDbAttrs", func(ctx context.Context) error {
		err := tx.SelectContext(ctx, &rows, `
			SELECT CustomSetting, [Value]
			FROM CustomSettings
			WHERE CustomSetting LIKE 'Kx.Room.Attribute.%.Text'
		`)
		if err != nil {
			return fmt.Errorf("failed to read from GERM: %w", err)
		}
		return nil
	})

	return rows, err
}

func buildAttrs(dbAttrs []dbAttr) attributes {
	defaultAttr := attributes{
		Disabled: "Accessible",
		Smoking:  "Smoking",
		Vip:      "VIP",
		LastLet:  "Last Let",
	}
	attr := mergeAttr(defaultAttr, dbAttrs)
	return attr
}

func mergeAttr(attr attributes, customRows []dbAttr) attributes {
	rep := strings.NewReplacer("Kx.Room.Attribute.", "", ".Text", "")
	for _, custom := range customRows {
		k := rep.Replace(custom.Key)
		switch k {
		case "Disabled":
			attr.Disabled = custom.Value
		case "Smoking":
			attr.Smoking = custom.Value
		case "VIP":
			attr.Vip = custom.Value
		case "LastLet":
			attr.LastLet = custom.Value
		}
	}
	return attr
}

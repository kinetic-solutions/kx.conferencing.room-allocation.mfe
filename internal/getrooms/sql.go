package getrooms

const (
	sqlGetCustomSettings = `
	SELECT Value FROM Customsettings where customsetting = @CustomSetting
	`
	sqlGetRooms = `
	WITH Occupants AS (
		SELECT ResidentialRoomID, Date, COUNT(PersonID) AS Count FROM ResidentialRoomOccupants
		WHERE EventModuleID = @EventModuleID
		AND Date BETWEEN @FirstNight and @LastNight
		AND PersonID <> -99999 --ignore the system person
		GROUP BY ResidentialRoomID, Date
	)
	SELECT
	RR.ResidentialRoomID,
	RR.Name AS BedroomName,
	RR.BedroomTypeID,
	BT.Description AS BedroomType,
	SB.SiteID,
	SB.AreaID,
	RR.BlockID,
	LTRIM(RTRIM(SB.Name)) AS BlockName,
	RR.SubBlockID,
	ISNULL(LTRIM(RTRIM(SSB.Name)), '') AS SubBlockName,
	RR.TypicalCapacity,
	RR.Capacity,
	RR.Disabled,
	RR.Smoking,
	RR.VIP,
	RR.LastLet,
	(SELECT ISNULL(MAX(Count), 0) FROM Occupants WHERE ResidentialRoomID = RR.ResidentialRoomID) AS Occupied,
	LTRIM(RTRIM(RR.MaidStatus)) as MaidStatus
	FROM ResidentialRooms RR
	JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomTypeID
	JOIN SiteBlocks SB on SB.BlockID = RR.BlockID
	LEFT JOIN SiteBlocks SSB ON SSB.BlockID = RR.SubBlockID
	WHERE
	(@includeOccupied = 1 OR (SELECT ISNULL(MAX(Count), 0) FROM Occupants WHERE ResidentialRoomID = RR.ResidentialRoomID) = 0)
	AND (@roomName IS NULL OR RR.Name LIKE '%' + @roomName + '%')
	AND (@siteID IS NULL OR SB.SiteID = @siteID)
	AND (@areaID IS NULL OR SB.AreaID = @areaID)
	AND (@blockID IS NULL OR RR.BlockID = @blockID)
	AND (@subBlockID IS NULL OR RR.SubBlockID = @subBlockID)
	AND (@bedroomTypeID IS NULL OR RR.BedroomTypeID = @bedroomTypeID)
	AND (@smoking IS NULL OR RR.Smoking = @smoking)
	AND (@vip is NULL OR RR.VIP = @vip)
	AND (@disabled IS NULL OR RR.Disabled = @disabled)
	AND (@lastLet IS NULL OR RR.LastLet = @lastLet)
	AND (@maidStatus IS NULL OR RR.MaidStatus = @maidStatus)
	--ignore flats
	AND BT.BookByRoom = 0
	AND BT.RoomInFlat = 0
	--ignore inactive rooms
	AND ISNULL(RR.Inactive, 0) = 0
	AND NOT EXISTS(SELECT 1 FROM ResidentialRoomOccupants WHERE ResidentialRoomID = RR.ResidentialRoomID AND Date BETWEEN @FirstNight AND @LastNight AND EventModuleID <> @EventModuleID)
	AND NOT EXISTS(SELECT 1 FROM ViewResidentialRoomAvailabilityRestrictions WHERE ResidentialRoomID = RR.ResidentialRoomID AND FromDate <= @LastNight AND ToDate >= @FirstNight AND Commercial = 1)
	ORDER BY SB.SequenceNo, SSB.SequenceNo, RR.DisplayOrder
	OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY
	`

	sqlGetOutOfServiceRooms = `
		SELECT	DISTINCT RRR.ResidentialRoomID
		from	ResidentialRoomRestrictions rrr
		join	ResidentialRoomRestrictionheader rrrh on rrrh.residentialroomrestrictionid =  rrr.residentialroomrestrictionid
		join	ResidentialRooms RR ON RR.ResidentialRoomID = RRR.ResidentialRoomID
		WHERE	enddate >= @FirstNight AND startdate <= @LastNight
	`
)

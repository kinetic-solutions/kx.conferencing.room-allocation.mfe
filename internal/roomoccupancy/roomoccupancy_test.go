package roomoccupancy

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestAddAllocation(t *testing.T) {
	t.Run("when the room exists, can add an allocation and retrieve it", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		firstNight, _ := time.Parse("2006-01-02", "2022-01-01")
		lastNight, _ := time.Parse("2006-01-02", "2022-01-05")
		r.NoError(roomOccupancy.AddRoomAllocation(2, roomID, firstNight, lastNight))

		allocations, _ := roomOccupancy.GetAllocationsForRoom(1)
		r.Equal([]RoomAllocation{{ID: 2, RoomID: roomID, FirstNight: firstNight, LastNight: lastNight}}, allocations)
		r.Equal(0, roomOccupancy.MaxPersonSequenceID)
	})
}

func TestGetAllAllocations(t *testing.T) {
	r := require.New(t)

	t.Run("it returns all room allocations", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		d1, _ := time.Parse("2006-01-02", "2021-01-02")
		d2, _ := time.Parse("2006-01-02", "2021-01-05")
		r.NoError(roomOccupancy.AddRoomAllocation(2, roomID, d1, d2))

		d3, _ := time.Parse("2006-01-02", "2021-02-01")
		d4, _ := time.Parse("2006-01-02", "2021-02-05")
		r.NoError(roomOccupancy.AddRoomAllocation(3, roomID, d3, d4))

		got := roomOccupancy.GetAllAllocations()
		wanted := []RoomAllocation{
			{ID: 2, RoomID: 1, FirstNight: d1, LastNight: d2},
			{ID: 3, RoomID: 1, FirstNight: d3, LastNight: d4},
		}
		r.Equal(got, wanted)
	})
}
func TestGetAllocationContainingDates(t *testing.T) {
	r := require.New(t)

	t.Run("it only returns the room allocation that contains the given dates", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		r.NoError(roomOccupancy.AddRoomAllocation(2, roomID, allocationFirstNight, allocationLastNight))

		allocationFirstNight, _ = time.Parse("2006-01-02", "2021-02-01")
		allocationLastNight, _ = time.Parse("2006-01-02", "2021-02-05")
		r.NoError(roomOccupancy.AddRoomAllocation(3, roomID, allocationFirstNight, allocationLastNight))

		firstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		lastNight, _ := time.Parse("2006-01-02", "2021-01-05")

		got, _ := roomOccupancy.GetAllocationForDates(roomID, firstNight, lastNight)
		wantedFirstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		wantedLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		r.Equal(got, &RoomAllocation{ID: 2, RoomID: 1, FirstNight: wantedFirstNight, LastNight: wantedLastNight})
	})

	t.Run("if no allocations exist then return nil", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		firstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		lastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		got, _ := roomOccupancy.GetAllocationForDates(roomID, firstNight, lastNight)
		r.Nil(got)
	})

	t.Run("if the room doesn't exist then return an error", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		firstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		lastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		_, err := roomOccupancy.GetAllocationForDates(roomID, firstNight, lastNight)
		r.EqualError(err, "no room exists for roomID 1")
	})
}

func TestIsRoomOnRoomingList(t *testing.T) {
	r := require.New(t)

	t.Run("if the allocation exists within the given dates then return true, otherwise return false", func(t *testing.T) {
		type test struct {
			firstNight string
			lastNight  string
			want       bool
		}
		tests := []test{
			{firstNight: "2021-01-01", lastNight: "2021-01-05", want: false},
			{firstNight: "2021-01-02", lastNight: "2021-01-06", want: false},
			{firstNight: "2021-02-01", lastNight: "2021-02-06", want: false},
			{firstNight: "2021-01-02", lastNight: "2021-01-05", want: true},
			{firstNight: "2021-01-03", lastNight: "2021-01-05", want: true},
			{firstNight: "2021-01-03", lastNight: "2021-01-04", want: true},
		}
		for _, tc := range tests {
			roomOccupancy := New(11)
			roomID := 1
			allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-02")
			allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
			r.NoError(roomOccupancy.AddRoomAllocation(2, roomID, allocationFirstNight, allocationLastNight))

			firstNight, _ := time.Parse("2006-01-02", tc.firstNight)
			lastNight, _ := time.Parse("2006-01-02", tc.lastNight)
			isRoomOnRoomingList, _ := roomOccupancy.IsRoomOnRoomingList(roomID, firstNight, lastNight)
			r.Equal(tc.want, isRoomOnRoomingList)
		}
	})

	t.Run("if no allocations exist then return false", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		isRoomOnRoomingList, _ := roomOccupancy.IsRoomOnRoomingList(roomID, time.Now(), time.Now())
		r.Equal(false, isRoomOnRoomingList)
	})

	t.Run("if the room doesn't exist then return an error", func(t *testing.T) {
		roomOccupancy := New(11)
		roomID := 1

		_, err := roomOccupancy.IsRoomOnRoomingList(roomID, time.Now(), time.Now())
		r.EqualError(err, "no room exists for roomID 1")
	})
}

func TestAddOccupant(t *testing.T) {
	t.Run("when the room exists but it doesn't have a room allocation, can add an occupant and retrieve it", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		now := time.Now()
		roomOccupancy.AddRoomOccupant(roomID, 10, 0, now)

		got, _ := roomOccupancy.GetRoomOccupants(1, now)
		want := Occupants{Allocated: 1, Reserved: 1, Occupants: []RoomOccupant{{PersonID: 10, PersonSequenceID: 0, Date: now, RoomID: roomID}}}
		r.Equal(want, got)
	})

	t.Run("if an occupant is added with personID -99999 then the allocated count is not updated, but the reserved count is updated and the maxPersonSequenceID is updated if the occupant has a bigger personSequenceID than previously", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		now := time.Now()
		roomOccupancy.AddRoomOccupant(roomID, -99999, 10, now)

		got, _ := roomOccupancy.GetRoomOccupants(1, now)
		want := Occupants{Allocated: 0, Reserved: 1, Occupants: []RoomOccupant{{PersonID: -99999, PersonSequenceID: 10, Date: now, RoomID: roomID}}}
		r.Equal(want, got)
		r.Equal(10, roomOccupancy.MaxPersonSequenceID)

		//Add another one with a lower PersonSequenceID, the maxPersonSequenceID shouldn't be updated
		roomOccupancy.AddRoomOccupant(roomID, -99999, 8, now)
		r.Equal(10, roomOccupancy.MaxPersonSequenceID)
	})

	t.Run("if room already has a fake occupant with id -99999 then if a real occupant is added, the fake one is overriden and the count is updated", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		now := time.Now()
		roomOccupancy.AddRoomOccupant(roomID, -99999, 10, now)
		roomOccupancy.AddRoomOccupant(roomID, 10, 1, now)

		got, _ := roomOccupancy.GetRoomOccupants(1, now)
		want := Occupants{Allocated: 1, Reserved: 1, Occupants: []RoomOccupant{{PersonID: 10, PersonSequenceID: 1, Date: now, RoomID: roomID}}}
		r.Equal(want, got)
	})
}

func TestDeleteRoomAllocation(t *testing.T) {
	t.Run("an error is returned if the allocation doesn't exist", func(t *testing.T) {
		r := require.New(t)
		roomOccupancy := New(11)
		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		r.EqualError(roomOccupancy.DeleteRoomAllocation(1, allocationFirstNight, allocationLastNight), "no room allocation exists for roomID 1")
	})

	t.Run("if the dates to be deleted are the same as the dates of the allocation then remove the allocation", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")

		firstNight, _ := time.Parse("2006-01-02", "2019-01-01")
		lastNight, _ := time.Parse("2006-01-02", "2019-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)
		roomOccupancy.AddRoomAllocation(2, roomID, firstNight, lastNight)

		roomOccupancy.DeleteRoomAllocation(roomID, allocationFirstNight, allocationLastNight)
		allocations, _ := roomOccupancy.GetAllocationsForRoom(roomID)

		r.Equal([]RoomAllocation{
			{ID: 2, RoomID: roomID, FirstNight: firstNight, LastNight: lastNight},
		}, allocations)
	})

	t.Run("firstNight equal but lastNight different", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)

		d1, _ := time.Parse("2006-01-02", "2021-01-10")
		d2, _ := time.Parse("2006-01-02", "2021-01-15")
		roomOccupancy.AddRoomAllocation(2, roomID, d1, d2)

		lastNight, _ := time.Parse("2006-01-02", "2021-01-03")
		roomOccupancy.DeleteRoomAllocation(roomID, allocationFirstNight, lastNight)
		allocations, _ := roomOccupancy.GetAllocationsForRoom(roomID)

		expectedFirstNight, _ := time.Parse("2006-01-02", "2021-01-04")
		expectedLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		r.Equal([]RoomAllocation{
			{ID: 1, RoomID: roomID, FirstNight: expectedFirstNight, LastNight: expectedLastNight},
			{ID: 2, RoomID: roomID, FirstNight: d1, LastNight: d2},
		}, allocations)
	})

	t.Run("lastNight equal but firstNight different", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)

		d1, _ := time.Parse("2006-01-02", "2020-12-10")
		d2, _ := time.Parse("2006-01-02", "2020-12-15")
		roomOccupancy.AddRoomAllocation(2, roomID, d1, d2)

		firstNight, _ := time.Parse("2006-01-02", "2021-01-03")
		roomOccupancy.DeleteRoomAllocation(roomID, firstNight, allocationLastNight)
		allocations, _ := roomOccupancy.GetAllocationsForRoom(roomID)

		expectedFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		expectedLastNight, _ := time.Parse("2006-01-02", "2021-01-02")
		r.Equal([]RoomAllocation{
			{ID: 1, RoomID: roomID, FirstNight: expectedFirstNight, LastNight: expectedLastNight},
			{ID: 2, RoomID: roomID, FirstNight: d1, LastNight: d2},
		}, allocations)
	})

	t.Run("firstNight and lastNight are different", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)

		d1, _ := time.Parse("2006-01-02", "2020-12-10")
		d2, _ := time.Parse("2006-01-02", "2020-12-15")
		roomOccupancy.AddRoomAllocation(2, roomID, d1, d2)

		firstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		lastNight, _ := time.Parse("2006-01-02", "2021-01-03")
		roomOccupancy.DeleteRoomAllocation(roomID, firstNight, lastNight)
		allocations, _ := roomOccupancy.GetAllocationsForRoom(roomID)

		e1, _ := time.Parse("2006-01-02", "2021-01-01")
		e2, _ := time.Parse("2006-01-02", "2021-01-01")
		e3, _ := time.Parse("2006-01-02", "2021-01-04")
		e4, _ := time.Parse("2006-01-02", "2021-01-05")
		r.Equal([]RoomAllocation{
			{ID: 2, RoomID: roomID, FirstNight: d1, LastNight: d2},
			{RoomID: roomID, FirstNight: e1, LastNight: e2},
			{RoomID: roomID, FirstNight: e3, LastNight: e4},
		}, allocations)
	})

	t.Run("firstNight and lastNight are not within any existing allocation", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-01")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)

		d1, _ := time.Parse("2006-01-02", "2020-12-10")
		d2, _ := time.Parse("2006-01-02", "2020-12-15")
		roomOccupancy.AddRoomAllocation(2, roomID, d1, d2)

		firstNight, _ := time.Parse("2006-01-02", "2021-01-21")
		lastNight, _ := time.Parse("2006-01-02", "2021-01-24")
		roomOccupancy.DeleteRoomAllocation(roomID, firstNight, lastNight)
		allocations, _ := roomOccupancy.GetAllocationsForRoom(roomID)

		r.Equal([]RoomAllocation{
			{ID: 1, RoomID: roomID, FirstNight: allocationFirstNight, LastNight: allocationLastNight},
			{ID: 2, RoomID: roomID, FirstNight: d1, LastNight: d2},
		}, allocations)
	})
}

func TestDeleteRoomOccupant(t *testing.T) {
	t.Run("an error is returned if the room doesn't exist", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)

		r.EqualError(roomOccupancy.DeleteRoomOccupant(1, 1, 1, time.Now()), "no room exists for roomID 1")
	})

	t.Run("when the occupant doesn't exist, an error is returned", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		date, _ := time.Parse("2006-01-02", "2022-01-02")
		d2, _ := time.Parse("2006-01-02", "2022-01-03")
		roomOccupancy.AddRoomOccupant(roomID, 10, 0, d2)
		r.EqualError(roomOccupancy.DeleteRoomOccupant(roomID, 10, 0, date), "no occupant exists for roomID 1 on date 2022-01-02 00:00:00 +0000 UTC with personID 10 and personSequenceID 0")
	})

	t.Run("when the room exists but it doesn't have a room allocation, can delete an occupant", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1
		now := time.Now()
		roomOccupancy.AddRoomOccupant(roomID, 10, 0, now)
		roomOccupancy.DeleteRoomOccupant(roomID, 10, 0, now)

		occupants, _ := roomOccupancy.GetRoomOccupants(1, now)
		r.Equal(Occupants{Allocated: 0, Reserved: 0, Occupants: []RoomOccupant{}}, occupants)
	})

	t.Run("when the room has a matching allocation and the final occupant is deleted, a new occupant with personID -99999 is added, although the count should still be 0", func(t *testing.T) {
		r := require.New(t)

		roomOccupancy := New(11)
		roomID := 1

		allocationFirstNight, _ := time.Parse("2006-01-02", "2021-01-02")
		allocationLastNight, _ := time.Parse("2006-01-02", "2021-01-05")
		roomOccupancy.AddRoomAllocation(1, roomID, allocationFirstNight, allocationLastNight)

		roomOccupancy.AddRoomOccupant(roomID, 10, 0, allocationFirstNight)
		roomOccupancy.DeleteRoomOccupant(roomID, 10, 0, allocationFirstNight)

		occupants, _ := roomOccupancy.GetRoomOccupants(1, allocationFirstNight)
		r.Equal(Occupants{Allocated: 0, Reserved: 1, Occupants: []RoomOccupant{{
			Date:             allocationFirstNight,
			PersonID:         -99999,
			PersonSequenceID: 1,
			RoomID:           roomID,
		}},
		}, occupants)

		r.Equal(1, roomOccupancy.MaxPersonSequenceID)
	})
}

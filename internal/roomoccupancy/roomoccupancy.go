package roomoccupancy

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type BedroomType struct {
	BedroomTypeID int
	Description   string
	BookByRoom    bool
	RoomInFlat    bool
}
type Room struct {
	ID          int
	Capacity    int
	RoomName    string
	BedroomType BedroomType
	BlockID     int
}

type RoomAllocation struct {
	ID         int
	RoomID     int
	FirstNight time.Time
	LastNight  time.Time
}

type RoomOccupant struct {
	Date             time.Time
	RoomID           int
	PersonID         int
	PersonSequenceID int
}

type RoomOccupancy struct {
	EventModuleID       int
	MaxPersonSequenceID int
	roomIndex           map[int]roomIndex
}

type Occupants struct {
	//Allocated is the count of real occupants, it doesn't include people with ID -99999
	Allocated int
	//Reserved is the count of reserved occupants, it includes people with ID -99999 that represent a reserved space in a room
	Reserved  int
	Occupants []RoomOccupant
}

type roomIndex struct {
	Allocations []RoomAllocation
	Occupants   map[time.Time]Occupants
}

//New makes a new RoomOccupancy
func New(eventModuleID int) *RoomOccupancy {
	return &RoomOccupancy{
		EventModuleID: eventModuleID,
		roomIndex:     map[int]roomIndex{},
	}
}

//NewFromDatabase makes a new RoomOccupancy using the given transaction to get the required data from the database
func NewFromDatabase(ctx context.Context, tx *sqlx.Tx, eventModuleID int) (*RoomOccupancy, error) {
	roomOccupancy := New(eventModuleID)

	roomAllocations := []dbResidentialRoomAllocation{}
	if err := fetchRoomAllocation(ctx, tx, eventModuleID, &roomAllocations); err != nil {
		return nil, err
	}
	for _, a := range roomAllocations {
		if err := roomOccupancy.AddRoomAllocation(a.ID, a.ResidentialRoomID, a.FirstNight, a.LastNight); err != nil {
			return nil, err
		}
	}

	occupants := []dbRoomOccupant{}
	if err := fetchRoomOccupants(ctx, tx, eventModuleID, &occupants); err != nil {
		return nil, err
	}
	for _, o := range occupants {
		if err := roomOccupancy.AddRoomOccupant(o.ResidentialRoomID, o.PersonID, o.PersonSequenceID, o.Date); err != nil {
			return nil, err
		}
	}

	return roomOccupancy, nil
}

func newRoomIndex() roomIndex {
	return roomIndex{
		Allocations: []RoomAllocation{},
		Occupants:   map[time.Time]Occupants{},
	}
}

//AddRoomAllocation adds a room allocation, if the room doesn't exist an error is returned
func (ro *RoomOccupancy) AddRoomAllocation(id, roomID int, firstNight, lastNight time.Time) error {
	_, ok := ro.roomIndex[roomID]
	if !ok {
		ro.roomIndex[roomID] = newRoomIndex()
	}

	index := ro.roomIndex[roomID]
	index.Allocations = append(index.Allocations, RoomAllocation{
		ID:         id,
		RoomID:     roomID,
		FirstNight: firstNight,
		LastNight:  lastNight,
	})

	ro.roomIndex[roomID] = index
	return nil
}

//GetAllAllocations gets all room allocations.
func (ro *RoomOccupancy) GetAllAllocations() []RoomAllocation {
	allocations := []RoomAllocation{}
	for _, a := range ro.roomIndex {
		allocations = append(allocations, a.Allocations...)
	}

	return allocations
}

//GetAllocationsForRoom gets all room allocations for the given room.
//The room might be allocated in two separate chunks.
//e.g the first allocation runs from 1st->10th March, and a second allocation runs from 15th->20th March
func (ro *RoomOccupancy) GetAllocationsForRoom(roomID int) ([]RoomAllocation, error) {
	index := ro.roomIndex[roomID]

	return index.Allocations, nil
}

//GetAllocationForDates gets the allocation for the given room that contains the selected dates
func (ro *RoomOccupancy) GetAllocationForDates(roomID int, firstNight, lastNight time.Time) (*RoomAllocation, error) {
	roomIndex, ok := ro.roomIndex[roomID]
	if !ok {
		return nil, errNoRoom(roomID)
	}
	for _, a := range roomIndex.Allocations {
		firstNightOk := a.FirstNight.Before(firstNight) || a.FirstNight.Equal(firstNight)
		lastNightOk := a.LastNight.After(lastNight) || a.LastNight.Equal(lastNight)
		if firstNightOk && lastNightOk {
			return &a, nil
		}
	}

	return nil, nil
}

//IsRoomOnRoomingList returns true or false depending on if the room is on the rooming list or not
//The room is considered to be on the rooming list if a room allocation exists for those dates
func (ro *RoomOccupancy) IsRoomOnRoomingList(roomID int, firstNight, lastNight time.Time) (bool, error) {
	allocation, err := ro.GetAllocationForDates(roomID, firstNight, lastNight)
	if err != nil {
		return false, err
	}

	if allocation != nil {
		return true, nil
	}

	return false, nil
}

//AddRoomOccupant adds a new occupant to the given room for the given date
//If the room doesn't exist then an error is returned
//If the person is real (not -99999) then the Occupancy AllocatedCount is also updated
//If the person is fake (-99999) then the maxPersonSequenceID is compared to the fake persons SequenceID and updated if it's larger
func (ro *RoomOccupancy) AddRoomOccupant(roomID, personID, personSequenceID int, date time.Time) error {
	_, ok := ro.roomIndex[roomID]
	if !ok {
		ro.roomIndex[roomID] = newRoomIndex()
	}
	index := ro.roomIndex[roomID]
	_, dateExists := index.Occupants[date]
	if !dateExists {
		index.Occupants[date] = Occupants{
			Occupants: []RoomOccupant{},
		}
	}

	newOccupant := RoomOccupant{
		PersonID:         personID,
		PersonSequenceID: personSequenceID,
		RoomID:           roomID,
		Date:             date,
	}
	//If the room is on the rooming list, and the AllocatedCount of the room is 0, this means we need to update the fake -99999 record instead of adding a new one
	occupants := index.Occupants[date]
	if occupants.Allocated == 0 && len(occupants.Occupants) != 0 {
		occupants.Occupants[0] = newOccupant
	} else {
		occupants.Occupants = append(occupants.Occupants, newOccupant)
	}

	//Only update the AllocatedCount if it's a real person. Person -99999 is not real, and only added to keep backoffice working
	if personID != -99999 {
		occupants.Allocated++
	}
	//The reserved count is the total number of occupants, including people with ID -99999 that represent reserved spaces
	occupants.Reserved = len(occupants.Occupants)
	if personID == -99999 && personSequenceID > ro.MaxPersonSequenceID {
		ro.MaxPersonSequenceID = personSequenceID
	}

	index.Occupants[date] = occupants
	ro.roomIndex[roomID] = index
	return nil
}

//GetRoomOccupants gets all room occupants for the given room and date
func (ro *RoomOccupancy) GetRoomOccupants(roomID int, date time.Time) (Occupants, error) {
	occupants := Occupants{
		Occupants: []RoomOccupant{},
	}

	index, ok := ro.roomIndex[roomID]
	if !ok {
		return occupants, nil
	}

	_, dateExists := index.Occupants[date]
	if !dateExists {
		return occupants, nil
	}

	return index.Occupants[date], nil
}

//DeleteRoomAllocation deletes the given room allocation from the room
func (ro *RoomOccupancy) DeleteRoomAllocation(roomID int, deleteFrom, deleteTo time.Time) error {
	index, ok := ro.roomIndex[roomID]
	if !ok {
		return errNoRoomAllocation(roomID)
	}

	for i, a := range index.Allocations {
		if a.FirstNight.Equal(deleteFrom) && a.LastNight.Equal(deleteTo) {
			index.Allocations = removeAllocation(index.Allocations, i)
			ro.roomIndex[roomID] = index
			break
		}

		// if the date range is "inside" the allocation, we need to split
		// the existing allocation into two
		if deleteTo.Before(a.LastNight) && deleteFrom.After(a.FirstNight) {
			index.Allocations = removeAllocation(index.Allocations, i)
			ro.roomIndex[roomID] = index

			ro.AddRoomAllocation(0, roomID, a.FirstNight, deleteFrom.AddDate(0, 0, -1))
			ro.AddRoomAllocation(0, roomID, deleteTo.AddDate(0, 0, 1), a.LastNight)
			break
		}

		// if date we want to deleteTo is before the lastNight of the allocation
		// then we just need to update the first night
		if deleteTo.Before(a.LastNight) && (deleteFrom.Equal(a.FirstNight)) {
			index.Allocations[i].FirstNight = deleteTo.AddDate(0, 0, 1)
			ro.roomIndex[roomID] = index
			break
		}

		// if date we want to deleteFrom is after the firstNight of the allocation
		// then we just need to update the last night
		if deleteFrom.After(a.FirstNight) && (deleteTo.Equal(a.LastNight)) {
			index.Allocations[i].LastNight = deleteFrom.AddDate(0, 0, -1)
			ro.roomIndex[roomID] = index
			break
		}
	}

	return nil
}

//DeleteRoomOccupant deletes the given occupant from the room
//If the room doesn't exist, or the occupant can't be found then an error is returned
//If deleting the person results in an empty room, then if the room is ont he rooming list a fake person with id -99999 is created
func (ro *RoomOccupancy) DeleteRoomOccupant(roomID, personID, personSequenceID int, date time.Time) error {
	index, ok := ro.roomIndex[roomID]
	if !ok {
		return errNoRoom(roomID)
	}

	_, dateExists := index.Occupants[date]
	if !dateExists {
		return fmt.Errorf("no occupant exists for roomID %v on date %v with personID %v and personSequenceID %v", roomID, date, personID, personSequenceID)
	}
	occupants := index.Occupants[date]
	for i, o := range occupants.Occupants {
		if o.PersonID == personID && o.PersonSequenceID == personSequenceID {
			occupants.Occupants = removeOccupant(occupants.Occupants, i)
			occupants.Allocated--
			break
		}
	}

	//The reserved count is the total number of occupants, including people with ID -99999 that represent reserved spaces
	occupants.Reserved = len(occupants.Occupants)

	index.Occupants[date] = occupants
	ro.roomIndex[roomID] = index

	//If we are left with 0 occupants, and the room has a matching allocation for this date, then we need to add a new personID with -99999
	//This is because backOffice always required rooms that have allocations to have at least 1 occupant. So if there are no real occupants, then a fake occupant with personID -99999 must be used
	isRoomOnRoomingList, err := ro.IsRoomOnRoomingList(roomID, date, date)
	if err != nil {
		return err
	}
	if isRoomOnRoomingList && occupants.Allocated == 0 {
		ro.AddRoomOccupant(roomID, -99999, ro.MaxPersonSequenceID+1, date)
	}

	return nil
}

//PersistRoomOccupants takes the room occupants in memory and persists them to the database
//The database is updated to match the contents of the in memory array, so it's important to make sure the in memory array matches the real world!
func (ro *RoomOccupancy) PersistRoomOccupants(ctx context.Context, tx *sqlx.Tx) error {
	syncroreq := make([]syncRoomOccupantRequest, 0)
	for _, ri := range ro.roomIndex {
		for _, ro := range ri.Occupants {
			for _, o := range ro.Occupants {
				syncroreq = append(syncroreq, syncRoomOccupantRequest{
					PersonID:          o.PersonID,
					PersonSequenceID:  o.PersonSequenceID,
					ResidentialRoomID: o.RoomID,
					Date:              o.Date,
				})
			}
		}
	}
	return syncRoomOccupants(ctx, tx, ro.EventModuleID, syncroreq)
}

//PersistRoomAllocations takes the room allocations in memory and persists them to the database
//The database is updated to match the contents of the in memory array, so it's important to make sure the in memory array matches the real world!
func (ro *RoomOccupancy) PersistRoomAllocations(ctx context.Context, tx *sqlx.Tx, userID int, timestamp time.Time) error {
	syncrareq := make([]syncRoomAllocationsRequest, 0)
	for _, ri := range ro.roomIndex {
		for _, ra := range ri.Allocations {
			syncrareq = append(syncrareq, syncRoomAllocationsRequest{
				ID:                ra.ID,
				ResidentialRoomID: ra.RoomID,
				FirstNight:        ra.FirstNight,
				LastNight:         ra.LastNight,
				CreationUserID:    userID,
				CreationDate:      timestamp,
			})
		}
	}
	return syncRoomAllocations(ctx, tx, ro.EventModuleID, syncrareq)
}

func errNoRoom(roomID int) error {
	return fmt.Errorf("no room exists for roomID %v", roomID)
}

func errNoRoomAllocation(roomID int) error {
	return fmt.Errorf("no room allocation exists for roomID %v", roomID)
}

func removeOccupant(slice []RoomOccupant, s int) []RoomOccupant {
	//re-slicing like this is a relatively expensive operation, so it might require optimisation if it causes any performance problems
	return append(slice[:s], slice[s+1:]...)
}

func removeAllocation(slice []RoomAllocation, s int) []RoomAllocation {
	//re-slicing like this is a relatively expensive operation, so it might require optimisation if it causes any performance problems
	return append(slice[:s], slice[s+1:]...)
}

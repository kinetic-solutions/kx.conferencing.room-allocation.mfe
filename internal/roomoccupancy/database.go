package roomoccupancy

import (
	"context"
	"database/sql"
	"encoding/xml"
	"fmt"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

type dbResidentialRoomAllocation struct {
	ID                int       `db:"EMRRAID"`
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	FirstNight        time.Time `db:"FirstNight"`
	LastNight         time.Time `db:"LastNight"`
}

// fetchRoomAllocation gets all the roomingList of the given event
func fetchRoomAllocation(c context.Context, tx *sqlx.Tx, eventModuleID int, ra *[]dbResidentialRoomAllocation) error {
	return xray.Capture(c, "attendees.getRoomAllocations", func(ctx context.Context) error {
		return tx.SelectContext(ctx, ra, `
	SELECT
		EMRRA.EMRRAID,
		EMRRA.ResidentialRoomID,
		EMRRA.FirstNight,
		EMRRA.LastNight
	FROM EventModuleResidentialRoomAllocation EMRRA
	WHERE EMRRA.EventModuleID = @EventModuleID
`, sql.Named("EventModuleID", eventModuleID))
	})
}

type dbRoomOccupant struct {
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	PersonID          int       `db:"PersonID"`
	Date              time.Time `db:"Date"`
	PersonSequenceID  int       `db:"PersonSequenceID"`
}

// fetchRoomOccupants gets all the roomOccupants of the given event
func fetchRoomOccupants(c context.Context, tx *sqlx.Tx, eventModuleID int, dest *[]dbRoomOccupant) error {
	return xray.Capture(c, "attendees.getRoomOccupants", func(ctx context.Context) error {
		err := tx.SelectContext(ctx, dest, `
	SELECT
		RRO.ResidentialRoomID,
		RRO.PersonID,
		RRO.Date,
		RRO.PersonSequenceID
	FROM ResidentialRoomOccupants RRO
	WHERE
		RRO.EventModuleID = @EventModuleID
`,
			sql.Named("EventModuleID", eventModuleID),
		)
		return err
	})
}

type syncRoomOccupantRequest struct {
	PersonID          int       `xml:"PersonID,attr"`
	ResidentialRoomID int       `xml:"ResidentialRoomID,attr"`
	PersonSequenceID  int       `xml:"PersonSequenceID,attr"`
	Date              time.Time `xml:"Date,attr"`
}

func syncRoomOccupants(c context.Context, tx *sqlx.Tx, eventModuleID int, requests []syncRoomOccupantRequest) error {
	return xray.Capture(c, "attendees.syncRoomOccupants", func(ctx context.Context) error {
		xmlString, err := xml.Marshal(requests)
		if err != nil {
			return fmt.Errorf("marshalling insertRoomOccupants requests to xml: %w", err)
		}

		sqlQuery := `
		DECLARE @XML XML
		SET @XML = @XMLSTRING;

		WITH ro AS
		(
		SELECT
		ResidentialRoomID, EventModuleID, PersonID, PersonSequenceID, Date, BedIndex, BookingStatus, PaidDeposit, SingleOccupant, WakeupCallTime, Notes
		FROM ResidentialRoomOccupants
		WHERE EventModuleID = @EventModuleID
		)
		MERGE ro AS Target
		USING ( SELECT
			a.value('./@ResidentialRoomID','int') ResidentialRoomID,
			@EventModuleID EventModuleID,
			a.value('./@PersonID','int') PersonID,
			a.value('./@PersonSequenceID','int') PersonSequenceID,
			a.value('./@Date','datetime') Date
		FROM @XML.nodes('/syncRoomOccupantRequest') AS x(a )
		)	AS Source
		ON Source.EventModuleID = Target.EventModuleID
			AND Source.ResidentialRoomID = Target.ResidentialRoomID
			AND Source.Date = Target.Date
			AND Source.PersonID = Target.PersonID
			AND Source.PersonSequenceID = Target.PersonSequenceID
		-- For Inserts
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (ResidentialRoomID, EventModuleID, PersonID, PersonSequenceID, Date, BedIndex, BookingStatus, PaidDeposit, SingleOccupant, WakeupCallTime, Notes)
			VALUES (ResidentialRoomID, EventModuleID, PersonID, PersonSequenceID, Date, 0,        '',            0,           0,              0,              '')

		-- For Deletes
		WHEN NOT MATCHED BY SOURCE THEN DELETE;
		`

		_, err = tx.ExecContext(ctx, sqlQuery,
			sql.Named("XMLSTRING", xmlString),
			sql.Named("EventModuleID", eventModuleID))
		if err != nil {
			return err
		}

		return nil
	})
}

type syncRoomAllocationsRequest struct {
	ID                int       `xml:"EMRRAID,attr"`
	ResidentialRoomID int       `xml:"ResidentialRoomID,attr"`
	FirstNight        time.Time `xml:"FirstNight,attr"`
	LastNight         time.Time `xml:"LastNight,attr"`
	CreationUserID    int       `xml:"CreationUserID,attr"`
	CreationDate      time.Time `xml:"CreationDate,attr"`
}

func syncRoomAllocations(c context.Context, tx *sqlx.Tx, eventModuleID int, requests []syncRoomAllocationsRequest) error {
	return xray.Capture(c, "attendees.syncRoomAllocations", func(ctx context.Context) error {
		xmlString, err := xml.Marshal(requests)
		if err != nil {
			return fmt.Errorf("marshalling syncRoomAllocations requests to xml: %w", err)
		}

		sqlQuery := `
		DECLARE @XML XML
		SET @XML = @XMLSTRING;

		WITH ra AS
		(
		SELECT
		EMRRAID, EventModuleID, ResidentialRoomID, FirstNight, LastNight, CreationDate, CreationUserID
		FROM EventModuleResidentialRoomAllocation
		WHERE EventModuleID = @EventModuleID
		)
		MERGE ra AS Target
		USING ( SELECT
			a.value('./@EMRRAID','int') EMRRAID,
			@EventModuleID EventModuleID,
			a.value('./@ResidentialRoomID','int') ResidentialRoomID,
			a.value('./@FirstNight','datetime') FirstNight,
			a.value('./@LastNight','datetime') LastNight,
			a.value('./@CreationUserID','int') CreationUserID,
			a.value('./@CreationDate','datetime') CreationDate
		FROM @XML.nodes('/syncRoomAllocationsRequest') AS x(a )
		)	AS Source
		ON Source.EMRRAID = Target.EMRRAID
		-- For Inserts
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (EventModuleID, ResidentialRoomID, FirstNight, LastNight, CreationUserID, CreationDate)
			VALUES (EventModuleID, ResidentialRoomID, FirstNight, LastNight, CreationUserID, CreationDate)
		
		-- For Updates
		WHEN MATCHED THEN UPDATE SET
			Target.FirstNight = Source.FirstNight,
			Target.LastNight  = Source.LastNight

		-- For Deletes
		WHEN NOT MATCHED BY SOURCE THEN DELETE;
		`

		_, err = tx.ExecContext(ctx, sqlQuery,
			sql.Named("XMLSTRING", xmlString),
			sql.Named("EventModuleID", eventModuleID))
		if err != nil {
			return err
		}

		return nil
	})
}

// Package getattendee gets an attendee for an event.
package getattendee

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/eventmodule"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/lambdacontext"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/roomoccupancy"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

const eventIDPathParam, attendeeIDPathParam = "eventID", "attendeeID"

// New returns a LambdaFunc that responds to API Gateway requests
// by returning the event attendees from GERM.
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.GetAttendee", func(ctx context.Context) error {
		lambdaCtx := &lambdacontext.RoomAllocationLambdaContext{
			LambdaContext: lc,
		}

		eventID, err := lambdaCtx.GetEventID()
		if err != nil {
			return invalidEventID(err.Error())
		}

		attendeeID, err := parseAttendeeID(lc.Request.PathParameters)
		if err != nil {
			return err
		}

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}
		defer tx.Rollback()

		event := &eventmodule.EventModule{}
		if err := getEvent(ctx, eventID, tx, event); err != nil {
			return err
		}

		ro, err := roomoccupancy.NewFromDatabase(ctx, tx, event.EventModuleID)
		allocations := ro.GetAllAllocations()
		hasRoomingList := len(allocations) > 0

		log.Debugw("reading event attendee", "eventId", eventID)
		attendee := dbAttendee{}
		err = getAttendee(lc.Context(), tx, event.EventModuleID, attendeeID, &attendee)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				return attendeeNotFound(attendeeID)
			}
			return err
		}
		attrNames := &AttributeNames{}
		if err := getAttributeNames(ctx, event, tx, attrNames); err != nil {
			return fmt.Errorf("reading attribute names from database: %w", err)
		}

		roomAllocation := &dbRoomAllocation{}
		err = getRoomAllocation(lc.Context(), tx, event.EventModuleID, attendeeID, roomAllocation)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Debugw("this attendee hasn't got a room allocated")
				return lc.JSON(http.StatusOK, attendee.toUI(attrNames, nil, hasRoomingList))
			} else {
				return err
			}
		}

		if err := getRoomOccupants(ctx, event, tx, roomAllocation.ResidentialRoomID, &attendee, &roomAllocation.Occupied); err != nil {
			return fmt.Errorf("reading occupants from database: %w", err)
		}
		return lc.JSON(http.StatusOK, attendee.toUI(attrNames, roomAllocation, hasRoomingList))
	})
}

// getAttendee queries the database for the list of attendees
func getAttendee(ctx context.Context, tx *sqlx.Tx, eventModuleID int, attendeeId int, row *dbAttendee) error {
	return xray.Capture(ctx, "query.getEventAttendee", func(c context.Context) error {
		return tx.GetContext(c, row, sqlSelectAttendee, sql.Named("EventModuleID", eventModuleID), sql.Named("AttendeeID", attendeeId))
	})
}

// getRoomAloocation queries the database for the room allocation for the given attendee
func getRoomAllocation(ctx context.Context, tx *sqlx.Tx, eventModuleID int, attendeeId int, row *dbRoomAllocation) error {
	return xray.Capture(ctx, "query.getRoomAllocation", func(c context.Context) error {
		return tx.GetContext(c, row, sqlSelectRoomAllocation,
			sql.Named("EventModuleID", eventModuleID),
			sql.Named("AttendeeID", attendeeId))
	})
}

// getRoomOccupants gets the room occupants from the database
func getRoomOccupants(ctx context.Context, event *eventmodule.EventModule, tx *sqlx.Tx, roomID int, attendee *dbAttendee, dest *int) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := attendee.ArrivalDate
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := attendee.DepartureDate.AddDate(0, 0, -1)
	err := tx.GetContext(ctx, dest, sqlGetRoomOccupants,
		sql.Named("eventModuleID", event.EventModuleID),
		sql.Named("roomID", roomID),
		sql.Named("arrival", firstNight),
		sql.Named("departure", lastNight))

	return err
}

// getEvent gets the event from the database
func getEvent(ctx context.Context, eventID int, tx *sqlx.Tx, dest *eventmodule.EventModule) error {
	sqlErr := tx.GetContext(ctx, dest, "SELECT EventModuleID, EventID FROM EventModules WHERE EventID = @eventID", sql.Named("eventID", eventID))
	if sqlErr == sql.ErrNoRows {
		return eventNotFound(eventID)
	}
	return sqlErr
}

// invalidEventID returns a request error with a bad request message and status code 400
func invalidEventID(msg string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusBadRequest,
		Message: "Bad Request",
		Fields:  []lambdawrapper.FieldError{{Field: eventIDPathParam, Error: msg}},
	}
}

// invalidAttendeeID returns a request error with a bad request message and status code 400
func invalidAttendeeID(msg string) lambdawrapper.RequestError {
	return lambdawrapper.RequestError{
		Status:  http.StatusBadRequest,
		Message: "Bad Request",
		Fields:  []lambdawrapper.FieldError{{Field: attendeeIDPathParam, Error: msg}},
	}
}

// eventNotFound returns a request error with a not found message and status code 404
func eventNotFound(id int) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: fmt.Sprintf("event %d not found", id),
	}
}

// attendeeNotFound returns a request error with a not found message and status code 404
func attendeeNotFound(id int) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: fmt.Sprintf("attendee %d not found", id),
	}
}

// parseAttendeeID parses the path parameters to get the attendee ID
func parseAttendeeID(params map[string]string) (int, error) {
	raw, ok := params[attendeeIDPathParam]
	if !ok {
		return 0, invalidAttendeeID("attendee id is required")
	}

	id, err := strconv.Atoi(raw)
	if err != nil {
		return 0, invalidAttendeeID("attendee id must be an integer")
	}

	if id < 1 {
		return 0, invalidAttendeeID("attendee id must be greater than 0")
	}

	return id, nil
}

// getAttributeNames gets the names of the attributes that the customer has configured
func getAttributeNames(ctx context.Context, event *eventmodule.EventModule, tx *sqlx.Tx, dest *AttributeNames) error {
	sqlGetCustomSettings := `
	SELECT Value FROM Customsettings where customsetting = @CustomSetting
	`
	//VIP
	if err := tx.GetContext(ctx, &dest.VIP, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.VIP.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.VIP = "VIP"
		} else {
			return err
		}
	}
	//Smoking
	if err := tx.GetContext(ctx, &dest.Smoking, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.Smoking.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.Smoking = "Smoking"
		} else {
			return err
		}
	}
	//Disabled
	if err := tx.GetContext(ctx, &dest.Disabled, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.Disabled.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.Disabled = "Disabled"
		} else {
			return err
		}
	}
	//LastLet
	if err := tx.GetContext(ctx, &dest.LastLet, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.LastLet.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.LastLet = "Last Let"
		} else {
			return err
		}
	}
	return nil
}

package getattendee

import (
	"database/sql"
	"fmt"
	"time"
)

// uiTime is a custom type we can use to control the json representation of the date
// the UI requires the date to not have a timezone, but by default a Z is added to indicate UTC
type uiTime time.Time

func (t uiTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("2006-01-02T15:04:05"))
	return []byte(stamp), nil
}

type uiAttribute struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type pulseEvent struct {
	EventID        int  `json:"id"`
	SiteID         int  `json:"siteId"`
	HasRoomingList bool `json:"hasRoomingList"`
}

type uiRow struct {
	ID             int               `json:"id"`
	Forename       string            `json:"forename"`
	Surname        string            `json:"surname"`
	ArrivalDate    uiTime            `json:"arrival"`
	DepartureDate  uiTime            `json:"departure"`
	Block          *block            `json:"block"`
	BedroomType    *idname           `json:"bedroomType"`
	Attributes     []uiAttribute     `json:"attributes"`
	Event          pulseEvent        `json:"event"`
	RoomAllocation *uiRoomAllocation `json:"roomAllocation,omitempty"`
	IsCheckedIn    bool              `json:"isCheckedIn"`
}
type uiRoomAllocation struct {
	BedroomID       int           `json:"bedroomID"`
	BedroomName     string        `json:"bedroomName"`
	BlockID         int           `json:"blockID"`
	BlockName       string        `json:"blockName"`
	SubBlockID      int           `json:"subBlockID"`
	SubBlockName    string        `json:"subBlockName"`
	BedroomTypeID   int           `json:"bedroomTypeID"`
	BedroomType     string        `json:"bedroomType"`
	AreaID          int           `json:"areaID"`
	SiteID          int           `json:"siteID"`
	ActualCapacity  int           `json:"actualCapacity"`
	TypicalCapacity int           `json:"typicalCapacity"`
	Occupied        int           `json:"occupied"`
	Status          string        `json:"status"`
	Attributes      []uiAttribute `json:"attributes"`
	IsFlat          bool          `json:"isFlat"`
}
type idname struct {
	Id   int32  `json:"id"`
	Name string `json:"name"`
}

type block struct {
	Id     int32  `json:"id"`
	Name   string `json:"name"`
	AreaID int32  `json:"areaId"`
	SiteID int32  `json:"siteId"`
}

type dbRoomAllocation struct {
	ResidentialRoomID int    `db:"ResidentialRoomID"`
	BedroomName       string `db:"BedroomName"`
	SiteID            int    `db:"SiteID"`
	AreaID            int    `db:"AreaID"`
	BlockID           int    `db:"BlockID"`
	BlockName         string `db:"BlockName"`
	SubBlockID        int    `db:"SubBlockID"`
	SubBlockName      string `db:"SubBlockName"`
	BedroomTypeID     int    `db:"BedroomTypeID"`
	BedroomType       string `db:"BedroomType"`
	ActualCapacity    int    `db:"Capacity"`
	TypicalCapacity   int    `db:"TypicalCapacity"`
	MaidStatus        string `db:"MaidStatus"`
	VIP               bool   `db:"VIP"`
	Disabled          bool   `db:"Disabled"`
	Smoking           bool   `db:"Smoking"`
	LastLet           bool   `db:"LastLet"`
	Occupied          int    `db:"Occupied"`
	BookByRoom        bool   `db:"BookByRoom"`
	RoomInFlat        bool   `db:"RoomInFlat"`
}
type dbAttendee struct {
	EventModuleDelegateID int            `db:"EventModuleDelegateID"`
	Forename              string         `db:"Forename"`
	Surname               string         `db:"Surname"`
	ArrivalDate           time.Time      `db:"ArrivalDate"`
	DepartureDate         time.Time      `db:"DepartureDate"`
	CheckedInDate         sql.NullTime   `db:"CheckedInDate"`
	VIP                   bool           `db:"VIP"`
	Disabled              bool           `db:"Disabled"`
	Smoker                bool           `db:"Smoker"`
	EventID               int            `db:"EventID"`
	SiteID                int            `db:"SiteID"`
	BlockSiteID           sql.NullInt32  `db:"BlockSiteID"`
	BlockAreaID           sql.NullInt32  `db:"BlockAreaID"`
	BlockID               sql.NullInt32  `db:"BlockID"`
	BlockName             sql.NullString `db:"BlockName"`
	BedroomTypeID         sql.NullInt32  `db:"BedroomTypeID"`
	BedroomTypeName       sql.NullString `db:"BedroomTypeName"`
}

//AttributeNames stores the custom names given to each attribute.
//The customer can override these names in the database
type AttributeNames struct {
	Smoking  string
	VIP      string
	Disabled string
	LastLet  string
}

func (r *dbRoomAllocation) toUI(attrNames *AttributeNames) *uiRoomAllocation {
	roomAllocation := uiRoomAllocation{
		BedroomID:       r.ResidentialRoomID,
		BedroomName:     r.BedroomName,
		BlockID:         r.BlockID,
		BlockName:       r.BlockName,
		SubBlockID:      r.SubBlockID,
		SubBlockName:    r.SubBlockName,
		BedroomTypeID:   r.BedroomTypeID,
		BedroomType:     r.BedroomType,
		AreaID:          r.AreaID,
		SiteID:          r.SiteID,
		ActualCapacity:  r.ActualCapacity,
		TypicalCapacity: r.TypicalCapacity,
		Status:          r.MaidStatus,
		Attributes:      []uiAttribute{},
		Occupied:        r.Occupied,
	}
	if r.Smoking {
		roomAllocation.Attributes = append(roomAllocation.Attributes, uiAttribute{Key: "smoking", Value: attrNames.Smoking})
	}

	if r.Disabled {
		roomAllocation.Attributes = append(roomAllocation.Attributes, uiAttribute{Key: "disabled", Value: attrNames.Disabled})
	}

	if r.VIP {
		roomAllocation.Attributes = append(roomAllocation.Attributes, uiAttribute{Key: "vip", Value: attrNames.VIP})
	}

	if r.LastLet {
		roomAllocation.Attributes = append(roomAllocation.Attributes, uiAttribute{Key: "lastLet", Value: attrNames.LastLet})
	}

	roomAllocation.IsFlat = r.BookByRoom || r.RoomInFlat
	return &roomAllocation
}

// toUI converts a database row to a UI Row
func (r *dbAttendee) toUI(attrNames *AttributeNames, roomAllocation *dbRoomAllocation, hasRoomingList bool) uiRow {
	var b *block = nil
	if r.BlockID.Valid && r.BlockName.Valid {
		b = &block{
			Id:     r.BlockID.Int32,
			Name:   r.BlockName.String,
			AreaID: r.BlockAreaID.Int32,
			SiteID: r.BlockSiteID.Int32,
		}
	}

	var bedtype *idname = nil
	if r.BedroomTypeID.Valid && r.BedroomTypeName.Valid {
		bedtype = &idname{
			Id:   r.BedroomTypeID.Int32,
			Name: r.BedroomTypeName.String,
		}
	}

	event := pulseEvent{
		EventID:        r.EventID,
		SiteID:         r.SiteID,
		HasRoomingList: hasRoomingList,
	}

	row := uiRow{
		ID:            r.EventModuleDelegateID,
		Forename:      r.Forename,
		Surname:       r.Surname,
		ArrivalDate:   uiTime(r.ArrivalDate),
		DepartureDate: uiTime(r.DepartureDate),
		Block:         b,
		BedroomType:   bedtype,
		Attributes:    []uiAttribute{},
		Event:         event,
	}
	if r.Smoker {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "smoking", Value: attrNames.Smoking})
	}

	if r.Disabled {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "disabled", Value: attrNames.Disabled})
	}

	if r.VIP {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "vip", Value: attrNames.VIP})
	}

	if roomAllocation != nil {
		row.RoomAllocation = roomAllocation.toUI(attrNames)
	}

	if r.CheckedInDate.Valid {
		// ignore if the checkedindate is a germ null date (1899-12-31)
		t, _ := time.Parse("2006-01-02", "1970-01-01")
		if r.CheckedInDate.Time.After(t) {
			row.IsCheckedIn = true
		}
	}

	return row
}

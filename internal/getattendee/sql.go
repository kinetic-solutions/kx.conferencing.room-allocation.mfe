package getattendee

const (
	sqlSelectAttendee = `
	SELECT
		EMD.EventModuleDelegateID,
		P.Forename,
		P.Surname,
		P.VIP as VIP,
		P.Disabled as Disabled,
		P.Smoker as Smoker,
		EMD.ArrivalDate,
		EMD.DepartureDate,
		EMD.CheckedInDate,
		EMD.BlockID,
		EM.EventID,
		EM.SiteID,
		SB.SiteID AS BlockSiteID,
		SB.AreaID AS BlockAreaID,
		RTRIM(SB.Name) AS BlockName,
		EMD.BedroomTypeID,
		RTRIM(BT.Description) AS BedroomTypeName
	FROM
		EventModuleDelegates EMD
	JOIN People P ON P.PersonID = EMD.PersonID
	JOIN EventModules EM ON EM.EventModuleID = EMD.EventModuleID
	LEFT JOIN SiteBlocks SB ON SB.BlockID = EMD.BlockID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = EMD.BedroomTypeID
	WHERE
		EMD.EventModuleID = @EventModuleID
		AND EMD.EventModuleDelegateID = @AttendeeID
	`

	sqlSelectRoomAllocation = `
	SELECT
	RR.ResidentialRoomID,
	RR.Name AS BedroomName,
	RR.BedroomTypeID,
	BT.Description AS BedroomType,
	SB.SiteID,
	SB.AreaID,
	RR.BlockID,
	LTRIM(RTRIM(SB.Name)) AS BlockName,
	RR.SubBlockID,
	ISNULL(LTRIM(RTRIM(SSB.Name)), '') AS SubBlockName,
	RR.TypicalCapacity,
	RR.Capacity,
	RR.Disabled,
	RR.Smoking,
	RR.VIP,
	RR.LastLet,
	ISNULL(BT.RoomInFlat, 0) AS RoomInFlat,
	ISNULL(BT.BookByRoom, 0) AS BookByRoom,
	LTRIM(RTRIM(RR.MaidStatus)) as MaidStatus
	FROM ResidentialRoomOccupants RRO
	JOIN EventModuleDelegates EMD ON RRO.PersonID = EMD.PersonID AND RRO.PersonSequenceID = EMD.PersonSequenceID AND EMD.EventModuleID = RRO.EventModuleID
	LEFT JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON RR.BedroomTypeID = BT.BedroomTypeID
	LEFT JOIN SiteBlocks SB on SB.BlockID = RR.BlockID
	LEFT JOIN SiteBlocks SSB ON SSB.BlockID = RR.SubBlockID
	WHERE EMD.EventModuleID = @EventModuleID
	AND EMD.EventModuleDelegateID = @AttendeeID
	AND RRO.Date = CAST(DATEADD(day, -1, EMD.DepartureDate) as Date)
	`
	sqlGetRoomOccupants = `
	SELECT MAX(count) AS Max FROM
	(SELECT
		COUNT(RRO.PersonId) AS Count
		FROM ResidentialRoomOccupants RRO
		WHERE RRO.EventModuleID = @EventModuleID
		AND Date BETWEEN CAST(@Arrival as Date) and CAST(@Departure as Date)
		AND RRO.ResidentialRoomID = @RoomID
		GROUP BY RRO.Date
	) a;
	`
)

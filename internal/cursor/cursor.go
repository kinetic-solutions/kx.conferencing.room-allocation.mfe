// Package cursor provides helper functions to create and parse cursors
package cursor

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

var (
	// define the base64 encoding that should be used for all operations
	enc = base64.RawURLEncoding
)

// Create creates and encodes a Cursor using JSON
// The generated cursors are encoded as base64
func Create(data interface{}) (string, error) {
	payload, err := json.Marshal(data)

	if err != nil {
		return "", fmt.Errorf("cannot encode cursor: %w", err)
	}

	return enc.EncodeToString(payload), nil
}

// Parse converts the cursor backinto its original form.
func Parse(raw string, result interface{}) error {
	decoded, err := enc.DecodeString(raw)

	if err != nil {
		return err
	}

	if err := json.Unmarshal(decoded, result); err != nil {
		return fmt.Errorf("cannot decode JSON: %w", err)
	}

	return nil
}

// Package getreservationrooms gets all rooms with a reservation.
package getreservationrooms

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/cursor"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/eventmodule"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/lambdacontext"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

// New returns a LambdaFunc that responds to API Gateway requests
func New(lc *lambdawrapper.LambdaContext) error {
	log, db := lc.Log, lc.DB
	log.Infow("parsing request")

	return xray.Capture(lc.Context(), "roomAllocation.GetReservationRooms", func(ctx context.Context) error {
		lambdaCtx := &lambdacontext.RoomAllocationLambdaContext{
			LambdaContext: lc,
		}

		eventID, err := lambdaCtx.GetEventID()
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "eventID", Error: err.Error()}},
			}
		}

		query := &getReservationsRoomQuery{}
		if err := parseQuery(lambdaCtx.Request.QueryStringParameters, query); err != nil {
			return err
		}
		log.Infow("parsed query parameters", "arrival", query.Arrival, "departure", query.Departure)

		tx, err := db.BeginTxx(lc.Context(), &sql.TxOptions{Isolation: sql.LevelReadCommitted})
		if err != nil {
			return fmt.Errorf("cannot start read transaction: %w", err)
		}

		event := &eventmodule.EventModule{}
		if err := getEvent(ctx, eventID, tx, event); err != nil {
			return err
		}

		rooms := []dbRoomRow{}
		hasMoreRooms := false
		if err := getReservationRooms(ctx, event, tx, query, &rooms, &hasMoreRooms); err != nil {
			return fmt.Errorf("reading rooms from database: %w", err)
		}
		log.Infow("finished reading rooms from database", "count", len(rooms))

		outOfService := []dbOutOfServiceRoom{}
		if err := getOutOfServiceRooms(ctx, event, tx, query, &outOfService); err != nil {
			return fmt.Errorf("reading out of service rooms from database: %w", err)
		}

		//Create a map from the out of service rooms to speed up lookup
		outOfServiceIndex := map[int][]dbOutOfServiceRoom{}
		for _, v := range outOfService {
			_, ok := outOfServiceIndex[v.ResidentialRoomID]
			if !ok {
				outOfServiceIndex[v.ResidentialRoomID] = []dbOutOfServiceRoom{}
			}
			outOfServiceIndex[v.ResidentialRoomID] = append(outOfServiceIndex[v.ResidentialRoomID], v)
		}

		availableRooms := []dbRoomRow{}
		for _, r := range rooms {
			_, isOutOfService := outOfServiceIndex[r.ResidentialRoomID]
			if !isOutOfService {
				availableRooms = append(availableRooms, r)
			}
		}

		attrNames := &AttributeNames{}
		if err := getAttributeNames(ctx, event, tx, attrNames); err != nil {
			return fmt.Errorf("reading attribute names from database: %w", err)
		}

		defer tx.Rollback()
		mappedRooms := mapRows(ctx, availableRooms, attrNames)

		//Construct the API response
		apiResponse := apiResponse{Rooms: mappedRooms}
		//Add the cursor if there are more rooms in the database that haven't been fetched yet
		if hasMoreRooms {
			query.Page = query.Page + 1
			cursor, err := cursor.Create(query)
			if err != nil {
				return err
			}

			apiResponse.Next = cursor
		}
		return lc.JSON(http.StatusOK, apiResponse)
	})
}

// eventNotFound returns a request error with a not found message and status code 404
func eventNotFound(id int) error {
	return lambdawrapper.RequestError{
		Status:  http.StatusNotFound,
		Message: fmt.Sprintf("event %d not found", id),
	}
}

// getEvent gets the event from the database
func getEvent(ctx context.Context, eventID int, tx *sqlx.Tx, dest *eventmodule.EventModule) error {
	sqlErr := tx.GetContext(ctx, dest, "SELECT EventModuleID, EventID FROM EventModules WHERE EventID = @eventID", sql.Named("eventID", eventID))
	if sqlErr == sql.ErrNoRows {
		return eventNotFound(eventID)
	}
	return sqlErr
}

// getReservationRooms gets the list of rooms that have a bedroom reservation from the database
func getReservationRooms(ctx context.Context, event *eventmodule.EventModule, tx *sqlx.Tx, query *getReservationsRoomQuery, dest *[]dbRoomRow, hasMoreRooms *bool) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := query.Arrival
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := query.Departure.AddDate(0, 0, -1)
	rooms := []dbRoomRow{}
	//Incrementing the pageSize by 1 will allow us to check if there are more rooms past the
	//current pageSize that may need fetching in the future
	pageSize := query.PageSize + 1

	if err := tx.SelectContext(ctx, &rooms, sqlReservationRooms,
		sql.Named("EventModuleID", event.EventModuleID),
		sql.Named("FirstNight", firstNight),
		sql.Named("LastNight", lastNight),
		sql.Named("includeOccupied", query.IncludeOccupied),
		sql.Named("roomName", query.RoomName),
		sql.Named("siteID", query.SiteID),
		sql.Named("areaID", query.AreaID),
		sql.Named("blockID", query.BlockID),
		sql.Named("subBlockID", query.SubBlockID),
		sql.Named("bedroomTypeID", query.BedroomTypeID),
		sql.Named("smoking", query.Smoking),
		sql.Named("vip", query.VIP),
		sql.Named("disabled", query.Disabled),
		sql.Named("lastLet", query.LastLet),
		sql.Named("maidStatus", query.MaidStatus),
		sql.Named("Offset", (query.Page-1)*query.PageSize),
		sql.Named("PageSize", pageSize)); err != nil {
		return err
	}

	//Since we query the data using pageSize + 1 if the rows we got back are more than the expected pageSize then there must me more rooms available
	//If this happens, we should let the API know to return a cursor to fetch them
	if len(rooms) > query.PageSize {
		*hasMoreRooms = true
		//We can throw away the last element in the array, since we fetched one more than necessary
		*dest = rooms[:len(rooms)-1]
	} else {
		*dest = rooms
	}
	return nil
}

// getOutOfServiceRooms gets the list of room ids that are out of service
func getOutOfServiceRooms(ctx context.Context, event *eventmodule.EventModule, tx *sqlx.Tx, query *getReservationsRoomQuery, dest *[]dbOutOfServiceRoom) error {
	//The first night the room is available needs to be the night the attendee arrives
	firstNight := query.Arrival
	//The last night the room is available needs to be the night before the attendee departs
	lastNight := query.Departure.AddDate(0, 0, -1)

	err := tx.SelectContext(ctx, dest, sqlGetOutOfServiceRooms,
		sql.Named("firstNight", firstNight),
		sql.Named("lastNight", lastNight))

	return err
}

// getAttributeNames gets the names of the attributes that the customer has configured
func getAttributeNames(ctx context.Context, event *eventmodule.EventModule, tx *sqlx.Tx, dest *AttributeNames) error {
	sqlGetCustomSettings := `
	SELECT Value FROM Customsettings where customsetting = @CustomSetting
	`
	//VIP
	if err := tx.GetContext(ctx, &dest.VIP, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.VIP.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.VIP = "VIP"
		} else {
			return err
		}
	}
	//Smoking
	if err := tx.GetContext(ctx, &dest.Smoking, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.Smoking.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.Smoking = "Smoking"
		} else {
			return err
		}
	}
	//Disabled
	if err := tx.GetContext(ctx, &dest.Disabled, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.Disabled.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.Disabled = "Disabled"
		} else {
			return err
		}
	}
	//LastLet
	if err := tx.GetContext(ctx, &dest.LastLet, sqlGetCustomSettings, sql.Named("CustomSetting", "Kx.Room.Attribute.LastLet.Text")); err != nil {
		if err == sql.ErrNoRows {
			dest.LastLet = "Last Let"
		} else {
			return err
		}
	}
	return nil
}

//mapRows maps a database row to a ui row
func mapRows(ctx context.Context, rows []dbRoomRow, attrNames *AttributeNames) []uiRow {
	items := []uiRow{}
	xray.Capture(ctx, "transform.mapRows", func(c context.Context) error {
		for i := 0; i < len(rows); i++ {
			items = append(items, rows[i].toUI(attrNames))
		}
		return nil
	})
	return items
}

//parseQuery parses and validates the query string parameters into a struct
//if the arrival or departure are missing, then an error is returned
//if the arrival or departure are not valid dates, then an error is returned
func parseQuery(queryParams map[string]string, dest *getReservationsRoomQuery) error {
	rawCursor, ok := queryParams["cursor"]
	if ok {
		return cursor.Parse(rawCursor, dest)
	}

	//if there's no cursor then we must be on page 1
	dest.Page = 1

	arrivalString, ok := queryParams["arrival"]
	if !ok {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "arrival", Error: "arrival is required"}},
		}
	}

	departureString, ok := queryParams["departure"]
	if !ok {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "departure", Error: "departure is required"}},
		}
	}

	const dateLayout = "2006-01-02"
	arrival, error := time.Parse(dateLayout, arrivalString)
	if error != nil {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "arrival", Error: "arrival is not a date"}},
		}
	}

	departure, error := time.Parse(dateLayout, departureString)
	if error != nil {
		return lambdawrapper.RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  []lambdawrapper.FieldError{{Field: "departure", Error: "departure is not a date"}},
		}
	}

	dest.Arrival = arrival
	dest.Departure = departure

	pageSizeString, ok := queryParams["pageSize"]
	//Page size is not required, so only try to parse it if it exists
	if ok {
		pageSize, err := strconv.Atoi(pageSizeString)
		if err != nil {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "pageSize", Error: "pageSize must be an integer"}},
			}
		}

		if pageSize < 1 {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "pageSize", Error: "pageSize must be greater than 0"}},
			}
		}
		dest.PageSize = pageSize
	} else {
		//If it wasn't set then set a default
		dest.PageSize = 100
	}

	//parse includeOccupied - should default to true
	includeOccupied, err := parseBooleanParameter(queryParams, "includeOccupied")
	if err != nil {
		return err
	}
	if includeOccupied == nil {
		//Default to true
		dest.IncludeOccupied = true
	} else {
		dest.IncludeOccupied = *includeOccupied
	}

	//parse roomName - optional
	//must be greater than 0 characters
	roomNameString, ok := queryParams["roomName"]
	if ok {
		if len(roomNameString) == 0 {
			return lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: "roomName", Error: "roomName must have length greater than 0"}},
			}
		}
		dest.RoomName = &roomNameString
	}

	//parse siteID - optional
	siteID, err := parseIntegerParameter(queryParams, "siteID")
	if err != nil {
		return err
	}
	dest.SiteID = siteID

	//parse areaID - optional
	areaID, err := parseIntegerParameter(queryParams, "areaID")
	if err != nil {
		return err
	}
	dest.AreaID = areaID

	//parse blockID - optional
	blockID, err := parseIntegerParameter(queryParams, "blockID")
	if err != nil {
		return err
	}
	dest.BlockID = blockID

	//parse subBlockID - optional
	subBlockID, err := parseIntegerParameter(queryParams, "subBlockID")
	if err != nil {
		return err
	}
	dest.SubBlockID = subBlockID

	//parse bedroomTypeID - optional
	bedroomTypeID, err := parseIntegerParameter(queryParams, "bedroomTypeID")
	if err != nil {
		return err
	}
	dest.BedroomTypeID = bedroomTypeID

	//parse smoking - optional
	smoking, err := parseBooleanParameter(queryParams, "smoking")
	if err != nil {
		return err
	}
	dest.Smoking = smoking

	//parse vip - optional
	vip, err := parseBooleanParameter(queryParams, "vip")
	if err != nil {
		return err
	}
	dest.VIP = vip

	//parse disabled - optional
	disabled, err := parseBooleanParameter(queryParams, "disabled")
	if err != nil {
		return err
	}
	dest.Disabled = disabled

	//parse lastLet - optional
	lastLet, err := parseBooleanParameter(queryParams, "lastLet")
	if err != nil {
		return err
	}
	dest.LastLet = lastLet

	//parse maidStatus - optional
	maidStatus, ok := queryParams["maidStatus"]
	if ok {
		dest.MaidStatus = &maidStatus
	}

	return nil
}

func parseIntegerParameter(queryParams map[string]string, field string) (*int, error) {
	strVal, ok := queryParams[field]
	if ok {
		val, err := strconv.Atoi(strVal)
		if err != nil {
			return nil, lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: field, Error: fmt.Sprintf("%s must be an integer", field)}},
			}
		}
		return &val, nil
	}

	return nil, nil
}

func parseBooleanParameter(queryParams map[string]string, field string) (*bool, error) {
	strVal, ok := queryParams[field]
	if ok {
		val, err := strconv.ParseBool(strVal)
		if err != nil {
			return nil, lambdawrapper.RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields:  []lambdawrapper.FieldError{{Field: field, Error: fmt.Sprintf("%s must be a boolean", field)}},
			}
		}
		return &val, nil
	}

	return nil, nil
}

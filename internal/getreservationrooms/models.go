package getreservationrooms

import (
	"database/sql"
	"time"
)

type getReservationsRoomQuery struct {
	Arrival   time.Time `json:"arrival"`
	Departure time.Time `json:"departure"`
	//Whether to include occupied rooms in the response, defaults to true
	IncludeOccupied bool    `json:"includeOccupied"`
	RoomName        *string `json:"roomName,omitempty"`
	SiteID          *int    `json:"siteID,omitempty"`
	AreaID          *int    `json:"areaID,omitempty"`
	BlockID         *int    `json:"blockID,omitempty"`
	SubBlockID      *int    `json:"subBlockID,omitempty"`
	BedroomTypeID   *int    `json:"bedroomTypeID,omitempty"`
	Smoking         *bool   `json:"smoking,omitempty"`
	VIP             *bool   `json:"vip,omitempty"`
	Disabled        *bool   `json:"disabled,omitempty"`
	LastLet         *bool   `json:"lastLet,omitempty"`
	MaidStatus      *string `json:"maidStatus,omitempty"`
	PageSize        int     `json:"pageSize"`
	Page            int     `json:"page"`
}
type apiResponse struct {
	Rooms []uiRow `json:"rooms"`
	Next  string  `json:"next,omitempty"`
}
type uiAttribute struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
type uiRow struct {
	BedroomID       int           `json:"bedroomID"`
	BedroomName     string        `json:"bedroomName"`
	BlockID         int           `json:"blockID"`
	BlockName       string        `json:"blockName"`
	SubBlockID      int           `json:"subBlockID"`
	SubBlockName    string        `json:"subBlockName"`
	BedroomTypeID   int           `json:"bedroomTypeID"`
	BedroomType     string        `json:"bedroomType"`
	AreaID          int           `json:"areaID"`
	SiteID          int           `json:"siteID"`
	ActualCapacity  int           `json:"actualCapacity"`
	TypicalCapacity int           `json:"typicalCapacity"`
	Occupied        int           `json:"occupied"`
	Status          string        `json:"status"`
	Attributes      []uiAttribute `json:"attributes"`
}
type dbRoomRow struct {
	ResidentialRoomID   int           `db:"ResidentialRoomID"`
	BedroomName         string        `db:"BedroomName"`
	SiteID              int           `db:"SiteID"`
	AreaID              int           `db:"AreaID"`
	BlockID             int           `db:"BlockID"`
	BlockName           string        `db:"BlockName"`
	SubBlockID          int           `db:"SubBlockID"`
	SubBlockName        string        `db:"SubBlockName"`
	BedroomTypeID       int           `db:"BedroomTypeID"`
	BedroomType         string        `db:"BedroomType"`
	ActualCapacity      int           `db:"Capacity"`
	TypicalCapacity     int           `db:"TypicalCapacity"`
	MaidStatus          string        `db:"MaidStatus"`
	VIP                 bool          `db:"VIP"`
	Disabled            bool          `db:"Disabled"`
	Smoking             bool          `db:"Smoking"`
	LastLet             bool          `db:"LastLet"`
	SiteBlockSequenceNo int           `db:"SBSequenceNo"`
	SubBlockSequenceNo  sql.NullInt32 `db:"SSBSequenceNo"`
	DisplayOrder        int           `db:"DisplayOrder"`
	Occupied            int           `db:"Occupied"`
}

type dbOutOfServiceRoom struct {
	ResidentialRoomID int `db:"ResidentialRoomID"`
}

//AttributeNames stores the custom names given to each attribute.
//The customer can override these names in the database
type AttributeNames struct {
	Smoking  string
	VIP      string
	Disabled string
	LastLet  string
}

// toUI converts a database row to a UI Row
func (r *dbRoomRow) toUI(attrNames *AttributeNames) uiRow {
	row := uiRow{
		BedroomID:       r.ResidentialRoomID,
		BedroomName:     r.BedroomName,
		BlockID:         r.BlockID,
		BlockName:       r.BlockName,
		SubBlockID:      r.SubBlockID,
		SubBlockName:    r.SubBlockName,
		BedroomTypeID:   r.BedroomTypeID,
		BedroomType:     r.BedroomType,
		AreaID:          r.AreaID,
		SiteID:          r.SiteID,
		ActualCapacity:  r.ActualCapacity,
		TypicalCapacity: r.TypicalCapacity,
		Occupied:        r.Occupied,
		Status:          r.MaidStatus,
		Attributes:      []uiAttribute{},
	}

	if r.MaidStatus == "" {
		row.Status = "C"
	}

	if r.Smoking {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "smoking", Value: attrNames.Smoking})
	}

	if r.Disabled {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "disabled", Value: attrNames.Disabled})
	}

	if r.VIP {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "vip", Value: attrNames.VIP})
	}

	if r.LastLet {
		row.Attributes = append(row.Attributes, uiAttribute{Key: "lastLet", Value: attrNames.LastLet})
	}

	return row
}

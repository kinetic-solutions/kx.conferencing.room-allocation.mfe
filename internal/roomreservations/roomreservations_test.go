package roomreservations

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestAddAllocation(t *testing.T) {
	t.Run("if reservation already exists return error", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		id := 10
		blockID := 1
		bedroomTypeID := 5
		allocation := 100
		roomReservation.AddReservation(id, blockID, bedroomTypeID, date, allocation)
		r.EqualError(roomReservation.AddReservation(id, blockID, bedroomTypeID, date, allocation), "reservation for blockID 1, bedroomTypeID 5 already exists for date")
	})
	t.Run("can add a reservation and retrieve it", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5
		allocation := 100
		roomReservation.AddReservation(10, blockID, bedroomTypeID, date, allocation)

		reservation, _ := roomReservation.GetReservation(blockID, bedroomTypeID, date)
		r.Equal(&RoomReservation{ID: 10, BlockID: blockID, BedroomTypeID: bedroomTypeID, Allocation: allocation, Date: date}, reservation)
	})
	t.Run("can add a reservations for different dates on the same bedroom type and block", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		firstDate, _ := time.Parse("2006-01-02", "2022-01-01")
		secondDate, _ := time.Parse("2006-01-03", "2022-01-01")
		id := 10
		blockID := 1
		bedroomTypeID := 5
		allocation := 100
		roomReservation.AddReservation(id, blockID, bedroomTypeID, firstDate, allocation)
		r.NoError(roomReservation.AddReservation(id, blockID, bedroomTypeID, secondDate, allocation))
	})
}

func TestGetAllocation(t *testing.T) {
	t.Run("if no allocation exists then null is returned", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		reservation, _ := roomReservation.GetReservation(blockID, bedroomTypeID, date)
		r.Nil(reservation)
	})
}

func TestUpdateReservation(t *testing.T) {
	t.Run("if no reservation exists then an error is returned", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		r.EqualError(roomReservation.UpdateReservation(blockID, bedroomTypeID, date, 10), "reservation does not exist for blockID 1, bedroomTypeID 5 on that date")
	})

	t.Run("can update the allocation of an existing reservation", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		id := 10
		blockID := 1
		bedroomTypeID := 5
		roomReservation.AddReservation(id, blockID, bedroomTypeID, date, 100)
		roomReservation.UpdateReservation(blockID, bedroomTypeID, date, 200)

		reservation, _ := roomReservation.GetReservation(blockID, bedroomTypeID, date)
		r.Equal(&RoomReservation{BlockID: blockID, ID: id, BedroomTypeID: bedroomTypeID, Allocation: 200, Date: date}, reservation)
	})
}

func TestAddOccupiedRoom(t *testing.T) {
	t.Run("can add occupied rooms and retrieve them", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		roomReservation.AddOccupiedRoom(10, blockID, bedroomTypeID, date)
		roomReservation.AddOccupiedRoom(11, blockID, bedroomTypeID, date)

		occupied, _ := roomReservation.GetOccupiedRooms(blockID, bedroomTypeID, date)
		r.Equal([]OccupiedRoom{
			{ID: 10, Date: date, BlockID: blockID, BedroomTypeID: bedroomTypeID},
			{ID: 11, Date: date, BlockID: blockID, BedroomTypeID: bedroomTypeID},
		}, occupied)
	})

	t.Run("can add occupied rooms to a date that already has a reservation", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		roomReservation.AddReservation(10, blockID, bedroomTypeID, date, 10)
		roomReservation.AddOccupiedRoom(11, blockID, bedroomTypeID, date)

		occupied, _ := roomReservation.GetOccupiedRooms(blockID, bedroomTypeID, date)
		r.Equal([]OccupiedRoom{
			{ID: 11, Date: date, BlockID: blockID, BedroomTypeID: bedroomTypeID},
		}, occupied)
	})
}
func TestGetOccupiedRooms(t *testing.T) {
	t.Run("if no occupied rooms exist then an empty array is returned", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		occupied, _ := roomReservation.GetOccupiedRooms(blockID, bedroomTypeID, date)
		r.Equal([]OccupiedRoom{}, occupied)
	})
}

func TestSpareCapacity(t *testing.T) {
	t.Run("if the number of reservations matches the number of occupied rooms for a given date", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5
		//Allocate 2 reservations
		roomReservation.AddReservation(1, blockID, bedroomTypeID, date, 2)
		//a reservation for a differnet blockID shouldn't affect the results
		roomReservation.AddReservation(2, blockID+1, bedroomTypeID, date, 2)
		//a reservation for a differnet bedroomType shouldn't affect the results
		roomReservation.AddReservation(2, blockID, bedroomTypeID+1, date, 2)
		//a reservation for a different date shouldn't affect the results
		roomReservation.AddReservation(3, blockID, bedroomTypeID, date.AddDate(0, 0, 1), 2)

		//2 Occupied rooms
		roomReservation.AddOccupiedRoom(1, blockID, bedroomTypeID, date)
		roomReservation.AddOccupiedRoom(2, blockID, bedroomTypeID, date)
		spare, _ := roomReservation.SpareCapacity(blockID, bedroomTypeID, date)
		//Zero spare
		r.Equal(0, spare)
	})

	t.Run("if the number of reservations is greater than the number of occupied rooms for a given date", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		//Allocate 10 reservations
		roomReservation.AddReservation(1, blockID, bedroomTypeID, date, 10)
		//2 Occupied rooms
		roomReservation.AddOccupiedRoom(1, blockID, bedroomTypeID, date)
		roomReservation.AddOccupiedRoom(2, blockID, bedroomTypeID, date)
		spare, _ := roomReservation.SpareCapacity(blockID, bedroomTypeID, date)
		//Eight spare
		r.Equal(8, spare)
	})

	t.Run("if the number of reservations is less than the number of occupied rooms for a given date", func(t *testing.T) {
		r := require.New(t)

		roomReservation := New(11)

		date, _ := time.Parse("2006-01-02", "2022-01-01")
		blockID := 1
		bedroomTypeID := 5

		//Allocate 1 reservations
		roomReservation.AddReservation(1, blockID, bedroomTypeID, date, 1)
		//2 Occupied rooms
		roomReservation.AddOccupiedRoom(1, blockID, bedroomTypeID, date)
		roomReservation.AddOccupiedRoom(2, blockID, bedroomTypeID, date)
		spare, _ := roomReservation.SpareCapacity(blockID, bedroomTypeID, date)
		//1 over-allocated
		r.Equal(-1, spare)
	})
}

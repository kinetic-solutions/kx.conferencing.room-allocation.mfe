package roomreservations

import (
	"context"
	"database/sql"
	"encoding/xml"
	"fmt"
	"time"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
)

type dbEventModuleBlockBedroomAllocation struct {
	EMBBAID       int       `db:"EMBBAID"`
	BlockID       int       `db:"BlockID"`
	BedroomTypeID int       `db:"BedroomTypeID"`
	Date          time.Time `db:"Date"`
	Allocation    int       `db:"Allocation"`
}

// fetchRoomReservations gets all the room reservations of the given event
func fetchRoomReservations(c context.Context, tx *sqlx.Tx, eventModuleID int, rr *[]dbEventModuleBlockBedroomAllocation) error {
	return xray.Capture(c, "attendees.getRoomReservations", func(ctx context.Context) error {
		return tx.SelectContext(ctx, rr, `
	SELECT
		EMBBAID.EMBBAID,
		EMBBAID.BlockID,
		EMBBAID.BedroomTypeID,
		EMBBAID.Date,
		EMBBAID.Allocation
	FROM EventModuleBlockBedroomAllocation EMBBAID
	WHERE EMBBAID.EventModuleID = @EventModuleID
`, sql.Named("EventModuleID", eventModuleID))
	})
}

type dbOccupiedRoom struct {
	ResidentialRoomID int       `db:"ResidentialRoomID"`
	BlockID           int       `db:"BlockID"`
	BedroomTypeID     int       `db:"BedroomTypeID"`
	Date              time.Time `db:"Date"`
}

// fetchOccupiedRooms gets all the occupied rooms for the event
func fetchOccupiedRooms(c context.Context, tx *sqlx.Tx, eventModuleID int, rr *[]dbOccupiedRoom) error {
	return xray.Capture(c, "attendees.fetchOccupiedRooms", func(ctx context.Context) error {
		return tx.SelectContext(ctx, rr, `
	SELECT DISTINCT 
		RRO.ResidentialRoomID, 
		RR.BedroomTypeID,
		RR.BlockID, 
		RRO.Date
	FROM ResidentialRoomOccupants  RRO
	LEFT JOIN ResidentialRooms RR ON RRO.ResidentialRoomID = RR.ResidentialRoomID
	WHERE RRO.EventModuleID = @EventModuleID
	AND RRO.PersonID <> -99999
`, sql.Named("EventModuleID", eventModuleID))
	})
}

type syncRoomReservationRequest struct {
	EMBBAID       int       `xml:"EMBBAID,attr"`
	BlockID       int       `xml:"BlockID,attr"`
	BedroomTypeID int       `xml:"BedroomTypeID,attr"`
	Allocation    int       `xml:"Allocation,attr"`
	Date          time.Time `xml:"Date,attr"`
}

func syncRoomReservations(c context.Context, tx *sqlx.Tx, eventModuleID int, requests []syncRoomReservationRequest) error {
	return xray.Capture(c, "attendees.syncRoomReservations", func(ctx context.Context) error {
		xmlString, err := xml.Marshal(requests)
		if err != nil {
			return fmt.Errorf("marshalling syncRoomReservationRequest requests to xml: %w", err)
		}

		sqlQuery := `
		DECLARE @XML XML
		SET @XML = @XMLSTRING;

		WITH ro AS
		(
		SELECT
		EMBBAID, BlockID, BedroomTypeID, Allocation, Date, EventModuleID
		FROM EventModuleBlockBedroomAllocation
		WHERE EventModuleID = @EventModuleID
		)
		MERGE ro AS Target
		USING ( SELECT
			a.value('./@EMBBAID','int') EMBBAID,
			@EventModuleID EventModuleID,
			a.value('./@BlockID','int') BlockID,
			a.value('./@BedroomTypeID','int') BedroomTypeID,
			a.value('./@Allocation','int') Allocation,
			a.value('./@Date','datetime') Date
		FROM @XML.nodes('/syncRoomReservationRequest') AS x(a )
		)	AS Source
		ON Source.EMBBAID = Target.EMBBAID
		-- For Inserts
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (EventModuleID, BlockID, BedroomTypeID, Allocation, Date)
			VALUES (EventModuleID, BlockID, BedroomTypeID, Allocation, Date)

		-- For Updates
		WHEN MATCHED THEN UPDATE SET
			Target.Allocation = Source.Allocation

		-- For Deletes
		WHEN NOT MATCHED BY SOURCE THEN DELETE;
		`

		_, err = tx.ExecContext(ctx, sqlQuery,
			sql.Named("XMLSTRING", xmlString),
			sql.Named("EventModuleID", eventModuleID))
		if err != nil {
			return err
		}

		return nil
	})
}

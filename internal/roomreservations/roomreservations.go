package roomreservations

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type OccupiedRoom struct {
	ID            int
	Date          time.Time
	BlockID       int
	BedroomTypeID int
}
type RoomReservation struct {
	ID            int
	BlockID       int
	BedroomTypeID int
	Allocation    int
	Date          time.Time
	Status        string
}

type RoomReservations struct {
	EventModuleID    int
	reservationIndex map[blockBedroomTypeKey]reservationIndex
}
type blockBedroomTypeKey struct {
	blockID       int
	bedroomTypeID int
}
type reservationIndex struct {
	OccupiedRooms map[time.Time][]OccupiedRoom
	Reservations  map[time.Time]RoomReservation
}

//New makes a new RoomReservations
func New(eventModuleID int) *RoomReservations {
	return &RoomReservations{
		EventModuleID:    eventModuleID,
		reservationIndex: map[blockBedroomTypeKey]reservationIndex{},
	}
}

func newIndex() reservationIndex {
	return reservationIndex{
		Reservations:  map[time.Time]RoomReservation{},
		OccupiedRooms: map[time.Time][]OccupiedRoom{},
	}
}

//NewFromDatabase makes a new RoomReservations using the given transaction to get the required data from the database
func NewFromDatabase(ctx context.Context, tx *sqlx.Tx, eventModuleID int) (*RoomReservations, error) {
	roomReservations := New(eventModuleID)

	reservations := []dbEventModuleBlockBedroomAllocation{}
	if err := fetchRoomReservations(ctx, tx, eventModuleID, &reservations); err != nil {
		return nil, err
	}
	for _, a := range reservations {
		if err := roomReservations.AddReservation(a.EMBBAID, a.BlockID, a.BedroomTypeID, a.Date, a.Allocation); err != nil {
			return nil, err
		}
	}

	occupied := []dbOccupiedRoom{}
	if err := fetchOccupiedRooms(ctx, tx, eventModuleID, &occupied); err != nil {
		return nil, err
	}
	for _, o := range occupied {
		if err := roomReservations.AddOccupiedRoom(o.ResidentialRoomID, o.BlockID, o.BedroomTypeID, o.Date); err != nil {
			return nil, err
		}
	}

	return roomReservations, nil
}

//AddReservation adds a reservation to the RoomReservations
func (rr *RoomReservations) AddReservation(id, blockID, bedroomTypeID int, date time.Time, allocation int) error {
	newReservation := RoomReservation{
		ID:            id,
		BlockID:       blockID,
		BedroomTypeID: bedroomTypeID,
		Allocation:    allocation,
		Date:          date,
	}

	key := blockBedroomTypeKey{blockID: blockID, bedroomTypeID: bedroomTypeID}
	_, ok := rr.reservationIndex[key]
	if !ok {
		rr.reservationIndex[key] = newIndex()
	}

	index := rr.reservationIndex[key]
	_, dateExists := index.Reservations[date]
	if dateExists {
		return fmt.Errorf("reservation for blockID %v, bedroomTypeID %v already exists for date", blockID, bedroomTypeID)
	}

	rr.reservationIndex[key].Reservations[date] = newReservation
	return nil
}

//GetReservation gets the bedroom reservations for the given Block/BedroomTpye/date
//If a reservation doesn't exist, nil is returned
func (rr *RoomReservations) GetReservation(blockID, bedroomTypeID int, date time.Time) (*RoomReservation, error) {
	key := blockBedroomTypeKey{blockID: blockID, bedroomTypeID: bedroomTypeID}
	index := rr.reservationIndex[key]

	reservation, ok := index.Reservations[date]
	if !ok {
		return nil, nil
	}
	return &reservation, nil
}

//UpdateReservation updates a reservation
func (rr *RoomReservations) UpdateReservation(blockID, bedroomTypeID int, date time.Time, allocation int) error {
	key := blockBedroomTypeKey{blockID: blockID, bedroomTypeID: bedroomTypeID}
	index, ok := rr.reservationIndex[key]
	if !ok {
		return fmt.Errorf("reservation does not exist for blockID %v, bedroomTypeID %v on that date", blockID, bedroomTypeID)
	}

	reservation, dateExists := index.Reservations[date]
	if !dateExists {
		return fmt.Errorf("reservation does not exist for blockID %v, bedroomTypeID %v on that date", blockID, bedroomTypeID)
	}

	reservation.Allocation = allocation
	index.Reservations[date] = reservation
	rr.reservationIndex[key] = index
	return nil
}

//AddReservation adds a reservation to the RoomReservations
func (rr *RoomReservations) AddOccupiedRoom(id, blockID, bedroomTypeID int, date time.Time) error {
	newOccupiedRoom := OccupiedRoom{
		ID:            id,
		BlockID:       blockID,
		BedroomTypeID: bedroomTypeID,
		Date:          date,
	}

	key := blockBedroomTypeKey{blockID: blockID, bedroomTypeID: bedroomTypeID}
	_, ok := rr.reservationIndex[key]
	if !ok {
		rr.reservationIndex[key] = newIndex()
	}

	index := rr.reservationIndex[key]
	_, dateExists := index.OccupiedRooms[date]
	if !dateExists {
		index.OccupiedRooms[date] = []OccupiedRoom{}
	}

	rr.reservationIndex[key].OccupiedRooms[date] = append(rr.reservationIndex[key].OccupiedRooms[date], newOccupiedRoom)
	return nil
}

//GetOccupiedRooms gets the occupied rooms fo the given Block/BedroomType/date
//If no occupied rooms exist, an empty array is returned
func (rr *RoomReservations) GetOccupiedRooms(blockID, bedroomTypeID int, date time.Time) ([]OccupiedRoom, error) {
	key := blockBedroomTypeKey{blockID: blockID, bedroomTypeID: bedroomTypeID}
	index := rr.reservationIndex[key]

	occupied, ok := index.OccupiedRooms[date]
	if !ok {
		return []OccupiedRoom{}, nil
	}
	return occupied, nil
}

//SpareCapacity will calculate how much spare reservation capacity is left for the given block/bedroom/date
func (rr *RoomReservations) SpareCapacity(blockID, bedroomTypeID int, date time.Time) (int, error) {
	reserved, err := rr.GetReservation(blockID, bedroomTypeID, date)
	if err != nil {
		return 0, fmt.Errorf("getting reservations: %w", err)
	}
	occupied, err := rr.GetOccupiedRooms(blockID, bedroomTypeID, date)
	if err != nil {
		return 0, fmt.Errorf("getting occupied rooms: %w", err)
	}
	return reserved.Allocation - len(occupied), nil
}

//PersistRoomReservations takes the reservations in memory and persists them to the database
//The database is updated to match the contents of the in memory array, so it's important to make sure the in memory array matches the real world!
func (rr *RoomReservations) PersistRoomReservations(ctx context.Context, tx *sqlx.Tx) error {
	syncroreq := make([]syncRoomReservationRequest, 0)
	for _, ri := range rr.reservationIndex {
		for _, r := range ri.Reservations {
			syncroreq = append(syncroreq, syncRoomReservationRequest{
				EMBBAID:       r.ID,
				BlockID:       r.BlockID,
				BedroomTypeID: r.BedroomTypeID,
				Allocation:    r.Allocation,
				Date:          r.Date,
			})
		}
	}
	return syncRoomReservations(ctx, tx, rr.EventModuleID, syncroreq)
}

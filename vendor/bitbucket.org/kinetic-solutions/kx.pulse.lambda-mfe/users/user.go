package users

import (
	"encoding/json"
	"strings"

	"gopkg.in/square/go-jose.v2/jwt"
)

const (
	scopesSeperator = " "
)

// Scopes is a list of scopes defined on the token
type Scopes []string

// UnmarshalJSON converts the raw scopes to a list of scopes
func (s *Scopes) UnmarshalJSON(b []byte) (err error) {
	r, err := splitClaim(b, scopesSeperator)

	if err != nil {
		return
	}

	*s = Scopes(r)

	return
}

// Has checks that the user has all of the supplied scopes
func (s Scopes) Has(expected ...string) bool {
	lookup := map[string]bool{}

	for _, scope := range s {
		lookup[scope] = true
	}

	for _, required := range expected {
		if _, ok := lookup[required]; !ok {
			return false
		}
	}

	return true
}

// splitClaim takes a raw JSON string and splits it into a string array.
func splitClaim(raw []byte, sep string) ([]string, error) {
	s := ""

	if err := json.Unmarshal(raw, &s); err != nil {
		return nil, err
	}

	if s == "" {
		return []string{}, nil
	}

	return strings.Split(s, sep), nil
}

// User represents a decoded and parsed user Token
type User struct {
	// embed all the standard OIDC user claims
	jwt.Claims

	Scopes Scopes `json:"scope,omitempty"`
}

package lambdawrapper

import (
	"context"

	"github.com/aws/aws-xray-sdk-go/xray"
)

// tracing initialises request specific tracing
func tracing(name string) Middleware {
	fn := func(next ServeFunc) ServeFunc {
		return func(lc *LambdaContext) error {
			lc.Log.Debugw("creating trace context", "name", name)

			// create a new subsegment and update the context object
			return xray.Capture(lc.ctx, name, func(ctx context.Context) error {
				return next(lc.WithContext(ctx))
			})
		}
	}

	return fn
}

package lambdawrapper

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	krn "bitbucket.org/kinetic-solutions/kx.go.krn"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

var (
	validate   *validator.Validate
	translator ut.Translator
)

// init registers the valdation framework and any translation we require
func init() {
	validate = validator.New()

	// when creating the field names in validation errors we want to use the
	// json tag names, not the go field names.
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	// register english translations for validation errors
	en := en.New()
	uni := ut.New(en, en)
	translator, _ = uni.GetTranslator("en")
	en_translations.RegisterDefaultTranslations(validate, translator)

	// register custom type for KRN validation
	validate.RegisterCustomTypeFunc(krn.KRNValuer, krn.KRN{})
}

// ParseJSON parses the JSON string s into out. If the json conversion
// fails a Bad Request RequestError is returned. If the JSON data is
// successfully parsed out object will be validated. If a validation rules
// fail a Bad Request error is returned specifying which field
// failed validation. Unknown errors such as unhandled JSON conversion
// errors are returned as standard errors.
func ParseJSON(s string, out interface{}) error {
	// unmarshal the body into JSON capturing any syntax issues
	if err := json.Unmarshal([]byte(s), out); err != nil {
		switch v := err.(type) {
		case *json.SyntaxError:
			return RequestError{
				Status:  http.StatusBadRequest,
				Message: "Body is invalid JSON",
			}
		case *json.UnmarshalTypeError:
			return RequestError{
				Status:  http.StatusBadRequest,
				Message: "Bad Request",
				Fields: []FieldError{{
					Field: v.Field,
					Error: fmt.Sprintf("expected %s; got %s", v.Type.Name(), v.Value),
				}}}
		default:
			return fmt.Errorf("cannot marshal body: %w", err)
		}
	}

	// validate the outputted body
	err := validate.Struct(out)

	// if the validation failed convert the error into a request error. If there
	// are no validation errors return nil.
	switch v := err.(type) {
	case nil:
		return nil
	case *validator.InvalidValidationError:
		return fmt.Errorf("cannot validate message body: %w", v)
	case validator.ValidationErrors:
		fields := make([]FieldError, len(v))

		// translate each for field level the errors
		for i, e := range v {
			fields[i] = FieldError{Field: e.Field(), Error: e.Translate(translator)}
		}

		return RequestError{
			Status:  http.StatusBadRequest,
			Message: "Bad Request",
			Fields:  fields,
		}
	default:
		return fmt.Errorf("unknown error validating request: %w", err)
	}
}

package lambdawrapper

import (
	"errors"
	"fmt"
)

// AuthoriserKey holds the name of a key potentially stored inside
// the auth context
type AuthoriserKey string

var (
	// ErrAuthClaimNotFound is returned when a claim hasn't been passed
	// from the authoriser.
	ErrAuthClaimNotFound = errors.New("claim not found")

	AppName          = AuthoriserKey("appName")
	AppRoles         = AuthoriserKey("appRoles")
	TenantLocale     = AuthoriserKey("tenantLocale")
	TenantGermUserId = AuthoriserKey("tenantGermUserId")

	tenantId           = AuthoriserKey("tenantId")
	tenantCode         = AuthoriserKey("tenantCode")
	tenantJurisdiction = AuthoriserKey("tenantJurisdiction")
	tenantTimeZone     = AuthoriserKey("tenantTimeZone")
	tenantGermConn     = AuthoriserKey("tenantGermConn")
	user               = AuthoriserKey("user")
	sub                = AuthoriserKey("sub")
	tenantCurrencyCode = AuthoriserKey("tenantCurrencyCode")
)

// rawAuthClaims gives access methods to claims
// passed from the lambda authoriser
type rawAuthClaims map[string]interface{}

func (rac rawAuthClaims) string(key AuthoriserKey, allowEmpty bool) (string, error) {
	raw, exists := rac[string(key)]

	if !exists {
		if !allowEmpty {
			return "", ErrAuthClaimNotFound
		}
		return "", nil
	}

	val, ok := raw.(string)

	if !ok {
		return "", fmt.Errorf("cannot convert property type %t into a string", raw)
	}

	if val == "" && !allowEmpty {
		return "", errors.New("empty string passed")
	}

	return val, nil
}

// authContext is the parsed data created by the Lambda Authoriser
type authContext struct {
	// tenantCode is parsed from the URI
	tenantCode string

	// tenantID for the request
	tenantID string

	// tenantJurisdiction tenants jurisdiction
	tenantJurisdiction string

	// tenantTimeZone tenants timezone
	tenantTimeZone string

	// tenantLocale is the tenant's locale.
	tenantLocale string

	// tenantCurrencyCode is the tenant's currency code
	tenantCurrencyCode string

	// subject is the users Sub claim
	subject string

	// claims is the b64 encoded claims claims
	claims string

	// germConn is the connection string specific to the current tenancy
	germConn string

	// raw allows access to none required claims, such as the users GERM
	// user id, if its been passed
	raw rawAuthClaims
}

// parseAuthContext reads data set by the lambda authoriser
func parseAuthContext(next ServeFunc) ServeFunc {
	fn := func(lc *LambdaContext) error {
		authorizer := lc.Request.RequestContext.Authorizer
		log := lc.Log

		if authorizer == nil {
			return errors.New("request missing authorizer property")
		}

		data := authorizer.Lambda

		if data == nil {
			return errors.New("request missing lambda property")
		}

		res := &authContext{raw: rawAuthClaims(data)}

		values := []struct {
			key        AuthoriserKey
			writeTo    *string
			annotate   bool
			allowEmpty bool
		}{
			{key: tenantId, writeTo: &res.tenantID, annotate: true},
			{key: tenantCode, writeTo: &res.tenantCode, annotate: true},
			{key: tenantJurisdiction, writeTo: &res.tenantJurisdiction, annotate: true},
			{key: tenantTimeZone, writeTo: &res.tenantTimeZone, annotate: true},
			{key: TenantLocale, writeTo: &res.tenantLocale, annotate: true, allowEmpty: true},
			{key: tenantCurrencyCode, writeTo: &res.tenantCurrencyCode, annotate: true, allowEmpty: true},
			{key: tenantGermConn, writeTo: &res.germConn},
			{key: user, writeTo: &res.claims},
			{key: sub, writeTo: &res.subject, annotate: true},
		}

		log.Debugw("parsing authoriser context")

		for _, v := range values {
			val, err := res.raw.string(v.key, v.allowEmpty)

			if err != nil {
				return fmt.Errorf("cannot read key %q from authoriser context: %w", v.key, err)
			}

			if v.annotate {
				lc.Annotate(string(v.key), val)
			}

			*v.writeTo = val
		}

		lc.Log = log.With(
			"tenantId", res.tenantID,
			"user", res.subject,
		)

		lc.authContext = res

		return next(lc)
	}

	return fn
}

package lambdawrapper

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

// LambdaContext gives access to common request objects such as
// logging, GERM connection, and the request details.
type LambdaContext struct {
	// parent HACK: we're now making copies of the LambdaContext, rather than
	// using locking. This means we need a way to propegate the response up
	// the copies. It could be better to return the Response object from the handlers.
	parent      *LambdaContext
	authContext *authContext
	res         *events.APIGatewayV2HTTPResponse
	ctx         context.Context

	DB      *sqlx.DB
	Log     *zap.SugaredLogger
	Request *events.APIGatewayV2HTTPRequest
}

// Annotate can be used to add data to the tracing context. Developers must
// ensure the data doesn't contain Private or Sensitive information. The returned
// error is safe to ignore depending on the importance of the annotation.
func (lc *LambdaContext) Annotate(key string, value interface{}) error {
	log := lc.Log.With("key", key, "value", value)

	log.Debugw("creating annotation")

	if err := xray.AddAnnotation(lc.Context(), key, value); err != nil {
		log.Warnw("cannot store annotation", "err", err)
		return fmt.Errorf("annotating key %q failed: %w", key, err)
	}

	return nil
}

// Context returns the currently set context
func (lc *LambdaContext) Context() context.Context {
	return lc.ctx
}

// WithContext creates a clone of the current LambdaContext with the new
// context set.
func (lc *LambdaContext) WithContext(ctx context.Context) *LambdaContext {
	if ctx == nil {
		panic("context is required")
	}

	// clone the current LambdaContext and update the inner ctx
	lc2 := &LambdaContext{}
	*lc2 = *lc
	lc2.parent = lc
	lc2.ctx = ctx

	return lc2
}

// Tenancy returns details about the current requests tenancy
type Tenancy struct {
	// ID is the internal immutable Tenant ID
	ID string

	// Code is the friendly tenant code, e.g. paytest1
	Code string

	// Jurisdiction of the tenancy, e.g. UK
	Jurisdiction string

	// TimeZone is the TimeZone of the tenancy, e.g. Europe/London
	TimeZone string

	// Locale of the tenancy, e.g. en-US or en-GB
	Locale string

	// Currency code of the tenancy, e.g. JPY or GBP (default)
	CurrencyCode string
}

// Tenancy returns details about the current requests tenancy
func (lc *LambdaContext) Tenancy() Tenancy {
	return Tenancy{
		ID:           lc.authContext.tenantID,
		Code:         lc.authContext.tenantCode,
		Jurisdiction: lc.authContext.tenantJurisdiction,
		TimeZone:     lc.authContext.tenantTimeZone,
		Locale:       lc.authContext.tenantLocale,
		CurrencyCode: lc.authContext.tenantCurrencyCode,
	}
}

// AuthoriserStringValue allows access to known keys set by the Authoriser.
func (lc *LambdaContext) AuthoriserStringValue(key AuthoriserKey) (string, error) {
	return lc.authContext.raw.string(key, false)
}

// User decodes the requests user into u. A simple user struct
// can be found in users.User.
func (lc *LambdaContext) User(u interface{}) error {
	reader := strings.NewReader(lc.authContext.claims)
	b64Reader := base64.NewDecoder(base64.RawStdEncoding, reader)

	if err := json.NewDecoder(b64Reader).Decode(u); err != nil {
		return fmt.Errorf("cannot decode user JSON: %w", err)
	}

	return nil
}

// NoContent is a convenience method to return a 204 response with no body
func (lc *LambdaContext) NoContent() error {
	return lc.SetResponse(&events.APIGatewayV2HTTPResponse{
		StatusCode: http.StatusNoContent,
	})
}

// JSON creates a new API Gateway response setting the appropriate headers
// and status codes.
func (lc *LambdaContext) JSON(status int, body interface{}) error {
	return xray.Capture(lc.Context(), "json.response", func(ctx context.Context) error {
		b, err := json.Marshal(body)

		if err != nil {
			return fmt.Errorf("failed to marshal body: %w", err)
		}

		return lc.SetResponse(&events.APIGatewayV2HTTPResponse{
			StatusCode:      status,
			Body:            string(b),
			IsBase64Encoded: false,
			Headers: map[string]string{
				"Content-Type": "application/json",
			},
		})
	})
}

// SetResponse sets the response for the event
func (lc *LambdaContext) SetResponse(res *events.APIGatewayV2HTTPResponse) error {
	if res == nil {
		return fmt.Errorf("res is required")
	}

	if res.StatusCode < 100 || res.StatusCode >= 600 {
		return fmt.Errorf("status code invalid: %d", res.StatusCode)
	}

	if res.StatusCode != http.StatusNoContent && res.Body == "" {
		return fmt.Errorf("body required for none %d response codes", http.StatusNoContent)
	}

	// this object is potentially a copy of another context
	// so we need to propugate the response up the tree

	current := lc

	for current != nil {
		current.res, current = res, current.parent
	}

	return nil
}

// getResponse returns the set response. If no response has been set an error is returned.
func (lc *LambdaContext) getResponse() (*events.APIGatewayV2HTTPResponse, error) {
	// check that a response has been set
	if lc.res == nil {
		return nil, errors.New("no response set")
	}

	return lc.res, nil
}

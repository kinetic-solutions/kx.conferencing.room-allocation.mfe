package lambdawrapper

import (
	"fmt"

	"github.com/aws/aws-lambda-go/lambdacontext"
)

// requestID picks up the current request ID and sets
// it onto the logger
func requestID(next ServeFunc) ServeFunc {
	fn := func(lc *LambdaContext) error {
		lambdaCtx, ok := lambdacontext.FromContext(lc.Context())

		if !ok {
			return fmt.Errorf("context is not a AWS Lambda context")
		}

		lc.Log = lc.Log.With("requestId", lambdaCtx.AwsRequestID)

		return next(lc)
	}

	return fn
}

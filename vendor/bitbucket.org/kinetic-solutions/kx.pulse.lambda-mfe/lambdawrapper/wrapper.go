package lambdawrapper

import (
	"context"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"go.uber.org/zap"
)

// ServeFunc is the function that can be wrapped
// and returned as a Lambda function.
type ServeFunc func(*LambdaContext) error

// Middleware can be injected before the handler is ran. Each middleware
// has the opportunity to modify the request before the handler is executed.
type Middleware func(ServeFunc) ServeFunc

// A Handler responds to a Lambda event.
type Handler interface {
	Serve(*LambdaContext) error
}

// Lambda is the function signature that AWS expects to handle API Gateway
// requests. Our implementation will provide logging and sql server access to
// a delegate.
type Lambda func(ctx context.Context, event *events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error)

// Wrap takes the raw API gateway request, parses out expected properties,
// creates a tenant-specific database connection, and sets up logging.
func Wrap(log *zap.SugaredLogger, h Handler, mw ...Middleware) Lambda {
	return WrapFunc(log, h.Serve, mw...)
}

// WrapFunc takes a ServeFunc which is then used to handle the
// lambda request.
func WrapFunc(log *zap.SugaredLogger, serve ServeFunc, mw ...Middleware) Lambda {
	log.Debugw("creating middleware pipeline")

	// create the standard stack stack
	stack := []Middleware{
		unknownErrorHandler, tracing("mfe"), requestID,
		requestLog(time.Now), parseAuthContext, tenantDB,
	}

	// inject the supplied middleware before the handler
	stack = append(stack, mw...)

	// add the handler to the request pipeline
	stack = append(stack, tracing("mfe.handler"), requestErrorHandler)

	// add all the middleware functions. This is done in reverse
	// of the actual execution order.
	for i := len(stack) - 1; i >= 0; i-- {
		serve = stack[i](serve)
	}

	log.Debugw("created pipeline")

	// return the initial API gateway entrypoint
	return func(ctx context.Context, event *events.APIGatewayV2HTTPRequest) (*events.APIGatewayV2HTTPResponse, error) {
		log.Debugw("executing pipeline")

		lc := &LambdaContext{Request: event, Log: log, ctx: ctx}

		if err := serve(lc); err != nil {
			return nil, err
		}

		return lc.getResponse()
	}
}

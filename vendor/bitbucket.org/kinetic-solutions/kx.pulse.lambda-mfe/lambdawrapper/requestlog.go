package lambdawrapper

import "time"

func requestLog(now func() time.Time) func(next ServeFunc) ServeFunc {
	fn := func(next ServeFunc) ServeFunc {
		return func(lc *LambdaContext) error {
			reqCtx := lc.Request.RequestContext
			startedAt := now()

			defer func() {
				p := recover()

				var statusCode, bodyLength int

				if res, err := lc.getResponse(); err == nil {
					statusCode = res.StatusCode
					bodyLength = len(res.Body)
				} else {
					statusCode = 500
				}

				duration := now().Sub(startedAt)

				lc.Log.Infow("access-result",
					"method", reqCtx.HTTP.Method,
					"path", reqCtx.HTTP.Path,
					"protocol", reqCtx.HTTP.Protocol,
					"sourceIP", reqCtx.HTTP.SourceIP,
					"userAgent", reqCtx.HTTP.UserAgent,
					"routeKey", reqCtx.RouteKey,
					"statusCode", statusCode,
					"duration", duration.Milliseconds(),
					"bodyLen", bodyLength,
				)

				if p != nil {
					panic(p)
				}
			}()

			return next(lc)
		}
	}

	return fn
}

package lambdawrapper

import "fmt"

// RequestError allows downstream handlers to return an error
// that will be handled as a response message.
type RequestError struct {
	Status  int          `json:"-"`
	Message string       `json:"message"`
	Fields  []FieldError `json:"fields,omitempty"`
}

// FieldError describes an error with a specific field
type FieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// Error returns the error message
func (e RequestError) Error() string {
	return fmt.Sprintf("query returned HTTP status %d: %s", e.Status, e.Message)
}

// requestErrorHandler intercepts a bubbling known
// request errors and handles them. If the error is
// unknown it's allowed to bubble to the next middleware.
func requestErrorHandler(next ServeFunc) ServeFunc {
	fn := func(lc *LambdaContext) error {
		// call the next middleware, capturing any errors
		err := next(lc)

		if err == nil {
			// ensure that the response valid
			if _, err = lc.getResponse(); err == nil {
				return nil
			}
		}

		// developer has passed a request error so we can
		// convert it to JSON for them
		if v, ok := err.(RequestError); ok {
			return lc.JSON(v.Status, v)
		}

		// unknown error, allow it to bubble up the stack
		return err
	}

	return fn
}

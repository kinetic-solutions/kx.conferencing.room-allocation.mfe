package lambdawrapper

import (
	"database/sql"
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"

	_ "github.com/denisenkom/go-mssqldb"
)

// tenantDB fetches the MS-SQL database connection string from the request
// object and creates a new connection. Before passing the DB on the it's
// wrapped in AWS XRAY.
func tenantDB(next ServeFunc) ServeFunc {
	const driver = "sqlserver"

	// replaces the EF specific connection string properties with expected ones
	dsnReplacer := strings.NewReplacer("uid=", "user id=", "pwd=", "password=", "encrypt=false", "encrypt=disable")

	fn := func(lc *LambdaContext) error {
		// stop development time mistakes
		if lc.DB != nil {
			panic("DB already set for request")
		}

		conn := dsnReplacer.Replace(lc.authContext.germConn)

		lc.Log.Debugw("connecting to tenants GERM")

		var db *sql.DB
		var err error

		// create a database object through XRAY so that we can trace the calls
		if os.Getenv("ENABLE_X_RAY_DB_TRACING") == "true" {
			db, err = xray.SQLContext(driver, conn)
		} else {
			db, err = sql.Open(driver, conn)
		}

		if err != nil {
			return fmt.Errorf("failed to create GERM DB object: %w", err)
		}

		// close the database at the end of the request
		defer func() {
			lc.Log.Debugw("closing tenant DB connection")

			if err := db.Close(); err != nil {
				lc.Log.Errorw("error while closing tenant DB connection", "err", err)
			}
		}()

		// convert the standard DB object into a SQLx object for easier lookups
		lc.DB = sqlx.NewDb(db, driver)

		return next(lc)
	}

	return fn
}

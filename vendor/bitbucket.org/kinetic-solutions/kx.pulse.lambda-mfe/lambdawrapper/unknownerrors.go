package lambdawrapper

import (
	"errors"

	"github.com/aws/aws-xray-sdk-go/xray"
)

// unknownErrorHandler intercepts a bubbling error and hides it
// from the end user
func unknownErrorHandler(next ServeFunc) ServeFunc {
	fn := func(lc *LambdaContext) error {
		err := next(lc)

		if err == nil {
			return nil
		}

		lc.Log.Errorw("unknown error processing request", "err", err)

		xray.AddError(lc.Context(), err)

		// hide the error from the end user
		return errors.New("unhandled error")
	}

	return fn
}

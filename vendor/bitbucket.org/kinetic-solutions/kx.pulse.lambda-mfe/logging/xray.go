package logging

import (
	"fmt"

	"github.com/aws/aws-xray-sdk-go/xraylog"
	"go.uber.org/zap"
)

// xrayLogger implements xray's logging interface allowing us
// to log to Zap rather than directly to standard out.
type xrayLogger struct {
	logger *zap.SugaredLogger
}

func (x *xrayLogger) Log(level xraylog.LogLevel, msg fmt.Stringer) {
	var method func(msg string, keysAndValues ...interface{})

	switch level {
	case xraylog.LogLevelDebug:
		method = x.logger.Debugw
	case xraylog.LogLevelInfo:
		method = x.logger.Infow
	case xraylog.LogLevelWarn:
		method = x.logger.Warnw
	case xraylog.LogLevelError:
		method = x.logger.Errorw
	}

	method("xray message", "msg", msg.String())
}

// Xray converts the supplied logger into a xray logger
func Xray(logger *zap.SugaredLogger) xraylog.Logger {
	return &xrayLogger{logger: logger}
}

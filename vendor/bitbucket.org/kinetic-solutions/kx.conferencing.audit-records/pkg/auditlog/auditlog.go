// Package auditlog provides domain models for audit records,
// an interface for consumers to expect from an audit log publisher,
// and an SNS implementation of that interface.
package auditlog

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"

	krn "bitbucket.org/kinetic-solutions/kx.pulse.krn"
)

// Publisher defines the Publish method that audit log Publishers will provide.
type Publisher interface {
	// Publish the given audit record for the given tenant.
	// Returns a message id for the published record.
	Publish(ctx context.Context, tenantID string, m Message) (string, error)
}

// Message is the message that gets published.
type Records []Record

func (rs *Records) UnmarshalJSON(b []byte) error {
	//Some services send a single Record as an object on the SNS event
	//However some services send an array
	//We need to handle the JSON unmarshalling manually to make sure both cases can be properly deserialized
	var v interface{}
	err := json.Unmarshal(b, &v)
	if err != nil {
		return err
	}

	//Get the type of the deserialize json
	items := reflect.ValueOf(v)
	switch items.Kind() {
	case reflect.Map:
		//If it's a map, then this is the old format of message
		//So we need to convert it to a Record and add it to the slice
		jsonString, err := json.Marshal(items.Interface())
		if err != nil {
			return err
		}
		r := Record{}
		if err := json.Unmarshal(jsonString, &r); err != nil {
			return err
		}
		*rs = append(*rs, r)
	case reflect.Slice:
		//If it's a slice, then this is the new format of message
		//So convert each item in the slice to a Record
		*rs = make(Records, 0, items.Len())
		for i := 0; i < items.Len(); i++ {
			item := items.Index(i)
			jsonString, err := json.Marshal(item.Interface())
			if err != nil {
				return err
			}
			r := Record{}
			if err := json.Unmarshal(jsonString, &r); err != nil {
				return err
			}
			*rs = append(*rs, r)
		}
	}
	return nil
}

// Message is the body of the message that gets published.
type Message struct {
	Records   []Record `json:"records"`
	Publisher string   `json:"publisher"`
	Category  string   `json:"category"`
	EventID   int      `json:"eventId"`
	LogType   LogType  `json:"logType"`
}

// Offloaded Message is the body of the message that gets published.
type OffloadedMessage struct {
	S3Bucket string `json:"s3Bucket"`
	S3Key    string `json:"s3Key"`
}

//TODO: Delete this once all clients use the new version of the Message
func (m *Message) UnmarshalJSON(b []byte) error {
	//Some services send a Record struct on the SNS event
	//However some services send the newer Message struct which contains an array of Records
	//We need to handle the JSON unmarshalling manually to make sure both cases can be properly deserialized
	var v interface{}
	err := json.Unmarshal(b, &v)
	if err != nil {
		return err
	}
	items := reflect.ValueOf(v)
	records := items.MapIndex(reflect.ValueOf("records"))
	isOldSchema := records == reflect.Value{}

	if isOldSchema {
		r := v1Record{}
		if err := json.Unmarshal(b, &r); err != nil {
			return err
		}
		m.Records = append(m.Records, r.toRecord())
		resource := m.Records[0].AuditLog.Resource
		parts := strings.Split(resource, "/")
		logtype := parts[0]

		m.LogType = LogType(logtype)
		m.Category = r.Category
		m.Publisher = r.Publisher
		m.EventID = r.EventID
	} else {
		type message Message
		if err := json.Unmarshal(b, (*message)(m)); err != nil {
			return err
		}
	}

	return nil
}

//TODO: Delete this once all clients use the new version
// v1Record is the old schema that is still used by some old clients
type v1Record struct {
	AuditLog  krn.KRN                `json:"auditLogKRN"`
	EventID   int                    `json:"eventId"`
	UserID    int                    `json:"userId"`
	Operation string                 `json:"operation"`
	Message   []string               `json:"message"`
	Meta      map[string]interface{} `json:"meta"`
	Category  string                 `json:"category"`
	Publisher string                 `json:"publisher"`
}

func (r *v1Record) toRecord() Record {
	return Record{
		AuditLog:  r.AuditLog,
		UserID:    r.UserID,
		Message:   r.Message,
		Meta:      r.Meta,
		Operation: r.Operation,
	}
}

// Record is the body of the message that gets published.
type Record struct {
	AuditLog  krn.KRN                `json:"auditLogKRN"`
	UserID    int                    `json:"userId"`
	Operation string                 `json:"operation"`
	Message   []string               `json:"message"`
	Meta      map[string]interface{} `json:"meta"`
}

// GetMetaString reads a string from the Record's meta dictionary.
func (r Record) GetMetaString(key string) (string, bool) {
	val, ok := r.Meta[key]
	if !ok {
		return "", false
	}

	s, ok := val.(string)
	return s, ok
}

// GetMetaInt reads an integer from the Record's meta dictionary.
func (r Record) GetMetaInt(key string) (int, bool) {
	val, ok := r.Meta[key]
	if !ok {
		return 0, false
	}

	f, ok := val.(float64)
	n := int(f)
	if float64(n) != f {
		return 0, false
	}
	return n, ok
}

// GetMetaDate reads a date from the Record's meta dictionary.
func (r Record) GetMetaDate(key string) (time.Time, bool) {
	val, ok := r.Meta[key]
	if !ok {
		return time.Time{}, false
	}

	switch v := val.(type) {
	case time.Time:
		return time.Date(v.Year(), v.Month(), v.Day(), 0, 0, 0, 0, v.Location()), true
	case string:
		t, err := time.Parse("2006-01-02", v)
		return t, err == nil
	default:
		return time.Time{}, false
	}
}

// GetMetaTime reads a timestamp from the Record's meta dictionary.
func (r Record) GetMetaTime(key string) (time.Time, bool) {
	val, ok := r.Meta[key]
	if !ok {
		return time.Time{}, false
	}

	switch v := val.(type) {
	case time.Time:
		return v, true
	case string:
		t, err := time.Parse(time.RFC3339, v)
		return t, err == nil
	default:
		return time.Time{}, false
	}
}

type LogType string

const (
	// EventModuleAuditLogs is the logtype for audit records which get saved
	// into the GERM table 'EventModuleAuditLogs'.
	EventModuleAuditLogs LogType = "EventModuleAuditLogs"

	// CateringAuditLogs is the logtype for audit records which get saved
	// into the GERM table 'CateringAuditLogs'
	CateringAuditLogs LogType = "CateringAuditLogs"

	// StatusHistory is the logtype for audit records which get saved
	// into the GERM table 'StatusHistory'
	StatusHistory LogType = "StatusHistory"
)

// KRN assembles a KRN for the audit-records service.
func KRN(tenantID string, logtype LogType, table, key string) krn.KRN {
	return krn.KRN{
		Partition:    "kx",
		Service:      "audit-records",
		Jurisdiction: "",
		TenantID:     tenantID,
		Resource:     fmt.Sprintf("%s/%s/%s", logtype, table, key),
	}
}

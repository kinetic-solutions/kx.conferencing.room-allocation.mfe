// Package sns provides an implementation of audit.Publisher using SNS.
package sns

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/pkg/errors"
)

// snsPublish is the minimum interface we require in order to
// send messages to SNS.  We will accept any implementation that does this.
type snsPublish interface {
	PublishWithContext(context.Context, *sns.PublishInput, ...request.Option) (*sns.PublishOutput, error)
}

// New creates a new SnsPublisher that uses the given SNS client.
func New(sns snsPublish, topicArn string) Publisher {
	return Publisher{
		sns:   sns,
		topic: topicArn,
	}
}

// Publisher is our production implementation of auditlog.Publisher that
// publishes audit records to SNS.
type Publisher struct {
	sns        snsPublish
	topic      string
	httpClient *http.Client
}

var _ auditlog.Publisher = Publisher{}

// snsString is a string that we can take the address of, since the AWS SDK
// requires a pointer to a string, to use below.
var snsString string = "String"

// these constants are keys we will use to set attributes on our SNS messages.
const (
	tenantIDAttr = "tenantId"
	categoryAttr = "category"
)

// Publish implements the Publisher interface by publishing audit records to SNS.
// Will abort if the context is cancelled or its deadline expires.
func (p Publisher) Publish(ctx context.Context,
	tenantID string, m auditlog.Message) (string, error) {
	type result struct {
		messageID string
		err       error
	}
	// buffer with one value so the goroutine won't get blocked.
	ch := make(chan result, 1)

	go func() {
		defer close(ch)
		bytes, err := json.Marshal(m)
		if err != nil {
			ch <- result{err: errors.Wrap(err, "failed to marshal audit record")}
			return
		}

		body := string(bytes)

		snsResult, err := p.sns.PublishWithContext(ctx, &sns.PublishInput{
			Message: &body,
			MessageAttributes: map[string]*sns.MessageAttributeValue{
				tenantIDAttr: {
					DataType:    &snsString,
					StringValue: &tenantID,
				},
				categoryAttr: {
					DataType:    &snsString,
					StringValue: &m.Category,
				},
			},
			TopicArn: &p.topic,
		})

		res := result{err: err}
		if snsResult != nil && snsResult.MessageId != nil {
			res.messageID = *snsResult.MessageId
		}

		// if ch was unbuffered, this could block
		ch <- res
	}()

	for {
		select {
		case <-ctx.Done():
			// if ch was unbuffered, cancelling the context would cause the
			// above goroutine to block, creating a leak.
			return "", errors.Wrap(ctx.Err(), "context cancelled before audit record was published")
		case res := <-ch:
			return res.messageID, errors.Wrap(res.err, "failed to publish sns message")
		}
	}
}

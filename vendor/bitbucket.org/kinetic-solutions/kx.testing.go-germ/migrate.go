package germtesting

import (
	"context"
	"database/sql"
	"embed"
	"fmt"
	"io/fs"
	"path"

	"github.com/denisenkom/go-mssqldb/batch"
)

//go:embed migrations/*.sql
var migrations embed.FS

// migrator is used to initiate a fresh GERM database
type migrator struct{ log Log }

// migrate runs all the migrations in the migrations folder
func (m *migrator) migrate(ctx context.Context, db *sql.DB) error {
	if err := execBatch(ctx, configureDB, false, db.ExecContext); err != nil {
		return fmt.Errorf("database config: %w", err)
	}

	const migrationsDir = "migrations"

	toRun, err := fs.ReadDir(migrations, migrationsDir)

	if err != nil {
		return fmt.Errorf("cannot read migrations: %w", err)
	}

	m.log("found %d migrations", len(toRun))

	if len(toRun) == 0 {
		return nil
	}

	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}

	defer tx.Rollback()

	// HACK: some GERM procs are broken, until they've been fixed
	// we'll have to ignore the errors
	allowErrors := map[string]bool{
		"0005_procs_KxMobile.sql": true,
	}

	for _, migration := range toRun {
		name := migration.Name()

		m.log("running migration: %s", name)

		content, err := migrations.ReadFile(path.Join(migrationsDir, name))

		if err != nil {
			return fmt.Errorf("cannot read migration %q: %w", name, err)
		}

		if err = execBatch(ctx, string(content), allowErrors[name], tx.ExecContext); err != nil {
			return fmt.Errorf("migration %q: %w", name, err)
		}

		m.log("migration %s complete", name)
	}

	m.log("commiting migrations")

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}

// execBatch splits the statement up into batchs on
// the GO command and executes the command.
func execBatch(ctx context.Context, query string, allowErrors bool, exec func(ctx context.Context, query string, args ...interface{}) (sql.Result, error)) error {
	for _, stm := range batch.Split(string(query), "GO") {
		if _, err := exec(ctx, stm); err != nil && !allowErrors {
			return fmt.Errorf("statement %q: %w", stm, err)
		}
	}
	return nil
}

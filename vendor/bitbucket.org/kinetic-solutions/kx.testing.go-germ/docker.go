package germtesting

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var (
	// errContainerNotFound returned when `docker inspect` is unable to find
	// the supplied container name.
	errContainerNotFound = errors.New("cannot find container")
)

// DockerOptions are used to configure the docker server behavior
type DockerOptions struct {
	// Image defaults to mcr.microsoft.com/mssql/server:latest if no supplied
	Image string

	// Name the docker image name, defaults to GERM-testing-sql
	Name string

	// Env vars to set on start-up
	Env map[string]string
}

// LatestSQL is configured to pull the leastest
// official MS-SQL docker image.
func LatestSQL() DockerOptions {
	return DockerOptions{
		Image: "mcr.microsoft.com/mssql/server:latest",
		Name:  "GERM-testing-sql",
		Env: map[string]string{
			"ACCEPT_EULA": "Y",
			"SA_PASSWORD": fmt.Sprintf("%s!!!!", rndHex(12)),
			"MSSQL_PID":   "Developer",
		},
	}
}

type dockerServer struct {
	c *container
}

func (ds *dockerServer) conn() Connection {
	return Connection{
		Username: "sa",
		Password: ds.c.SAPassword,
		Port:     ds.c.Port,
		Server:   ds.c.Host,
	}
}

// Docker starts a new Docker server instance
func Docker(ctx context.Context, opt DockerOptions) (Server, error) {
	if opt.Image == "" {
		return nil, fmt.Errorf("image required")
	}

	if opt.Name == "" {
		return nil, fmt.Errorf("name required")
	}

	existing, err := getContainer(ctx, opt)

	// found an already running container
	if err == nil {
		return &dockerServer{c: existing}, nil
	}

	// check that the error was a not found error
	if !errors.Is(err, errContainerNotFound) {
		return nil, err
	}

	// container hasn't yet been created so run the docker run command
	args := []string{"run", "--rm", "-P", "-d", "--name", opt.Name}

	for k, v := range opt.Env {
		args = append(args, "-e", fmt.Sprintf("%s=%s", k, v))
	}

	args = append(args, opt.Image)
	cmd := exec.CommandContext(ctx, "docker", args...)

	if out, err := cmd.CombinedOutput(); err != nil {
		return nil, fmt.Errorf("cannot start container %q: %w, %s", opt.Name, err, string(out))
	}

	// container should now be created so we can re-inspect its details
	existing, err = getContainer(ctx, opt)

	if err != nil {
		return nil, fmt.Errorf("failed to read created container: %w", err)
	}

	result := &dockerServer{c: existing}
	dsn := createConnectionString(result, "master")
	// ensure we can connect to the database
	for {
		if db, err := newDatabaseConn(ctx, dsn); err == nil {
			if err := db.Close(); err != nil {
				return nil, fmt.Errorf("cannot close test connection: %w", err)
			}
			return result, nil
		}
		<-time.After(time.Millisecond * 500)
	}
}

// container tracks information about a docker container started for tests.
type container struct {
	// ID is the id of the docker container
	ID string

	// Host is the IP of the container
	Host string

	// SAPassword is the password stored in the env of the container
	SAPassword string

	// Port is the port that the container is listening to
	Port int
}

// getContainer returns details about an existing docker container
func getContainer(ctx context.Context, opt DockerOptions) (*container, error) {
	out, err := exec.CommandContext(ctx, "docker", "inspect", opt.Name).Output()

	switch v := err.(type) {
	case nil:
		break
	case *exec.ExitError:
		switch v.ExitCode() {
		case 1:
			return nil, errContainerNotFound
		default:
			return nil, fmt.Errorf("docker inspect returned status %d: %s", v.ExitCode(), string(v.Stderr))
		}
	default:
		return nil, fmt.Errorf("failed to inspect container")
	}

	var doc []struct {
		ID    string `json:"Id"`
		State struct {
			Running bool `json:"Running"`
		} `json:"State"`
		Config struct {
			Env []string `json:"Env"`
		} `json:"Config"`
		NetworkSettings struct {
			Ports struct {
				TCP1433 []struct {
					HostIP   string `json:"HostIp"`
					HostPort string `json:"HostPort"`
				} `json:"1433/tcp"`
			} `json:"Ports"`
		} `json:"NetworkSettings"`
	}

	if err := json.Unmarshal(out, &doc); err != nil {
		return nil, fmt.Errorf("could not decode json: %w", err)
	}

	c := doc[0]

	if !c.State.Running {
		return nil, fmt.Errorf("container is created but not running")
	}

	network := c.NetworkSettings.Ports.TCP1433[0]
	result := &container{
		ID:   c.ID,
		Host: network.HostIP,
	}

	if result.Port, err = strconv.Atoi(network.HostPort); err != nil {
		return nil, fmt.Errorf("cannot convert network port to int: %w", err)
	}

	for _, env := range c.Config.Env {
		parts := strings.SplitN(env, "=", 2)
		if parts[0] == "SA_PASSWORD" {
			result.SAPassword = parts[1]
			break
		}
	}

	return result, nil
}

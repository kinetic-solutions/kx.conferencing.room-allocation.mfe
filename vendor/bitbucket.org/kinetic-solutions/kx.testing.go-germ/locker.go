package germtesting

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	mssql "github.com/denisenkom/go-mssqldb"
)

var (
	errAlreadyLocked = errors.New("already locked")
)

// locker uses SP_GETAPPLOCK and SP_RELEASEAPPLOCK to lock
// databases while tests are running. This allows more than
// one process to share a database server.
type locker struct {
	tx *sql.Tx
}

// lock creates a lock for the given name. If the name is already
// locked errAlreadyLocked is returned.
func (l *locker) lock(ctx context.Context, name string) error {
	var rs mssql.ReturnStatus

	if _, err := l.tx.QueryContext(ctx, "SP_GETAPPLOCK", sql.Named("Resource", name),
		sql.Named("LockMode", "Exclusive"), sql.Named("LockOwner", "Session"),
		sql.Named("LockTimeout", 0), sql.Named("DbPrincipal", "public"), &rs,
	); err != nil {
		return fmt.Errorf("cannot lock %q: %w", name, err)
	}

	// See https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-getapplock-transact-sql?view=sql-server-ver15#return-code-values
	// for result codes.
	if rs >= 0 {
		return nil
	}

	return errAlreadyLocked
}

// unlock releases the lock for a give name
func (l *locker) unlock(ctx context.Context, name string) error {
	var rs mssql.ReturnStatus

	if _, err := l.tx.QueryContext(ctx, "SP_RELEASEAPPLOCK", sql.Named("Resource", name),
		sql.Named("LockOwner", "Session"), sql.Named("DbPrincipal", "public"), &rs,
	); err != nil {
		return fmt.Errorf("failed to release lock %q: %w", name, err)
	}

	if rs < 0 {
		return fmt.Errorf("cannot unlock %q: SP_RELEASEAPPLOCK returned %d", name, rs)
	}

	return nil
}

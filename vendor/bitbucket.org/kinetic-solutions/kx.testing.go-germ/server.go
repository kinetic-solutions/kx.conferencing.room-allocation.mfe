package germtesting

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"sync"
	"time"

	mssql "github.com/denisenkom/go-mssqldb"
)

var errPoolClosed = errors.New("pool closed")

// Log is used to output log message
type Log func(format string, args ...interface{})

type Connection struct {
	Username string
	Password string
	Port     int
	Server   string
}

// Server is the current database server
type Server interface {
	// Conn returns the current connection
	conn() Connection
}

// existingServer can be used to configure a connection
// to an already running server.
type existingServer struct {
	details Connection
}

func (es existingServer) conn() Connection {
	return es.details
}

// Existing can be used to configure a server that is
// already running.
func Existing(conn Connection) Server {
	return existingServer{details: conn}
}

// PoolOptions is used to configure the database pool, use
// NewPoolOptions to construct defaults.
type PoolOptions struct {
	// Server is the details of the MS-SQL server to connect to
	Server Server

	// MaxDatabases will limit the number of new database created. Set to
	// 0 for unlimited databases.
	MaxDatabases int

	// NamePrefix is used to group pooled databases together, allowing
	// more than one pool of databases on a server.
	NamePrefix string

	// LeasePollInterval specifies how often Lease should check to
	// see if a new database has been freed
	LeasePollInterval time.Duration
}

// NewPoolOptions returns a PoolOptions configured with the
// default options
func NewPoolOptions(server Server) PoolOptions {
	return PoolOptions{
		Server:            server,
		MaxDatabases:      20,
		NamePrefix:        "germ_",
		LeasePollInterval: 100 * time.Millisecond,
	}
}

// PooledDatabase can be used to interact with
// a pooled database.
type PooledDatabase struct {
	name, dsn string
	db        *sql.DB
}

// DB returns the *sql.DB object
func (pc *PooledDatabase) DB() *sql.DB {
	return pc.db
}

// DSN returns the connection string used
// to connect to the database
func (pc *PooledDatabase) DSN() string {
	return pc.dsn
}

// DatabasePool manages connections to the databases.
type DatabasePool struct {
	mux      sync.RWMutex
	log      Log
	opt      PoolOptions
	host     *sql.DB
	migrator *migrator
	locker   *locker
	leased   map[*PooledDatabase]bool
}

// Lease will fetch a new database connection for the pool. If no
// connection is avaliable it will wait for one.
func (dp *DatabasePool) Lease(ctx context.Context) (*PooledDatabase, error) {
	for {
		if err := dp.checkClosed(); err != nil {
			return nil, err
		}

		db, err := dp.findDatabase(ctx)

		if err != nil {
			return nil, fmt.Errorf("failed to lease connection: %w", err)
		}

		// check that we were able to find a database to lease
		if db != nil {
			dp.mux.Lock()
			defer dp.mux.Unlock()

			// keep track of the leased DB's so that Close can shut them all down
			dp.leased[db] = true

			return db, nil
		}

		// no database was available, wait a specified amount
		// of time, or until the context is canceled.
		select {
		case <-time.After(dp.opt.LeasePollInterval):
		case <-ctx.Done():
			return nil, ctx.Err()
		}
	}
}

// Return returns the given connection back to the pool
func (dp *DatabasePool) Return(ctx context.Context, db *PooledDatabase) error {
	if err := dp.checkClosed(); err != nil {
		return err
	}

	if ctx == nil {
		return fmt.Errorf("ctx is required")
	}

	if db == nil {
		return fmt.Errorf("db is required")
	}

	// remove the connection from the leased collection
	dp.mux.Lock()
	defer dp.mux.Unlock()

	// TODO: most of this doesn't need a global lock.
	return dp.innerReturn(ctx, db)
}

// Close shuts down the current pool
func (dp *DatabasePool) Close(ctx context.Context) error {
	if err := dp.checkClosed(); err != nil {
		return err
	}

	dp.mux.Lock()
	defer dp.mux.Unlock()

	// TODO: unify logic in checkClosed()
	// lock has been gained, recheck the pool is still open
	if dp.host == nil {
		return errPoolClosed
	}

	// force close all database connections still in use
	for db := range dp.leased {
		if err := dp.innerReturn(ctx, db); err != nil {
			return fmt.Errorf("cannot close db connection: %w", err)
		}
	}

	// close the host connection
	if err := dp.host.Close(); err != nil {
		return fmt.Errorf("failed to close host connection: %w", err)
	}

	// mark the pool closed
	dp.host = nil

	return nil
}

func (dp *DatabasePool) innerReturn(ctx context.Context, db *PooledDatabase) error {
	if _, exists := dp.leased[db]; !exists {
		return fmt.Errorf("db doesn't belong to the pool")
	}

	if err := dp.locker.unlock(ctx, db.name); err != nil {
		return fmt.Errorf("cannot release database lock: %w", err)
	}

	// close the database connection to break any thing
	// that's using the connection after closing
	if err := db.db.Close(); err != nil {
		return fmt.Errorf("cannot close db: %w", err)
	}

	// stop tracking the database
	delete(dp.leased, db)

	return nil
}

// isClosed checks to see if the pool is closed
func (dp *DatabasePool) checkClosed() error {
	dp.mux.RLock()
	defer dp.mux.RUnlock()

	if dp.host == nil {
		return errPoolClosed
	}

	return nil
}

// findDatabase will look for a unlocked database, otherwise if there is capacity
// for more databases on the server it will create another database, if nither
// is possible it returns nil.
func (dp *DatabasePool) findDatabase(ctx context.Context) (*PooledDatabase, error) {
	asPooledDatabase := func(name string) (*PooledDatabase, error) {
		dsn := createConnectionString(dp.opt.Server, name)
		db, err := newDatabaseConn(ctx, dsn)

		if err != nil {
			return nil, fmt.Errorf("cannot connect to db %q: %w", name, err)
		}

		return &PooledDatabase{
			name: name,
			dsn:  dsn,
			db:   db,
		}, nil
	}

	databases, err := dp.getDatabases(ctx)

	if err != nil {
		return nil, fmt.Errorf("cannot reading existing databases: %w", err)
	}

	// attempt to lock an existing database
	for _, db := range databases {
		// don't attempt to lock already locked databaess
		if db.locked {
			continue
		}

		switch err := dp.locker.lock(ctx, db.name); err {
		case nil:
			// we've gained a lock on the database so make sure
			// the snapshot has been restored
			dp.log("restoring snapshot of db %q", db.name)

			restoreSQL := fmt.Sprintf(`
				ALTER DATABASE %[1]s SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
				RESTORE DATABASE %[1]s FROM DATABASE_SNAPSHOT = '%[1]s_ss';
				ALTER DATABASE %[1]s SET MULTI_USER;`,
				db.name,
			)

			if _, err := dp.host.ExecContext(ctx, restoreSQL); err != nil {
				return nil, fmt.Errorf("failed to restore snapshot for db %q: %w", db.name, err)
			}

			return asPooledDatabase(db.name)
		case errAlreadyLocked:
			// something beat us to the lock, try the next db
		default:
			return nil, fmt.Errorf("cannot list existing dbs: %w", err)
		}
	}

	// cannot gain a lock on an existing database, check if we can create a new one
	if dp.opt.MaxDatabases <= 0 || len(databases) < dp.opt.MaxDatabases {
		name := fmt.Sprintf("%s%s", dp.opt.NamePrefix, rndHex(8))

		if err := dp.locker.lock(ctx, name); err != nil {
			return nil, fmt.Errorf("cannot create new lock: %w", err)
		}

		if _, err := dp.host.ExecContext(ctx, fmt.Sprintf(createDatabase, name)); err != nil {
			return nil, fmt.Errorf("cannot create new DB %q: %w", name, err)
		}

		setup := func() (*PooledDatabase, error) {
			db, err := asPooledDatabase(name)

			if err != nil {
				return nil, fmt.Errorf("cannot connect to new DB %q: %w", name, err)
			}

			if err := dp.migrator.migrate(ctx, db.db); err != nil {
				return nil, fmt.Errorf("failed to migrate database: %w", err)
			}

			if _, err := dp.host.ExecContext(ctx, fmt.Sprintf(createSnapshot, name)); err != nil {
				return nil, fmt.Errorf("cannot snapshot db: %w", err)
			}

			return db, nil
		}

		db, err := setup()

		if err != nil {
			dp.log("database setup failed. Removing db %s", name)
			if _, err := dp.host.ExecContext(ctx, fmt.Sprintf(`
				ALTER DATABASE %[1]s SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
				DROP DATABASE %[1]s`, name)); err != nil {
				return nil, fmt.Errorf("cannot remove error state db: %w", err)
			}

			return nil, err
		}

		return db, nil
	}

	// cannot find a unstarted db, cannot create a new one, return nothing
	return nil, nil
}

type databaseStatus struct {
	name   string
	locked bool
}

// getDatabases returns a list of existing databases and there current
// lock status. Snapshots and database in other prefixes are filtered out.
func (dp *DatabasePool) getDatabases(ctx context.Context) ([]databaseStatus, error) {
	const getDBStatus = `
		SELECT
			d.name name,
			CASE WHEN l.resource_description IS NULL THEN 0 ELSE 1 END locked
		FROM
			master.sys.databases d
		LEFT JOIN
			sys.dm_tran_locks l ON l.resource_description like '%' + d.name + '%'
		WHERE
			d.name like @name + '%'
		AND
			d.source_database_id IS NULL`

	rows, err := dp.host.QueryContext(ctx, getDBStatus, sql.Named("name", dp.opt.NamePrefix))

	if err != nil {
		return nil, fmt.Errorf("cannot read existing databases locks: %w", err)
	}

	defer rows.Close()

	result := []databaseStatus{}

	for rows.Next() {
		db := databaseStatus{}

		if err := rows.Scan(&db.name, &db.locked); err != nil {
			return nil, fmt.Errorf("cannot read row: %w", err)
		}

		result = append(result, db)
	}

	return result, nil
}

// NewPool creates a new database pool with the given options. To use the default
// options call NewPoolOptions.
func NewPool(ctx context.Context, log Log, opt PoolOptions) (*DatabasePool, error) {
	if ctx == nil {
		return nil, fmt.Errorf("ctx required")
	}

	if log == nil {
		return nil, fmt.Errorf("log is required")
	}

	if opt.Server == nil {
		return nil, fmt.Errorf("opt.server is required")
	}

	log("connecting to master database")

	master, err := newDatabaseConn(ctx, createConnectionString(opt.Server, "master"))

	if err != nil {
		return nil, err
	}

	res := &DatabasePool{
		opt:      opt,
		log:      log,
		host:     master,
		leased:   map[*PooledDatabase]bool{},
		locker:   &locker{},
		migrator: &migrator{log: log},
	}

	if res.locker.tx, err = master.Begin(); err != nil {
		return nil, fmt.Errorf("cannot start locker tx: %w", err)
	}

	return res, nil
}

// newDatabaseConn creates a new database connection
func newDatabaseConn(ctx context.Context, dsn string) (*sql.DB, error) {
	connector, err := mssql.NewConnector(dsn)

	if err != nil {
		return nil, fmt.Errorf("cannot connect to db: %w", err)
	}

	db := sql.OpenDB(connector)

	if err := db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("cannot ping DB: %w", err)
	}

	return db, nil
}

func createConnectionString(server Server, dbName string) string {
	conn := server.conn()
	dsn := fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s&connection+timeout=30",
		conn.Username, conn.Password, conn.Server, conn.Port, dbName)

	return dsn
}

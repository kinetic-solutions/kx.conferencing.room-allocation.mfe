/****** Object:  StoredProcedure [KxAudit].[DelAuditAccess]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [KxAudit].[DelAuditAccess]
	@AuditAccessID int
AS
DELETE
	[KxAudit].AuditAccess
WHERE
	AuditAccessID	= @AuditAccessID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [KxAudit].[GetAuditAccess]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [KxAudit].[GetAuditAccess]
	@AuditAccessID int
AS
SELECT
	AC.*
FROM	[KxAudit].AuditAccess AC
WHERE
	AC.AuditAccessID	= @AuditAccessID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [KxAudit].[NewAuditAccess]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [KxAudit].[NewAuditAccess]
	@AuditAccessID int OUTPUT,
	@ParentAuditAccessID int,
	@Application varchar(max),
	@FormName varchar(max),
	@Caption varchar(max),
	@UserID int,
	@DateTimeEntered datetime,
	@DateTimeExited datetime
AS
DECLARE @KeepDays int
SET @KeepDays = IsNull((SELECT TOP 1 Value from CustomSettings where CustomSetting = 'Kx.Audit.Access.Keep.Days'), 90)
DELETE FROM KxAudit.AuditAccess WHERE DateTimeEntered IS NOT NULL AND DateTimeEntered < getdate() - @KeepDays
DELETE FROM KxAudit.AuditAccess WHERE DateTimeExited IS NOT NULL AND DateTimeExited < getdate() - @KeepDays
INSERT INTO
	[KxAudit].AuditAccess
	(
	FormName,
	ParentAuditAccessID,
	[Application],
	Caption,
	UserID,
	DateTimeEntered,
	DateTimeExited
	)
VALUES
	(
	@FormName,
	@ParentAuditAccessID,
	@Application,
	IsNull(@Caption, ''),
	@UserID,
	CASE WHEN @DateTimeEntered < 29000 THEN NULL ELSE @DateTimeEntered END,
	CASE WHEN @DateTimeExited < 29000 THEN NULL ELSE @DateTimeExited END
	)
SET 	@AuditAccessID = SCOPE_IDENTITY()
RETURN 	@AuditAccessID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [KxAudit].[SetAuditAccess]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [KxAudit].[SetAuditAccess]
	@AuditAccessID int,
	@ParentAuditAccessID int,
	@Application varchar(max),
	@FormName varchar(max),
	@Caption varchar(max),
	@UserID int,
	@DateTimeEntered datetime,
	@DateTimeExited datetime
AS
UPDATE
	[KxAudit].AuditAccess
SET
	ParentAuditAccessID = @ParentAuditAccessID,
	[Application] = @Application,
	FormName = @FormName,
	Caption = IsNull(@Caption, IsNull(Caption, '')),
	UserID = @UserID,
	DateTimeEntered = CASE WHEN @DateTimeEntered < 29000 THEN NULL ELSE @DateTimeEntered END,
	DateTimeExited = CASE WHEN @DateTimeExited < 29000 THEN NULL ELSE @DateTimeExited END
WHERE
	AuditAccessID	= @AuditAccessID
SET NOCOUNT OFF
GO
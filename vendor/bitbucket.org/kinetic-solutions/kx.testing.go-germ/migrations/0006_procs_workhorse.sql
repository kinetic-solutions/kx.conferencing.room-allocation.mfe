/****** Object:  StoredProcedure [WorkHorse].[DelSchedules]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[DelSchedules]
	@ScheduleID int
AS
DELETE
	[WorkHorse].Schedules
WHERE
	ScheduleID	= @ScheduleID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[GetCompletedWIPGroups]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[GetCompletedWIPGroups]
	@WipGroupID int = 0
as
SELECT *,
	CASE WHEN EXISTS (SELECT NULL FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID AND W.Errored <> 0) THEN 1 ELSE 0 END as WipErrored,
	(SELECT COUNT(*) FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID) as StudentCount,
	(SELECT SUM(CASE WHen W.Progress IS NULL THEN 1 ELSE W.Progress END) FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID and W.Errored = 0) as ArchivedSuccessful
FROM WorkHorse.WIPGroup WG
WHERE (@WipGroupID = 0 OR @WipGroupID = WG.WipGroupID)
AND WG.Completed = 0
AND WG.Aborted = 0
AND WG.ReadyForProcessing = 1
AND WG.Processing = 1
AND IsNull((SELECT COUNT(*) FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID and W.Aborted = 0 and W.Completed = 0 and W.Errored = 0), 0) = 0
GO
/****** Object:  StoredProcedure [WorkHorse].[GetSchedules]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[GetSchedules]
	@ScheduleID int
AS
SELECT
	S.*
FROM	[WorkHorse].Schedules S
WHERE
	S.ScheduleID	= @ScheduleID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[GetStandAloneWIP]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[GetStandAloneWIP]
as
SELECT *,
	(SELECT COUNT(*) FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID AND W.Aborted = 0 AND W.Completed = 0 AND W.Errored = 0) as WIPUnits
FROM WorkHorse.WIP WG
WHERE WG.Processing = 0
AND WG.Completed = 0
AND WG.Aborted = 0
AND WG.Errored = 0
AND NOT WG.WipGroupID IN (SELECT WipGroupID FROM WorkHorse.WIPGroup WG)
AND WipType = 'SQL'
ORDER BY WG.WipGroupID
GO
/****** Object:  StoredProcedure [WorkHorse].[GetWIP]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[GetWIP]
	@WipGroupID int
as
SELECT W.*, WG.WipType as GroupWipType, WG.Description 
FROM WorkHorse.WIP W
JOIN WorkHorse.WIPGroup WG ON WG.WipGroupID = W.WipGroupID
WHERE IsNull(W.WipGroupID, 0) = @WipGroupID
AND	W.Completed = 0
AND W.Aborted = 0
AND W.Errored = 0
ORDER BY W.WipID
GO
/****** Object:  StoredProcedure [WorkHorse].[GetWIPGroups]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[GetWIPGroups]
as
SELECT *,
	(SELECT COUNT(*) FROM WorkHorse.WIP W WHERE W.WipGroupID = WG.WipGroupID AND W.Aborted = 0 AND W.Completed = 0 AND W.Errored = 0) as WIPUnits
FROM WorkHorse.WIPGroup WG
WHERE WG.ReadyForProcessing = 1
AND WG.Completed = 0
AND WG.Aborted = 0
ORDER BY WG.WipGroupID
GO
/****** Object:  StoredProcedure [WorkHorse].[NewSchedules]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[NewSchedules]
	@ScheduleID int OUTPUT,
	@ScheduleType varchar(40),
	@ScheduleTitle varchar(255),
	@ScheduleAction varchar(40),
	@LastRunDate datetime,
	@NextRunDate datetime,
	@RunAt datetime,
	@RunEveryType varchar(1),
	@RunEveryInc int,
	@Options varchar(max),
	@Running bit,
	@EmailAddress varchar(max),
	@Inactive bit
AS
INSERT INTO
	[WorkHorse].Schedules
	(
	ScheduleType,
	ScheduleTitle,
	ScheduleAction,
	LastRunDate,
	NextRunDate,
	RunAt,
	RunEveryType,
	RunEveryInc,
	Options,
	Running,
	EmailAddress,
	Inactive
	)
VALUES
	(
	@ScheduleType,
	@ScheduleTitle,
	@ScheduleAction,
	@LastRunDate,
	@NextRunDate,
	@RunAt,
	@RunEveryType,
	@RunEveryInc,
	@Options,
	@Running,
	@EmailAddress,
	@Inactive
	)
SELECT
	@ScheduleID = SCOPE_IDENTITY()
RETURN @ScheduleID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[NewWIP]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[NewWIP]
	@WipID int OUTPUT,
	@WipGroupID int,
	@WipType varchar(50),
	@IdentityID1 int,
	@IdentityID2 int,
	@IdentityID3 int,
	@IdentityID4 int,
	@IdentityString varchar(255),
	@WipInfo varchar(max),
	@CreationDateTime datetime,
	@CreationUserID int,
	@Processing bit,
	@StartedAt datetime,
	@Completed bit,
	@CompletionDateTime datetime,
	@Aborted bit,
	@AbortionDateTime datetime,
	@AbortedByUserID int,
	@Errored bit,
	@ErrorDateTime datetime,
	@ErrorMessage varchar(max),
	@OutOfHours bit,
	@Progress int
AS
INSERT INTO
	[WorkHorse].WIP
	(
	WipGroupID,
	WipType,
	IdentityID1,
	IdentityID2,
	IdentityID3,
	IdentityID4,
	IdentityString,
	WipInfo,
	CreationDateTime,
	CreationUserID,
	Processing,
	StartedAt,
	Completed,
	CompletionDateTime,
	Aborted,
	AbortionDateTime,
	AbortedByUserID,
	Errored,
	ErrorDateTime,
	ErrorMessage,
	OutOfHours,
	Progress
	)
VALUES
	(
	@WipGroupID,
	@WipType,
	@IdentityID1,
	@IdentityID2,
	@IdentityID3,
	@IdentityID4,
	@IdentityString,
	@WipInfo,
	@CreationDateTime,
	@CreationUserID,
	@Processing,
	@StartedAt,
	@Completed,
	@CompletionDateTime,
	@Aborted,
	@AbortionDateTime,
	@AbortedByUserID,
	@Errored,
	@ErrorDateTime,
	@ErrorMessage,
	@OutOfHours,
	@Progress
	)
SELECT
	@WipID = SCOPE_IDENTITY()
RETURN @WipID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[NewWIPGroup]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[NewWIPGroup]
	@WipGroupID int OUTPUT,
	@WipType varchar(50),
	@Description varchar(255),
	@CreationDateTime datetime,
	@CreationUserID int,
	@Processing bit,
	@StartedAt datetime,
	@Completed bit,
	@CompletionDateTime datetime,
	@EmailAddresses varchar(max),
	@CompletedMessage varchar(max),
	@Aborted bit,
	@AbortionDateTime datetime,
	@AbortedByUserID int,
	@OutOfHours bit,
	@ReadyForProcessing bit
AS
INSERT INTO
	[WorkHorse].WIPGroup
	(
	WipType,
	Description,
	CreationDateTime,
	CreationUserID,
	Processing,
	StartedAt,
	Completed,
	CompletionDateTime,
	EmailAddresses,
	CompletedMessage,
	Aborted,
	AbortionDateTime,
	AbortedByUserID,
	OutOfHours,
	ReadyForProcessing
	)
VALUES
	(
	@WipType,
	@Description,
	@CreationDateTime,
	@CreationUserID,
	@Processing,
	@StartedAt,
	@Completed,
	@CompletionDateTime,
	@EmailAddresses,
	@CompletedMessage,
	@Aborted,
	@AbortionDateTime,
	@AbortedByUserID,
	@OutOfHours,
	@ReadyForProcessing
	)
SELECT
	@WipGroupID = SCOPE_IDENTITY()
RETURN @WipGroupID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[SetSchedules]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[SetSchedules]
	@ScheduleID int,
	@ScheduleType varchar(40),
	@ScheduleTitle varchar(255),
	@ScheduleAction varchar(40),
	@LastRunDate datetime,
	@NextRunDate datetime,
	@RunAt datetime,
	@RunEveryType varchar(1),
	@RunEveryInc int,
	@Options varchar(max),
	@Running bit,
	@EmailAddress varchar(max),
	@Inactive bit
AS
UPDATE
	[WorkHorse].Schedules
SET
	ScheduleType = @ScheduleType,
	ScheduleTitle = @ScheduleTitle,
	ScheduleAction = @ScheduleAction,
	LastRunDate = @LastRunDate,
	NextRunDate = @NextRunDate,
	RunAt = @RunAt,
	RunEveryType = @RunEveryType,
	RunEveryInc = @RunEveryInc,
	Options = @Options,
	Running = @Running,
	EmailAddress = @EmailAddress,
	Inactive = @Inactive
WHERE
	ScheduleID	= @ScheduleID
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [WorkHorse].[UpdateWIPCompleted]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[UpdateWIPCompleted]
	@WipID int
as
UPDATE WorkHorse.WIP SET Processing = 0, Completed = 1, CompletionDateTime = GETDATE()
WHERE WipID = @WipID
GO
/****** Object:  StoredProcedure [WorkHorse].[UpdateWIPErrored]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[UpdateWIPErrored]
	@WipID int,
	@Message varchar(max)
as
UPDATE WorkHorse.WIP SET Processing = 0, Errored = 1, ErrorDateTime = GETDATE(), ErrorMessage = @Message
WHERE WipID = @WipID
GO
/****** Object:  StoredProcedure [WorkHorse].[UpdateWIPGroupCompleted]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[UpdateWIPGroupCompleted]
	@WipGroupID int,
	@CompletedMessage varchar(max)
as
UPDATE WorkHorse.WIPGroup SET Processing = 0, Completed = 1, CompletionDateTime = GETDATE(), CompletedMessage = @CompletedMessage, ReadyForProcessing = 0
WHERE WipGroupID = @WipGroupID
GO
/****** Object:  StoredProcedure [WorkHorse].[UpdateWIPGroupProcessing]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[UpdateWIPGroupProcessing]
	@WipGroupID int
as
UPDATE WorkHorse.WIPGroup SET Processing = 1, StartedAt = getdate() WHERE Processing = 0 AND WipGroupID = @WipGroupID
GO
/****** Object:  StoredProcedure [WorkHorse].[UpdateWIPProcessing]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[UpdateWIPProcessing]
	@WipID int
as
UPDATE WorkHorse.WIP SET Processing = 1, StartedAt = getdate() WHERE WipID = @WipID
UPDATE WorkHorse.WIPGroup SET Processing = 1, StartedAt = getdate() WHERE Processing = 0 AND WipGroupID = (SELECT ISNull(WipGroupID, 0) FROM WIP WHERE WipID = @WipID)
GO
/****** Object:  StoredProcedure [WorkHorse].[WIPCreateOrUpdate]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [WorkHorse].[WIPCreateOrUpdate]
	@WipID int OUTPUT,
	@WipGroupID int,
	@WipType varchar(255),
	@IdentityID1 int,
	@IdentityID2 int,
	@IdentityID3 int,
	@IdentityID4 int,
	@IdentityString varchar(255),
	@WipInfo varchar(max),
	@CreationDateTime datetime,
	@CreationUserID int,
	@OutOfHours bit
as
IF @WipID = 0
BEGIN
	INSERT INTO WorkHorse.WIP (WipGroupID, WipType, IdentityID1, IdentityID2, IdentityID3, IdentityID4, IdentityString, WipInfo, CreationDateTime, CreationUserID, Processing, Completed,
		Aborted, Errored, OutOfHours)
	VALUES (@WipGroupID, @WipType, @IdentityID1, @IdentityID2, @IdentityID3, @IdentityID4, @IdentityString, @WipInfo, @CreationDateTime, @CreationUserID, 0, 0, 0, 0, @OutOfHours)
	SET @WipID = SCOPE_IDENTITY()
END
ELSE
BEGIN
	UPDATE Workhorse.WIP SET
		WipGroupID = @WipGroupID,
		WipType = @WipType,
		IdentityID1 = @IdentityID1,
		IdentityID2 = @IdentityID2,
		IdentityID3 = @IdentityID3,
		IdentityID4 = @IdentityID4,
		IdentityString = @IdentityString,
		WipInfo = @WipInfo,
		CreationDateTime = @CreationDateTime,
		CreationUserID = @CreationUserID,
		OutOfHours = @OutOfHours
	WHERE WipID = @WipID
END
GO

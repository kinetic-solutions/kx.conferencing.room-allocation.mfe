/****** Object:  Trigger [syllabus].[Kx2SyllabusImportCopy]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [syllabus].[Kx2SyllabusImportCopy]
   ON  [syllabus].[Kx2Syllabus]
   AFTER INSERT,UPDATE
AS 
DECLARE            
	@User varchar(255)
SET @User = system_user
IF NOT @User = 'SPLUSKINETIC'
BEGIN
	INSERT INTO Kx2Syllabus2
	SELECT * FROM Inserted 
END
GO
ALTER TABLE [syllabus].[Kx2Syllabus] ENABLE TRIGGER [Kx2SyllabusImportCopy]
GO

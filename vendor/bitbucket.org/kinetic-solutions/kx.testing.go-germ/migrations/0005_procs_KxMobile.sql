/****** Object:  StoredProcedure [KxMobile].[DelApplications]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[DelApplications]
	@ApplicationID int
AS

DELETE
	KxMobile.Applications
WHERE
	ApplicationID	= @ApplicationID
 
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[GetApplications]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
 

CREATE PROCEDURE [KxMobile].[GetApplications]
	@ApplicationID int=0,
	@Application varchar(255)= ''
AS

SET @ApplicationID = IsNull(@ApplicationID, 0)
SET @Application = ISNull(@Application, '')

SELECT
	A.ApplicationID,
	A.Application,
	A.Inactive,
	A.LicenceInfo,
	A.FeatureAccess,
	A.FullDateFormat,
	A.LongDateFormat,
	A.ShortDateFormat,
	A.TimeFormat,
	A.CurrencyFormat,
	A.Settings,
	A.Lang
FROM	KxMobile.Applications A
WHERE
	(@ApplicationID = 0 OR A.ApplicationID	= @ApplicationID)
AND	(@Application ='' OR A.Application = @Application)
 
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[NewApplications]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
 

CREATE PROCEDURE [KxMobile].[NewApplications]
	@ApplicationID int,
	@Application varchar(255),
	@Inactive bit,
	@LicenceInfo varchar(max),
	@FeatureAccess varchar(max),
	@FullDateFormat varchar(20),
	@LongDateFormat varchar(20),
	@ShortDateFormat varchar(20),
	@TimeFormat varchar(20),
	@CurrencyFormat varchar(20),
	@Settings varchar(max),
	@Lang int
AS

INSERT INTO
	KxMobile.Applications
	(
	ApplicationID,
	Application,
	Inactive,
	LicenceInfo,
	FeatureAccess,
	FullDateFormat,
	LongDateFormat,
	ShortDateFormat,
	TimeFormat,
	CurrencyFormat,
	Settings,
	Lang
	)
VALUES
	(
	@ApplicationID,
	@Application,
	@Inactive,
	@LicenceInfo,
	@FeatureAccess,
	@FullDateFormat,
	@LongDateFormat,
	@ShortDateFormat,
	@TimeFormat,
	@CurrencyFormat,
	@Settings,
	@Lang
	)
 
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[qryApprovalDevice]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryApprovalDevice]
	@DeviceID			int,
	@ApplicationID		int,
	@UserID				int,
	@CreationUserID		int,
	@RegistrationDate	Datetime
AS

DECLARE @FeatureAccess varchar(max)

SELECT @FeatureAccess = A.FeatureAccess
FROM	KxMobile.Applications A
WHERE	A.ApplicationID = @ApplicationID

IF EXISTS (SELECT NULL FROM UserGroups UG JOIN Users U ON U.UserGroupID = UG.UserGroupID WHERE U.UserID = @UserID AND IsNull(UG.KxMobileFeatureAccess, '') <> '')
BEGIN
	SELECT @FeatureAccess = UG.KxMobileFeatureAccess FROM UserGroups UG JOIN Users U ON U.UserGroupID = UG.UserGroupID WHERE U.UserID = @UserID
END
	
IF NOT EXISTS (SELECT NULL FROM KxMobile.AppApproval  AA1 WHERE AA1.ApplicationID = @ApplicationID and AA1.UserID = @UserID AND AA1.DeviceID = @DeviceID)
BEGIN
	INSERT INTO KxMobile.AppApproval (ApplicationID, DeviceID, ApprovalDate, UserID, SecurityPin, FeatureAccess, CreationUserID)
	VALUES (@ApplicationID, @DeviceID, @RegistrationDate, @UserID, '', @FeatureAccess, @CreationUserID)
END

GO
/****** Object:  StoredProcedure [KxMobile].[qryAssignDelegateToRoom]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryAssignDelegateToRoom]
	@EventModuleDelegateID	int,
	@ResidentialRoomID int,
	@UserID int,
	@AppName varchar(20),
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE	@EventModuleID		int,
	@PersonID		int,
	@FromDate		datetime,
	@ToDate			datetime,
	@BookingStatus		varchar(50),
	@BedIndex		tinyint,
	@PersonSequenceID	int,
	@SingleOccupant		bit,
	@Notes			varchar(255),
	@WakeUpCallTime		datetime,
	@Created		int,
	@CreationUserID		int,
	@DepatureDate		DateTime

SELECT	@EventModuleID = EMD.EventModuleID,
	@PersonID = EMD.PersonID,
	@FromDate = CAST(FLOOR(CAST(EMD.ArrivalDate as money)) as DateTime),
	@ToDate = CAST(FLOOR(CAST(EMD.DepartureDate as money)) as DateTime) -1,
	@BookingStatus = null,
	@BedIndex = 0,
	@PersonSequenceID = EMD.PersonSequenceID,
	@SingleOccupant = 0,
	@Notes = '',
	@WakeUpCallTime =  CAST(FLOOR(CAST(getdate() as money)) as DateTime)
FROM	EventModuleDelegates EMD
WHERE	EMD.EventModuleDelegateID = @EventModuleDelegateID

DECLARE @ALREADY int

SELECT @Already = COUNT(*) FROM ResidentialRoomOccupants R WHERE R.Date BETWEEN @FromDate and @ToDate AND R.PersonID = @PersonID AND R.PersonSequenceID = @PersonSequenceID

IF @ALREADY > 0 
BEGIN
	UPDATE ResidentialRoomOccupants 
	SET ResidentialRoomID = @ResidentialRoomID
	WHERE Date BETWEEN @FromDate and @ToDate AND PersonID = @PersonID AND PersonSequenceID = @PersonSequenceID
END
ELSE
BEGIN
	exec dbo.NewResidentialRoomOccupants @ResidentialRoomID,@EventModuleID,@PersonID, @FromDate, @ToDate, NULL,0,@PersonSequenceID,0,'',@WakeupCallTime,@Created output,1,NULL
END

IF EXISTS(SELECT NULL FROM EventModuleResidentialRoomAllocation RA WHERE RA.EventModuleID = @EventModuleID)
BEGIN
	IF NOT EXISTS (SELECT NULL FROM EventModuleResidentialRoomAllocation RA WHERE RA.EventModuleID = @EventModuleID AND RA.ResidentialRoomID = @ResidentialRoomID)
	BEGIN
		INSERT INTO EventModuleResidentialRoomAllocation (EventModuleID, FirstNight, LastNight, ResidentialRoomID, CreationDate, CreationUserID)
		VALUES (@EventModuleID, @FromDate, @ToDate, @ResidentialRoomID, getdate(), @UserID)
	END
END

SET	@DepatureDate = @ToDate + 1

exec qryUpdateBedroomReservations @EventModuleID, @PersonID, @PersonSequenceID, 0, @ResidentialRoomID, @FromDate, @FromDate, @DepatureDate, @DepatureDate, 0, 0, 0 
 
DELETE	FROM WakeUpCalls
WHERE	EventModuleID = @EventModuleID
AND	ResidentialRoomID NOT IN (
		SELECT	RRO.ResidentialRoomID 
		FROM	ResidentialRoomOccupants RRO
		JOIN	WakeUpCalls WUC ON WUC.ResidentialRoomID = RRO.ResidentialRoomID
				AND WUC.PersonID = RRO.PersonID
				AND WUC.PersonSequenceID = RRO.PersonSequenceID
				AND WUC.Date = RRO.Date
		WHERE	RRO.EventModuleID = @EventModuleID)
DECLARE @RoomAuditLogOperationID int
SELECT @RoomAuditLogOperationID = RoomAuditLogOperationID FROM RoomAuditLogOperation WHERE Code = 'ALLOCATED'

INSERT INTO RoomAuditLog(ResidentialRoomID, CreationUserID, CreationDate, RoomAuditLogOperationID, PersonID, PersonSequenceID, EventModuleID, Notes, Form)
	VALUES(@ResidentialRoomID,@UserID,GETDATE(),@RoomAuditLogOperationID,@PersonID,@PersonSequenceID,@EventModuleID,'Assign To Room',@AppName)


GO
/****** Object:  StoredProcedure [KxMobile].[qryAuditStatsByDevice]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryAuditStatsByDevice]
	@ApplicationID int,
	@DeviceID int
AS

SELECT
	A.AuditID,
	A.StoredProc,
	A.StoredProcParams,
	A.AuditInfo,
	A.RecordDateTime	
FROM	KxMobile.Audit A
WHERE	A.ApplicationID = @ApplicationID
AND		A.DeviceID = @DeviceID
ORDER BY A.AuditID DESC


GO
/****** Object:  StoredProcedure [KxMobile].[qryAuditStatsDevices]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryAuditStatsDevices]
AS

SELECT
	*
FROM	KxMobile.Devices
ORDER BY DeviceName 


GO
/****** Object:  StoredProcedure [KxMobile].[qryAuditStatsExecute]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryAuditStatsExecute]
	@SQL nvarchar(max),
	@OverrideRollback bit=0
AS

BEGIN TRANSACTION

EXEC(@SQL)

IF IsNull(@OverrideRollback, 0) =1
	COMMIT
ELSE
	ROLLBACK


GO
/****** Object:  StoredProcedure [KxMobile].[qryAuditStatsExecutions]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryAuditStatsExecutions]
AS

SELECT
	A.StoredProc,
	Count(*) as Executions,
	Min(CAST(DATEDIFF(MILLISECOND, A.RecordDateTime, A2.RecordDateTime) as float)/1000) as MinDuration,
	Max(CAST(DATEDIFF(MILLISECOND, A.RecordDateTime, A2.RecordDateTime) as float)/1000) as MaxDuration,
	AVG(CAST(DATEDIFF(MILLISECOND, A.RecordDateTime, A2.RecordDateTime) as float)/1000) as AvgDuration,
	MAX(A.RecordDateTime) as LastTimeRun
FROM KxMobile.Audit A
JOIN KxMobile.Audit A2 ON A2.StoredProc = A.StoredProc AND A2.StoredProcParams = A.StoredProcParams AND A2.AuditID > A.AuditID AND A.ApplicationID = A2.ApplicationID AND a.DeviceID = A2.DeviceID
WHERE	DATEDIFF(MINUTE, A.RecordDateTime, A2.RecordDateTime) < 10
AND	NOT EXISTS (SELECT NULL FROM KxMobile.Audit A3 WHERE A3.StoredProc = A.StoredProc AND A3.StoredProcParams = A.StoredProcParams AND A3.AuditID > A.AuditID AND A3.AuditID < A2.AuditID AND A.ApplicationID = A2.ApplicationID AND a.DeviceID = A2.DeviceID AND A3.AuditInfo = 'Complete')
AND	A.StoredProc <> ''
AND A.AuditInfo = 'Start'
AND A2.AuditInfo = 'Complete'
GROUP BY
	A.StoredProc
ORDER BY 1


GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckInCustomFields]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckInCustomFields]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	AL.EntityID,
		CF.FieldName
FROM	KxMobile.AppLookups AL
JOIN	CustomFields2 CF ON CF.CustomFieldID = AL.EntityID
WHERE	AL.ApplicationID = @ApplicationID
AND		AL.EntityType = 'EMDCheckInUDF'
ORDER BY 
	AL.Integer1


GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckInDelegate]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckInDelegate]
	@EventModuleDelegateID	int,
	@UserID int,
	@PaymentMethodID int,
	@CarReg varchar(255),
	@BarAccount bit,
	@Mobile varchar(255)='',
	@Telephone varchar(255)='',
	@Email varchar(255)='',
	@Signature image='',
	@StoreSignature bit=0,
	@HKNotes varchar(max)='',
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @CheckedInDate		datetime, @PersonID int
SET	@CheckedInDate = getDate();

DECLARE @EventModuleID1 int, @PersonID1 int, @PersonSequenceID1 int

SELECT @EventModuleID1 = D.EventModuleID, @PersonID1 = D.PersonID, @PersonSequenceID1 = D.PersonSequenceID FROM EventModuleDelegates D WHERE D.EventModuleDelegateID = @EventModuleDelegateID

SET @BarAccount = NullIf(@BarAccount, 0)
SET @CarReg = NullIf(@CarReg, '') 

exec dbo.SetEventModuleDelegates @EventModuleID = @EventModuleID1, @PersonID = @PersonID1, @PersonSequenceID = @PersonSequenceID1, @CheckedInDate = @CheckedInDate, @ModifiedUserID = @UserID,
		@AllowBarPosting = @BarAccount, @CarRegNumber = @CarReg, @CheckInNotes	= @HKNotes, @CheckedOutDate = '18990101'

UPDATE
	PersonFinancial
SET
	PaymentMethodID		= @PaymentMethodID
WHERE
	PersonID		= @PersonID1
AND @PaymentMethodID > 0

UPDATE
	People
SET
	Email		= IsNull(NullIf(@Email, ''), Email),
	Mobile		= IsNull(NullIf(@Mobile, ''), Mobile),
	Telephone	= IsNull(NullIf(@Telephone, ''), Telephone)
WHERE
	PersonID		= @PersonID1

IF @StoreSignature = 1
BEGIN
	DELETE FROM Documents where EntityID = @EventModuleDelegateID and EntityType = 'KxMobileCheckInSign'

	INSERT INTO Documents (DocumentType, EntityID, EntityType, Compressed, Document) 
		VALUES('KxMobileCheckInSignature.png', @EventModuleDelegateID, 'KxMobileCheckInSign', 0, @Signature) 
END


GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckInList]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckInList]
	@SiteID int=0,
	@AreaID int = 0,
	@BlockID int = 0,
	@Date datetime=0,
	@OrderBy varchar(255)='',
	@EventTypeID int=0,
	@EventID int=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SET NOCOUNT ON

DECLARE
	@ForDate bit,
	@ShowStudents bit

SET @ForDate = CASE WHEN (@Date < '1980-01-01') OR (@Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS float)) AS datetime)) THEN 0 ELSE 0 END

SET	@ShowStudents = IsNull((SELECT VALUE FROM CustomSettings WHERE Customsetting = 'Resident.Show.Students.On.Checkin'),0)

DECLARE @CheckinCutoff int
DECLARE @CutoffDaysFromDeparture int  /*DLD 29/3/06 */
DECLARE @WalkInEventTypeID int
DECLARE @SurnameFirst int
--SET @CheckinCutoff = ISNULL((SELECT TOP 1 CAST(Value AS int) FROM CustomSettings WHERE CustomSetting = 'CHECK_IN_DELAY'), 0)

/* DLD 29/3/06 These values are now stored in Control table */
SET @CheckinCutoff = ISNULL((SELECT TOP 1 CheckInCutoffDays FROM Control), 0)
SET @CutoffDaysFromDeparture = ISNULL((SELECT TOP 1 CheckInCutoffDaysFromDeparture FROM Control), 0)
SET @WalkInEventTypeID = ISNULL((SELECT TOP 1 WalkInEventTypeID FROM Control), 0)
SET @SurnameFirst = ISNULL((SELECT TOP 1 SurnameFirst FROM Control), 0)

DECLARE	@NewFlats int
SELECT	@NewFlats = IsNull(Value,0) FROM CustomSettings WHERE CustomSetting = 'Residential.2009.Flat.Reservation.System'
SET @NewFlats = IsNull(@NewFlats,0)

CREATE TABLE #CheckInList  (Name nvarchar(255), ArrivalDate DateTime, DepartureDate Datetime, EventID int, EventModuleID int, Description varchar(255), CheckedInDate datetime,  CheckedOutDate datetime, PersonID int,
			PersonSequenceID int, ReceivedMedical bit, Type Char(1), NumOccupants int, ResidentialRoomID int, SingleOccupancy bit, ExtNo varchar(50),
			NotesExist int, Info varchar(255), DelegateNotes varchar(8000), CheckInNotes varchar(8000), HouseKeepingNotes varchar(8000), Resident bit,
			RoomName varchar(255), RoomExternalID varchar(255), RoomType varchar(255), BookByRoom bit, BlockID int, BlockName varchar(255), SubBlockName varchar(255),
			Surname nvarchar(255), Forename nvarchar(255), Smoker bit, [Disabled] bit, VIP bit, DelegateEventModuleID int, HouseKeepingReqNoteID int, EventSiteID int,
			BedroomSiteID int, CreatorSiteID int, MaidStatus varchar(20), PreviousPersonID int, AreaID int, AreaDescription varchar(255), WalkIn bit, PaymentType varchar(255),
			CreateCharges bit, PMSEnabled bit, AllowBarPosting bit, CarRegNumber varchar(50), RouteChargesToPersonID int, RouteChargesToPersonSequenceID int, PhoneAccessCode varchar(20),
			DelegateGroupID int, DelegateGroup varchar(255), GroupColour varchar(50), HasNotes bit, HousekeepingNotesFound bit, DelegateNotesFound bit,
			CheckInNotesFound bit, AllocationType varchar(1), HeaderRoomName varchar(255), SortOrder varchar(255), HeaderResidentialRoomID int, RTFNotes varchar(8000),
			GroupShortName varchar(4), EventModuleDelegateID int, DisplayOrder int, GuestType varchar(1), DelegateCount Integer, ID int identity(1,1)
			)
			 
CREATE TABLE #EventModuleDelegates (
	EventModuleID int NOT NULL,
	PersonID int NOT NULL,
	PersonSequenceID int NOT NULL,
	DelegateGroupID int NULL,
	ArrivalDate datetime NULL,
	DepartureDate datetime NULL,
	CheckedInDate datetime NULL,
	CheckedOutDate datetime NULL,
	ReceivedMedical bit NULL,
	Type char(1) NULL,
	CreationUserID int NOT NULL,
	Notes text NULL,
	SingleOccupancy bit NULL,
	Resident bit NULL,
	CheckInNotes text NULL,
	ResidentialRoomID int NULL,
	BlockID int NULL,
	BedroomTypeID int NULL,
	NumOccupants int NOT NULL,
	PaymentTypeID int NULL,
	AllowBarPosting bit NOT NULL,
	CarRegNumber varchar(20) NULL,
	RouteChargesToPersonID int NULL,
	HousekeepingNotes text NULL,
	PhoneAccessCode varchar(2) NULL,
	RouteChargesToPersonSequenceID int NULL,
	EventModuleDelegateID int,
	ArrivalDateOnly DateTime,
	DepartureDateOnly DateTime,
	GroupNumber int,
	ListedAdults int,
	GuestType varchar(1)
	)

INSERT INTO #EventModuleDelegates
SELECT
	EMD.EventModuleID,
	PersonID,
	PersonSequenceID,
	DelegateGroupID,
	EMD.ArrivalDate,
	DepartureDate,
	CheckedInDate,
	CheckedOutDate,
	ReceivedMedical,
	[Type],
	EMD.CreationUserID,
	EMD.Notes,
	SingleOccupancy,
	Resident,
	CheckInNotes,
	ResidentialRoomID,
	BlockID,
	BedroomTypeID,
	NumOccupants,
	PaymentTypeID,
	AllowBarPosting,
	CarRegNumber,
	RouteChargesToPersonID,
	HousekeepingNotes,
	PhoneAccessCode,
	RouteChargesToPersonSequenceID,
	EventModuleDelegateID,
	CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS datetime) as ArrivalDateOnly,
	CAST(FLOOR(CAST(EMD.DepartureDate AS float)) AS datetime) as DepartureDateOnly,
	0,
	0,
	CASE WHEN EXISTS (SELECT * FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID) THEN 'S' ELSE 'G' END
FROM
	EventModuleDelegates EMD
JOIN	EventModules EM ON EMD.EventModuleID = EM.EventModuleID
JOIN	Events E ON E.EventID = EM.EventID
WHERE
	(ISNULL(EMD.CheckedInDate,0) <= 2)
AND	ISNULL(@SiteID, 0) <> -1
AND (@EventID = 0 OR @EventID = EM.EventID)
AND (@EventTypeID = 0 OR @EventTypeID = E.EventTypeID)
/*Only include those where ArrivalDate is today or within the last x days (x = @CutoffDays)
	i.e. as soon we reach @CutoffDays after the arrival date then the person is not shown on the check-in list anymore*/
/*Only include those with Departure Date in the future or within the last x days (x= @CutoffDaysFromDeparture)
i.e. a person not checked-in continues to show for @CutoffDaysFromDeparture after their scheduled departure date. */
AND	(@ForDate = 1
	OR (CAST(FLOOR(CAST(@Date AS money)) AS datetime)
	        BETWEEN CAST(FLOOR(CAST(EMD.ArrivalDate AS money)) AS datetime) AND
		CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS money) + @CheckinCutoff))
		--AND EMD.DepartureDate > CAST(FLOOR(CAST(@Date AS float)) AS money) - @CutoffDaysFromDeparture))
AND	EMD.PersonID > 0
AND	(@ShowStudents = 1 OR NOT EXISTS (SELECT NULL FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID))
AND	(EMD.EventModuleID > 0 OR EMD.EventModuleID = -9999) --Warwick to include Students for vaction residents
--AND	(@Date < '1980-01-01' OR CAST(FLOOR(CAST(EMD.ArrivalDate AS money)) AS DateTime) = @Date)

CREATE INDEX IX1 ON #EventModuleDelegates(EventModuleID, PersonID, PersonSequenceID, ArrivalDateOnly)
CREATE INDEX IX1B ON #EventModuleDelegates(EventModuleID)

UPDATE EMD
SET	GroupNumber = EMBBAD.GroupNumber, ListedAdults = 0
FROM	#EventModuleDelegates EMD
JOIN	EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBAD.EventModuleID = EMD.EventModuleID AND EMBBAD.PersonID = EMD.PersonID AND EMD.PersonSequenceID =  EMBBAD.PersonSequenceID

UPDATE	#EventModuleDelegates
SET	ListedAdults = (SELECT	COUNT(DISTINCT(EMBBAD.PersonID))
			FROM	EventModuleBlockBedroomAllocationDelegates EMBBAD
			WHERE	EMBBAD.EventModuleID = #EventModuleDelegates.EventModuleID
			AND	#EventModuleDelegates.GroupNumber = EMBBAD.GroupNumber
			AND	EMBBAD.PersonID <> -999)

CREATE TABLE #ResidentialRoomOccupants (EventModuleID int, PersonID int, PersonSequencEID int, ResidentialRoomID int, Date DateTime)

INSERT INTO #ResidentialRoomOccupants
SELECT	RRO.EventModuleID, RRO.PersonID, RRO.PersonSequenceID, RRO.ResidentialRoomID, RRO.Date
FROM	ResidentialRoomOccupants RRO
JOIN	#EventModuleDelegates D ON D.EventModuleID = RRO.EventModuleID AND D.PersonID = RRO.PersonID AND D.PersonSequenceID = RRO.PersonSequenceID AND RRO.Date = D.ArrivalDateOnly

CREATE INDEX IX6 ON #ResidentialRoomOccupants(EventModuleID, PersonID, PersonSequenceID, Date)

CREATE TABLE #EMBBA (EventModuleID int, ResidentialRoomID int, Date DateTime, AllocationType varchar(1))
CREATE INDEX IDX_EMBBA1 ON #EMBBA (EventModuleID, ResidentialRoomID, Date)

INSERT INTO #CheckInList
SELECT	DISTINCT
	CASE WHEN @SurnameFirst = 1 THEN
		CASE WHEN P.Surname = '' THEN P.Forename WHEN P.Forename = '' THEN P.Surname ELSE P.Surname + ', ' + P.Forename END + CASE WHEN P.Title = '' THEN '' ELSE ' (' + P.Title + ')' END
	ELSE
		LTRIM(P.Title + ' ') + LTRIM(P.Forename + ' ') + P.Surname END as Name,
	EMD.ArrivalDate,
	EMD.DepartureDate,
	EM.EventID,
	EMD.EventModuleID,
	RTrim(LTrim(EM.Description)) as Description,
	CAST(0 AS DateTime) AS CheckedInDate,
	CAST(0 AS DateTime) AS CheckedOutDate,
	EMD.PersonID,
	EMD.PersonSequenceID,
	EMD.ReceivedMedical,
	EMD.Type,
	EMD.NumOccupants,
	RRO.ResidentialRoomID,
	EMD.SingleOccupancy,
	R.ExtNo,
	0 as NotesExist,
	R.Info,
	ISNULL(RTRIM(CAST(EMD.Notes AS varchar(max))), '') AS DelegateNotes,
	ISNULL(RTRIM(CAST(EMD.CheckInNotes AS varchar(max))), '') AS CheckInNotes,
	ISNULL(RTRIM(CAST(EMD.HousekeepingNotes AS varchar(max))), '') AS HousekeepingNotes,
	EMD.Resident,
	CASE
	WHEN R.ResidentialRoomID IS NULL THEN
		PBT.Description
	WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		CAST(RRH.Name as varchar(255))
	ELSE
		CAST(CASE WHEN BT.BookByRoom = 1 THEN LEFT(RTRIM(BB.Name), PATINDEX('% %', BB.Name)) + ': ' ELSE '' END + RTRIM(R.Name) AS varchar(255))
	END AS RoomName,
	R.ExternalID AS RoomExternalID,
	CASE WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		BTH.Description
	ELSE
		BT.Description
	END as RoomType,
	BT.BookByRoom,
	R.BlockID,
	SB.Name as BlockName,
	BB.Name as SubBlockName,
	P.Surname,
	P.Forename,
	P.Smoker,
	P.Disabled,
	P.VIP,
	EMD.EventModuleID as DelegateEventModuleID,
	ISNULL(P.HouseKeepingReqNoteID, 0) AS HouseKeepingReqNoteID,
	EM.SiteID as EventSiteID,
	SB.SiteID as BedroomSiteID,
	U.SiteID AS CreatorSiteID,
	CASE WHEN IsNull(R.MaidStatus, 'X') = '' THEN 'I' ELSE R.MaidStatus END as MaidStatus,
	0 as PreviousPersonID,
	SB.AreaID,
	A.Description AS AreaDescription,
	CASE E.EventTypeID WHEN @WalkInEventTypeID THEN CAST(1 AS Bit) ELSE CAST(0 AS Bit) END AS WalkIn,
	PT.Description as PaymentType,
	PT.CreateCharges,
	SIT.PMSEnabled,
	EMD.AllowBarPosting,
	EMD.CarRegNumber,
	EMD.RouteChargesToPersonID,
	IsNull(EMD.RouteChargesToPersonSequenceID, 0) as RouteChargesToPersonSequenceID,--removed
	EMD.PhoneAccessCode,
	EMD.DelegateGroupID,
	DG.Description AS DelegateGroup,
	DG.GroupColour,
	0 as HasNotes,
	0 as HousekeepingNotesFound,
	0 as DelegateNotesFound,
	0 as CheckInNotesFound,
	CASE WHEN @NewFlats = 1 THEN EMBBA.AllocationType ELSE '' END as AllocationType,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN IsNull(RRH.Name, '') ELSE '' END as HeaderRoomName,
	'' as SortOrder,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN RRH.ResidentialRoomID
			WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'R' THEN R.ResidentialRoomID ELSE 0 END AS HeaderResidentialRoomID,
	'' as RTFNotes,
	RTRIM(CASE WHEN RTrim(IsNull(DG.ShortName, '')) = '' THEN LEFT(LTRIM(DG.Description),1) ELSE DG.ShortName END) as GroupShortName,
	EventModuleDelegateID,
	R.DisplayOrder,
	EMD.GuestType,
	(SELECT COUNT(*) FROM #EventModuleDelegates EMD2 WHERE EMD2.EventModuleID = EMD.EventModuleID) as DelegateCount
FROM
	#EventModuleDelegates EMD
	JOIN EventModules EM ON EM.EventModuleID = EMD.EventModuleID
	JOIN Events E ON E.EventID = EM.EventID
	JOIN Users U ON U.UserID = EMD.CreationUserID
	JOIN People P ON P.PersonID = EMD.PersonID
	JOIN Status ST ON ST.StatusID = EM.StatusID
	LEFT JOIN SiteBlocks PB ON PB.BlockID = EMD.BlockID
	LEFT JOIN BedroomTypes PBT ON PBT.BedroomTypeID = EMD.BedroomTypeID
	LEFT JOIN #ResidentialRoomOccupants RRO
		ON    	RRO.EventModuleID 	= EMD.EventModuleID
		AND	RRO.PersonID		= EMD.PersonID
		AND	RRO.PersonSequenceID	= EMD.PersonSequenceID
		AND	RRO.Date		= EMD.ArrivalDateOnly
	LEFT JOIN ResidentialRooms R ON R.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
	LEFT JOIN SiteBlocks BB ON BB.BlockID = R.SubBlockID
	LEFT JOIN SiteBlocks SB ON SB.BlockID = R.BlockID
	LEFT JOIN Sites SIT ON SIT.SiteID = SB.SiteID
	LEFT JOIN Areas A ON A.AreaID = SB.AreaID
	LEFT JOIN DelegateGroups DG ON DG.DelegateGroupID = EMD.DelegateGroupID
	LEFT JOIN EventModuleBlockBedroomAllocation /* #EMBBA*/ EMBBA ON EMBBA.EventModuleID = EMD.EventModuleID AND EMBBA.ResidentialRoomID = R.ResidentialRoomID AND EMBBA.Date = RRO.Date
	LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = R.HeaderResidentialRoomID
	LEFT JOIN BedroomTypes BTH ON BTH.BedroomTypeID = RRH.BedroomTypeID
	LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
	LEFT JOIN SiteBlocks SB2 ON SB2.BlockID = EMD.BlockID
WHERE
	ST.EffectCapacity = 1
AND (@SiteID = 0 OR IsNull(SB.SiteID, IsNull(SB2.SiteID, @SiteID)) = @SiteID)
AND	(@AreaID = 0 OR IsNull(SB.AreaID, IsNull(SB2.AreaID, 0)) = @AreaID)
AND	(@BlockID = 0 OR IsNull(SB.BlockID, IsNull(SB2.BlockID, 0)) = @BlockID)
AND	(@NewFlats = 0 OR not (ISNULL(EMBBA.AllocationType,'') = 'F' AND R.HeaderResidentialRoomID = -1))
AND	ISNULL(@SiteID, 0) <> -1

UPDATE D SET HasNotes = CAST(CASE WHEN IsNull(D.HousekeepingNotes,'') + IsNull(D.DelegateNotes,'') + IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS Bit) FROM #CheckInList D
UPDATE D SET NotesExist = CAST(CASE WHEN D.HasNotes = 1 THEN 1 ELSE 0 END AS Int) FROM #CheckInList D
UPDATE D SET HousekeepingNotesFound = CAST(CASE WHEN IsNull(D.HouseKeepingNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET DelegateNotesFound = CAST(CASE WHEN IsNull(D.DelegateNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET CheckInNotesFound = CAST(CASE WHEN IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.DelegateNotes, '')) <> '' THEN ISNULL('<b>Reg Card Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.DelegateNotes AS varchar(max)), ''), '') ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.CheckInNotes, '')) <> '' THEN CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Check In Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.CheckInNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.HouseKeepingNotes, '')) <> '' THEN	CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Housekeeping Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.HouseKeepingNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RtfNotes = REPLACE(RTFNotes, CHAR(13)+CHAR(10), '<br/>')  FROM #CheckInList D WHERE D.RTFNotes <> ''

DECLARE @Yesterday datetime

SET @Yesterday = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)-1

UPDATE	CL
SET	PreviousPersonID =
		ISNULL((
			SELECT TOP 1
				RRO.PersonID
			FROM	ResidentialRoomOccupants RRO WITH (NOLOCK)
			JOIN	EventModuleDelegates EMD WITH (NOLOCK) ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
			WHERE	(EMD.CheckedInDate > 2)
			AND	(EMD.CheckedOutDate <= 2)
			AND	CAST(FLOOR(CAST(EMD.ArrivalDate AS money)) AS datetime) <= @Yesterday
			AND	CAST(FLOOR(CAST(EMD.DepartureDate AS money)) AS datetime) >= @Yesterday
			AND     CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = @Yesterday
			AND	EMD.PersonID > 0
			AND	NOT EXISTS (SELECT NULL FROM ST2AcademicYears AY WHERE AY.EventModuleID = RRO.EventModuleID)
			AND	RRO.ResidentialRoomID = CL.ResidentialRoomID
			AND	CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = (SELECT MAX(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = RRO.EventModuleID AND RRO1.PersonID = RRO.PersonID AND RRO1.PersonSequenceID = RRO.PersonSequenceID)
			ORDER BY
				RRO.Date DESC
		), 0)
FROM	#CheckInList CL
	
SELECT
	C.*,
	LTrim(IsNull(STUFF((SELECT ', ' + RTRIM(CV2.FieldName) + '=' + D.FieldValue
			  FROM CustomFields2Values D
			  JOIN CustomFields2 CV2 ON CV2.CustomFieldID = D.CustomFieldID
			  WHERE D.EntityType = 'EventModuleDelegates' AND D.EntityID = C.EventModuleDelegateID 
			  ORDER BY CV2.FieldName DESC
			  FOR XML PATH('')), 1, 1, ''), '')) as UDFs
FROM
	#CheckInList C
ORDER BY
--	CASE WHEN @OrderBy = 'Bedroom' THEN DisplayOrder ELSE NULL END,
--	CASE WHEN @OrderBy = 'Bedroom' THEN HeaderRoomName ELSE NULL END,
	CASE WHEN @OrderBy = 'Event' THEN EventID ELSE NULL END,
	Surname,
	Forename--,
	--DisplayOrder,
	--RoomName

DROP TABLE #CheckInList

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckInReportDetails]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckInReportDetails]
	@EventModuleDelegateID int,
	@Lang int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @SurnameFirst int
DECLARE @WalkInEventTypeID int
SET @SurnameFirst = ISNULL((SELECT TOP 1 SurnameFirst FROM Control), 0)
SET @WalkInEventTypeID = ISNULL((SELECT TOP 1 WalkInEventTypeID FROM Control), 0)
DECLARE	@NewFlats int
SELECT	@NewFlats = IsNull(Value,0) FROM CustomSettings WHERE CustomSetting = 'Residential.2009.Flat.Reservation.System'
SET @NewFlats = IsNull(@NewFlats,0)

CREATE TABLE #CheckInList  (Name nvarchar(255), ArrivalDate DateTime, EventID int, EventModuleID int, Description varchar(255), CheckedInDate datetime,  CheckedOutDate datetime, PersonID int,
			PersonSequenceID int, DepartureDate Datetime, ReceivedMedical bit, Type Char(1), NumOccupants int, ResidentialRoomID int, SingleOccupancy bit, ExtNo varchar(50),
			NotesExist int, Info varchar(255), DelegateNotes varchar(max), CheckInNotes varchar(max), HouseKeepingNotes varchar(max), Resident bit,
			RoomName varchar(255), RoomExternalID varchar(255), RoomType varchar(255), BookByRoom bit, BlockID int, BlockName varchar(255), SubBlockName varchar(255),
			Surname nvarchar(255), Forename nvarchar(255), Smoker bit, [Disabled] bit, VIP bit, DelegateEventModuleID int, HouseKeepingReqNoteID int, EventSiteID int,
			BedroomSiteID int, CreatorSiteID int, MaidStatus varchar(20), PreviousPersonID int, AreaID int, AreaDescription varchar(255), WalkIn bit, PaymentType varchar(255),
			CreateCharges bit, PMSEnabled bit, AllowBarPosting bit, CarRegNumber varchar(50), RouteChargesToPersonID int, RouteChargesToPersonSequenceID int, PhoneAccessCode varchar(20),
			DelegateGroupID int, DelegateGroup varchar(255), GroupColour varchar(50), HasNotes bit, HousekeepingNotesFound bit, DelegateNotesFound bit,
			CheckInNotesFound bit, AllocationType varchar(1), HeaderRoomName varchar(255), SortOrder varchar(255), HeaderResidentialRoomID int, RTFNotes varchar(max),
			GroupShortName varchar(4), EventModuleDelegateID int, DisplayOrder int, GuestType varchar(1), DelegateCount Integer, ID int identity(1,1), UDFs varchar(max)
			)
			 
CREATE TABLE #EventModuleDelegates (
	EventModuleID int NOT NULL,
	PersonID int NOT NULL,
	PersonSequenceID int NOT NULL,
	DelegateGroupID int NULL,
	ArrivalDate datetime NULL,
	DepartureDate datetime NULL,
	CheckedInDate datetime NULL,
	CheckedOutDate datetime NULL,
	ReceivedMedical bit NULL,
	Type char(1) NULL,
	CreationUserID int NOT NULL,
	Notes text NULL,
	SingleOccupancy bit NULL,
	Resident bit NULL,
	CheckInNotes text NULL,
	ResidentialRoomID int NULL,
	BlockID int NULL,
	BedroomTypeID int NULL,
	NumOccupants int NOT NULL,
	PaymentTypeID int NULL,
	AllowBarPosting bit NOT NULL,
	CarRegNumber varchar(20) NULL,
	RouteChargesToPersonID int NULL,
	HousekeepingNotes text NULL,
	PhoneAccessCode varchar(2) NULL,
	RouteChargesToPersonSequenceID int NULL,
	EventModuleDelegateID int,
	ArrivalDateOnly DateTime,
	DepartureDateOnly DateTime,
	GroupNumber int,
	ListedAdults int,
	GuestType varchar(1)
	)

INSERT INTO #EventModuleDelegates
SELECT
	EventModuleID,
	PersonID,
	PersonSequenceID,
	DelegateGroupID,
	ArrivalDate,
	DepartureDate,
	CheckedInDate,
	CheckedOutDate,
	ReceivedMedical,
	Type,
	CreationUserID,
	Notes,
	SingleOccupancy,
	Resident,
	CheckInNotes,
	ResidentialRoomID,
	BlockID,
	BedroomTypeID,
	NumOccupants,
	PaymentTypeID,
	AllowBarPosting,
	CarRegNumber,
	RouteChargesToPersonID,
	HousekeepingNotes,
	PhoneAccessCode,
	RouteChargesToPersonSequenceID,
	EventModuleDelegateID,
	CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS datetime) as ArrivalDateOnly,
	CAST(FLOOR(CAST(EMD.DepartureDate AS float)) AS datetime) as DepartureDateOnly,
	0,
	0,
	CASE WHEN EXISTS (SELECT * FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID) THEN 'S' ELSE 'G' END
FROM
	EventModuleDelegates EMD
WHERE
	(@EventModuleDelegateID = EMD.EventModuleDelegateID)

CREATE INDEX IX1 ON #EventModuleDelegates(EventModuleID, PersonID, PersonSequenceID, ArrivalDateOnly)
CREATE INDEX IX1B ON #EventModuleDelegates(EventModuleID)

UPDATE EMD
SET	GroupNumber = EMBBAD.GroupNumber, ListedAdults = 0
FROM	#EventModuleDelegates EMD
JOIN	EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBAD.EventModuleID = EMD.EventModuleID AND EMBBAD.PersonID = EMD.PersonID AND EMD.PersonSequenceID =  EMBBAD.PersonSequenceID

UPDATE	#EventModuleDelegates
SET	ListedAdults = (SELECT	COUNT(DISTINCT(EMBBAD.PersonID))
			FROM	EventModuleBlockBedroomAllocationDelegates EMBBAD
			WHERE	EMBBAD.EventModuleID = #EventModuleDelegates.EventModuleID
			AND	#EventModuleDelegates.GroupNumber = EMBBAD.GroupNumber
			AND	EMBBAD.PersonID <> -999)

CREATE TABLE #ResidentialRoomOccupants (EventModuleID int, PersonID int, PersonSequencEID int, ResidentialRoomID int, Date DateTime)

INSERT INTO #ResidentialRoomOccupants
SELECT	RRO.EventModuleID, RRO.PersonID, RRO.PersonSequenceID, RRO.ResidentialRoomID, RRO.Date
FROM	ResidentialRoomOccupants RRO
JOIN	#EventModuleDelegates D ON D.EventModuleID = RRO.EventModuleID AND D.PersonID = RRO.PersonID AND D.PersonSequenceID = RRO.PersonSequenceID AND RRO.Date = D.ArrivalDateOnly

CREATE INDEX IX6 ON #ResidentialRoomOccupants(EventModuleID, PersonID, PersonSequenceID, Date)

CREATE TABLE #EMBBA (EventModuleID int, ResidentialRoomID int, Date DateTime, AllocationType varchar(1))

CREATE INDEX IDX_EMBBA1 ON #EMBBA (EventModuleID, ResidentialRoomID, Date)

INSERT INTO #CheckInList
SELECT	DISTINCT
	CASE WHEN @SurnameFirst = 1 THEN
		CASE WHEN P.Surname = '' THEN P.Forename WHEN P.Forename = '' THEN P.Surname ELSE P.Surname + ', ' + P.Forename END + CASE WHEN P.Title = '' THEN '' ELSE ' (' + P.Title + ')' END
	ELSE
		LTRIM(P.Title + ' ') + LTRIM(P.Forename + ' ') + P.Surname END as Name,
	EMD.ArrivalDate,
	EM.EventID,
	EMD.EventModuleID,
	RTrim(LTrim(EM.Description)) as Description,
	CAST(0 AS DateTime) AS CheckedInDate,
	CAST(0 AS DateTime) AS CheckedOutDate,
	EMD.PersonID,
	EMD.PersonSequenceID,
	EMD.DepartureDate,
	EMD.ReceivedMedical,
	EMD.Type,
	EMD.NumOccupants,
	RRO.ResidentialRoomID,
	EMD.SingleOccupancy,
	R.ExtNo,
	0 as NotesExist,
	R.Info,
	ISNULL(RTRIM(CAST(EMD.Notes AS varchar(max))), '') AS DelegateNotes,
	ISNULL(RTRIM(CAST(EMD.CheckInNotes AS varchar(max))), '') AS CheckInNotes,
	ISNULL(RTRIM(CAST(EMD.HousekeepingNotes AS varchar(max))), '') AS HousekeepingNotes,
	EMD.Resident,
	CASE
	WHEN R.ResidentialRoomID IS NULL THEN
		PBT.Description
	WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		CAST(RRH.Name as varchar(255))
	ELSE
		CAST(CASE WHEN BT.BookByRoom = 1 THEN LEFT(RTRIM(BB.Name), PATINDEX('% %', BB.Name)) + ': ' ELSE '' END + RTRIM(R.Name) AS varchar(255))
	END + ' in ' + RTRIM(SB.Name) AS RoomName,
	R.ExternalID AS RoomExternalID,
	CASE WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		BTH.Description
	ELSE
		BT.Description
	END as RoomType,
	BT.BookByRoom,
	R.BlockID,
	SB.Name as BlockName,
	BB.Name as SubBlockName,
	P.Surname,
	P.Forename,
	P.Smoker,
	P.Disabled,
	P.VIP,
	EMD.EventModuleID as DelegateEventModuleID,
	ISNULL(P.HouseKeepingReqNoteID, 0) AS HouseKeepingReqNoteID,
	EM.SiteID as EventSiteID,
	SB.SiteID as BedroomSiteID,
	U.SiteID AS CreatorSiteID,
	CASE WHEN IsNull(R.MaidStatus, 'X') = '' THEN 'I' ELSE R.MaidStatus END as MaidStatus,
	0 as PreviousPersonID,
	SB.AreaID,
	A.Description AS AreaDescription,
	CASE E.EventTypeID WHEN @WalkInEventTypeID THEN CAST(1 AS Bit) ELSE CAST(0 AS Bit) END AS WalkIn,
	PT.Description as PaymentType,
	PT.CreateCharges,
	SIT.PMSEnabled,
	EMD.AllowBarPosting,
	EMD.CarRegNumber,
	EMD.RouteChargesToPersonID,
	IsNull(EMD.RouteChargesToPersonSequenceID, 0) as RouteChargesToPersonSequenceID,--removed
	EMD.PhoneAccessCode,
	EMD.DelegateGroupID,
	DG.Description AS DelegateGroup,
	DG.GroupColour,
	0 as HasNotes,
	0 as HousekeepingNotesFound,
	0 as DelegateNotesFound,
	0 as CheckInNotesFound,
	CASE WHEN @NewFlats = 1 THEN EMBBA.AllocationType ELSE '' END as AllocationType,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN IsNull(RRH.Name, '') ELSE '' END as HeaderRoomName,
	'' as SortOrder,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN RRH.ResidentialRoomID
			WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'R' THEN R.ResidentialRoomID ELSE 0 END AS HeaderResidentialRoomID,
	'' as RTFNotes,
	RTRIM(CASE WHEN RTrim(IsNull(DG.ShortName, '')) = '' THEN LEFT(LTRIM(DG.Description),1) ELSE DG.ShortName END) as GroupShortName,
	EventModuleDelegateID,
	R.DisplayOrder,
	EMD.GuestType,
	(SELECT COUNT(*) FROM #EventModuleDelegates EMD2 WHERE EMD2.EventModuleID = EMD.EventModuleID) as DelegateCount,
	REPLACE(LTrim(IsNull(STUFF((SELECT RTRim(CV2.FieldName) + ' : ' + D.FieldValue + '\\n'
			  FROM CustomFields2Values D
			  JOIN CustomFields2 CV2 ON CV2.CustomFieldID = D.CustomFieldID
			  WHERE D.EntityType = 'EventModuleDelegates' AND D.EntityID = EMD.EventModuleDelegateID 
			  ORDER BY CV2.FieldName DESC
			  FOR XML PATH('')), 1, 0, ''), '')), '\\n' , CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)) as UDFs
FROM
	#EventModuleDelegates EMD
	JOIN EventModules EM ON EM.EventModuleID = EMD.EventModuleID
	JOIN Events E ON E.EventID = EM.EventID
	JOIN Users U ON U.UserID = EMD.CreationUserID
	JOIN People P ON P.PersonID = EMD.PersonID
	JOIN Status ST ON ST.StatusID = EM.StatusID
	LEFT JOIN SiteBlocks PB ON PB.BlockID = EMD.BlockID
	LEFT JOIN BedroomTypes PBT ON PBT.BedroomTypeID = EMD.BedroomTypeID
	LEFT JOIN #ResidentialRoomOccupants RRO
		ON    	RRO.EventModuleID 	= EMD.EventModuleID
		AND	RRO.PersonID		= EMD.PersonID
		AND	RRO.PersonSequenceID	= EMD.PersonSequenceID
		AND	RRO.Date		= EMD.ArrivalDateOnly
	LEFT JOIN ResidentialRooms R ON R.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
	LEFT JOIN SiteBlocks BB ON BB.BlockID = R.SubBlockID
	LEFT JOIN SiteBlocks SB ON SB.BlockID = R.BlockID
	LEFT JOIN Sites SIT ON SIT.SiteID = SB.SiteID
	LEFT JOIN Areas A ON A.AreaID = SB.AreaID
	LEFT JOIN DelegateGroups DG ON DG.DelegateGroupID = EMD.DelegateGroupID
	LEFT JOIN EventModuleBlockBedroomAllocation /* #EMBBA*/ EMBBA ON EMBBA.EventModuleID = EMD.EventModuleID AND EMBBA.ResidentialRoomID = R.ResidentialRoomID AND EMBBA.Date = RRO.Date
	LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = R.HeaderResidentialRoomID
	LEFT JOIN BedroomTypes BTH ON BTH.BedroomTypeID = RRH.BedroomTypeID
	LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
WHERE
	(@EventModuleDelegateID = EMD.EventModuleDelegateID)

UPDATE D SET HasNotes = CAST(CASE WHEN IsNull(D.HousekeepingNotes,'') + IsNull(D.DelegateNotes,'') + IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS Bit) FROM #CheckInList D
UPDATE D SET NotesExist = CAST(CASE WHEN D.HasNotes = 1 THEN 1 ELSE 0 END AS Int) FROM #CheckInList D
UPDATE D SET HousekeepingNotesFound = CAST(CASE WHEN IsNull(D.HouseKeepingNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET DelegateNotesFound = CAST(CASE WHEN IsNull(D.DelegateNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET CheckInNotesFound = CAST(CASE WHEN IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET RTFNotes = ISNULL('{\rtf' +
				NULLIF(
					ISNULL('\b Reg Card Notes:\b0  ' + NULLIF(CAST(D.DelegateNotes AS varchar(max)), '') + '\par', '') +
					ISNULL('\b Check In Notes:\b0  ' + NULLIF(CAST(D.CheckInNotes AS varchar(max)), '') + '\par', '') +
					ISNULL('\b Housekeeping Notes:\b0  ' + NULLIF(CAST(D.HouseKeepingNotes AS varchar(max)), '') + '\par', ''),
					'') + '}', '') FROM #CheckInList D
UPDATE D SET RTFNotes = dbo.fnParseRTF(RTFNotes) FROM #CheckInList D

DECLARE @Yesterday datetime

SET @Yesterday = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)-1

CREATE INDEX IDXAAC ON #CheckInList (EventModuleID, PersonID, PersonSequenceID)

DECLARE	@EventModuleID int,
	@PersonID int,
	@PersonSequenceID int

SELECT	TOP 1 @EventModuleID = EventModuleID, @PersonID = PersonID, @PersonSequenceID = PersonSequenceID from #CheckInList

DECLARE @EventID int,
	@Format int

SET @Lang = 2

SET @Format = 103
IF @Lang = 2 SET @Format = 101

SELECT @EventID = EventID FROM EventModules WHERE EventModuleID = @EventModuleID

SET @PersonSequenceID = ISNULL(@PersonSequenceID, 0)

SELECT
	PersonID,
	PersonSequenceID,
	MIN(CAST(FLOOR(CAST(ArrivalDate AS Float)) AS DateTime)) AS FirstDate,
	MAX(CAST(FLOOR(CAST(DepartureDate AS Float)) - 1 AS DateTime)) AS LastDate
INTO
	#DelegateDates
FROM
	EventModuleDelegates
WHERE
	EventModuleID = @EventModuleID
AND (@PersonID = 0 OR PersonID = @PersonID AND ISNULL(PersonSequenceID, 0) = @PersonSequenceID)
GROUP BY
	PersonID,
	PersonSequenceID

SELECT DISTINCT
	D.EventChargePeriodID,
	D.ChargeTypeID,
	D.MatrixID,
	D.Rate,
	CAST(1 AS bit) AS ContainsNegativeElement
INTO
	#RatesWithNegativeElements
FROM
	(
	SELECT
		EventChargePeriodID,
		ChargeTypeID,
		MatrixID,
		Rate
	FROM
		fnEventChargeTypeRateDetails(@EventID)
	WHERE
		DetailRate < 0
	) D

DECLARE @FirstNight datetime, @LastNight datetime

SELECT
	ECP.EventChargePeriodID,
	ECP.FromDate,
	ECP.ToDate,
	DD.PersonID,
	DD.PersonSequenceID,
	D.Date,
	ECTR.ChargeTypeID,
	ECTR.MatrixID,
	ECTR.Rate,
	CT.AmountType
INTO
	#DailyRates
FROM
	(
	SELECT
		EventChargePeriodID,
		CASE WHEN FromDate <= 0 THEN NULL ELSE FromDate END AS FromDate,
		CASE WHEN ToDate <= 0 THEN NULL ELSE ToDate END AS ToDate
	FROM
		EventChargePeriods ECP
	WHERE
		ECP.EventID = @EventID
	) ECP
	JOIN EventChargeTypeRates ECTR ON ECTR.EventChargePeriodID = ECP.EventChargePeriodID
	JOIN ChargeTypes CT ON CT.ChargeTypeID = ECTR.ChargeTypeID
	JOIN EventChargePeriodDelegates ECPD ON ECPD.EventChargePeriodID = ECP.EventChargePeriodID
	JOIN #DelegateDates DD ON DD.PersonID = ECPD.PersonID AND DD.PersonSequenceID = ISNULL(ECPD.PersonSequenceID, 0)
	JOIN Dates D ON D.Date BETWEEN ISNULL(ECP.FromDate, DD.FirstDate) AND ISNULL(ECP.ToDate, DD.LastDate)

DECLARE curNetRates CURSOR LOCAL FOR
SELECT
	EventChargePeriodID, ChargeTypeID, MatrixID, Rate, Date
FROM
	#DailyRates
WHERE
	AmountType = 'N'
	
DECLARE
	@EventChargePeriodID int,
	@ChargeTypeID int,
	@MatrixID int,
	@Rate money,
	@Date datetime

OPEN curNetRates
FETCH curNetRates INTO @EventChargePeriodID, @ChargeTypeID, @MatrixID, @Rate, @Date
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC CalcHeaderVAT
		@ChargeTypeID,
		@MatrixID,
		1,
		@Rate,
		@Rate,
		@Rate OUTPUT,
		@Rate,
		@Date=@Date

	UPDATE
		#DailyRates
	SET
		Rate = @Rate
	WHERE
		EventChargePeriodID = @EventChargePeriodID
	AND ChargeTypeID = @ChargeTypeID
	AND MatrixID = @MatrixID
	AND Date = @Date
	
	FETCH curNetRates INTO @EventChargePeriodID, @ChargeTypeID, @MatrixID, @Rate, @Date
END
CLOSE curNetRates
DEALLOCATE curNetRates

SELECT
	EventChargePeriodID,
	MIN(Date) AS FromDate,
	MAX(Date) AS ToDate,
	FromDate AS ActualFromDate,
	ToDate AS ActualToDate,
	ChargeTypeID,
	MatrixID,
	Rate
INTO
	#Rates
FROM
	#DailyRates
GROUP BY
	EventChargePeriodID,
	FromDate,
	ToDate,
	ChargeTypeID,
	MatrixID,
	Rate

SELECT
	R.EventChargePeriodID,
	CONVERT(VarChar, R.FromDate, @Format) AS FromDate,
	CONVERT(VarChar, R.ToDate, @Format) AS ToDate,
	R.FromDate AS ActualFromDate,
	R.ToDate AS ActualToDate,

	CAST(CAST(DATENAME(dw, FromDate) as varchar(3)) + ' ' + CAST(DAY(FromDate) AS Varchar(2)) + ' to ' + CAST(DATENAME(dw, ToDate) as varchar(3)) + ' ' + CAST(DAY(ToDate) AS Varchar(2)) AS VarChar(255)) AS Dates,
	CT.Description AS ChargeType,
	CTRM.ExternalDescription AS SpecificPrice,
	R.Rate,
	CAST(R.ToDate - R.FromDate + 1 AS int) * R.Rate AS Total,
	ISNULL(RWNE.ContainsNegativeElement, 0) AS ContainsNegativeElement,
	CAST(CASE WHEN CTRM.BlockID > 0 THEN 1 ELSE 0 END AS bit) AS Accommodation,
	CAST(CASE WHEN F.FacilityID > 0 THEN 1 ELSE 0 END AS bit) AS Catering,

	CAST(
	CASE WHEN
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL JOIN EventModuleDelegateAccountCharges EMDAC ON EMDAC.EventModuleID = @EventModuleID AND EMDAC.AccountChargeID = EMDSL.AccountChargeID AND EventChargePeriodID = R.EventChargePeriodID)
	OR
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL JOIN EventModuleAccountCharges EMDAC ON EMDAC.EventModuleID = @EventModuleID AND EMDAC.AccountChargeID = EMDSL.AccountChargeID AND EventChargePeriodID = R.EventChargePeriodID)
	OR
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL WHERE EMDSL.PersonID = @PersonID AND EMDSL.EventModuleID = @EventModuleID AND EMDSL.EventChargePeriodID IS NULL)
	THEN 1 ELSE 0 END AS bit) AS Applied,

	ISNULL(NULLIF(CTRM.ExternalDescription, ''), CT.Description) + ISNULL(' - ' + NULLIF(F.Description, ''), '') AS ExternalDescription,
	ISNULL(F.FacilityID, 0) AS FacilityID,
	R.ChargeTypeID,
	ISNULL(ACD.EventModuleDayCateringAreaID,0) as EventModuleDayCateringAreaID,

	DC.DelegateCount,
	DD.PersonID,
	DD.PersonSequenceID,
	(SELECT COUNT(*) FROM #Rates r1 WHERE r1.EventChargePeriodID = R.EventChargePeriodID) as RateCount
INTO	#RatesResults
FROM
	#Rates R
	JOIN ChargeTypes CT ON CT.ChargeTypeID = R.ChargeTypeID
	LEFT JOIN EventChargePeriodDelegates ECPD ON ECPD.EventChargePeriodID = R.EventChargePeriodID
	LEFT JOIN #DelegateDates DD ON DD.PersonID = ECPD.PersonID AND DD.PersonSequenceID = ECPD.PersonSequenceID
	LEFT JOIN ChargeTypeRateMatrix CTRM ON CTRM.MatrixID = R.MatrixID
	LEFT JOIN #RatesWithNegativeElements RWNE ON RWNE.EventChargePeriodID = R.EventChargePeriodID AND RWNE.ChargeTypeID = R.ChargeTypeID AND ISNULL(RWNE.MatrixID, 0) = ISNULL(R.MatrixID, 0) AND RWNE.Rate = R.Rate
	LEFT JOIN AdditionalCateringData ACD ON ACD.EventChargePeriodID = R.EventChargePeriodID
	LEFT JOIN Facilities F ON F.FacilityID = ACD.FacilityID
	LEFT JOIN (
	SELECT
		EventChargePeriodID,
		COUNT(*) AS DelegateCount
	FROM
		EventChargePeriodDelegates
	GROUP BY
		EventChargePeriodID
	) DC ON DC.EventChargePeriodID = R.EventChargePeriodID
ORDER BY
	R.FromDate,
	R.ToDate

SELECT
	Cl.*,
	ISNULL(P.AddressID, 0) AS AddressID,
	ISNULL(P.CompanyAddressID, 0) AS COmpanyAddressID,
	ISNULL(P.CompanyID, 0) AS CompanyID,
	IsNull(PF.PaymentMethodID, 0) as PaymentMethodID,
        PF.CardNumber,
        PF.AuthCode,
        PF.AuthAmount,
        ISNULL(PM.Description, 'None Selected') AS PaymentMethod,
        CAST(CASE WHEN PF.AuthCode <> '' THEN 1 ELSE 0 END AS Bit) AS Authorised,
	EMD.PaymentTypeID,
        ISNULL(PT.Description, 'None Specified') AS Description,
	RR.FromDate,
	RR.ToDate,
	RR.ActualFromDate,
	RR.ActualToDate,
	RR.Dates,
	RR.ChargeType,
	RR.SpecificPrice,
	RR.Rate,
	RR.Total,
	RR.ContainsNegativeElement,
	RR.Accommodation,
	RR.Catering,
	RR.Applied,
	RR.ExternalDescription,
	RR.FacilityID,
	RR.ChargeTypeID,
	RR.EventModuleDayCateringAreaID,
	RR.DelegateCount as DelgateCountRates,
	RR.RateCount,
	(SELECT SUM(RR.Total) FROM #RatesResults RR WHERE PersonID = CL.PersonID) as FullTotal,
	P.Email,
	P.Mobile,
	P.Telephone,
	E.EventTypeID,
	D.Document
FROM
	#CheckInList CL
JOIN	EventModuleDelegates EMD ON EMD.EventModuleDelegateID = CL.EventModuleDelegateID
JOIN	People P ON P.PersonID = CL.PersonID
JOIN	EventModules EM ON EM.EventModuleID = CL.EventModuleID 
JOIN	Events E ON E.EventID = EM.EventID
LEFT JOIN PersonFinancial PF ON PF.PersonID = P.PersonID
LEFT JOIN PaymentMethods PM ON PM.PaymentMethodID = PF.PaymentMethodID
LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
LEFT JOIN #RatesResults RR ON RR.PersonID = CL.PersonID
LEFT JOIN Documents D  ON D.EntityType = 'KxMobileCheckInSign' AND D.EntityID = @EventModuleDelegateID

ORDER BY 
	RR.FromDate 

DROP TABLE #ResidentialRoomOccupants
DROP TABLE #EMBBA
DROP TABLE #EventModuleDelegates
DROP TABLE #CheckInList
DROP TABLE #Rates
DROP TABLE #DailyRates
DROP TABLE #RatesWithNegativeElements
DROP TABLE #DelegateDates
DROP TABLE #RatesResults


GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckOutDelegate]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckOutDelegate]
	@EventModuleDelegateID	int,
	@UserID int,
	@Signature image='',
	@StoreSignature bit=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @CheckedOutDate		datetime
SET	@CheckedOutDate = getDate();

DECLARE @EventModuleID1 int, @PersonID1 int, @PersonSequenceID1 int

SELECT @EventModuleID1 = D.EventModuleID, @PersonID1 = D.PersonID, @PersonSequenceID1 = D.PersonSequenceID FROM EventModuleDelegates D WHERE D.EventModuleDelegateID = @EventModuleDelegateID

exec dbo.SetEventModuleDelegates @EventModuleID = @EventModuleID1, @PersonID = @PersonID1, @PersonSequenceID = @PersonSequenceID1, @CheckedOutDate = @CheckedOutDate, @ModifiedUserID = @UserID

IF @StoreSignature = 1
BEGIN
	DELETE FROM Documents where EntityID = @EventModuleDelegateID and EntityType = 'KxMobileCheckOutSign'

	INSERT INTO Documents (DocumentType, EntityID, EntityType, Compressed, Document) 
		VALUES('KxMobileCheckOutSignature.png', @EventModuleDelegateID, 'KxMobileCheckOutSign', 0, @Signature) 
END


GO
/****** Object:  StoredProcedure [KxMobile].[qryCheckOutList]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryCheckOutList]
	@SiteID int=0,
	@AreaID int = 0,
	@BlockID int = 0,
	@Date datetime=0,
	@OrderBy varchar(255)='',
	@EventTypeID int=0,
	@EventID int=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SET NOCOUNT ON

DECLARE
	@ForDate bit

SET @ForDate = CASE WHEN (@Date < '1980-01-01') OR (@Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS float)) AS datetime)) THEN 1 ELSE 1 END

DECLARE @WalkInEventTypeID int
DECLARE @SurnameFirst int
--SET @CheckinCutoff = ISNULL((SELECT TOP 1 CAST(Value AS int) FROM CustomSettings WHERE CustomSetting = 'CHECK_IN_DELAY'), 0)

/* DLD 29/3/06 These values are now stored in Control table */
SET @WalkInEventTypeID = ISNULL((SELECT TOP 1 WalkInEventTypeID FROM Control), 0)
SET @SurnameFirst = ISNULL((SELECT TOP 1 SurnameFirst FROM Control), 0)

DECLARE	@NewFlats int
SELECT	@NewFlats = IsNull(Value,0) FROM CustomSettings WHERE CustomSetting = 'Residential.2009.Flat.Reservation.System'
SET @NewFlats = IsNull(@NewFlats,0)

CREATE TABLE #CheckOutList  (Name nvarchar(255), ArrivalDate DateTime, DepartureDate Datetime, EventID int, EventModuleID int, Description varchar(255), CheckedInDate datetime,  CheckedOutDate datetime, PersonID int,
			PersonSequenceID int, ReceivedMedical bit, Type Char(1), NumOccupants int, ResidentialRoomID int, SingleOccupancy bit, ExtNo varchar(50),
			NotesExist int, Info varchar(255), DelegateNotes varchar(8000), CheckInNotes varchar(8000), HouseKeepingNotes varchar(8000), Resident bit,
			RoomName varchar(255), RoomExternalID varchar(255), RoomType varchar(255), BookByRoom bit, BlockID int, BlockName varchar(255), SubBlockName varchar(255),
			Surname nvarchar(255), Forename nvarchar(255), Smoker bit, [Disabled] bit, VIP bit, DelegateEventModuleID int, HouseKeepingReqNoteID int, EventSiteID int,
			BedroomSiteID int, CreatorSiteID int, MaidStatus varchar(20), PreviousPersonID int, AreaID int, AreaDescription varchar(255), WalkIn bit, PaymentType varchar(255),
			CreateCharges bit, PMSEnabled bit, AllowBarPosting bit, CarRegNumber varchar(50), RouteChargesToPersonID int, RouteChargesToPersonSequenceID int, PhoneAccessCode varchar(20),
			DelegateGroupID int, DelegateGroup varchar(255), GroupColour varchar(50), HasNotes bit, HousekeepingNotesFound bit, DelegateNotesFound bit,
			CheckInNotesFound bit, AllocationType varchar(1), HeaderRoomName varchar(255), SortOrder varchar(255), HeaderResidentialRoomID int, RTFNotes varchar(8000),
			GroupShortName varchar(4), EventModuleDelegateID int, DisplayOrder int, GuestType varchar(1), DelegateCount Integer, ID int identity(1,1), InvoiceID int,
			OutstandingBalance money)
			 
CREATE TABLE #EventModuleDelegates (
	EventModuleID int NOT NULL,
	PersonID int NOT NULL,
	PersonSequenceID int NOT NULL,
	DelegateGroupID int NULL,
	ArrivalDate datetime NULL,
	DepartureDate datetime NULL,
	CheckedInDate datetime NULL,
	CheckedOutDate datetime NULL,
	ReceivedMedical bit NULL,
	Type char(1) NULL,
	CreationUserID int NOT NULL,
	Notes text NULL,
	SingleOccupancy bit NULL,
	Resident bit NULL,
	CheckInNotes text NULL,
	ResidentialRoomID int NULL,
	BlockID int NULL,
	BedroomTypeID int NULL,
	NumOccupants int NOT NULL,
	PaymentTypeID int NULL,
	AllowBarPosting bit NOT NULL,
	CarRegNumber varchar(20) NULL,
	RouteChargesToPersonID int NULL,
	HousekeepingNotes text NULL,
	PhoneAccessCode varchar(2) NULL,
	RouteChargesToPersonSequenceID int NULL,
	EventModuleDelegateID int,
	ArrivalDateOnly DateTime,
	DepartureDateOnly DateTime,
	GroupNumber int,
	ListedAdults int,
	GuestType varchar(1)
	)

INSERT INTO #EventModuleDelegates
SELECT
	EM.EventModuleID,
	PersonID,
	PersonSequenceID,
	DelegateGroupID,
	EM.ArrivalDate,
	DepartureDate,
	CheckedInDate,
	CheckedOutDate,
	ReceivedMedical,
	Type,
	EM.CreationUserID,
	EM.Notes,
	SingleOccupancy,
	Resident,
	CheckInNotes,
	ResidentialRoomID,
	BlockID,
	BedroomTypeID,
	NumOccupants,
	PaymentTypeID,
	AllowBarPosting,
	CarRegNumber,
	RouteChargesToPersonID,
	HousekeepingNotes,
	PhoneAccessCode,
	RouteChargesToPersonSequenceID,
	EventModuleDelegateID,
	CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS datetime) as ArrivalDateOnly,
	CAST(FLOOR(CAST(EMD.DepartureDate AS float)) AS datetime) as DepartureDateOnly,
	0,
	0,
	CASE WHEN EXISTS (SELECT * FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID) THEN 'S' ELSE 'G' END
FROM
	EventModuleDelegates EMD
JOIN	EventModules EM ON EMD.EventModuleID = EM.EventModuleID
JOIN	Events E ON E.EventID = EM.EventID
WHERE
	(ISNULL(EMD.CheckedInDate,0) > 29000)
AND	(ISNULL(EMD.CheckedOUTDate,0) < 29000)
AND	ISNULL(@SiteID, 0) <> -1
AND (@EventID = 0 OR @EventID = EM.EventID)
AND (@EventTypeID = 0 OR @EventTypeID = E.EventTypeID)
AND	(@ForDate = 1
	OR (CAST(FLOOR(CAST(@Date AS float)) AS datetime)
	        BETWEEN CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS datetime) AND
		CAST(FLOOR(CAST(EMD.DepartureDate AS float)) AS datetime)))
AND	EMD.PersonID > 0
AND	(NOT EXISTS (SELECT NULL FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID))
AND	(EMD.EventModuleID > 0 OR EMD.EventModuleID = -9999) --Warwick to include Students for vaction residents
AND	(@Date < '1980-01-01' OR @DATE BETWEEN CAST(FLOOR(CAST(EMD.ArrivalDate AS Float)) AS DateTime) AND CAST(FLOOR(CAST(EMD.DepartureDate AS Float)) AS DateTime))

CREATE INDEX IX1 ON #EventModuleDelegates(EventModuleID, PersonID, PersonSequenceID, ArrivalDateOnly)
CREATE INDEX IX1B ON #EventModuleDelegates(EventModuleID)

UPDATE EMD
SET	GroupNumber = EMBBAD.GroupNumber, ListedAdults = 0
FROM	#EventModuleDelegates EMD
JOIN	EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBAD.EventModuleID = EMD.EventModuleID AND EMBBAD.PersonID = EMD.PersonID AND EMD.PersonSequenceID =  EMBBAD.PersonSequenceID

UPDATE	#EventModuleDelegates
SET	ListedAdults = (SELECT	COUNT(DISTINCT(EMBBAD.PersonID))
			FROM	EventModuleBlockBedroomAllocationDelegates EMBBAD
			WHERE	EMBBAD.EventModuleID = #EventModuleDelegates.EventModuleID
			AND	#EventModuleDelegates.GroupNumber = EMBBAD.GroupNumber
			AND	EMBBAD.PersonID <> -999)

CREATE TABLE #ResidentialRoomOccupants (EventModuleID int, PersonID int, PersonSequencEID int, ResidentialRoomID int, Date DateTime)

INSERT INTO #ResidentialRoomOccupants
SELECT	RRO.EventModuleID, RRO.PersonID, RRO.PersonSequenceID, RRO.ResidentialRoomID, RRO.Date
FROM	ResidentialRoomOccupants RRO
JOIN	#EventModuleDelegates D ON D.EventModuleID = RRO.EventModuleID AND D.PersonID = RRO.PersonID AND D.PersonSequenceID = RRO.PersonSequenceID AND RRO.Date = D.ArrivalDateOnly

CREATE INDEX IX6 ON #ResidentialRoomOccupants(EventModuleID, PersonID, PersonSequenceID, Date)

CREATE TABLE #AmountPaid (EventModuleID int, PersonID int, PersonSequenceID int, Amount money)

INSERT INTO #AmountPaid
SELECT
	D.EventModuleID,
	D.PersonID,
	D.PersonSequenceID,
	SUM(CT.Amount) AS Amount
FROM
	#EventModuleDelegates D
	JOIN CashTransactions CT ON CT.EventModuleID = D.EventModuleID AND CT.PersonID = D.PersonID AND ISNULL(CT.PersonSequenceID,0) = D.PersonSequenceID
	JOIN InvoiceHeaders IH ON IH.InvoiceID = CT.InvoiceID
WHERE
	IH.InvoiceType = 'INVOICE'
GROUP BY
	D.EventModuleID,
	D.PersonID,
	D.PersonSequenceID

CREATE INDEX [IX_PersonID] ON #AmountPaid (EventModuleID, PersonID, PersonSequenceID) ON [PRIMARY]

CREATE TABLE #TotalAmount (EventModuleID int, PersonID int, PersonSequenceID int, Amount money)

CREATE TABLE #DelegateCharges (EventModuleID int, PersonID int, PersonSequenceID int, TotalAmountCharged money, InvoiceID int)

INSERT INTO #DelegateCharges
SELECT
	EMDAC.EventModuleID,
	EMDAC.PersonID,
	ISNULL(EMDAC.PersonSequenceID,0) AS PersonSequenceID,
	EMDAC.TotalAmountCharged,
	EMDAC.InvoiceID
FROM
	#EventModuleDelegates D
	JOIN EventModuleDelegateAccountCharges EMDAC ON EMDAC.EventModuleID=D.EventModuleID AND EMDAC.PersonID = D.PersonID
WHERE
	ISNULL(EMDAC.HiddenByAccountChargeID, 0) = 0
AND 	ISNULL(EMDAC.PersonSequenceID,0) = D.PersonSequenceID

CREATE INDEX [IX_PersonID] ON #DelegateCharges (EventModuleID, PersonID, PersonSequenceID) ON [PRIMARY]

INSERT INTO #TotalAmount
SELECT
	EventModuleID,
	PersonID,
	PersonSequenceID,
	SUM(TotalAmountCharged) AS Amount
FROM
	#DelegateCharges
GROUP BY
	EventModuleID,
	PersonID,
	PersonSequenceID

CREATE INDEX [IX_PersonID] ON #TotalAmount (EventModuleID, PersonID, PersonSequenceID) ON [PRIMARY]

CREATE TABLE #EMBBA (EventModuleID int, ResidentialRoomID int, Date DateTime, AllocationType varchar(1))
CREATE INDEX IDX_EMBBA1 ON #EMBBA (EventModuleID, ResidentialRoomID, Date)

INSERT INTO #CheckOutList
SELECT	DISTINCT
	CASE WHEN @SurnameFirst = 1 THEN
		CASE WHEN P.Surname = '' THEN P.Forename WHEN P.Forename = '' THEN P.Surname ELSE P.Surname + ', ' + P.Forename END + CASE WHEN P.Title = '' THEN '' ELSE ' (' + P.Title + ')' END
	ELSE
		LTRIM(P.Title + ' ') + LTRIM(P.Forename + ' ') + P.Surname END as Name,
	EMD.ArrivalDate,
	EMD.DepartureDate,
	EM.EventID,
	EMD.EventModuleID,
	RTrim(LTrim(EM.Description)) as Description,
	CAST(0 AS DateTime) AS CheckedInDate,
	CAST(0 AS DateTime) AS CheckedOutDate,
	EMD.PersonID,
	EMD.PersonSequenceID,
	EMD.ReceivedMedical,
	EMD.Type,
	EMD.NumOccupants,
	RRO.ResidentialRoomID,
	EMD.SingleOccupancy,
	R.ExtNo,
	0 as NotesExist,
	R.Info,
	ISNULL(RTRIM(CAST(EMD.Notes AS varchar(max))), '') AS DelegateNotes,
	ISNULL(RTRIM(CAST(EMD.CheckInNotes AS varchar(max))), '') AS CheckInNotes,
	ISNULL(RTRIM(CAST(EMD.HousekeepingNotes AS varchar(max))), '') AS HousekeepingNotes,
	EMD.Resident,
	CASE
	WHEN R.ResidentialRoomID IS NULL THEN
		PBT.Description
	WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		CAST(RRH.Name as varchar(255))
	ELSE
		CAST(CASE WHEN BT.BookByRoom = 1 THEN LEFT(RTRIM(BB.Name), PATINDEX('% %', BB.Name)) + ': ' ELSE '' END + RTRIM(R.Name) AS varchar(255))
	END AS RoomName,
	R.ExternalID AS RoomExternalID,
	CASE WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		BTH.Description
	ELSE
		BT.Description
	END as RoomType,
	BT.BookByRoom,
	R.BlockID,
	SB.Name as BlockName,
	BB.Name as SubBlockName,
	P.Surname,
	P.Forename,
	P.Smoker,
	P.Disabled,
	P.VIP,
	EMD.EventModuleID as DelegateEventModuleID,
	ISNULL(P.HouseKeepingReqNoteID, 0) AS HouseKeepingReqNoteID,
	EM.SiteID as EventSiteID,
	SB.SiteID as BedroomSiteID,
	U.SiteID AS CreatorSiteID,
	CASE WHEN IsNull(R.MaidStatus, 'X') = '' THEN 'I' ELSE R.MaidStatus END as MaidStatus,
	0 as PreviousPersonID,
	SB.AreaID,
	A.Description AS AreaDescription,
	CASE E.EventTypeID WHEN @WalkInEventTypeID THEN CAST(1 AS Bit) ELSE CAST(0 AS Bit) END AS WalkIn,
	PT.Description as PaymentType,
	PT.CreateCharges,
	SIT.PMSEnabled,
	EMD.AllowBarPosting,
	EMD.CarRegNumber,
	EMD.RouteChargesToPersonID,
	IsNull(EMD.RouteChargesToPersonSequenceID, 0) as RouteChargesToPersonSequenceID,--removed
	EMD.PhoneAccessCode,
	EMD.DelegateGroupID,
	DG.Description AS DelegateGroup,
	DG.GroupColour,
	0 as HasNotes,
	0 as HousekeepingNotesFound,
	0 as DelegateNotesFound,
	0 as CheckInNotesFound,
	CASE WHEN @NewFlats = 1 THEN EMBBA.AllocationType ELSE '' END as AllocationType,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN IsNull(RRH.Name, '') ELSE '' END as HeaderRoomName,
	'' as SortOrder,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN RRH.ResidentialRoomID
			WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'R' THEN R.ResidentialRoomID ELSE 0 END AS HeaderResidentialRoomID,
	'' as RTFNotes,
	RTRIM(CASE WHEN RTrim(IsNull(DG.ShortName, '')) = '' THEN LEFT(LTRIM(DG.Description),1) ELSE DG.ShortName END) as GroupShortName,
	EventModuleDelegateID,
	R.DisplayOrder,
	EMD.GuestType,
	(SELECT COUNT(*) FROM #EventModuleDelegates EMD2 WHERE EMD2.EventModuleID = EMD.EventModuleID) as DelegateCount,
	(SELECT TOP 1
			InvoiceID
	FROM
			InvoiceHeaders
	WHERE
			InvoiceType = 'INVOICE'
	AND     EventModuleID = EMD.EventModuleID
	AND     PersonID = EMD.PersonID
	AND     ISNULL(PersonSequenceID, 0) = EMD.PersonSequenceID
	ORDER BY
			InvoiceID DESC) as InvoiceID,
	ISNULL(TA.Amount, 0) - ISNULL(AP.Amount, 0) AS TotalAmountDue
FROM
	#EventModuleDelegates EMD
	JOIN EventModules EM ON EM.EventModuleID = EMD.EventModuleID
	JOIN Events E ON E.EventID = EM.EventID
	JOIN Users U ON U.UserID = EMD.CreationUserID
	JOIN People P ON P.PersonID = EMD.PersonID
	JOIN Status ST ON ST.StatusID = EM.StatusID
	LEFT JOIN SiteBlocks PB ON PB.BlockID = EMD.BlockID
	LEFT JOIN BedroomTypes PBT ON PBT.BedroomTypeID = EMD.BedroomTypeID
	LEFT JOIN #ResidentialRoomOccupants RRO
		ON    	RRO.EventModuleID 	= EMD.EventModuleID
		AND	RRO.PersonID		= EMD.PersonID
		AND	RRO.PersonSequenceID	= EMD.PersonSequenceID
		AND	RRO.Date		= EMD.ArrivalDateOnly
	LEFT JOIN ResidentialRooms R ON R.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
	LEFT JOIN SiteBlocks BB ON BB.BlockID = R.SubBlockID
	LEFT JOIN SiteBlocks SB ON SB.BlockID = R.BlockID
	LEFT JOIN Sites SIT ON SIT.SiteID = SB.SiteID
	LEFT JOIN Areas A ON A.AreaID = SB.AreaID
	LEFT JOIN DelegateGroups DG ON DG.DelegateGroupID = EMD.DelegateGroupID
	LEFT JOIN EventModuleBlockBedroomAllocation /* #EMBBA*/ EMBBA ON EMBBA.EventModuleID = EMD.EventModuleID AND EMBBA.ResidentialRoomID = R.ResidentialRoomID AND EMBBA.Date = RRO.Date
	LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = R.HeaderResidentialRoomID
	LEFT JOIN BedroomTypes BTH ON BTH.BedroomTypeID = RRH.BedroomTypeID
	LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
	LEFT JOIN SiteBlocks SB2 ON SB2.BlockID = EMD.BlockID
	LEFT JOIN #TotalAmount TA ON TA.EventModuleID = EMD.EventModuleID AND TA.PersonID = EMD.PersonID AND TA.PersonSequenceID = EMD.PersonSequenceID
	LEFT JOIN #AmountPaid AP ON AP.EventModuleID = EMD.EventModuleID AND AP.PersonID = EMD.PersonID AND AP.PersonSequenceID = EMD.PersonSequenceID
WHERE
	ST.EffectCapacity = 1
AND (@SiteID = 0 OR IsNull(SB.SiteID, IsNull(SB2.SiteID, @SiteID)) = @SiteID)
AND	(@AreaID = 0 OR IsNull(SB.AreaID, IsNull(SB2.AreaID, 0)) = @AreaID)
AND	(@BlockID = 0 OR IsNull(SB.BlockID, IsNull(SB2.BlockID, 0)) = @BlockID)
AND	(@NewFlats = 0 OR not (ISNULL(EMBBA.AllocationType,'') = 'F' AND R.HeaderResidentialRoomID = -1))
AND	ISNULL(@SiteID, 0) <> -1

UPDATE D SET HasNotes = CAST(CASE WHEN IsNull(D.HousekeepingNotes,'') + IsNull(D.DelegateNotes,'') + IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS Bit) FROM #CheckOutList D
UPDATE D SET NotesExist = CAST(CASE WHEN D.HasNotes = 1 THEN 1 ELSE 0 END AS Int) FROM #CheckOutList D
UPDATE D SET HousekeepingNotesFound = CAST(CASE WHEN IsNull(D.HouseKeepingNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckOutList D
UPDATE D SET DelegateNotesFound = CAST(CASE WHEN IsNull(D.DelegateNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckOutList D
UPDATE D SET CheckInNotesFound = CAST(CASE WHEN IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckOutList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.DelegateNotes, '')) <> '' THEN ISNULL('<b>Reg Card Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.DelegateNotes AS varchar(max)), ''), '') ELSE '' END
			 FROM #CheckOutList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.CheckInNotes, '')) <> '' THEN CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Check In Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.CheckInNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckOutList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.HouseKeepingNotes, '')) <> '' THEN	CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Housekeeping Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.HouseKeepingNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckOutList D
UPDATE D SET RtfNotes = REPLACE(RTFNotes, CHAR(13)+CHAR(10), '<br/>')  FROM #CheckOutList D WHERE D.RTFNotes <> ''

DECLARE @Yesterday datetime

SET @Yesterday = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)-1

UPDATE	CL
SET	PreviousPersonID =
		ISNULL((
			SELECT TOP 1
				RRO.PersonID
			FROM	ResidentialRoomOccupants RRO WITH (NOLOCK)
			JOIN	EventModuleDelegates EMD WITH (NOLOCK) ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
			WHERE	(EMD.CheckedInDate > 2)
			AND	(EMD.CheckedOutDate <= 2)
			AND	CAST(FLOOR(CAST(EMD.ArrivalDate AS money)) AS datetime) <= @Yesterday
			AND	CAST(FLOOR(CAST(EMD.DepartureDate AS money)) AS datetime) >= @Yesterday
			AND     CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = @Yesterday
			AND	EMD.PersonID > 0
			AND	NOT EXISTS (SELECT NULL FROM ST2AcademicYears AY WHERE AY.EventModuleID = RRO.EventModuleID)
			AND	RRO.ResidentialRoomID = CL.ResidentialRoomID
			AND	CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = (SELECT MAX(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = RRO.EventModuleID AND RRO1.PersonID = RRO.PersonID AND RRO1.PersonSequenceID = RRO.PersonSequenceID)
			ORDER BY
				RRO.Date DESC
		), 0)
FROM	#CheckOutList CL
	
SELECT
	C.*,
	LTrim(IsNull(STUFF((SELECT ', ' + RTrim(CV2.FieldName) + '=' + D.FieldValue
			  FROM CustomFields2Values D
			  JOIN CustomFields2 CV2 ON CV2.CustomFieldID = D.CustomFieldID
			  WHERE D.EntityType = 'EventModuleDelegates' AND D.EntityID = C.EventModuleDelegateID 
			  ORDER BY CV2.FieldName DESC
			  FOR XML PATH('')), 1, 1, ''), '')) as UDFs
FROM
	#CheckOutList C
ORDER BY
	CASE WHEN @OrderBy = 'Event' THEN EventID ELSE NULL END,
	Surname,
	Forename

DROP TABLE #CheckOutList
DROP TABLE #AmountPaid
DROP TABLE #DelegateCharges
DROP TABLE #EMBBA
DROP TABLE #EventModuleDelegates
DROP TABLE #ResidentialRoomOccupants
DROP TABLE #TotalAmount

SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[qryDelegateUDFs]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryDelegateUDFs]
	@EventModuleDelgateID int,
	@CustomFieldIDs varchar(max),
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	CFV.CustomFieldID,
		CFV.FieldValue,
		CHARINDEX(';'+CAST(CFV.CustomFieldID AS varchar)+';', ';'+@CustomFieldIDs+';') as Position
FROM	CustomFields2Values CFV 
WHERE	CFV.EntityType = 'EventModuleDelegates'
AND		CFV.EntityID = @EventModuleDelgateID
AND		CHARINDEX(';'+CAST(CFV.CustomFieldID AS varchar)+';', ';'+@CustomFieldIDs+';') > 0
ORDER BY
	Position


GO
/****** Object:  StoredProcedure [KxMobile].[qryDeviceApplicationInfo]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryDeviceApplicationInfo]
	@Application		varchar(255),
	@UniqueID			varchar(255),
	@RegistrationDate	Datetime,
	@DeviceType			varchar(50) = '',
	@OS					varchar(50) = '',
	@Version			varchar(50) ='',
	@DeviceInfo			varchar(max) = '',
	@DeviceName			nvarchar(255) = ''
AS	

DECLARE @ApplicationID int, @DeviceID int, @ApprovalDate DateTime, @SecurityPin varchar(255), @FeatureAccess varchar(max)
SET @DeviceID = -1 -- Invalid
SET @ApprovalDate = NULL
SET @SecurityPin = ''
SET @FeatureAccess = ''

SELECT @ApplicationID = ApplicationID FROM KxMobile.Applications A WHERE A.Application = @Application

IF @ApplicationID = 0
	SET @DeviceID = -2 -- Unknown Application
ELSE
BEGIN
	SELECT @DeviceID = DeviceID FROM KxMobile.Devices D WHERE D.UniqueIdentifier = ISNull(@UniqueID, 'Dummy')
	IF IsNull(@DeviceID, -1) = -1
	BEGIN
		INSERT INTO KxMobile.Devices (UniqueIdentifier, DeviceType, OS, VERSION, DeviceInfo, RegistrationDate, DeviceName)
			VALUES (ISNull(@UniqueID, 'Dummy'), IsNull(@DeviceType, 'Unknown'), ISNull(@OS, ''), IsNull(@Version, ''), IsNull(@DeviceInfo, ''), @RegistrationDate, @DeviceName)

		SET @DeviceID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE KxMobile.Devices SET DeviceType = @DeviceType, OS = @OS, [VERSION] = @Version, DeviceInfo = @DeviceInfo, DeviceName = @DeviceName
		WHERE DeviceID = @DeviceID
	END

	SELECT @ApprovalDate = MAX(ISNull(AA.ApprovalDate, 0)) FROM KxMobile.AppApproval AA WHERE AA.ApplicationID = @ApplicationID AND AA.DeviceID = @DeviceID
/*
	IF EXISTS (SELECT NULL FROM CustomSettings WHERE CustomSetting = 'KxMobile.Auto.Authenicate.Testing')
	BEGIN	
		SET @FeatureAccess = '<FeatureAccess CopyLevel="User">
			<MenuOptions>
				<MenuOption Name="EventRunning" Title="Events Running" Enabled="False" Lic="LIC_EventRunning">
					<Features>
						<Feature Name="ViewClient" Title="View Client Details" Enabled="True"/>
						<Feature Name="ViewMeetingRooms" Title="View Meeting Rooms" Enabled="True"/>
						<Feature Name="ViewCatering" Title="View Catering" Enabled="True"/>
						<Feature Name="ViewBedrooms" Title="View Bedrooms" Enabled="True"/>
						<Feature Name="ViewQuoteSummary" Title="View Quote Summary" Enabled="True"/>
						<Feature Name="ViewNotes" Title="View Notes" Enabled="True"/>
						<Feature Name="ViewDialogue" Title="View Dialogue" Enabled="True"/>
						<Feature Name="ViewDelegates" Title="View Delegates" Enabled="True"/>
						<Feature Name="ViewFinancialSummary" Title="View Financial Summary" Enabled="True"/>
						<Feature Name="ViewMailings" Title="View Mailings" Enabled="True"/>
						<Feature Name="ViewCheckList" Title="View Check List" Enabled="True"/>
						<Feature Name="ViewDocuments" Title="View Mailings" Enabled="True"/>
						<Feature Name="ViewContract" Title="View Contact" Enabled="True"/>
						<Feature Name="PrintOES" Title="Print Operational Event Schedule" Enabled="True"/>
					</Features>
				</MenuOption>
				<MenuOption Name="ConferenceRoomPlan" Title="Conference Room Plan" Enabled="False" Lic="LIC_ConferenceRoomPlan">
					<Features>
						<Feature Name="OpenEvent" Title="Open Event" Enabled="True"/>
					</Features>
				</MenuOption>
				<MenuOption Name="CheckIn" Title="Check-In/Out" Enabled="False" Lic="LIC_CheckInOut">
					<Features>
						<Feature Name="BookRoom" Title="Operator can Assign Room" Enabled="True"/>
					</Features>
				</MenuOption>
				<MenuOption Name="HousekeepingCheck" Title="Housekeeping Check" Enabled="True" Lic="LIC_HousekeepingCheck">
					<Features>
						<Feature Name="AllowInspected" Title="Allowed to set room to Inspected" Enabled="True"/>
						<Feature Name="ShowHKGuestName" Title="Show Guest Name" Enabled="True"/>
					</Features>
				</MenuOption>
				<MenuOption Name="Dashboard" Title="Dashboard" Enabled="True" Lic="LIC_Dashboard">
					<Features>
						<Feature Name="BedsInUse" Title="Show Beds In Use Dashboard" Enabled="True"/>
					</Features>
				</MenuOption>
			</MenuOptions>
		</FeatureAccess>'

		DECLARE @UserID int
		SET @UserID = 1

		IF NOT EXISTS (SELECT NULL FROM KxMobile.AppApproval  AA1 WHERE AA1.ApplicationID = @ApplicationID and AA1.UserID = @UserID AND AA1.DeviceID = @DeviceID)
		BEGIN
			INSERT INTO KxMobile.AppApproval (ApplicationID, DeviceID, ApprovalDate, UserID, SecurityPin, FeatureAccess, CreationUserID)
			VALUES (@ApplicationID, @DeviceID, @RegistrationDate, @UserID, '', @FeatureAccess, @UserID)
		END
	END
*/
END

DECLARE @ConfRoomPlanAMColour varchar(255), @ConfRoomPlanPMColour varchar(255), @ConfRoomPlanEveColour varchar(255), @OutofServiceColour varchar(255),
		@MeetingColour varchar(255), @UnavailableColour varchar(255), @SelectedEventColour varchar(255), @CateringColoursExternal varchar(255),
		@CateringColoursInternal varchar(255), @DefaultBookingStartTime datetime, @DefaultBookingEndTime datetime

SELECT TOP 1
		@ConfRoomPlanAMColour = ConfRoomPlanAMColour,
		@ConfRoomPlanEveColour = ConfRoomPlanEveColour,
		@ConfRoomPlanPMColour = ConfRoomPlanPMColour,
		@OutofServiceColour = OutofServiceColour,
		@MeetingColour = MeetingColour,
		@UnavailableColour = UnavailableColour,
		@SelectedEventColour = SelectedEventColour,
		@CateringColoursExternal = CateringColoursExternal,
		@CateringColoursInternal = CateringColoursInternal,
		@DefaultBookingStartTime = DefaultBookingStartTime,
		@DefaultBookingEndTime = DefaultBookingEndTime
FROM Control

SELECT	A.*
INTO #Temp
FROM	KXMobile.Applications A
WHERE A.ApplicationID = IsNull(@ApplicationID, 0)

SELECT	@DeviceID as DeviceID,
		IsNULL(@ApprovalDate, 0) as ApprovalDate,
		T.*,
		@ConfRoomPlanAMColour as ConfRoomPlanAMColour,
		@ConfRoomPlanEveColour as ConfRoomPlanEveColour,
		@ConfRoomPlanPMColour as ConfRoomPlanPMColour,
		@OutofServiceColour as OutofServiceColour,
		@MeetingColour as MeetingColour,
		@UnavailableColour as UnavailableColour,
		@SelectedEventColour as SelectedEventColour,
		@CateringColoursExternal as CateringColoursExternal,
		@CateringColoursInternal as CateringColoursInternal,
		@DefaultBookingStartTime as DefaultBookingStartTime,
		@DefaultBookingEndTime as DefaultBookingEndTime
FROM	#Temp T

TRUNCATE TABLE #Temp
DROP TABLE #Temp

GO
/****** Object:  StoredProcedure [KxMobile].[qryDeviceUserForApp]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryDeviceUserForApp]
	@ApplicationID		int,
	@DeviceID			int
AS	

DECLARE @Licencee varchar(255)
SELECT TOP 1 @Licencee = L.Licencee FROM Licence L

SELECT	AA.UserID,
		U.FullName,
		U.Forename,
		U.Surname,
		AA.SecurityPin,
		AA.FeatureAccess,
		AA.AppApprovalID,
		U.SiteID,
		U.AreaID,
		A.LicenceInfo,
		@Licencee as Licencee
FROM	KxMobile.AppApproval AA
JOIN	ViewUsers U ON U.UserID = AA.UserID
JOIN	KxMobile.Applications A ON A.ApplicationID = AA.ApplicationID
WHERE	AA.ApplicationID = @ApplicationID
AND		AA.DeviceID = @DeviceID



GO
/****** Object:  StoredProcedure [KxMobile].[qryEMCRImage]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEMCRImage]
	@EventModuleConferenceRoomID int,
	@Index int=-1,
	@ApplicationID int=0,
	@DeviceID int=0
AS

CREATE TABLE #Results (ID int identity(0,1), Document image, DocumentType varchar(255)) 

INSERT INTO #Results
SELECT	D.Document, D.DocumentType
FROM	Documents D
WHERE	D.EntityID = @EventModuleConferenceRoomID
AND		D.EntityType = 'EMCRImage'

SELECT *
FROM	#Results
WHERE	(@Index = -1 OR ID = @Index)

DROP TABLE #Results


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventActions]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventActions]
	@EventModuleID int,
	@UserID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	EM.EventModuleID,
		CA.ActionID,
		CA.ActionUserID,
		U.FullName as UserName,
		P.PersonName,
		AT.Description as ActionType,
		IsNull(PR.Name, '') as Priority,
		CA.CreationDate,
		CA.ActionDate,
		CA.Notes,
		U2.FullName as CreatedBy
FROM	CompanyActions CA
JOIN	ActionTypes AT ON AT.ActionTypeID = CA.ActionTypeID
JOIN	EventActions EA ON EA.ActionID = CA.ActionID
JOIN	EventModules EM ON EM.EventID = EA.EventID
JOIN	ViewUsers U ON U.UserID = CA.ActionUserID
JOIN	ViewPeople P ON P.PersonID = CA.PersonID
JOIN	ViewUsers U2 ON U2.UserID = CA.CreationUserID
LEFT JOIN Priorities PR ON CA.PriorityID = PR.PriorityID
WHERE	@EventModuleId = EM.EventModuleID 
AND		IsNull(NextActionID, 0) = 0
AND		IsNull(CA.FinishedDate, 0) < 29000
AND		(@UserID = 0 OR @UserID = U.UserID)
AND		CAST(FLOOR(CAST(CA.ActionDate as money)) as DateTime) <= CAST(FLOOR(CAST(getdate() as money)) as DateTime)
ORDER BY
	ISNULL(PR.Priority, '9999'),
	CA.ActionDate DESC,
	AT.Description


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventBedrooms]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventBedrooms]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

--LEAVE BEGIN .... END AROUND WITH STATEMENT
BEGIN
WITH x AS 
( 
    SELECT 
        [Date] as GroupDate, 
        EventModuleID,
        BlockID,
        BedroomTypeID,
        Allocation,
        part = DATEDIFF(DAY, 0, [Date])  
        + DENSE_RANK() OVER 
        ( 
                PARTITION BY	EventModuleID,
				BlockID,
				BedroomTypeID,
				Allocation
                ORDER BY [Date] DESC 
        ) 
    FROM EventModuleBlockBedroomAllocation EMBBA
	WHERE EMBBA.EventModuleID = @EventModuleID
) 
SELECT	EventModuleID,
        BlockID,
        BedroomTypeID,
        MAX(Allocation) as Allocation,
        MinDate = MIN(GroupDate), 
		MaxDate = MAX(GroupDate) 
INTO	#Results
FROM 
    x 
GROUP BY  
	part, 
	EventModuleID,
        BlockID,
        BedroomTypeID,
        Allocation
ORDER BY 
	EventModuleID,  
	MinDate,
	BlockID,
	BedroomTypeID
END

SELECT	R.*,
		SB.Name as BlockName,
		BT.Description as BedroomType
FROM	#Results R
JOIN	SiteBlocks SB ON SB.BlockID = R.BlockID
JOIN	BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
ORDER BY
	SB.SequenceNo,
	SB.Name,
	BT.Description



DROP TABLE #Results

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventCatering]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventCatering]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	@EventModuleID as EventModuleID,
		EMDCA.EventModuleDayCateringAreaID,
		EMDCA.Date,
		EMDCA.EventDate,
		EMDCA.EventTime - CAST(FLOOR(CAST(EMDCA.EventTime as money)) as Datetime) + CAST(FLOOR(CAST(EMDCA.EventDate as money)) as Datetime) as TimeStart,
		EMDCA.EventEndTime - CAST(FLOOR(CAST(EMDCA.EventEndTime as money)) as Datetime) + CAST(FLOOR(CAST(EMDCA.EventDate as money)) as Datetime) as TimeEnd,
		EMDCA.Quantity as Covers,
		EMDCA.Description,
		CT.Description as BookingType,
		S.Name as SiteName,
		IsNull(CST.Description, '') as ServiceType,
		IsNull(CP.Description, '') as CateringProvider,
		IsNull(SB.Name, '') as Building,
		C.Name as ConferenceRoom,
		EMDCA.Notes as CateringNotes,
		EMDCA.DelivNotes as DeliveryNotes,
		REPLACE(REPLACE(IsNull(STUFF((SELECT RTRIM(C.Name) + CHAR(10) 
		  FROM EventModuleDayCateringConferenceRooms EMDCR
		  JOIN ConferenceRooms C ON C.ConferenceRoomID = EMDCR.ConferenceRoomID
		  WHERE EMDCR.EventModuleDayCateringAreaID = EMDCA.EventModuleDayCateringAreaID
		  ORDER BY C.ConferenceRoomSortNumber, C.Name DESC
		  FOR XML PATH('')), 1, 0, ''), ''), '&amp;', '&'), '&#x0D;', CHAR(13)) as AdditionalRooms,
		REPLACE(REPLACE(IsNull(STUFF((SELECT  RTRIM(F.Description) + ' x ' + CAST(EMDCF.Quantity as varchar(10)) + CHAR(13) + CHAR(10) 
		  FROM EventModuleDayCateringFacilities EMDCF
		  JOIN Facilities F ON F.FacilityID = EMDCF.FacilityID
		  WHERE EMDCF.EventModuleDayCateringAreaID = EMDCA.EventModuleDayCateringAreaID
		  AND	EMDCF.Cancelled = 0
		  ORDER BY F.ItemOrder, F.Description DESC
		  FOR XML PATH('')), 1, 0, ''), ''), '&amp;', '&'), '&#x0D;', CHAR(13)) as CateringItems
FROM	EventModuleDayCateringAreas EMDCA
JOIN	CateringTypes CT ON CT.CateringTypeID = EMDCA.CateringTypeID
JOIN	Sites S ON S.SiteID = EMDCA.SiteID
JOIN	ConferenceRooms C ON C.ConferenceRoomID = EMDCA.AreaID
LEFT JOIN CateringServiceTypes CST on CST.ServiceTypeID = EMDCA.ServiceTypeID 
LEFT JOIN CateringProviders CP ON CP.CateringProviderID = EMDCA.CateringProviderID
LEFT JOIN SiteBlocks SB ON SB.BlockID = EMDCA.BlockID
WHERE	EMDCA.EventModuleID = @EventModuleID
AND		EMDCA.Cancelled = 0
ORDER BY
	EventDate,
	EventTime


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventClients]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventClients]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT
	EP.EventPersonID,
	EC.EventCompanyID,
	EP.PersonID,
	ISNULL(EC.CompanyID, P.CompanyID) AS CompanyID,
    ISNULL(EC.AddressID, P.CompanyAddressID) AS AddressID,
	LTRIM(RTRIM(P.Title) + ' ') + LTRIM(RTRIM(P.Forename) + ' ') + RTRIM(P.Surname) AS PersonName,
	C.Name AS CompanyName,
	CR.Description AS CompanyRole,
	PR.Description AS PersonRole,
	EC.PrimaryCompany,
	C.InternalRecharge AS Internal,
	P.Forename,
	P.Surname,
	IsNull(CA.Department, '') as Department,
	IsNull(NullIf(CA.Telephone, ''), P.Telephone) as Telephone,
	ISNull(NullIf(CA.Email, ''), P.Email) as Email
INTO	#Results
FROM
	EventModules EM
	JOIN EventPeople EP ON EP.EventID = EM.EventID
	JOIN People P ON P.PersonID = EP.PersonID
	LEFT JOIN EventCompanies EC ON EC.EventCompanyID = EP.EventCompanyID
	LEFT JOIN Companies C ON C.CompanyID = EC.CompanyID
	LEFT JOIN CompanyRoles CR ON CR.CompanyRoleID = EC.CompanyRoleID
	LEFT JOIN PeopleRoles PR ON PR.PersonRoleID = EP.PersonRoleID
	LEFT JOIN CompanyAddresses CA ON CA.AddressID = P.CompanyAddressID
WHERE
	EM.EventModuleID = @EventModuleID

INSERT INTO #Results
SELECT
	0,
	EC.EventCompanyID,
	0,
	EC.CompanyID,
    EC.AddressID,
	'' AS PersonName,
	C.Name AS CompanyName,
	CR.Description AS CompanyRole,
	'' AS PersonRole,
	EC.PrimaryCompany,
    CAST(0 AS bit) AS Internal,
	'',
	'',
	'',
	'',
	''
FROM
	EventModules EM
	JOIN EventCompanies EC ON EC.EventID = EM.EventID
	JOIN Companies C ON C.CompanyID = EC.CompanyID
	LEFT JOIN CompanyRoles CR ON CR.CompanyRoleID = EC.CompanyRoleID
WHERE
	EM.EventModuleID = @EventModuleID
AND	NOT EXISTS (SELECT * FROM EventPeople EP WHERE EP.EventCompanyID = EC.EventCompanyID)
ORDER BY PrimaryCompany DESC

SELECT	@EventModuleID as EventModuleID,
		R.PersonID,
		R.PersonName,
		R.CompanyID,
		R.CompanyName,
		R.PrimaryCompany,
		R.CompanyRole,
		R.PersonRole,
		A.AddressID,
		A.Address,
		A.Town,
		A.County,
		A.Country,
		A.Postcode,
		R.Department,
		R.Telephone,
		R.EMail
FROM	#Results R
LEFT JOIN Addresses A ON A.AddressID = R.AddressID
ORDER BY
	R.PrimaryCompany DESC,
	R.Surname,
	R.Forename

DROP TABLE #Results

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventConferenceRooms]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventConferenceRooms]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @SDTD int
SET @SDTD = CAST(IsNull((Select TOP 1 Value from CustomSettings where CustomSetting = 'Event.Enhanced.Setup.Takedown'), '0') as int)

SELECT	@EventModuleID as EventModuleID,
		EMCR.EventModuleConferenceRoomID,
		C.ConferenceRoomID,
		C.Name as ConferenceRoom,
		A.Description as AreaName,
		EMCR.StartDateTime,
		EMCR.EndDateTime,
		CASE WHEN @SDTD = 1 then IsNull(EMCR.SetupStartTime, 0) ELSE 0 END as SetupStartTime,
		CASE WHEN @SDTD = 1 then IsNull(EMCR.TakeDownEndTime, 0) ELSE 0 END as TakedownEndTime,
		EMCR.People,
		L.Description as Layout,
		CRT.Description as RoomType,
		CASE WHEN EMCR.DailyUse = 1 THEN EMCR.DailyStartTime - CAST(FLOOR(CAST(EMCR.DailyStartTime as money)) as Datetime) + CAST(FLOOR(CAST(getdate() as money)) as Datetime) ELSE 0 END as DailyStartTime,
		CASE WHEN EMCR.DailyUse = 1 THEN EMCR.DailyEndTime - CAST(FLOOR(CAST(EMCR.DailyEndTime as money)) as Datetime) + CAST(FLOOR(CAST(getdate() as money)) as Datetime) ELSE 0 END as DailyEndTime,
		CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleConferenceRoomFacilities EMCF WHERE EMCR.EventModuleConferenceRoomID = EMCF.EventModuleConferenceRoomID) THEN 1 ELSE 0 END as bit) as HasEquipment,
		REPLACE(REPLACE(IsNull(STUFF((SELECT  RTRIM(F.Description) + ' x ' + CAST(EMCF.Quantity as varchar(10)) + CHAR(13) + CHAR(10) 
		  FROM EventModuleConferenceRoomFacilities EMCF
		  JOIN Facilities F ON F.FacilityID = EMCF.FacilityID
		  WHERE EMCF.EventModuleConferenceRoomID = EMCR.EventModuleConferenceRoomID
		  ORDER BY F.ItemOrder, F.Description DESC
		  FOR XML PATH('')), 1, 0, ''), ''), '&amp;', '&'), '&#x0D;', CHAR(13)) as Equipment,
		EMCR.MainRoom,
		CAST(@SDTD as Bit) as SUTDActive,
		(SELECT COUNT(*) FROM Documents D WHERE D.EntityType = 'EMCRImage' AND D.EntityID = EMCR.EventModuleConferenceRoomID) as DocumentCount,
		CAST(CASE WHEN IsNull(EMCR.RoomOccupantID,0) > 0 THEN 1 ELSE 0 END as bit) as IsBooked,
		L.LayoutID
FROM	EventModuleConferenceRooms EMCR
JOIN	Layouts L ON L.LayoutID = EMCR.LayoutID
JOIN	ConferenceRooms C ON C.ConferenceRoomID = EMCR.ConferenceRoomID
JOIN	Areas A ON A.AreaID = C.AreaID
LEFT JOIN ConferenceRoomTypes CRT ON CRT.ConferenceRoomTypeID = EMCR.ConferenceRoomTypeID
WHERE	EMCR.EventModuleID = @EventModuleID
ORDER BY
	EMCR.MainRoom DESC,
	C.ConferenceRoomSortNumber


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventDelegates]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventDelegates]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @VIP varchar(255), @Disabled varchar(255), @Smoker varchar(255)

SET @VIP = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.VIP.Text'), 'VIP')
SET @Disabled = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Disabled.Text'), 'Disabled')
SET @Smoker = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Smoking.Text'), 'Smoking')

SELECT	EMD.EventModuleID,
		P.PersonID,
		P.PersonName,
		P.Forename,
		P.Surname,
		P.Sex,
		EMD.ArrivalDate,
		EMD.DepartureDate,
		IsNull(SBR.Name, IsNull(SB.Name, '')) as BlockName,
		IsNull(BTR.Description, IsNull(BT.Description, '')) as BedroomType,
		IsNull(SSB.Name, '') as SubBlockName,
		IsNull(R.ResidentialRoomID, 0) as ResidentialRoomID,
		IsNull(R.Name, '') as RoomName,
		IsNull(DG.Description, '') as GroupName,
		IsNull(DG.ShortName, '') as GroupShortName,
		IsNull(DG.GroupColour, '') as GroupColour,
		IsNull(C.Name, '') as CompanyName,
		EMD.Resident,
		EMD.CheckInNotes,
		EMD.Notes,
		EMD.HousekeepingNotes,
		P.VIP,
		P.Disabled,
		P.Smoker,
		IsNull(EMD.CheckedInDate, 0) as CheckedInDate,
		IsNull(EMD.CheckedOutDate, 0) as CheckedOutDate,
		@VIP as VIPText,
		@Disabled as DisabledText,
		@Smoker as SmokerText,
		P.Email,
		EMD.SingleOccupancy as FixInRoom
FROM	EventModuleDelegates EMD
JOIN	ViewPeople P ON P.PersonID = EMD.PersonID
LEFT JOIN Companies C ON C.CompanyID = P.CompanyID
LEFT JOIN SiteBLocks SB ON SB.BlockID = EMD.BlockID
LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = EMD.BedroomTypeID
LEFT JOIN ResidentialRoomOccupants RO ON RO.EventModuleID = Emd.EventModuleID AND RO.PersonID = EMD.PersonID AND RO.PersonSequenceID = EMD.PersonSequenceID AND RO.Date = CAST(EMD.ArrivalDate as Date)
LEFT JOIN ResidentialRooms R ON R.ResidentialRoomID = RO.ResidentialRoomID
LEFT JOIN SiteBLocks SBR ON SBR.BlockID = R.BlockID
LEFT JOIN BedroomTypes BTR ON BTR.BedroomTypeID = R.BedroomTypeID
LEFT JOIN SiteBLocks SSB ON SSB.BlockID = R.SubBlockID
LEFT JOIN DelegateGroups DG ON DG.DelegateGroupID = EMD.DelegateGroupID

WHERE EMD.EventModuleID = @EventModuleID
ORDER BY
	EMD.ArrivalDate,
	P.Surname,
	P.Forename

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventDialogue]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventDialogue]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @CustomerDialogueActionTypeID int
SELECT @CustomerDialogueActionTypeID = C.CustomerDialogueActionTypeID FROM Control C

SELECT	EM.EventModuleID,
		CA.ActionUserID,
		U.FullName as UserName,
		P.PersonName,
		CA.CreationDate,
		CA.ActionDate,
		CA.Notes, 
		CA.FinishedDate,
		U2.FullName as CreatedBy
FROM	CompanyActions CA
JOIN	EventActions EA ON EA.ActionID = CA.ActionID
JOIN	EventModules EM ON EM.EventID = EA.EventID
JOIN	ViewUsers U ON U.UserID = CA.ActionUserID
JOIN	ViewPeople P ON P.PersonID = CA.PersonID
JOIN	ViewUsers U2 ON U2.UserID = CA.CreationUserID
WHERE	CA.ActionTypeID = @CustomerDialogueActionTypeID
AND		@EventModuleId = EM.EventModuleID 
AND		NextActionID = 0


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventDocuments]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventDocuments]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	EM.EventModuleID,
		D.Compressed,
		D.DocumentID,
		D.DocumentType
FROM	Documents D
JOIN	EventModules EM ON EM.EventModuleID = @EventModuleID AND D.EntityID = EM.EventID
ORDER BY
	D.DocumentType


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventFinancial]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventFinancial]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	IH.EventModuleID,
		IH.InvoiceID,
		IH.InvoiceNumber,
		P.PersonName,
		IsNull(C.Name, '') as CompanyName,
		IB.TotalExVATAmount,
		IB.TotalAmountInvoiced,
		IB.TotalSettled,
		U.FullName as RaisedBy,
		IH.TaxPointDate,
		IH.DueDate,
		CAST(CASE WHEN EXISTS (SELECT NULL FROM Documents D WHERE D.EntityID = IH.InvoiceID AND D.EntityType = 'Invoice') THEN 1 ELSE 0 END as bit) as InvoiceExists,
		(SELECT TOP 1 DocumentID FROM Documents D WHERE D.EntityID = IH.InvoiceID AND D.EntityType = 'Invoice') as DocumentID
FROM	InvoiceHeaders IH
JOIN	ViewPeople P ON P.PersonID = IH.PersonID
--LEFT JOIN	Addresses A ON A.AddressID = IH.AddressID
LEFT JOIN	InvoiceBalances IB ON IB.InvoiceID = IH.InvoiceID
JOIN	ViewUsers U ON U.UserID = IH.CreationUserID
LEFT JOIN Companies C ON C.CompanyID = IH.CompanyID
WHERE	IH.EventModuleID = @EventModuleID
ORDER BY 
	IH.InvoiceNumber


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventMailings]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventMailings]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	N.EventModuleID,
		N.Title,
		N.CreationDate,
		U.FullName,
		T.Description
FROM	MailshotHeader N
JOIN	ViewUsers U ON U.UserID = N.CreationUserID
JOIN	WordTemplates T ON T.WordTemplateID = N.TemplateID
WHERE	N.EventModuleID = @EventModuleID
ORDER BY
	N.CreationDate DESC

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventNotes]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventNotes]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

--LEAVE BEGIN .... END AROUND WITH STATEMENT
BEGIN
WITH x AS 
( 
	SELECT	EMD.Date as [GroupDate],
			EMDN.EventModuleID,
			EMDN.EventModuleDepartmentNoteID,
			D.Description as Category,
			EMDN.NoteTime - CAST(FLOOR(CAST(EMDN.NoteTime as money)) as Datetime) + CAST(FLOOR(CAST(GETDATE() as money)) as Datetime) as NoteTime,
			part = DATEDIFF(DAY, 0, EMD.[Date])  
		        + DENSE_RANK() OVER 
			( 
					PARTITION BY
					EMDN.EventModuleID,
					EMDN.EventModuleDepartmentNoteID,
					D.Description,
					NoteTime
					ORDER BY EMD.[Date] DESC 
	        ) 
	FROM	EventModuleDepartmentNotes EMDN
	JOIN	EventModuleDays EMD ON EMD.EventModuleID = EMDN.EventModuleID
	JOIN	Departments D ON D.DepartmentID = EMDN.DepartmentID
	JOIN	EventModuleDayDepartmentNotes EMDDN ON EMDDN.EventModuleDayID = EMD.EventModuleDayID AND EMDDN.EventModuleDepartmentNoteID = EMDN.EventModuleDepartmentNoteID
	WHERE EMDN.EventModuleID = @EventModuleID
	AND	  EMDDN.Selected = 1
) 
SELECT	EventModuleID,
        EventModuleDepartmentNoteID,
        Category,
		NoteTime,
        MinDate = MIN(GroupDate), 
		MaxDate = MAX(GroupDate) 
INTO	#Results
FROM 
    x 
GROUP BY  
	part,
	EventModuleID, 
	EventModuleDepartmentNoteID,
    Category,
	NoteTime
ORDER BY 
	EventModuleID,  
	Category,
	MinDate,
	NoteTime
END

SELECT	R.*,
		D.Notes
FROM	#Results R
JOIN	EventModuleDepartmentNotes D ON D.EventModuleDepartmentNoteID = R.EventModuleDepartmentNoteID

DROP TABLE #results

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventQuotes]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventQuotes]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @QuoteID int

SELECT	TOP 1 @QuoteID = EMLQ.QuoteID
FROM	EventModuleLatestQuotes EMLQ
WHERE	EMLQ.EventModuleID = @EventModuleID

DECLARE @Quotes TABLE (QuoteID int, UnknownChargeTypes bit, NegativeElements bit)

INSERT INTO
	@Quotes
SELECT
	QuoteID,
	0,
	0
FROM
	EventModuleQuotes
WHERE
	(@QuoteID = 0 OR QuoteID = @QuoteID)
AND	(@EventModuleID = 0 OR EventModuleID = @EventModuleID)
AND (@QuoteID > 0 OR @EventModuleID > 0)

DECLARE @Totals TABLE (QuoteID int, TotalNet money, TotalGross money)

INSERT INTO
	@Totals
SELECT
	Q.QuoteID,
	SUM(EMQD.TotalNet) AS TotalNet,
	SUM(EMQD.TotalGross) AS TotalGross
FROM
	@Quotes Q
	JOIN EventModuleQuoteDetailVAT EMQD ON EMQD.QuoteID = Q.QuoteID
GROUP BY
	Q.QuoteID

UPDATE
	Q
SET
	UnknownChargeTypes = 1
FROM
	@Quotes Q
WHERE
	EXISTS(
	SELECT
		NULL
	FROM
		EventModuleQuoteDetails
	WHERE
		QuoteID = Q.QuoteID
	AND ISNULL(ChargeTypeID, 0) = 0
	)

UPDATE
	Q
SET
	NegativeElements = 1
FROM
	@Quotes Q
WHERE
	EXISTS(
	SELECT
		NULL
	FROM
		EventModuleQuoteDetails QD
		JOIN QuoteDetailElements QDE ON QDE.QuoteDetailID = QD.QuoteDetailID
	WHERE
		QD.QuoteID = Q.QuoteID
	AND QDE.Rate < 0
	)
	
SELECT
	EM.EventModuleID,
	EMQ.QuoteID,
	EMQ.FinalQuote,
	EMQ.QuoteDate,
	EMQ.Description,
	T.TotalNet,
	T.TotalGross
FROM
	@Quotes Q
	JOIN EventModuleQuotes EMQ ON EMQ.QuoteID = Q.QuoteID
	JOIN EventModules EM ON EM.EventModuleID = EMQ.EventModuleID
	LEFT JOIN @Totals T ON T.QuoteID = EMQ.QuoteID
ORDER BY
	EMQ.QuoteID DESC





GO
/****** Object:  StoredProcedure [KxMobile].[qryEventRoomingList]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventRoomingList]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	R.ResidentialRoomID,
		RTrim(R.name) as RoomName,
		EMRRA.FirstNight,
		EMrRA.LastNight,
		SB.Name as BlockName,
		BT.Description as BedroomType
FROM	EventModuleResidentialRoomAllocation EMRRA 
JOIN	ResidentialRooms R ON R.ResidentialRoomID = EMRRA.ResidentialRoomID
JOIN	SiteBlocks SB ON SB.BlockID = R.BlockID
JOIN	BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
WHERE	EMRRA.EventModuleID = @EventModuleID
ORDER BY
	R.DisplayOrder,
	R.Name,
	SB.SequenceNo,
	SB.Name,
	BT.Description

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventsRunning]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventsRunning]
	@FromDate				Datetime,
	@ToDate					DateTime,
	@ResourceDate			DateTime,
	@WithBedrooms			bit,
	@WithConferenceRooms	bit,
	@WithCatering			bit,
	@EventModuleID			int=0,
	@ApplicationID			int=0,
	@DeviceID				int=0,
	@SiteID					int=0,
	@AreaID					int=0,
	@BlockID				int=0,
	@EventTypeID			int=0,
	@ExcludeBNB				bit=0,
	@EventManagerID			int=0,
	@EventStatusID			int=0,
	@EventTitle				varchar(255)='',
	@SortOrder				int=0,
	@CompanyID				int=0,
	@PersonID				int=0
AS

DECLARE @WalkIn int, @SBT int, @CancellationStatusId int, @TurndownStatusID int, @CourseBusinessTypeID int
SELECT @WalkIn = C.WalkInEventTypeID, @SBT = C.StudentEventBusinessTypeID, @CancellationStatusId = C.CancellationStatusID, @TurndownStatusID =  C.TurnDownStatusID, @CourseBusinessTypeID = CourseBusinessTypeID FROM Control C

IF @EventTypeID = @WalkIn 
	SET @ExcludeBNB = 0

SET	@EventModuleID = IsNull(@EventModuleID, 0)

SELECT DISTINCT E.EventID,
		EM.EventModuleID,
		EM.Description as EventTitle,
		ET.Description as EventType,
		EM.ArrivalDate,
		EM.StartDate,
		EM.EndDate,
		0 as PersonID,
		0 as CompanyID,
		(SELECT COUNT(*) FROM EventModuleConferenceRooms EMCR WHERE EMCR.EventModuleID = EM.EventModuleID AND @ResourceDate BETWEEN CAST(EMCR.StartDateTime as Date) AND CAST(EMCR.EndDateTime as Date)) as ConfCount,
		(SELECT COUNT(*) FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = EM.EventModuleID AND @ResourceDate = CAST(EMBBA.Date as Date)) as BedroomCount,
		(SELECT COUNT(*) FROM EventModuleDayCateringAreas EMDCA WHERE EMDCA.EventModuleID = EM.EventModuleID AND @ResourceDate = CAST(EMDCA.DAte as Date)) as CateringCount,
		CAST(CASE WHEN E.EventTypeID = @WalkIn THEN 1 ELSE 0 END as bit) as WalkIn,
		IsNull(U.FullName, '') as FullName,
		IsNull(U.Surname, '') as Surname,
		ISNull(U.Forename, '') as Forename,
		IsNull((SELECT TOP 1 SB2.SequenceNo FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), 9999999) as BlockSort,
		RTrim(IsNull((SELECT TOP 1 SB2.Name FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), '')) as BlockName,
		RTrim(IsNull((SELECT TOP 1 CR2.Name FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), '')) as MainRoom,
		CAST(CASE WHEN (SELECT COUNT(*) FROM EventModuleConferenceRooms EMCR WHERE EMCR.EventModuleID = EM.EventModuleID) > 0 THEN 1 ELSE 0 END as Bit) as HasConfRooms,
		CAST(CASE WHEN (SELECT COUNT(*) FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = EM.EventModuleID) > 0 THEN 1 ELSE 0 END as Bit) as HasBedrooms,
		CAST(CASE WHEN (SELECT COUNT(*) FROM EventModuleDayCateringAreas EMDCA WHERE EMDCA.EventModuleID = EM.EventModuleID) > 0 THEN 1 ELSE 0 END as Bit) as HasCatering,
		S.StatusID,
		S.Description as [Status],
		S.ForegroundColour,
		S.BackgroundColour
INTO #Results 
FROM	EventModules EM
JOIN	Events E ON E.EventID = EM.EventID
JOIN	EventTypes ET ON E.EventTypeID = ET.EventTypeID
JOIN	Status S ON S.StatusID = EM.StatusID
LEFT JOIN EventCompanies EC ON EC.EventID = E.EventID
LEFT JOIN EventPeople EP ON EP.EventCompanyID = EC.EventCompanyID
LEFT JOIN ViewUsers U ON U.UserID = EM.AccountManagerPersonID
WHERE	(@EventModuleID = EM.EventModuleID)
OR		(		@EventModuleID = 0
		AND		CAST(FLOOR(CAST(EM.ArrivalDate as money)) as DateTime) <= @ToDate
		AND		CAST(FLOOR(CAST(EM.EndDate as money)) as DateTime) >= @FromDate
		AND		ET.EventBusinessTypeID <> @SBT
		AND		ET.EventBusinessTypeID <> @CourseBusinessTypeID
		AND		not (EM.StatusID in (@CancellationStatusId, @TurndownStatusID))
		AND		(@EventStatusID = 0 OR @EventStatusID = EM.StatusID)
		AND		(@EventTypeID = 0 OR @EventTypeID = E.EventTypeID)
		AND		(@EventManagerID = 0 OR @EventManagerID = EM.AccountManagerPersonID)
		AND		(@EventTitle = '' OR EM.Description like '%' + @EventTitle + '%' OR CAST(E.EventID as varchar(25)) like @EventTitle  + '%')
		AND		(@ExcludeBNB = 0 OR E.EventTypeID <> @WalkIn)
		AND		(@SiteID = 0 OR (
					EM.SiteID = @SiteID
				OR	EXISTS(SELECT NULL FROM EventModuleConferenceRooms EMCR1 JOIN ConferenceRooms CR1 ON EMCR1.ConferenceRoomID = CR1.ConferenceRoomID WHERE EMCR1.EventModuleID = EM.EventModuleID AND CR1.SiteID = @SiteID)
				OR	EXISTS(SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA1 JOIN SiteBlocks SB1 ON EMBBA1.BlockID = sB1.BlockID WHERE EMBBA1.EventModuleID = EM.EventModuleID AND SB1.SiteID = @SiteID)
				))
		AND		(@AreaID = 0 OR (
					EXISTS(SELECT NULL FROM EventModuleConferenceRooms EMCR1 JOIN ConferenceRooms CR1 ON EMCR1.ConferenceRoomID = CR1.ConferenceRoomID WHERE EMCR1.EventModuleID = EM.EventModuleID AND CR1.AreaID = @AreaID)
				OR	EXISTS(SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA1 JOIN SiteBlocks SB1 ON EMBBA1.BlockID = sB1.BlockID WHERE EMBBA1.EventModuleID = EM.EventModuleID AND SB1.AreaID = @AreaID)
				))
		AND		(@BlockID = 0 OR (
					EXISTS(SELECT NULL FROM EventModuleConferenceRooms EMCR1 JOIN ConferenceRooms CR1 ON EMCR1.ConferenceRoomID = CR1.ConferenceRoomID WHERE EMCR1.EventModuleID = EM.EventModuleID AND CR1.BlockID = @BlockID)
				OR	EXISTS(SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA1 JOIN SiteBlocks SB1 ON EMBBA1.BlockID = sB1.BlockID WHERE EMBBA1.EventModuleID = EM.EventModuleID AND SB1.BlockID = @BlockID)
				))
		AND		(@CompanyID = 0 OR @CompanyID = EC.CompanyID)
		AND		(@PersonID = 0 OR @PersonID = EP.PersonID)
		)

UPDATE R
SET PersonID = IsNull(EP.PersonID, 0), CompanyID = IsNull(EC.CompanyID, 0)
FROM #Results R
LEFT JOIN EventCompanies EC ON EC.EventID = R.EventID AND EC.PrimaryCompany = 1
LEFT JOIN EventPeople EP ON EP.EventCompanyID = EC.EventCompanyID

UPDATE R
SET PersonID = IsNull( (SELECT TOP 1  PersonID FROM EventPeople EP WHERE EP.EventID = R.EventID), 0)
FROM #Results R
WHERE R.PersonID = 0


SELECT R.*,
	IsNull(C.Name, '') as CompanyName,
	P.PersonName
FROM	#Results R
LEFT JOIN Companies C ON C.CompanyID = R.CompanyID
LEFT JOIN ViewPeople P ON P.PersonID = R.PersonID
WHERE	(@WithBedrooms = 0 AND @WithCatering = 0 AND @WithConferenceRooms = 0)
OR		(
		(@WithConferenceRooms = 1 AND R.ConfCount > 0)
	OR	(@WithBedrooms = 1 AND R.BedroomCount > 0)
	OR	(@WithCatering = 1 AND R.CateringCount > 0))
ORDER BY
	CASE WHEN @SortOrder = 0 THEN R.ArrivalDate ELSE 0 END,
	CASE WHEN @SortOrder = 1 THEN IsNull(NullIf(R.Surname, ''), 'zzzzzzzzzzzzzzzzzz') ELSE '' END,
	CASE WHEN @SortOrder = 1 THEN IsNull(NullIf(R.Forename, ''), 'zzzzzzzzzzzzzzzzzz') ELSE '' END,
	CASE WHEN @SortOrder = 1 THEN R.ArrivalDate ELSE 0 END,
	CASE WHEN @SortOrder = 2 THEN R.BlockSort ELSE 0 END, 
	CASE WHEN @SortOrder = 2 THEN R.ArrivalDate ELSE 0 END,
	R.EventID

DROP TABLE #Results


GO
/****** Object:  StoredProcedure [KxMobile].[qryEventSummary]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventSummary]
	@EventModuleID int,
	@UserID	int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @WalkIn int, @CancellationStatusId int, @TurndownStatusID int, @CustomerDialogueActionTypeID int
SELECT @WalkIn = C.WalkInEventTypeID, @CancellationStatusId = C.CancellationStatusID, @TurndownStatusID =  C.TurnDownStatusID, @CustomerDialogueActionTypeID = C.CustomerDialogueActionTypeID FROM Control C

SELECT	E.EventID,
		EM.EventModuleID,
		EM.Description as EventTitle,
		EM.ArrivalDate,
		EM.StartDate,
		EM.EndDate,
		S.Name as SiteName,
		ET.Description as EventType,
		EM.DayPeople,
		EM.NightPeople,
		ST.Description as StatusType,
		U.FullName as CreatedBy,
		EM.CreationDate as CreatedOn,
		IsNull(EQ.CreationDate,0) as EnquiryDate,
		ISNull(ES.Description, '') as EnquirySource,
		ET.EventSections,
		0 as PersonID,
		0 as CompanyID,
		CAST(CASE WHEN E.EventTypeID = @WalkIn THEN 1 ELSE 0 END as bit) as WalkIn,
		ST.ForegroundColour,
		ST.BackgroundColour
INTO	#Results
FROM	Events E
JOIN	EventModules EM ON E.EventID = EM.EventID
JOIN	EventTypes ET ON ET.EventTypeID = E.EventTypeID
JOIN	Sites S ON S.SiteID = EM.SiteID
JOIN	Status ST ON ST.StatusID = EM.StatusID
JOIN	ViewUsers U ON U.UserID = EM.CreationUserID
LEFT JOIN Enquiries EQ ON EQ.EnquiryID = E.EnquiryID
LEFT JOIN EnquirySources ES ON ES.EnquirySourceID = EQ.EnquirySourceID
WHERE	@EventModuleID = EM.EventModuleID
AND		not (EM.StatusID in (@CancellationStatusId, @TurndownStatusID))


UPDATE R
SET PersonID = IsNull(EP.PersonID, 0), CompanyID = IsNull(EC.CompanyID, 0)
FROM #Results R
LEFT JOIN EventCompanies EC ON EC.EventID = R.EventID AND EC.PrimaryCompany = 1
LEFT JOIN EventPeople EP ON EP.EventCompanyID = EC.EventCompanyID

UPDATE R
SET PersonID = IsNull( (SELECT TOP 1  PersonID FROM EventPeople EP WHERE EP.EventID = R.EventID), 0)
FROM #Results R
WHERE R.PersonID = 0


SELECT R.*,
	IsNull(C.Name, '') as CompanyName,
	P.PersonName,
	IsNull(CA.Telephone, P.Telephone) as Telephone,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleConferenceRooms EMCR WHERE EMCR.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasConfRooms,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = R.EventModuleID AND EMBBA.Allocation > 0) THEN 1 ELSE 0 END as bit) as HasBedrooms,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleDayCateringAreas CA WHERE CA.EventModuleID = R.EventModuleID AND CA.Cancelled = 0) THEN 1 ELSE 0 END as bit) as HasCatering,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleLatestQuotes Q WHERE Q.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasQuotes,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleDepartmentNotes N WHERE N.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasNotes,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM CompanyActions CA JOIN EventActions EA ON EA.ActionID = CA.ActionID JOIN EventModules EM ON EM.EventID = EA.EventID WHERE CA.ActionTypeID = @CustomerDialogueActionTypeID AND R.EventModuleID = EM.EventModuleID and NextActionID = 0 AND (@UserID = 0 OR @UserID = CA.ActionUserID)) THEN 1 ELSE 0 END as bit) as HasDialogue,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventModuleDelegates N WHERE N.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasDelegates,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM CompanyActions CA JOIN EventActions EA ON EA.ActionID = CA.ActionID JOIN EventModules EM ON EM.EventID = EA.EventID WHERE CA.ActionTypeID <> @CustomerDialogueActionTypeID AND R.EventModuleID = EM.EventModuleID AND ISNull(CA.FinishedDate, 0) < 29000 and IsNull(NextActionID,0) = 0 and (@UserID = 0 OR @UserID = CA.ActionUserID) AND CAST(FLOOR(CAST(CA.ActionDate as money)) as DateTime) <= CAST(FLOOR(CAST(getdate() as money)) as DateTime)) THEN 1 ELSE 0 END as bit) as HasActions,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM Documents D where D.EntityType = 'EventAttachment' AND D.EntityID = R.EventID) THEN 1 ELSE 0 END as bit) as HasDocuments,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM MailshotHeader N WHERE N.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasMailings,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM InvoiceHeaders IH where IH.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasInvoices,
	CAST(CASE WHEN EXISTS (SELECT NULL FROM EventClientBookingContracts C JOIN EventModules EM ON EM.EventID = C.EventID JOIN Documents D ON D.EntityID = C.EventClientBookingContractID AND D.EntityType = 'BookingContract' where EM.EventModuleID = R.EventModuleID) THEN 1 ELSE 0 END as bit) as HasContracts,
	(SELECT TOP 1 D.DocumentID FROM EventClientBookingContracts C JOIN EventModules EM ON EM.EventID = C.EventID JOIN Documents D ON D.EntityID = C.EventClientBookingContractID AND D.EntityType = 'BookingContract' where EM.EventModuleID = R.EventModuleID) as ContractDocumentID
FROM	#Results R
LEFT JOIN Companies C ON C.CompanyID = R.CompanyID
LEFT JOIN ViewPeople P ON P.PersonID = R.PersonID
LEFT JOIN CompanyAddresses CA ON CA.CompanyAddressID = P.CompanyAddressID
--LEFT JOIN Addresses A ON A.AddressID = CA.AddressID
ORDER BY
	R.ArrivalDate,
	R.EventID

DROP TABLE #Results

GO
/****** Object:  StoredProcedure [KxMobile].[qryEventTimeTable]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryEventTimeTable]
	@EventModuleID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	T.EventModuleDayTimetableID,
		T.TimeDescription,
		T.RoomDescription,
		T.DateTime,
		T.Notes,
		C.Name as RoomName
FROM	EventModuleDayTimeTable T 
LEFT JOIN ConferenceRooms C ON C.ConferenceRoomID = T.ConferenceRoomID
WHERE	T.EventModuleID = @EventModuleID
ORDER BY
		T.DateTime


GO
/****** Object:  StoredProcedure [KxMobile].[qryFindVacantRoomsByEventModuleDelegateID]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryFindVacantRoomsByEventModuleDelegateID]
	@EventModuleDelegateID int,
	@RoomName	varchar(255),
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE	@FromDate		DateTime,
	@ToDate			DateTime,
	@SiteIDs		varchar(max),
	@AreaIDs		varchar(max),
	@BlockIDs		varchar(max),
	@BedroomTypeIDs		varchar(max),
	@MaidStatus		bit,
	@EventModuleID		int,
	@UserCurrentID		int,
	@MatchReservation	bit,
	@OnlyNonSmoking		bit,
	@OnlySmoking		bit,
	@OnlyDisabled		bit,
	@OnlyLastLet		bit,
	@OnlyVIP		bit,
	@OnlyReserved		bit,
	@EventBusinessTypeID	int,
	@SubBlockIDs		varchar(max),
	@PersonID		int,
	@PersonSequenceID	int,
	@DontOverrideDateParams	bit,
	@IncludeOccupiedRooms   bit,
	@OrderBy		varchar(50),
	@FlatRoomsForEvent	bit

SET	@MaidStatus = 0
SET	@DontOverrideDateParams = 1
SET	@IncludeOccupiedRooms = 1
SET	@OrderBy = 'Room Name'
SET	@UserCurrentID = 0
SET	@OnlyLastLet = 0 

DECLARE @VIP varchar(255), @Disabled varchar(255), @Smoker varchar(255), @LastLet varchar(255), @Date datetime

SET @VIP = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.VIP.Text'), 'VIP')
SET @Disabled = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Disabled.Text'), 'Disabled')
SET @Smoker = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Smoking.Text'), 'Smoking')
SET @LastLet = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.LastLet.Text'), 'LastLet')

SELECT	@FromDate = CAST(Floor(CAST(EMD.ArrivalDate as Money)) as DateTime),
	@ToDate =  CAST(Floor(CAST(EMD.DepartureDate as Money)) as DateTime),
	@SiteIDs = CASE WHEN @RoomName <> '' THEN '0' ELSE CAST(ISNULL(SB.SiteID, 0) as varchar(10)) END,
	@AreaIDs = CASE WHEN @RoomName <> '' THEN '0' ELSE CAST(ISNULL(SB.AreaID, 0) as varchar(10)) END,
	@BlockIDs = CASE WHEN @RoomName <> '' THEN '0' ELSE CAST(ISNULL(EMD.BlockID, 0) as varchar(10)) END,
	@BedroomTypeIDs = CASE WHEN @RoomName <> '' THEN '0' ELSE CAST(ISNULL(EMD.BedroomTypeID, 0) as varchar(10)) END,
	@EventModuleID = EMD.EventModuleID,
	@MatchReservation = 1,
	@OnlyNonSmoking = 0,
	@OnlySmoking = 0,
	@OnlyDisabled = 0,
	@OnlyVIP = 0,
	@OnlyReserved = 0,
	@EventBusinessTypeID = ET.EventBusinessTypeID,
	@SubBlockIDs = '0',
	@PersonID = EMD.PersonID,
	@PersonSequenceID = EMD.PersonSequenceID,
	@FlatRoomsForEvent = CASE WHEN E.EventTypeID = (SELECT TOP 1 C.WalkInEventTypeID FROM Control C) THEN 1 ELSE 0 END
FROM	EventModuleDelegates EMD
JOIN	People P ON P.PersonID = EMD.PersonID
JOIN	EventModules EM ON EM.EventModuleID = EMD.EventModuleID
JOIN	Events E ON E.EventID = EM.EventID
JOIN	EventTypes ET ON ET.EventTypeID = E.EventTypeID
LEFT JOIN SiteBlocks SB ON SB.BlockID = EMD.BlockID
WHERE	EMD.EventModuleDelegateID = @EventModuleDelegateID

--SELECT @BedroomTypeIDs, @BlockIDs, @AreaIDs, @SiteIDs

SET NOCOUNT ON

DECLARE	@NewFlats int
SELECT	@NewFlats = IsNull(Value,0) FROM CustomSettings WHERE CustomSetting = 'Residential.2009.Flat.Reservation.System'
SET @NewFlats = IsNull(@NewFlats,0)

SELECT
	@EventBusinessTypeID = ET.EventBusinessTypeID
FROM
	EventModules EM
	JOIN Events E ON E.EventID = EM.EventID
	JOIN EventTypes ET ON ET.EventTypeID = E.EventTypeID
WHERE
	EM.EventModuleID = @EventModuleID

CREATE TABLE #Allocations (BlockID int, BedroomTypeID int, Allocation int, ResidentialRoomID int)
CREATE TABLE #RestrictedRooms (BlockID int, BedroomTypeID int, ResidentialRoomID int, Date datetime)
CREATE TABLE #FullAllocations (BlockID int, BedroomTypeID int)
CREATE TABLE #TempOccupants (ResidentialRoomID int, Occupants int)

CREATE TABLE #AvailableRooms (ResidentialRoomID int, SubBlockID int, BlockID int, BedroomTypeID int, RoomName varchar(50), SubBlockName varchar(50),
				BlockName varchar(50), BedroomType varchar(50), LastLet bit, Smoking bit, Disabled bit,
				VIP bit, MaidStatus char(1), Info varchar(50), TypicalCapacity int, Capacity int,
				Occupants int, ActualAllocation int, HeaderResidentialRoomID int, Inactive bit,
				Restrictions int, Occupancy varchar(255), Date DateTime, DisplayOrder int)
CREATE TABLE #FinalResult (ResidentialRoomID int, SubBlockID int, BlockID int, BedroomTypeID int, RoomName varchar(50), SubBlockName varchar(50),
				BlockName varchar(50), BedroomType varchar(50), LastLet bit, Smoking bit, Disabled bit,
				VIP bit, MaidStatus char(1), Info varchar(50), TypicalCapacity int, Capacity int,
				Occupants int, ActualAllocation int, HeaderResidentialRoomID int, Inactive bit,
				Restrictions int, Occupancy varchar(255), Date DateTime, DisplayOrder int)

--SELECT top 10 * FROM ResidentialRoomOccupants

CREATE INDEX IDX1 ON #AvailableRooms (ResidentialRoomID)

CREATE TABLE #ResidentialRoomOccupants (ResidentialRoomID int, EventModuleID int, PersonID int, PersonSequenceID int, [Date] DateTime, SingleOccupant bit)


INSERT INTO #ResidentialRoomOccupants
SELECT
	ResidentialRoomID, EventModuleID, PersonID, PersonSequenceID, [Date], SingleOccupant
FROM
	ResidentialRoomOccupants
WHERE
	Date BETWEEN @FromDate AND @ToDate

DELETE FROM #ResidentialRoomOccupants WHERE PersonID = -999

CREATE INDEX IDX1 ON #ResidentialRoomOccupants(EventModuleID, PersonID, PersonSequenceID)

SELECT
	*
INTO
	#EventModuleBlockBedroomAllocation
FROM
	EventModuleBlockBedroomAllocation
WHERE
	Date BETWEEN @FromDate AND @ToDate

SELECT
	Date
INTO
	#Dates
FROM
	Dates
WHERE
	Date BETWEEN @FromDate and @ToDate

INSERT INTO #RestrictedRooms
SELECT	DISTINCT RR.BlockID, RR.BedroomTypeID, RRR.ResidentialRoomID, D.Date
from	ResidentialRoomRestrictions rrr
join	ResidentialRoomRestrictionheader rrrh on rrrh.residentialroomrestrictionid =  rrr.residentialroomrestrictionid
join	ResidentialRooms RR ON RR.ResidentialRoomID = RRR.ResidentialRoomID
JOIN	#Dates D ON D.Date BETWEEN StartDate AND ENDDATE
WHERE	enddate >= @FromDate AND startdate <= @ToDate

IF @EventModuleID > 0 AND @MatchReservation = 0 AND (NOT EXISTS (SELECT Value FROM CustomSettings WHERE CustomSetting='AllowDifferentAllocationOverBooking'))
BEGIN
	INSERT INTO #FullAllocations
	SELECT	DISTINCT BTSL.BlockID, BTSL.BedroomTypeID
	FROM	bedroomTypeSiteLimits BTSL
	JOIN	(
		SELECT	BlockID,
			BedroomTypeID,
			SUM(Allocation) as Allocation,
			Date
		FROM	(
			SELECT	BlockID,
				BedroomTypeID,
				SUM(Allocation) as Allocation,
				Date
			FROM	#EventModuleBlockBedroomAllocation
			WHERE	EventModuleID <> @EventModuleID
			AND	EventModuleID <> -99
			GROUP BY
				BlockID, BedroomTypeID, Date
			UNION ALL
			SELECT	BlockID,
				BedroomTypeID,
				1,
				Date
			FROM	#RestrictedRooms
			UNION ALL
			SELECT	ASRT.BlockID, ASRT.BedroomTypeID, ASDQ.CurrentRoomQuantity, DayDate
			FROM	AllocationSets AA
			JOIN	AllocationSetRoomTypes ASRT ON ASRT.AllocationSetID = AA.AllocationSetID
			JOIN	AllocationSetStatuses S ON S.StatusID = AA.StatusID
			JOIN	AllocationSetDayQuantities ASDQ ON ASRT.AllocationSetRoomTypeID = ASDQ.AllocationSetRoomTypeID
			JOIN	AllocationCompanies AC ON AC.AllocationSetID = AA.AllocationSetID
			WHERE	AA.enddate >= @FromDate AND AA.startdate <= @ToDate
			AND	DayDate BETWEEN @FromDate AND @ToDate
			AND	S.Active = 1
			AND	NOT EXISTS(SELECT NULL FROM EventModules EM JOIN EventCompanies EC ON EM.EventID = EC.EventID AND EC.CompanyID = AC.CompanyID WHERE EM.EventModuleID = @EventModuleID)
		) R
 		GROUP BY
			BlockID,
			BedroomTypeID,
			DATE
		) BB ON BB.BlockID = BTSL.BlockID and BTSL.BedroomTypeID = BB.BedroomTypeID
	WHERE	BB.Allocation >= BTSL.BedroomCount
END

Create INDEX IDX2 ON #FullAllocations(BlockID,BedroomTypeID)

--Create the temporary tables for parameters passed through
SELECT
	BedroomTypeID, RoomInFlat
INTO
	#BedroomTypes
FROM
	BedroomTypes
WHERE
	';' + @BedroomTypeIDs + ';' LIKE '%;' + CAST(BedroomTypeID AS varchar(255)) + ';%'

SELECT
	SiteID
INTO
	#Sites
FROM
	Sites
WHERE
	';' + @SiteIDs + ';' LIKE '%;' + CAST(SiteID AS varchar(255)) + ';%'

SELECT
	AreaID
INTO
	#Areas
FROM
	Areas
WHERE
	';' + @AreaIDs + ';' LIKE '%;' + CAST(AreaID AS varchar(255)) + ';%'

SELECT
	BlockID
INTO
	#Blocks
FROM
	SiteBlocks
WHERE
	';' + @BlockIDs + ';' LIKE '%;' + CAST(BlockID AS varchar(255)) + ';%'

SELECT
	BlockID
INTO
	#SubBlocks
FROM
	SiteBlocks
WHERE
	';' + @SubBlockIDs + ';' LIKE '%;' + CAST(BlockID AS varchar(255)) + ';%'

DECLARE @ArrivalDate datetime, @DepartureDate datetime, @Now datetime, @CheckDate Datetime, @EndTimeForPreviousDay datetime
SET @Now = CURRENT_TIMESTAMP
IF @FromDate < @Now
	SET @CheckDate = @FromDate
ELSE
	SET @CheckDate = @Now

SET @ArrivalDate = NULL
SET @EndTimeForPreviousDay = ISNULL((SELECT Value FROM CustomSettings WHERE CustomSetting = 'EndTimeForPreviousDay'), '0:00')
SET @EndTimeForPreviousDay = @EndTimeForPreviousDay - CAST(FLOOR(CAST(@EndTimeForPreviousDay AS Float)) AS DateTime)

DECLARE @DisplayHeaderAvailability int

SET @DisplayHeaderAvailability = ISNULL((SELECT Value FROM CustomSettings WHERE CustomSetting = 'Residential.Reservation.DisplayHeaderAvailability'), 0)

DECLARE		@GroupNumber int

SET		@GroupNumber = 0

IF @PersonID > 0
BEGIN
	SELECT
		@ArrivalDate = ArrivalDate,
		@DepartureDate = DepartureDate
	FROM
		EventModuleDelegates
	WHERE
		EventModuleID = @EventModuleID
	AND	PersonID = @PersonID
	AND	PersonSequenceID = @PersonSequenceID

	IF @DontOverrideDateParams = 0
	BEGIN
  	  SET @FromDate = CAST(FLOOR(CAST(@ArrivalDate AS Float)) AS DateTime)
	  SET @ToDate = CAST(FLOOR(CAST(@DepartureDate AS Float)) AS DateTime) - 1
	END

	IF @FromDate < CAST(FLOOR(CAST(@Now AS Float)) AS DateTime)
		AND @DontOverrideDateParams = 0
	BEGIN
        	SET @FromDate = CAST(FLOOR(CAST(@Now AS Float)) AS DateTime)
		IF @Now - CAST(FLOOR(CAST(@Now AS Float)) AS DateTime) < @EndTimeForPreviousDay
		          SET @FromDate = @FromDate - 1
	END

	SELECT	@GroupNumber = GroupNumber
	FROM	EventModuleBlockBedroomAllocationDelegates EMBBAD
	WHERE	EventModuleID = @EventModuleID
	AND	PersonID = @PersonID

END

IF @FromDate > @ToDate AND @DontOverrideDateParams = 0
  	SET @FromDate = CAST(FLOOR(CAST(@Now AS Float)) AS DateTime)


DECLARE @CurrentResidentialRoomID int

SELECT @CurrentResidentialRoomID = ResidentialRoomID FROM ResidentialRoomOccupants RRO WHERE RRO.EventModuleID = @EventModuleID AND RRO.PersonID = @PersonID AND RRO.PersonSequenceID = @PersonSequenceID AND Date = @FromDate

IF @EventModuleID > 0 AND @MatchReservation = 1
BEGIN

	INSERT INTO #Allocations
	SELECT
		A.BlockID,
		A.BedroomTypeID,
		CASE WHeN ISNULL(A.AllocationType, '') <> '' THEN
			MAX(A.Allocation * IsNull(BTH.Capacity, BT.Capacity) - ISNULL(B.Occupants,0))
		ELSE
			MAX(A.Allocation * BT.Capacity - ISNULL(B.Occupants, 0)) END
		AS Allocation,
		B.ResidentialRoomID
	FROM
		#EventModuleBlockBedroomAllocation A
		JOIN SiteBlocks SB ON SB.BlockID = A.BlockID
		JOIN BedroomTypes BT ON BT.BedroomTypeID = A.BedroomTypeID
		LEFT JOIN ResidentialRooms RR ON RR.ResidentialRoomID = A.ResidentialRoomID
		LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = RR.HeaderResidentialRoomID
		LEFT JOIN BedroomTypes BTH ON BTH.BedroomTypeID = RRH.BedroomTypeID
		LEFT JOIN
			(
			SELECT
				RRO.Date,
				RR.BlockID,
				RR.BedroomTypeID,
				COUNT(*) AS Occupants,
				RRO.ResidentialRoomID
			FROM
				#ResidentialRoomOccupants RRO
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
			WHERE
				RRO.EventModuleID = @EventModuleID
			AND	RR.ResidentialRoomID <> IsNull(@CurrentResidentialRoomID,0)
			AND	(@PersonID = 0 OR RRO.PersonID <> @PersonID OR RRO.PersonSequenceID <> @PersonSequenceID AND RRO.PersonID <> -999)
			GROUP BY
				RRO.Date,
				RR.BlockID,
				RR.BedroomTypeID,
				RRO.ResidentialRoomID
			) B
		ON
			B.Date = A.Date
		AND	B.BlockID = A.BlockID
		AND	B.BedroomTypeID = A.BedroomTypeID
	WHERE
		A.EventModuleID = @EventModuleID
	AND	A.Date BETWEEN @FromDate AND @ToDate
	GROUP BY
		A.BlockID,
		A.BedroomTypeID,
		B.ResidentialRoomID,
		A.AllocationType

	DELETE FROM #Allocations WHERE Allocation <= 0
END

SELECT 
	RR.ResidentialRoomID,
	RR.BedroomTypeID
INTO
	#Rooms
FROM
	ResidentialRooms RR
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID
WHERE
	(B.SiteID IN (SELECT SiteID FROM #Sites) OR @SiteIDs = '0')
AND	(B.AreaID IN (SELECT AreaID FROM #Areas) OR @AreaIDs = '0')
AND	(RR.BlockID IN (SELECT BlockID FROM #Blocks) OR @BlockIDs = '0')
AND	(RR.SubBlockID IN (SELECT BlockID FROM #SubBlocks) OR @SubBlockIDs = '0')
AND	(RR.BedroomTypeID IN (SELECT BedroomTypeID FROM #BedroomTypes) OR @BedroomTypeIDs = '0')
AND	(RR.HeaderResidentialRoomID < 1 OR IsNull(@NewFlats,0) = 1
		AND EXISTS (SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = @EventModuleID
				AND IsNull(EMBBA.AllocationType, '') <> ''))
AND	(IsNull(@NewFlats,0) = 0 OR RR.HeaderResidentialRoomID = 0 OR (RR.HeaderResidentialRoomID > 0 AND
		EXISTS (SELECT NULL FROM EventModuleBlockBedroomAllocation EMBBA WHERE EMBBA.EventModuleID = @EventModuleID
				AND IsNull(EMBBA.AllocationType, '') <> '')))
AND	(@OnlyReserved = 0 OR RR.ResidentialRoomID IN (SELECT ResidentialRoomID FROM EventModuleResidentialRooms WHERE Date
							BETWEEN @FromDate AND @ToDate AND EventModuleID = @EventModuleID))
AND	(@OnlyNonSmoking = 0 OR (@OnlyNonSmoking = 1 AND RR.Smoking = 0))
AND	(@OnlySmoking = 0 OR (@OnlySmoking = 1 AND RR.Smoking = 1))
AND	(@OnlyDisabled = 0 OR (@OnlyDisabled = 1 AND RR.Disabled = 1))
AND	(@OnlyLastLet = 0 OR (@OnlyLastLet = 1 AND RR.LastLet = 1))
AND	(@OnlyVIP = 0 OR (@OnlyVIP = 1 AND RR.VIP = 1))
AND	(@MaidStatus = 0 OR (@MaidStatus = 1 AND RR.MaidStatus in ('C', 'I')))
AND	(@MatchReservation = 0 OR Not EXISTS (SELECT NULL FROM EventModuleResidentialRoomAllocation RA WHERE RA.EventModuleID = @EventModuleID))
AND	(EXISTS (SELECT NULL FROM BedroomTypeSiteLimits BTSL WHERE BTSL.BlockID = RR.BlockID AND BTSL.BedroomTypeID = RR.BedroomTypeID))

INSERT INTO #Rooms
SELECT	RA.ResidentialRoomID,
	RR.BedroomTypeID
FROM	EventModuleResidentialRoomAllocation RA
JOIN	ResidentialRooms RR ON RR.ResidentialRoomID = rA.ResidentialRoomID
WHERE	RA.EventModuleID = @EventModuleID

SELECT
	A.ResidentialRoomID,
	MAX(A.Allocation) AS Allocation,
	MAX(A.ActualAllocation) AS ActualAllocation
INTO
	#RoomAllocations
FROM
	(
	SELECT
		ResidentialRoomID,
		Date,
		SUM(Allocation) AS Allocation,
		SUM(Actual) AS ActualAllocation
	FROM
		(
		SELECT
			ResidentialRoomID,
			EventModuleID,
			Date,
			MAX(ISNULL(Allocation, 0)) AS Allocation,
			MAX(ISNULL(Actual, 0)) AS Actual
		FROM
			(
			SELECT
				CASE WHEN IsNull(@NewFlats,0) = 1 THEN RR.ResidentialRoomID ELSE RR.HeaderResidentialRoomID END AS ResidentialRoomID,
				EventModuleID,
				Date,
				SUM(Allocation) AS Allocation,
				0 AS Actual
			FROM
				#EventModuleBlockBedroomAllocation EMBBA
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = EMBBA.ResidentialRoomID
				JOIN #Rooms RL ON RL.ResidentialRoomID = RR.ResidentialRoomID
			WHERE
				Date BETWEEN @FromDate AND @ToDate
			AND	RR.HeaderResidentialRoomID > 0
			AND	EMBBA.EventModuleID <> ISNULL(@EventModuleID, 0)
			GROUP BY
				RR.HeaderResidentialRoomID,
				RR.ResidentialRoomID,
				EventModuleID,
				EMBBA.Date

			UNION ALL

			SELECT
				COALESCE(EMBBAD.ResidentialRoomID, EMBBA.ResidentialRoomID) As ResidentialRoomID,
				EMBBA.EventModuleID,
				EMBBA.Date,
				SUM(EMBBA.Allocation) AS Allocation,
				0 AS Actual
			FROM
				#EventModuleBlockBedroomAllocation EMBBA
				JOIN EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBA.EventModuleID = EMBBAD.EventModuleID AND EMBBA.Date = EMBBAD.Date AND EMBBAD.ResidentialRoomID <> 0
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = COALESCE(EMBBAD.ResidentialRoomID, EMBBA.ResidentialRoomID)
			WHERE
				EMBBA.Date BETWEEN @FromDate AND @ToDate
			AND	RR.HeaderResidentialRoomID < 1
			AND	(EMBBA.EventModuleID <> ISNULL(@EventModuleID, 0) OR (EMBBA.EventModuleID = ISNULL(@EventModuleID, 0) AND @GroupNumber <> IsNull(EMBBAD.GroupNumber,0)))
			GROUP BY
				EMBBA.ResidentialRoomID,
				EMBBAD.ResidentialRoomID,
				EMBBA.EventModuleID,
				EMBBA.Date

			UNION ALL

			SELECT
				CASE WHEN RR.HeaderResidentialRoomID < 1 THEN
					RR.ResidentialRoomID
				ELSE
					CASE WHEN IsNull(@NewFlats,0) = 1 THEN RR.ResidentialRoomID ELSE RR.HeaderResidentialRoomID END
				END AS ResidentialRoomID,
				RRO.EventModuleID,
				RRO.Date,
				COUNT(RRO.PersonID) AS Allocation,
				COUNT(RRO.PersonID) AS Actual
			FROM
				#ResidentialRoomOccupants RRO
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
				JOIN #Rooms RL ON RL.ResidentialRoomID = RR.ResidentialRoomID
				JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
				/*DLD 31/3/06 This join replaces a Not Exists check which was very slow. The join is checked for
				Null values to exclude roooms for people who have moved room.*/
				LEFT JOIN (Select PersonId,PersonSequenceId,EventModuleId, Max(ResidentialRoomId) as ResidentialRoomId
						FROM ResidentialRoomOccupants
						WHERE Date >= @CheckDate
						AND EventModuleID = @EventModuleID
						GROUP BY PersonId,PersonSequenceId,EventModuleId) X
					ON X.PersonID = RRO.PersonID
						AND X.PersonSequenceID = RRO.PersonSequenceID


						AND X.EventModuleId = RRO.EventModuleId
						AND X.ResidentialRoomID <> RRO.ResidentialRoomID


			WHERE
				((Date BETWEEN @FromDate AND @ToDate)
				OR (@ArrivalDate <= @CheckDate AND EMD.CheckedInDate > '1980-01-01' AND EMD.CheckedOutDate < '1980-01-01'))
				AND X.PersonId IS NULL

			GROUP BY
				CASE WHEN RR.HeaderResidentialRoomID < 1 THEN
					RR.ResidentialRoomID
				ELSE
					CASE WHEN IsNull(@NewFlats,0) = 1 THEN RR.ResidentialRoomID ELSE RR.HeaderResidentialRoomID END
				END,
				RRO.EventModuleID,
				RRO.Date

		) A
		GROUP BY
			ResidentialRoomID,
			EventModuleID,
			Date
	) A
	GROUP BY
		ResidentialRoomID,
		Date
) A
GROUP BY
	A.ResidentialRoomID

IF @IncludeOccupiedRooms = 1
BEGIN
INSERT INTO
	#AvailableRooms
SELECT DISTINCT
	RR.ResidentialRoomID,
	RR.SubBlockID,
	RR.BlockID,
	RR.BedroomTypeID,
	RR.Name as RoomName,
	ISNULL(SB.Name, 'Others') as SubBlockName,
	B.Name as BlockName,
	BT.Description as BedroomType,
	RR.LastLet,
	RR.Smoking,
	RR.Disabled,
	RR.VIP,
	RR.MaidStatus,
	ISNULL(RR.Info, '') AS Info,
	RR.TypicalCapacity,
	RR.Capacity,
	CASE WHEN (SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID
                AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP -1 AS money)) AS datetime)
                AND RRO.ResidentialRoomID = RR.ResidentialRoomID AND EMD.CheckedInDate > '19800101' AND EMD.CheckedOutDate < '19800101'
        	) > 0 THEN 1
	ELSE ISNULL(A.Allocation, 0) + ISNULL(T.Occupants, 0) END AS Occupants,
	ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) AS ActualAllocation,
	RR.HeaderResidentialRoomID,
	IsNull(RR.Inactive,0) as InActive,
	AR.Restrictions,
	CASE
	  WHEN (SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID
                AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP -1 AS money)) AS datetime)
                AND RRO.ResidentialRoomID = RR.ResidentialRoomID  AND RRO.Date BETWEEN @FromDate AND @ToDate AND EMD.CheckedInDate > '19800101' AND EMD.CheckedOutDate < '19800101'
        	) > 0 THEN 'Occupied - Previous occupant not checked out'
	  WHEN (SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID
                AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)
                AND RRO.ResidentialRoomID = RR.ResidentialRoomID AND RRO.Date BETWEEN @FromDate AND @ToDate AND EMD.CheckedInDate > '19800101' AND EMD.CheckedOutDate < '19800101'
	        ) > 0 THEN 'Occupied - Previous occupant not checked out'
	  WHEN (SELECT COUNT(*) FROM ResidentialRoomRestrictions RRR
               JOIN ResidentialRoomRestrictionHeader RH ON RH.ResidentialRoomRestrictionID = RRR.ResidentialRoomRestrictionID
               WHERE RRR.ResidentialRoomID = RR.ResidentialRoomID
               AND CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime) BETWEEN RH.StartDate AND RH.EndDate
	       ) > 0 THEN 'Out Of Service'
	  WHEN ISNULL(A.Allocation, 0) + ISNULL(T.Occupants, 0) >= RR.Capacity AND
		(SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID
                AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.Date = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)
                AND RRO.ResidentialRoomID = RR.ResidentialRoomID  AND RRO.Date BETWEEN @FromDate AND @ToDate AND EMD.CheckedInDate < '19800101' AND EMD.CheckedOutDate < '19800101'
	        ) > 0 THEN 'Occupied - Room capacity reached/exceeded'
	  WHEN (SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID
                AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.ResidentialRoomID = RR.ResidentialRoomID AND RRO.Date BETWEEN @FromDate AND @ToDate
	        ) > 0 THEN 'Occupied - Reserved'
	  ELSE 'UnOccupied - Allocation is Full' END AS Occupancy,
	CAST(0 AS DateTime) AS Date,
	RR.DisplayOrder

FROM
	#Rooms RL
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RL.ResidentialRoomID
        JOIN #ResidentialRoomOccupants RRO ON RRO.ResidentialRoomID = RR.ResidentialRoomID
	JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID
	JOIN BedroomTypes BT ON BT.BedroomTypeID = RR.BedroomTypeID
	LEFT OUTER JOIN SiteBlocks SB ON SB.BlockID = RR.SubBlockID

	LEFT JOIN
		(
		SELECT
			AR.BlockID,
			AR.BedroomTypeID,
			COUNT(*) AS Restrictions
		FROM
			AvailabilityRestrictions AR
		WHERE
			AR.FromDate <= @ToDate
		AND	AR.ToDate >= @FromDate
		AND	AR.EventBusinessTypeID = @EventBusinessTypeID
		GROUP BY
			AR.BlockID,
			AR.BedroomTypeID
		) AR
	ON
		AR.BlockID = RR.BlockID
	AND	AR.BedroomTypeID = RR.BedroomTypeID

	LEFT JOIN (SELECT BlockID, BedroomTypeID, SUM(Allocation) as Allocation FROM #Allocations GROUP BY BlockID, BedroomTypeID) BBA
	ON	BBA.BlockID = RR.BlockID
	AND	BBA.BedroomTypeID = RR.BedroomTypeID

	LEFT JOIN
		#TempOccupants T
	ON
		T.ResidentialRoomID = RL.ResidentialRoomID

	LEFT JOIN
		#RoomAllocations A
	ON	A.ResidentialRoomID = RR.ResidentialRoomID

	LEFT JOIN
		(
		SELECT
			RRO.ResidentialRoomID,
			COUNT(*) AS OOSCount
		FROM
			#ResidentialRoomOccupants RRO
		WHERE
			RRO.Date BETWEEN @FromDate AND @ToDate
		AND	RRO.PersonID = -99
		AND	RRO.EventModuleID = -99
		GROUP BY
			RRO.ResidentialRoomID
		) OOS ON	OOS.ResidentialRoomID = RR.ResidentialRoomID
WHERE
	((ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) >= RR.Capacity) OR (EMD.CheckedInDate > '19800101' AND EMD.CheckedOutDate < '19800101'))
	--OR (ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) > 0)
AND	(@MatchReservation = 0 OR BBA.Allocation >= 1 OR (SELECT SUM(AB.Allocation) FROM #Allocations AB WHERE AB.BlockID=RR.BlockID AND AB.BedroomTypeID=RR.BedroomTypeID) >= 1)
AND	(COALESCE(OOS.OOSCount, 0) = 0)
AND	(@RoomName = '' OR RR.Name LIKE '%' + RTRIM(LTRIM(@RoomName)) + '%')
END

INSERT INTO
	#AvailableRooms
SELECT DISTINCT
	RR.ResidentialRoomID,
	RR.SubBlockID,
	RR.BlockID,
	RR.BedroomTypeID,
	RR.Name as RoomName,
	ISNULL(SB.Name, 'Others') as SubBlockName,
	B.Name as BlockName,
	BT.Description as BedroomType,
	RR.LastLet,
	RR.Smoking,
	RR.Disabled,
	RR.VIP,
	RR.MaidStatus,
	ISNULL(RR.Info, '') AS Info,
	RR.TypicalCapacity,
	RR.Capacity,
	ISNULL(A.Allocation, 0) + ISNULL(T.Occupants, 0) AS Occupants,
	ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) AS ActualAllocation,
	RR.HeaderResidentialRoomID,
	IsNull(RR.Inactive,0) as InActive,
	AR.Restrictions,
	'Unoccupied' AS Occupancy,
	CAST(0 AS DateTime) AS Date,
	RR.DisplayOrder
FROM
	#Rooms RL
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RL.ResidentialRoomID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID
	JOIN BedroomTypes BT ON BT.BedroomTypeID = RR.BedroomTypeID
	LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = RR.HeaderResidentialRoomID
	LEFT OUTER JOIN SiteBlocks SB ON SB.BlockID = RR.SubBlockID

	LEFT JOIN
		(
		SELECT
			AR.BlockID,
			AR.BedroomTypeID,
			COUNT(*) AS Restrictions
		FROM
			AvailabilityRestrictions AR
		WHERE
			AR.FromDate <= @ToDate
		AND	AR.ToDate >= @FromDate
		AND	AR.EventBusinessTypeID = @EventBusinessTypeID
		GROUP BY
			AR.BlockID,
			AR.BedroomTypeID
		) AR
	ON
		AR.BlockID = RR.BlockID
	AND	AR.BedroomTypeID = RR.BedroomTypeID

	LEFT JOIN (SELECT BlockID, BedroomTypeID, SUM(Allocation) as Allocation FROM #Allocations GROUP BY BlockID, BedroomTypeID) BBA
	ON	BBA.BlockID = RR.BlockID
	AND	BBA.BedroomTypeID = RR.BedroomTypeID

	LEFT JOIN
		#TempOccupants T
	ON
		T.ResidentialRoomID = RL.ResidentialRoomID

	LEFT JOIN
		#RoomAllocations A
	ON	A.ResidentialRoomID = RR.ResidentialRoomID

	LEFT JOIN
		(
		SELECT
			RRO.ResidentialRoomID,
			COUNT(*) AS OOSCount
		FROM
			#ResidentialRoomOccupants RRO
		WHERE
			RRO.Date BETWEEN @FromDate AND @ToDate
		AND	RRO.PersonID = -99
		AND	RRO.EventModuleID = -99
		GROUP BY
			RRO.ResidentialRoomID
		) OOS ON	OOS.ResidentialRoomID = RR.ResidentialRoomID
	LEFT JOIN
		(
		SELECT
			RR1.HeaderResidentialRoomID,
			COUNT(*) AS FlatCount
		FROM
			#ResidentialRoomOccupants RRO
		JOIN	ResidentialRooms RR1 ON RR1.ResidentialRoomID = RRO.ResidentialRoomID
		WHERE	RRO.Date BETWEEN @FromDate AND @ToDate
		AND	RR1.HeaderResidentialRoomID > 0
		GROUP BY
			RR1.HeaderResidentialRoomID
		) FB ON	FB.HeaderResidentialRoomID = RR.ResidentialRoomID
	LEFT JOIN
		(
		SELECT
			RR1.ResidentialRoomID,
			COUNT(*) AS FlatCount
		FROM
			#ResidentialRoomOccupants RRO
		JOIN	ResidentialRooms RR1 ON RR1.ResidentialRoomID = RRO.ResidentialRoomID
		WHERE	RRO.Date BETWEEN @FromDate AND @ToDate
		AND	RR1.HeaderResidentialRoomID > 0
		GROUP BY
			RR1.ResidentialRoomID
		) FB1 ON FB1.ResidentialRoomID = RR.ResidentialRoomID
WHERE
	(ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) < RR.Capacity OR RL.ResidentialRoomID = IsNull(@CurrentResidentialRoomID,0))  --AND (ISNULL(A.ActualAllocation, 0) + ISNULL(T.Occupants, 0) = 0))
AND	(@MatchReservation = 0 OR BBA.Allocation >= 1 OR (SELECT SUM(AB.Allocation) FROM #Allocations AB WHERE AB.BlockID=RR.BlockID AND (AB.BedroomTypeID=RR.BedroomTypeID or AB.BedroomTypeID = RRH.BedroomTypeID)) >= 1)
AND	(COALESCE(OOS.OOSCount, 0) = 0)
AND	((IsNull(@NewFlats,0) = 1 OR (@DisplayHeaderAvailability = 1 AND (ISNULL(A.ActualAllocation, 0) <= 0) AND (ISNULL(RR.HeaderResidentialRoomID, 0) = -1)) OR (ISNULL(RR.HeaderResidentialRoomID, 0) > -1)))
AND	(@RoomName = '' OR RR.Name LIKE '%' + RTRIM(LTRIM(@RoomName)) + '%')
AND	NOT EXISTS(SELECT NULL FROM #FullAllocations FA WHERE FA.BlockID = RR.BlockID AND FA.BedroomTypeID = RR.BedroomTypeID)
AND	NOT EXISTS(SELECT NULL FROM #RestrictedRooms FA WHERE FA.ResidentialRoomID = RR.ResidentialRoomID)
AND	((IsNull(@NewFlats, 0) = 0) OR ((IsNull(FB.FlatCount,0) < RR.Capacity) /* and ((IsNull(FB1.FlatCount,0) < RRH.Capacity))*/))
AND	Not EXISTS (SELECT NULL FROM #AvailableRooms AL4 WHERE AL4.ResidentialRoomID = RR.ResidentialRoomID)
AND	(SELECT COUNT(*) FROM #ResidentialRoomOccupants RRO
                JOIN EventModuleDelegates EMD ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
                WHERE RRO.Date BETWEEN @FromDate AND @ToDate
				AND RRO.ResidentialRoomID = RR.ResidentialRoomID AND EMD.CheckedInDate > '19800101' AND EMD.CheckedOutDate < '19800101') = 0

------------------------------------------------------------------------------------------------
INSERT INTO #FinalResult
	(ResidentialRoomID, SubBlockID, BlockID, BedroomTypeID, RoomName, SubBlockName,
	 BlockName, BedroomType, LastLet, Smoking, Disabled,
	 VIP, MaidStatus, Info, TypicalCapacity, Capacity,
	 Occupants, ActualAllocation, HeaderResidentialRoomID, Inactive,
	 Restrictions, Occupancy, Date, DisplayOrder)
SELECT DISTINCT
	ResidentialRoomID,
	SubBlockID,
	BlockID,
	BedroomTypeID,
	RoomName,
	SubBlockName,
	BlockName,
	BedroomType,
	LastLet,
	Smoking,
	Disabled,
	VIP,
	MaidStatus,
	Info,
	TypicalCapacity,
	Capacity,
	Occupants,
	ActualAllocation,
	HeaderResidentialRoomID,
	IsNull(Inactive,0) as InActive,
	Restrictions,
	--Just extra case statements below in case incorrect occupancy descriptions get through
	CASE
	WHEN (Occupants > 0) AND (Occupancy = 'Unoccupied') THEN
		'Occupied - Previous occupant not checked out'
	WHEN (Occupants = 0) AND (Occupancy = 'Occupied - Previous occupant not checked out') THEN
		'Unoccupied'
	WHEN (Occupants = 0) AND (Occupancy = 'Occupied - Room capacity reached/exceeded') THEN
		'Unoccupied'
	ELSE
 	 	Occupancy
	END AS Occupancy,
	Date,
	DisplayOrder
FROM
	#AvailableRooms

CREATE INDEX IDX1 ON #FinalResult (ResidentialRoomID)

-------------------------------------------------------------------------------------------
--No real need for #FinalResult temp table, could have done this in above step
--Just breaks everything down neatly
-------------------------------------------------------------------------------------------
SELECT TOP 200	F.*,
	@VIP as VIPText,
	@Disabled as DisabledText,
	@Smoker as SmokingText,
	@LastLet as LastLetText
FROM
	#FinalResult F
JOIN	ResidentialRooms RR ON RR.ResidentialRoomID = F.ResidentialRoomID
JOIN	BedroomTypes BT ON BT.BedroomTypeID = F.BedroomTypeID
JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
LEFT JOIN Areas A ON A.AreaID = SB.AreaID
WHERE
	F.Inactive = 0
AND	BT.RoomInFlat = 0 
AND	BT.BookByRoom = 0
ORDER BY
	LastLet,
	CASE WHEN @OrderBy='Room Name' THEN IsNull(A.DisplayOrder, 0) ELSE 0 END,
	CASE WHEN @OrderBy='Room Name' THEN IsNull(A.Description, '') ELSE '' END,
	CASE WHEN @OrderBy='Room Name' THEN SB.SequenceNo ELSE 0 END,
	CASE WHEN @OrderBy='Room Name' THEN SB.Name ELSE '' END,
	CASE WHEN @OrderBy='Room Name' THEN F.DisplayOrder ELSE 0 END,
	CASE WHEN @OrderBy='Room Name' THEN RoomName ELSE '' END,
	CASE WHEN @OrderBy='Bedroom Type' THEN BedroomType ELSE '' END,
	CASE WHEN @OrderBy='Typical Capacity' THEN F.TypicalCapacity ELSE '' END,
	CASE WHEN @OrderBy='Actual Capacity' THEN F.Capacity ELSE '' END,
	CASE WHEN @OrderBy='Occupants' THEN Occupants ELSE '' END,
	BlockName,
	SubBlockName

DROP TABLE #Allocations
DROP TABLE #TempOccupants
DROP TABLE #Rooms
DROP TABLE #AvailableRooms
DROP TABLE #ResidentialRoomOccupants
DROP TABLE #EventModuleBlockBedroomAllocation
DROP TABLE #FinalResult
DROP TABLE #RoomAllocations
DROP TABLE #FullAllocations
DROP TABLE #RestrictedRooms
DROP TABLE #Dates
DROP TABLE #BedroomTypes
DROP TABLE #Sites
DROP TABLE #Areas
DROP TABLE #Blocks
DROP TABLE #Subblocks


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetBlockFloors]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetBlockFloors]
	@SiteID int=0,
	@AreaID int=0,
	@BlockID int= 0,
	@SubBlockID int= 0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	DISTINCT IsNull(RR.FloorNumber, 0) as FloorNumber
FROM	ResidentialRooms RR
JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
WHERE	(@SiteID = 0 OR @SiteID = SB.SiteID)
AND		(@AreaID = 0 OR @AreaID = SB.AreaID)
aND		(@BlockID = 0 OR @BlockID = SB.BlockID)
AND		@SubBlockID = 0
UNION
SELECT	DISTINCT IsNull(RR.FloorNumber, 0) as FloorNumber
FROM	ResidentialRooms RR
JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
WHERE	(@SiteID = 0 OR @SiteID = SB.SiteID)
AND		(@AreaID = 0 OR @AreaID = SB.AreaID)
AND		(@BlockID = 0 OR @BlockID = SB.BlockID)
AND		RR.SubBlockID = @SubBlockID
AND		@SubBlockID > 0
ORDER BY IsNull(RR.FloorNumber, 0)


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetCheckInGuestDetailsWithRates]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetCheckInGuestDetailsWithRates]
	@EventModuleDelegateID int,
	@Lang int = 1,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @SurnameFirst int
DECLARE @WalkInEventTypeID int
SET @SurnameFirst = ISNULL((SELECT TOP 1 SurnameFirst FROM Control), 0)
SET @WalkInEventTypeID = ISNULL((SELECT TOP 1 WalkInEventTypeID FROM Control), 0)
DECLARE	@NewFlats int
SELECT	@NewFlats = IsNull(Value,0) FROM CustomSettings WHERE CustomSetting = 'Residential.2009.Flat.Reservation.System'
SET @NewFlats = IsNull(@NewFlats,0)

CREATE TABLE #CheckInList  (Name nvarchar(255), ArrivalDate DateTime, EventID int, EventModuleID int, Description varchar(255), CheckedInDate datetime,  CheckedOutDate datetime, PersonID int,
			PersonSequenceID int, DepartureDate Datetime, ReceivedMedical bit, Type Char(1), NumOccupants int, ResidentialRoomID int, SingleOccupancy bit, ExtNo varchar(50),
			NotesExist int, Info varchar(255), DelegateNotes varchar(max), CheckInNotes varchar(max), HouseKeepingNotes varchar(max), Resident bit,
			RoomName varchar(255), RoomExternalID varchar(255), RoomType varchar(255), BookByRoom bit, BlockID int, BlockName varchar(255), SubBlockName varchar(255),
			Surname nvarchar(255), Forename nvarchar(255), Smoker bit, [Disabled] bit, VIP bit, DelegateEventModuleID int, HouseKeepingReqNoteID int, EventSiteID int,
			BedroomSiteID int, CreatorSiteID int, MaidStatus varchar(20), PreviousPersonID int, AreaID int, AreaDescription varchar(255), WalkIn bit, PaymentType varchar(255),
			CreateCharges bit, PMSEnabled bit, AllowBarPosting bit, CarRegNumber varchar(50), RouteChargesToPersonID int, RouteChargesToPersonSequenceID int, PhoneAccessCode varchar(20),
			DelegateGroupID int, DelegateGroup varchar(255), GroupColour varchar(50), HasNotes bit, HousekeepingNotesFound bit, DelegateNotesFound bit,
			CheckInNotesFound bit, AllocationType varchar(1), HeaderRoomName varchar(255), SortOrder varchar(255), HeaderResidentialRoomID int, RTFNotes varchar(max),
			GroupShortName varchar(4), EventModuleDelegateID int, DisplayOrder int, GuestType varchar(1), DelegateCount Integer, ID int identity(1,1), UDFs varchar(max)
			)
			 
CREATE TABLE #EventModuleDelegates (
	EventModuleID int NOT NULL,
	PersonID int NOT NULL,
	PersonSequenceID int NOT NULL,
	DelegateGroupID int NULL,
	ArrivalDate datetime NULL,
	DepartureDate datetime NULL,
	CheckedInDate datetime NULL,
	CheckedOutDate datetime NULL,
	ReceivedMedical bit NULL,
	Type char(1) NULL,
	CreationUserID int NOT NULL,
	Notes text NULL,
	SingleOccupancy bit NULL,
	Resident bit NULL,
	CheckInNotes text NULL,
	ResidentialRoomID int NULL,
	BlockID int NULL,
	BedroomTypeID int NULL,
	NumOccupants int NOT NULL,
	PaymentTypeID int NULL,
	AllowBarPosting bit NOT NULL,
	CarRegNumber varchar(20) NULL,
	RouteChargesToPersonID int NULL,
	HousekeepingNotes text NULL,
	PhoneAccessCode varchar(2) NULL,
	RouteChargesToPersonSequenceID int NULL,
	EventModuleDelegateID int,
	ArrivalDateOnly DateTime,
	DepartureDateOnly DateTime,
	GroupNumber int,
	ListedAdults int,
	GuestType varchar(1)
	)

INSERT INTO #EventModuleDelegates
SELECT
	EventModuleID,
	PersonID,
	PersonSequenceID,
	DelegateGroupID,
	ArrivalDate,
	DepartureDate,
	CheckedInDate,
	CheckedOutDate,
	ReceivedMedical,
	Type,
	CreationUserID,
	Notes,
	SingleOccupancy,
	Resident,
	CheckInNotes,
	ResidentialRoomID,
	BlockID,
	BedroomTypeID,
	NumOccupants,
	PaymentTypeID,
	AllowBarPosting,
	CarRegNumber,
	RouteChargesToPersonID,
	HousekeepingNotes,
	PhoneAccessCode,
	RouteChargesToPersonSequenceID,
	EventModuleDelegateID,
	CAST(FLOOR(CAST(EMD.ArrivalDate AS float)) AS datetime) as ArrivalDateOnly,
	CAST(FLOOR(CAST(EMD.DepartureDate AS float)) AS datetime) as DepartureDateOnly,
	0,
	0,
	CASE WHEN EXISTS (SELECT * FROM ST2AcademicYears AY WHERE AY.EventModuleID = EMD.EventModuleID) THEN 'S' ELSE 'G' END
FROM
	EventModuleDelegates EMD
WHERE
	(@EventModuleDelegateID = EMD.EventModuleDelegateID)

CREATE INDEX IX1 ON #EventModuleDelegates(EventModuleID, PersonID, PersonSequenceID, ArrivalDateOnly)
CREATE INDEX IX1B ON #EventModuleDelegates(EventModuleID)

UPDATE EMD
SET	GroupNumber = EMBBAD.GroupNumber, ListedAdults = 0
FROM	#EventModuleDelegates EMD
JOIN	EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBAD.EventModuleID = EMD.EventModuleID AND EMBBAD.PersonID = EMD.PersonID AND EMD.PersonSequenceID =  EMBBAD.PersonSequenceID

UPDATE	#EventModuleDelegates
SET	ListedAdults = (SELECT	COUNT(DISTINCT(EMBBAD.PersonID))
			FROM	EventModuleBlockBedroomAllocationDelegates EMBBAD
			WHERE	EMBBAD.EventModuleID = #EventModuleDelegates.EventModuleID
			AND	#EventModuleDelegates.GroupNumber = EMBBAD.GroupNumber
			AND	EMBBAD.PersonID <> -999)

CREATE TABLE #ResidentialRoomOccupants (EventModuleID int, PersonID int, PersonSequencEID int, ResidentialRoomID int, Date DateTime)

INSERT INTO #ResidentialRoomOccupants
SELECT	RRO.EventModuleID, RRO.PersonID, RRO.PersonSequenceID, RRO.ResidentialRoomID, RRO.Date
FROM	ResidentialRoomOccupants RRO
JOIN	#EventModuleDelegates D ON D.EventModuleID = RRO.EventModuleID AND D.PersonID = RRO.PersonID AND D.PersonSequenceID = RRO.PersonSequenceID AND RRO.Date = D.ArrivalDateOnly

CREATE INDEX IX6 ON #ResidentialRoomOccupants(EventModuleID, PersonID, PersonSequenceID, Date)

CREATE TABLE #EMBBA (EventModuleID int, ResidentialRoomID int, Date DateTime, AllocationType varchar(1))

CREATE INDEX IDX_EMBBA1 ON #EMBBA (EventModuleID, ResidentialRoomID, Date)

INSERT INTO #CheckInList
SELECT	DISTINCT
	CASE WHEN @SurnameFirst = 1 THEN
		CASE WHEN P.Surname = '' THEN P.Forename WHEN P.Forename = '' THEN P.Surname ELSE P.Surname + ', ' + P.Forename END + CASE WHEN P.Title = '' THEN '' ELSE ' (' + P.Title + ')' END
	ELSE
		LTRIM(P.Title + ' ') + LTRIM(P.Forename + ' ') + P.Surname END as Name,
	EMD.ArrivalDate,
	EM.EventID,
	EMD.EventModuleID,
	RTrim(LTrim(EM.Description)) as Description,
	CAST(0 AS DateTime) AS CheckedInDate,
	CAST(0 AS DateTime) AS CheckedOutDate,
	EMD.PersonID,
	EMD.PersonSequenceID,
	EMD.DepartureDate,
	EMD.ReceivedMedical,
	EMD.Type,
	EMD.NumOccupants,
	RRO.ResidentialRoomID,
	EMD.SingleOccupancy,
	R.ExtNo,
	0 as NotesExist,
	R.Info,
	ISNULL(RTRIM(CAST(EMD.Notes AS varchar(max))), '') AS DelegateNotes,
	ISNULL(RTRIM(CAST(EMD.CheckInNotes AS varchar(max))), '') AS CheckInNotes,
	ISNULL(RTRIM(CAST(EMD.HousekeepingNotes AS varchar(max))), '') AS HousekeepingNotes,
	EMD.Resident,
	CASE
	WHEN R.ResidentialRoomID IS NULL THEN
		PBT.Description
	WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		CAST(RRH.Name as varchar(255))
	ELSE
		CAST(CASE WHEN BT.BookByRoom = 1 THEN LEFT(RTRIM(BB.Name), PATINDEX('% %', BB.Name)) + ': ' ELSE '' END + RTRIM(R.Name) AS varchar(255))
	END AS RoomName,
	R.ExternalID AS RoomExternalID,
	CASE WHEN EMD.ListedAdults = 1 AND @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN
		BTH.Description
	ELSE
		BT.Description
	END as RoomType,
	BT.BookByRoom,
	R.BlockID,
	SB.Name as BlockName,
	BB.Name as SubBlockName,
	P.Surname,
	P.Forename,
	P.Smoker,
	P.Disabled,
	P.VIP,
	EMD.EventModuleID as DelegateEventModuleID,
	ISNULL(P.HouseKeepingReqNoteID, 0) AS HouseKeepingReqNoteID,
	EM.SiteID as EventSiteID,
	SB.SiteID as BedroomSiteID,
	U.SiteID AS CreatorSiteID,
	CASE WHEN IsNull(R.MaidStatus, 'X') = '' THEN 'I' ELSE R.MaidStatus END as MaidStatus,
	0 as PreviousPersonID,
	SB.AreaID,
	A.Description AS AreaDescription,
	CASE E.EventTypeID WHEN @WalkInEventTypeID THEN CAST(1 AS Bit) ELSE CAST(0 AS Bit) END AS WalkIn,
	PT.Description as PaymentType,
	PT.CreateCharges,
	SIT.PMSEnabled,
	EMD.AllowBarPosting,
	EMD.CarRegNumber,
	EMD.RouteChargesToPersonID,
	IsNull(EMD.RouteChargesToPersonSequenceID, 0) as RouteChargesToPersonSequenceID,--removed
	EMD.PhoneAccessCode,
	EMD.DelegateGroupID,
	DG.Description AS DelegateGroup,
	DG.GroupColour,
	0 as HasNotes,
	0 as HousekeepingNotesFound,
	0 as DelegateNotesFound,
	0 as CheckInNotesFound,
	CASE WHEN @NewFlats = 1 THEN EMBBA.AllocationType ELSE '' END as AllocationType,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN IsNull(RRH.Name, '') ELSE '' END as HeaderRoomName,
	'' as SortOrder,
	CASE WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'F' THEN RRH.ResidentialRoomID
			WHEN @NewFlats = 1 AND EMBBA.AllocationType = 'R' THEN R.ResidentialRoomID ELSE 0 END AS HeaderResidentialRoomID,
	'' as RTFNotes,
	RTRIM(CASE WHEN RTrim(IsNull(DG.ShortName, '')) = '' THEN LEFT(LTRIM(DG.Description),1) ELSE DG.ShortName END) as GroupShortName,
	EventModuleDelegateID,
	R.DisplayOrder,
	EMD.GuestType,
	(SELECT COUNT(*) FROM #EventModuleDelegates EMD2 WHERE EMD2.EventModuleID = EMD.EventModuleID) as DelegateCount,
	LTrim(REPLACE(REPLACE(IsNull(STUFF((SELECT RTrim(CV2.FieldName) + '=' + D.FieldValue + CHAR(13)
			  FROM CustomFields2Values D
			  JOIN CustomFields2 CV2 ON CV2.CustomFieldID = D.CustomFieldID
			  WHERE D.EntityType = 'EventModuleDelegates' AND D.EntityID = EMD.EventModuleDelegateID 
			  ORDER BY CV2.FieldName DESC
			  FOR XML PATH('')), 1, 0, ''), ''), '&amp;', '&'), '&#x0D;', CHAR(13))) as UDFs
FROM
	#EventModuleDelegates EMD
	JOIN EventModules EM ON EM.EventModuleID = EMD.EventModuleID
	JOIN Events E ON E.EventID = EM.EventID
	JOIN Users U ON U.UserID = EMD.CreationUserID
	JOIN People P ON P.PersonID = EMD.PersonID
	JOIN Status ST ON ST.StatusID = EM.StatusID
	LEFT JOIN SiteBlocks PB ON PB.BlockID = EMD.BlockID
	LEFT JOIN BedroomTypes PBT ON PBT.BedroomTypeID = EMD.BedroomTypeID
	LEFT JOIN #ResidentialRoomOccupants RRO
		ON    	RRO.EventModuleID 	= EMD.EventModuleID
		AND	RRO.PersonID		= EMD.PersonID
		AND	RRO.PersonSequenceID	= EMD.PersonSequenceID
		AND	RRO.Date		= EMD.ArrivalDateOnly
	LEFT JOIN ResidentialRooms R ON R.ResidentialRoomID = RRO.ResidentialRoomID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
	LEFT JOIN SiteBlocks BB ON BB.BlockID = R.SubBlockID
	LEFT JOIN SiteBlocks SB ON SB.BlockID = R.BlockID
	LEFT JOIN Sites SIT ON SIT.SiteID = SB.SiteID
	LEFT JOIN Areas A ON A.AreaID = SB.AreaID
	LEFT JOIN DelegateGroups DG ON DG.DelegateGroupID = EMD.DelegateGroupID
	LEFT JOIN EventModuleBlockBedroomAllocation /* #EMBBA*/ EMBBA ON EMBBA.EventModuleID = EMD.EventModuleID AND EMBBA.ResidentialRoomID = R.ResidentialRoomID AND EMBBA.Date = RRO.Date
	LEFT JOIN ResidentialRooms RRH ON RRH.ResidentialRoomID = R.HeaderResidentialRoomID
	LEFT JOIN BedroomTypes BTH ON BTH.BedroomTypeID = RRH.BedroomTypeID
	LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
WHERE
	(@EventModuleDelegateID = EMD.EventModuleDelegateID)

UPDATE D SET HasNotes = CAST(CASE WHEN IsNull(D.HousekeepingNotes,'') + IsNull(D.DelegateNotes,'') + IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS Bit) FROM #CheckInList D
UPDATE D SET NotesExist = CAST(CASE WHEN D.HasNotes = 1 THEN 1 ELSE 0 END AS Int) FROM #CheckInList D
UPDATE D SET HousekeepingNotesFound = CAST(CASE WHEN IsNull(D.HouseKeepingNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET DelegateNotesFound = CAST(CASE WHEN IsNull(D.DelegateNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET CheckInNotesFound = CAST(CASE WHEN IsNull(D.CheckInNotes,'') <> '' THEN 1 ELSE 0 END AS bit) FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.DelegateNotes, '')) <> '' THEN ISNULL('<b>Reg Card Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.DelegateNotes AS varchar(max)), ''), '') ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.CheckInNotes, '')) <> '' THEN CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Check In Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.CheckInNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RTFNotes = RTFNotes +
				CASE WHEN RTRIM(ISNULL(D.HouseKeepingNotes, '')) <> '' THEN	CASE WHEN RTFNotes <> '' THEN  CHAR(13)+CHAR(10) ELSE '' END + ISNULL('<b>Housekeeping Notes:</b>' +CHAR(13)+CHAR(10)+  NULLIF(CAST(D.HouseKeepingNotes AS varchar(max)), ''), '')  ELSE '' END
			 FROM #CheckInList D
UPDATE D SET RtfNotes = REPLACE(RTFNotes, CHAR(13)+CHAR(10), '<br/>')  FROM #CheckInList D WHERE D.RTFNotes <> ''

DECLARE @Yesterday datetime

SET @Yesterday = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS money)) AS datetime)-1

UPDATE	CL
SET	PreviousPersonID =
		ISNULL((
			SELECT TOP 1
				RRO.PersonID
			FROM	ResidentialRoomOccupants RRO WITH (NOLOCK)
			JOIN	EventModuleDelegates EMD WITH (NOLOCK) ON EMD.EventModuleID = RRO.EventModuleID AND EMD.PersonID = RRO.PersonID AND EMD.PersonSequenceID = RRO.PersonSequenceID
			WHERE	(EMD.CheckedInDate > 2)
			AND	(EMD.CheckedOutDate <= 2)
			AND	CAST(FLOOR(CAST(EMD.ArrivalDate AS money)) AS datetime) <= @Yesterday
			AND	CAST(FLOOR(CAST(EMD.DepartureDate AS money)) AS datetime) >= @Yesterday
			AND     CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = @Yesterday
			AND	EMD.PersonID > 0
			AND	NOT EXISTS (SELECT NULL FROM ST2AcademicYears AY WHERE AY.EventModuleID = RRO.EventModuleID)
			AND	RRO.ResidentialRoomID = CL.ResidentialRoomID
			AND	CAST(FLOOR(CAST(RRO.Date AS money)) AS datetime) = (SELECT MAX(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = RRO.EventModuleID AND RRO1.PersonID = RRO.PersonID AND RRO1.PersonSequenceID = RRO.PersonSequenceID)
			ORDER BY
				RRO.Date DESC
		), 0)
FROM	#CheckInList CL
	
CREATE INDEX IDXAAC ON #CheckInList (EventModuleID, PersonID, PersonSequenceID)

DECLARE	@EventModuleID int,
	@PersonID int,
	@PersonSequenceID int

SELECT	TOP 1 @EventModuleID = EventModuleID, @PersonID = PersonID, @PersonSequenceID = PersonSequenceID from #CheckInList

DECLARE @YearAgo DateTime
SET @YearAgo = DateAdd(yy, -1, CAST(Floor(Cast(getdate() as money)) as Datetime))

SELECT DISTINCT
	EMD.EventModuleDelegateID,
	VP.PersonID,
	VP.PersonName,
	RR.ResidentialRoomID,
	RR.Name as RoomName,
	CAST(0 as int) as LastStayedEventModuleID,
	CAST(NULL as datetime) as LastStayedInDate,
	CAST(NULL as datetime) as LastStayedOutDate,
	CAST(0 as int) as LastStayedEventID,
	CAST('' as varchar(255)) as LastStayedEventTitle,
	CAST('' as int) as LastStayedInResidentialRoomID,
	CAST('' as varchar(255)) as LastStayedInRoom,
	CAST('' as varchar(255)) as LastStayedInBlock,
	CAST('' as varchar(255)) as LastStayedInBedroomType,
	CAST(0 as int) as NumberOfStays,
	CAST(0 as int) as SiteID,
	CAST('' as varchar(50)) as SiteName
INTO	#Results
FROM	EventModuleDelegates EMD
JOIN	ViewPeople VP ON VP.PersonID = EMD.PersonID
LEFT JOIN ResidentialRoomOccupants RRO ON RRO.EventModuleID = EMD.EventModuleID AND RRO.Date = CAST(FLOOR(CAST(EMD.ArrivalDate as money)) as Datetime) AND RRO.PersonID = EMD.PersonID AND EMD.PersonSequenceID = EMD.PersonSequenceID
LEFT JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
WHERE	EMD.EventModuleDelegateID = @EventModuleDelegateID

UPDATE R
SET	LastStayedEventModuleID = (
		SELECT TOP 1 EMD.EventModuleID
		FROM	EventModuleDelegates EMD
		JOIN	ResidentialRoomOccupants RRO ON RRO.EventModuleID = EMD.EventModuleID AND RRO.PersonID = EMD.PersonID AND RRO.PersonSequenceID = EMD.PersonSequenceID
		JOIN	ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
		JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
		WHERE	EMD.CheckedOutDate > 29000
		AND	EMD.PersonID = R.PersonID
		AND	(CAST(FLOOR(CAST(EMD.DepartureDate as money)) as Datetime) - 1) = RRO.Date
		AND	EMD.EventModuleID <> @EventModuleID
		ORDER BY
			EMD.CheckedOutDate DESC)
FROM	#Results R

UPDATE R
SET	LastStayedInDate = (SELECT TOP 1 EMD.ArrivalDate FROM EventModuleDelegates EMD WHERE EMD.EventModuleID = R.LastStayedEventModuleID AND EMD.PersonID = R.PersonID ORDER BY EMD.CheckedOutDate DESC)
FROM	#Results R

UPDATE R
SET	LastStayedOutDate = (SELECT TOP 1 EMd.DepartureDate FROM EventModuleDelegates EMD WHERE EMD.EventModuleID = R.LastStayedEventModuleID AND EMD.PersonID = R.PersonID ORDER BY EMD.CheckedOutDate DESC)
FROM	#Results R

UPDATE R
SET	NumberOfStays = (
	SELECT COUNT(*) FROM EventModuleDelegates EMD
	WHERE	EMD.PersonID = R.PersonID 
	AND	((CAST(FLOOR(CAST(EMD.CheckedInDate as money)) as DateTime) >= @YearAgo aND (EMD.CheckedOutDate > 29000))
		OR (EMD.CheckedOutDate > @YearAgo AND EMD.CheckedInDate < @YearAgo))
	AND	EMD.EventModuleID <> @EventModuleID)
FROM	#Results R

UPDATE R
SET	LastStayedEventID = EM.EventID, LastStayedEventTitle = EM.Description
FROM	#Results R
JOIN	EventModules EM ON EM.EventModuleID = R.LastStayedEventModuleID

UPDATE R
SET	LastStayedInResidentialRoomID = RRO.ResidentialRoomID
FROM	#Results R
JOIN	ResidentialRoomOccupants RRO ON RRO.EventModuleID = R.LastStayedEventModuleID AND RRO.Date = (CAST(FLOOR(CAST(R.LastStayedOutDate as money)) as Datetime) - 1) AND RRO.PersonID =R.PersonID


UPDATE R
SET	LastStayedInRoom = RR.Name, LastStayedInBlock = SB.Name, LastStayedInBedroomType = BT.Description, SiteName = RTRim(S.Name), SiteID = S.SiteID
FROM	#Results R
JOIN	ResidentialRooms RR ON RR.ResidentialRoomID = R.LastStayedInResidentialRoomID
JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
JOIN	BedroomTypes BT ON BT.BedroomTypeID = RR.BedroomTypeID
JOIN	Sites S ON S.SiteID = SB.SiteID

DECLARE @EventID int,
	@Format int

SET @Lang = 2

SET @Format = 103
IF @Lang = 2 SET @Format = 101

SELECT @EventID = EventID FROM EventModules WHERE EventModuleID = @EventModuleID

SET @PersonSequenceID = ISNULL(@PersonSequenceID, 0)

SELECT
	PersonID,
	PersonSequenceID,
	MIN(CAST(FLOOR(CAST(ArrivalDate AS Float)) AS DateTime)) AS FirstDate,
	MAX(CAST(FLOOR(CAST(DepartureDate AS Float)) - 1 AS DateTime)) AS LastDate
INTO
	#DelegateDates
FROM
	EventModuleDelegates
WHERE
	EventModuleID = @EventModuleID
AND (@PersonID = 0 OR PersonID = @PersonID AND ISNULL(PersonSequenceID, 0) = @PersonSequenceID)
GROUP BY
	PersonID,
	PersonSequenceID

SELECT DISTINCT
	D.EventChargePeriodID,
	D.ChargeTypeID,
	D.MatrixID,
	D.Rate,
	CAST(1 AS bit) AS ContainsNegativeElement
INTO
	#RatesWithNegativeElements
FROM
	(
	SELECT
		EventChargePeriodID,
		ChargeTypeID,
		MatrixID,
		Rate
	FROM
		fnEventChargeTypeRateDetails(@EventID)
	WHERE
		DetailRate < 0
	) D

DECLARE @FirstNight datetime, @LastNight datetime

SELECT
	ECP.EventChargePeriodID,
	ECP.FromDate,
	ECP.ToDate,
	DD.PersonID,
	DD.PersonSequenceID,
	D.Date,
	ECTR.ChargeTypeID,
	ECTR.MatrixID,
	ECTR.Rate,
	CT.AmountType
INTO
	#DailyRates
FROM
	(
	SELECT
		EventChargePeriodID,
		CASE WHEN FromDate <= 0 THEN NULL ELSE FromDate END AS FromDate,
		CASE WHEN ToDate <= 0 THEN NULL ELSE ToDate END AS ToDate
	FROM
		EventChargePeriods ECP
	WHERE
		ECP.EventID = @EventID
	) ECP
	JOIN EventChargeTypeRates ECTR ON ECTR.EventChargePeriodID = ECP.EventChargePeriodID
	JOIN ChargeTypes CT ON CT.ChargeTypeID = ECTR.ChargeTypeID
	JOIN EventChargePeriodDelegates ECPD ON ECPD.EventChargePeriodID = ECP.EventChargePeriodID
	JOIN #DelegateDates DD ON DD.PersonID = ECPD.PersonID AND DD.PersonSequenceID = ISNULL(ECPD.PersonSequenceID, 0)
	JOIN Dates D ON D.Date BETWEEN ISNULL(ECP.FromDate, DD.FirstDate) AND ISNULL(ECP.ToDate, DD.LastDate)

DECLARE curNetRates CURSOR LOCAL FOR
SELECT
	EventChargePeriodID, ChargeTypeID, MatrixID, Rate, Date
FROM
	#DailyRates
WHERE
	AmountType = 'N'
	
DECLARE
	@EventChargePeriodID int,
	@ChargeTypeID int,
	@MatrixID int,
	@Rate money,
	@Date datetime

OPEN curNetRates
FETCH curNetRates INTO @EventChargePeriodID, @ChargeTypeID, @MatrixID, @Rate, @Date
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC CalcHeaderVAT
		@ChargeTypeID,
		@MatrixID,
		1,
		@Rate,
		@Rate,
		@Rate OUTPUT,
		@Rate,
		@Date=@Date

	UPDATE
		#DailyRates
	SET
		Rate = @Rate
	WHERE
		EventChargePeriodID = @EventChargePeriodID
	AND ChargeTypeID = @ChargeTypeID
	AND MatrixID = @MatrixID
	AND Date = @Date
	
	FETCH curNetRates INTO @EventChargePeriodID, @ChargeTypeID, @MatrixID, @Rate, @Date
END
CLOSE curNetRates
DEALLOCATE curNetRates

SELECT
	EventChargePeriodID,
	MIN(Date) AS FromDate,
	MAX(Date) AS ToDate,
	FromDate AS ActualFromDate,
	ToDate AS ActualToDate,
	ChargeTypeID,
	MatrixID,
	Rate
INTO
	#Rates
FROM
	#DailyRates
GROUP BY
	EventChargePeriodID,
	FromDate,
	ToDate,
	ChargeTypeID,
	MatrixID,
	Rate

SELECT
	R.EventChargePeriodID,
	CONVERT(VarChar, R.FromDate, @Format) AS FromDate,
	CONVERT(VarChar, R.ToDate, @Format) AS ToDate,
	R.FromDate AS ActualFromDate,
	R.ToDate AS ActualToDate,

	CAST(CAST(DATENAME(dw, FromDate) as varchar(3)) + ' ' + CAST(DAY(FromDate) AS Varchar(2)) + ' to ' + CAST(DATENAME(dw, ToDate) as varchar(3)) + ' ' + CAST(DAY(ToDate) AS Varchar(2)) AS VarChar(255)) AS Dates,
	CT.Description AS ChargeType,
	CTRM.ExternalDescription AS SpecificPrice,
	R.Rate,
	CAST(R.ToDate - R.FromDate + 1 AS int) * R.Rate AS Total,
	ISNULL(RWNE.ContainsNegativeElement, 0) AS ContainsNegativeElement,
	CAST(CASE WHEN CTRM.BlockID > 0 THEN 1 ELSE 0 END AS bit) AS Accommodation,
	CAST(CASE WHEN F.FacilityID > 0 THEN 1 ELSE 0 END AS bit) AS Catering,

	CAST(
	CASE WHEN
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL JOIN EventModuleDelegateAccountCharges EMDAC ON EMDAC.EventModuleID = @EventModuleID AND EMDAC.AccountChargeID = EMDSL.AccountChargeID AND EventChargePeriodID = R.EventChargePeriodID)
	OR
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL JOIN EventModuleAccountCharges EMDAC ON EMDAC.EventModuleID = @EventModuleID AND EMDAC.AccountChargeID = EMDSL.AccountChargeID AND EventChargePeriodID = R.EventChargePeriodID)
	OR
		EXISTS (SELECT NULL FROM EventModuleDelegateStdChargeLog EMDSL WHERE EMDSL.PersonID = @PersonID AND EMDSL.EventModuleID = @EventModuleID AND EMDSL.EventChargePeriodID IS NULL)
	THEN 1 ELSE 0 END AS bit) AS Applied,

	ISNULL(NULLIF(CTRM.ExternalDescription, ''), CT.Description) + ISNULL(' - ' + NULLIF(F.Description, ''), '') AS ExternalDescription,
	ISNULL(F.FacilityID, 0) AS FacilityID,
	R.ChargeTypeID,
	ISNULL(ACD.EventModuleDayCateringAreaID,0) as EventModuleDayCateringAreaID,

	DC.DelegateCount,
	DD.PersonID,
	DD.PersonSequenceID,
	(SELECT COUNT(*) FROM #Rates r1 WHERE r1.EventChargePeriodID = R.EventChargePeriodID) as RateCount
INTO	#RatesResults
FROM
	#Rates R
	JOIN ChargeTypes CT ON CT.ChargeTypeID = R.ChargeTypeID
	LEFT JOIN EventChargePeriodDelegates ECPD ON ECPD.EventChargePeriodID = R.EventChargePeriodID
	LEFT JOIN #DelegateDates DD ON DD.PersonID = ECPD.PersonID AND DD.PersonSequenceID = ECPD.PersonSequenceID
	LEFT JOIN ChargeTypeRateMatrix CTRM ON CTRM.MatrixID = R.MatrixID
	LEFT JOIN #RatesWithNegativeElements RWNE ON RWNE.EventChargePeriodID = R.EventChargePeriodID AND RWNE.ChargeTypeID = R.ChargeTypeID AND ISNULL(RWNE.MatrixID, 0) = ISNULL(R.MatrixID, 0) AND RWNE.Rate = R.Rate
	LEFT JOIN AdditionalCateringData ACD ON ACD.EventChargePeriodID = R.EventChargePeriodID
	LEFT JOIN Facilities F ON F.FacilityID = ACD.FacilityID
	LEFT JOIN (
	SELECT
		EventChargePeriodID,
		COUNT(*) AS DelegateCount
	FROM
		EventChargePeriodDelegates
	GROUP BY
		EventChargePeriodID
	) DC ON DC.EventChargePeriodID = R.EventChargePeriodID
ORDER BY
	R.FromDate,
	R.ToDate

--SELECT * FROM #RatesResults
INSERT INTO #RatesResults
SELECT
	0,
	CONVERT(VarChar, CT.CreationDate , @Format) AS FromDate,
	CONVERT(VarChar, CT.CreationDate, @Format) AS ToDate,
	CT.CreationDate AS ActualFromDate,
	CT.CreationDate AS ActualToDate,
	CAST(CAST(DATENAME(dw, CT.CreationDate) as varchar(3)) + ' ' + CAST(DAY(CT.CreationDate) AS Varchar(2)) AS VarChar(255)) AS Dates,
	'Payment',
	'' AS SpecificPrice,
	SUM(CT.Amount * -1),
	SUM(CT.Amount * -1) AS Total,
	0 AS ContainsNegativeElement,
	0 AS Accommodation,
	0 AS Catering,
	1 AS Applied,
	PM.Description AS ExternalDescription,
	0 AS FacilityID,
	0,
	0 as EventModuleDayCateringAreaID,
	0,
	D.PersonID,
	D.PersonSequenceID,
	0 as RateCount
FROM
	#EventModuleDelegates D
	JOIN CashTransactions CT ON CT.EventModuleID = D.EventModuleID AND CT.PersonID = D.PersonID AND ISNULL(CT.PersonSequenceID,0) = D.PersonSequenceID
	JOIN InvoiceHeaders IH ON IH.InvoiceID = CT.InvoiceID
	JOIN PaymentMethods PM ON PM.PaymentMethodID = CT.PaymentMethodID
WHERE
	IH.InvoiceType = 'INVOICE'
GROUP BY
	D.EventModuleID,
	D.PersonID,
	D.PersonSequenceID,
	CT.CreationDate,
	PM.Description

SELECT
	Cl.*,
	ISNULL(P.AddressID, 0) AS AddressID,
	ISNULL(P.CompanyAddressID, 0) AS COmpanyAddressID,
	ISNULL(P.CompanyID, 0) AS CompanyID,
	IsNull(PF.PaymentMethodID, 0) as PaymentMethodID,
        PF.CardNumber,
        PF.AuthCode,
        PF.AuthAmount,
        ISNULL(PM.Description, 'None Selected') AS PaymentMethod,
        CAST(CASE WHEN PF.AuthCode <> '' THEN 1 ELSE 0 END AS Bit) AS Authorised,
	EMD.PaymentTypeID,
        ISNULL(PT.Description, 'None Specified') AS Description,
	R.LastStayedEventModuleID,
	R.LastStayedInDate,
	R.LastStayedOutDate,
	R.LastStayedEventID,
	R.LastStayedEventTitle,
	R.LastStayedInResidentialRoomID,
	R.LastStayedInRoom,
	R.LastStayedInBlock,
	R.LastStayedInBedroomType,
	R.NumberOfStays,
	R.SiteID,
	R.SiteName,
	RR.FromDate,
	RR.ToDate,
	RR.ActualFromDate,
	RR.ActualToDate,
	RR.Dates,
	RR.ChargeType,
	RR.SpecificPrice,
	RR.Rate,
	RR.Total,
	RR.ContainsNegativeElement,
	RR.Accommodation,
	RR.Catering,
	RR.Applied,
	RR.ExternalDescription,
	RR.FacilityID,
	RR.ChargeTypeID,
	RR.EventModuleDayCateringAreaID,
	RR.DelegateCount as DelgateCountRates,
	RR.RateCount,
	(SELECT SUM(RR.Total) FROM #RatesResults RR WHERE PersonID = CL.PersonID) as FullTotal,
	P.Email,
	P.Mobile,
	P.Telephone,
	E.EventTypeID
FROM
	#CheckInList CL
JOIN	EventModuleDelegates EMD ON EMD.EventModuleDelegateID = CL.EventModuleDelegateID
JOIN	People P ON P.PersonID = CL.PersonID
JOIN	#Results R ON R.EventModuleDelegateID = CL.EventModuleDelegateID
JOIN	EventModules EM ON EM.EventModuleID = CL.EventModuleID 
JOIN	Events E ON E.EventID = EM.EventID
LEFT JOIN PersonFinancial PF ON PF.PersonID = P.PersonID
LEFT JOIN PaymentMethods PM ON PM.PaymentMethodID = PF.PaymentMethodID
LEFT JOIN PaymentTypes PT ON PT.PaymentTypeID = EMD.PaymentTypeID
LEFT JOIN #RatesResults RR ON RR.PersonID = CL.PersonID
ORDER BY 
	RR.FromDate 

DROP TABLE #ResidentialRoomOccupants
DROP TABLE #EMBBA
DROP TABLE #EventModuleDelegates
DROP TABLE #CheckInList
DROP TABLE #Results
DROP TABLE #Rates
DROP TABLE #DailyRates
DROP TABLE #RatesWithNegativeElements
DROP TABLE #DelegateDates
DROP TABLE #RatesResults


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetCleaningRotas]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetCleaningRotas]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	DISTINCT CR.CleaningRotaID,
		CR.RotaName,
		SB.SiteID,
		SB.AreaID,
		SB.BlockID,
		IsNull(R.SubBlockID, -1) as SubBlockID
FROM	ResidentialRoomCleaningRotas CR
JOIN	ResidentialRooms R ON R.CleaningRotaID = CR.CleaningRotaID
JOIN	SiteBlocks SB ON SB.BlockID = R.BlockID
ORDER BY
	SB.SiteID,
	SB.AreaID,
	SB.BlockID,
	CR.RotaName

GO
/****** Object:  StoredProcedure [KxMobile].[qryGetClientSearch]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetClientSearch]
	@Search1 varchar(255),
	@Search2 varchar(255),
	@CompanyID int=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	TOP 100 P.PersonID,
		IsNull(P.Title, '') as Title,
		IsNull(P.Forename, '') as Forename,
		IsNull(P.Surname, '') as Surname,
		P.JobTitle,
		P.Sex,
		C.Name,
		CA.Department,
		P.Email
FROM	Companies C
JOIN	People P ON P.CompanyID = C.CompanyID
LEFT JOIN	Addresses A ON A.AddressID = P.CompanyAddressID
LEFT JOIN CompanyAddresses CA ON CA.CompanyID = C.CompanyID AND CA.AddressID = A.AddressID
WHERE	(@CompanyID = 0 or C.CompanyID = @CompanyID)
AND		IsNull(P.Inactive, 0) = 0
AND		IsNull(P.Contact, 0) = 1
AND		(
		(P.Forename like @Search1 + '%' and P.Surname like @Search2 + '%')
	or	(P.Forename like @Search2 + '%' and P.Surname like @Search1 + '%')
		)
ORDER BY 
	P.Surname,
	P.Forename,
	C.Name

GO
/****** Object:  StoredProcedure [KxMobile].[qryGetCompanySearch]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetCompanySearch]
	@CompanyName varchar(255),
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	TOP 100 C.CompanyID,
		C.Name,
		CA.Department,
		A.Town,
		A.County,
		A.Postcode
FROM	Companies C
JOIN	CompanyAddresses CA ON CA.CompanyID = C.CompanyID
JOIN	Addresses A ON A.AddressID = CA.AddressID
WHERE	C.Name like '%' + @CompanyName + '%'
AND		CA.Active = 1
ORDER BY 
	C.Name,
	A.Town,
	CA.Department

GO
/****** Object:  StoredProcedure [KxMobile].[qryGetConferenceRoomEquipment]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetConferenceRoomEquipment]
	@ConferenceRoomID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	CRDF.FacilityID,
	F.Description,
	CRDF.Quantity,
	F.Notes,
	F.ItemOrder as RowNumber
FROM	ConferenceRoomDefaultFacilities CRDF
JOIN	Facilities F ON F.FacilityID = CRDF.FacilityID
WHERE	CRDF.ConferenceRoomID = @ConferenceRoomID
ORDER BY F.ItemOrder, F.Description


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetConferenceRoomLayouts]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetConferenceRoomLayouts]
	@ConferenceRoomID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	CRL.LayoutID,
		L.Description,
		L.Picture,
		CRL.Capacity,
		CRL.DefaultID,
		(SELECT COUNT(*) fROM ConferenceRoomLayoutImages CRI WHERE CRI.ConferenceRoomID = CRL.ConferenceRoomID AND CRI.LayoutID = CRL.LayoutID) as ImageCount
FROM	ConferenceRoomLayouts CRL
JOIN	Layouts L ON L.LayoutID = CRL.LayoutID
WHERE	CRL.ConferenceRoomID = @ConferenceRoomID
ORDER BY DefaultID DESC, Description

GO
/****** Object:  StoredProcedure [KxMobile].[qryGetDelegateShortDetails]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetDelegateShortDetails]
	@EventModuleDelegateID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	P.PersonName,
		P.Forename,
		P.Surname,
		P.Email,
		EMD.EventModuleID,
		EMD.PersonID,
		EM.EventID,
		EM.Description,
		IsNull((SELECT TOP 1 RRO.ResidentialRoomID FROM ResidentialRoomOccupants RRO WHERE RRO.EventModuleID = EMD.EventModuleID AND RRO.PersonID = EMD.PersonID AND RRO.PersonSequenceID = EMD.PersonSequenceID ORDER BY RRO.Date DESC), 0) as RoomID,
		EMD.ArrivalDate,
		EMD.DepartureDate
INTO #Results
FROM EventModuleDelegates EMD
JOIN	ViewPeople P ON P.PersonID = EMD.PersonID
JOIN	EventModules EM ON EM.EventModuleID = EMD.EventModuleID
WHERE EMD.EventModuleDelegateID = @EventModuleDelegateID

SELECT	R.*,
		IsNull(R1.Name, '') as Roomname,
		IsNull(S.Name, '') as BlockName
FROM	#Results R
LEFT JOIN ResidentialRooms R1 ON R1.ResidentialRoomID = R.RoomID
LEFT JOIN SiteBlocks S ON S.BlockID = R1.BlockID


DROP TABLE #Results


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetDocument]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetDocument]
	@DocumentID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT *
FROM Documents D
WHERE D.DocumentID = @DocumentID


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetEventManagers]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetEventManagers]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	P.UserID,
		P.FullName
FROM	ViewUsers P
WHERE	IsNull(P.Inactive, 0) = 0
AND		IsNull(P.EventManager, 0) = 1
ORDER BY
	P.Surname, P.Forename


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetEventSearch]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetEventSearch]
	@Search varchar(255),
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @WalkIn int, @SBT int, @CancellationStatusId int, @TurndownStatusID int
SELECT @WalkIn = C.WalkInEventTypeID, @SBT = C.StudentEventBusinessTypeID, @CancellationStatusId = C.CancellationStatusID, @TurndownStatusID =  C.TurnDownStatusID FROM Control C

DECLARE @Date datetime
SET @Date = CAST(FLOOR(CAST(getdate() as money)) as datetime)


SELECT	TOP 100
		EM.EventID,
		EM.EventModuleID,
		EM.Description,
		0 as PersonID,
		0 as CompanyID,
		S.Description as [Status],
		EM.ArrivalDate,
		EM.StartDate,
		EM.EndDate,
		IsNull(U.FullName, '') as FullName,
		IsNull(U.Surname, '') as Surname,
		ISNull(U.Forename, '') as Forename,
		CAST(CASE WHEN E.EventTypeID = @WalkIn THEN 1 ELSE 0 END as bit) as WalkIn,
		IsNull((SELECT TOP 1 SB2.SequenceNo FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), 9999999) as BlockSort,
		RTrim(IsNull((SELECT TOP 1 SB2.Name FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), '')) as BlockName,
		RTrim(IsNull((SELECT TOP 1 CR2.Name FROM EventModuleConferenceRooms EMCR2 JOIN ConferenceRooms CR2 ON CR2.ConferenceRoomID = EMCR2.ConferenceRoomID JOIN SiteBlocks SB2 ON SB2.BlockID = CR2.BlockID WHERE EMCR2.EventModuleID = EM.EventModuleID AND EMCR2.MainRoom = 1 ORDER BY SB2.SequenceNo, CR2.Name), '')) as MainRoom
INTO	#Results
FROM	EventModules EM
JOIN	Events E ON E.EventID = EM.EventID
JOIN	Status S ON S.StatusID = EM.StatusID
LEFT JOIN EventCompanies EC ON EC.EventID = EM.EventID
LEFT JOIN Companies C ON C.CompanyID = EC.CompanyID
LEFT JOIN EventPeople EP ON EP.EventCompanyID = EC.EventCompanyID
LEFT JOIN ViewPeople P ON P.PersonID = EP.PersonID
LEFT JOIN ViewUsers U ON U.UserID = EM.AccountManagerPersonID
WHERE	(EM.Description like @Search + '%' OR CAST(EM.EventID as varchar(255)) like @Search + '%')  
AND		NOT (S.StatusID = @CancellationStatusId OR S.StatusID = @TurndownStatusID)
AND		EM.EndDate >= @Date

UPDATE R
SET PersonID = IsNull(EP.PersonID, 0), CompanyID = IsNull(EC.CompanyID, 0)
FROM #Results R
LEFT JOIN EventCompanies EC ON EC.EventID = R.EventID AND EC.PrimaryCompany = 1
LEFT JOIN EventPeople EP ON EP.EventCompanyID = EC.EventCompanyID

UPDATE R
SET PersonID = IsNull( (SELECT TOP 1  PersonID FROM EventPeople EP WHERE EP.EventID = R.EventID), 0)
FROM #Results R
WHERE R.PersonID = 0

SELECT TOP 100 R.*,
	IsNull(C.Name, '') as CompanyName,
	P.PersonName
FROM	#Results R
LEFT JOIN Companies C ON C.CompanyID = R.CompanyID
LEFT JOIN ViewPeople P ON P.PersonID = R.PersonID
ORDER BY
	R.ArrivalDate,
	R.EventID

DROP TABLE #Results



GO
/****** Object:  StoredProcedure [KxMobile].[qryGetEventStatus]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetEventStatus]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	P.StatusID,
		P.Description,
		P.StatusType
FROM	Status P
WHERE	IsNull(P.Inactive, 0) = 0
AND		P.StatusID NOT IN (SELECT C.CancellationStatusID FROM Control C UNION SELECT C.TurnDownStatusID FROM Control C) 
ORDER BY
	P.StatusLevel,
	P.Description


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetEventTypes]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetEventTypes]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	P.EventTypeID,
		P.Description,
		P.TermConditions
FROM	EventTypes P
WHERE	IsNull(P.Inactive, 0) = 0
ORDER BY
	Description


GO
/****** Object:  StoredProcedure [KxMobile].[qryGetPaymentMethods]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryGetPaymentMethods]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	P.PaymentMethodID,
	P.Description
FROM	PaymentMethods P
ORDER BY
	ListOrder,
	Description


GO
/****** Object:  StoredProcedure [KxMobile].[qryHKRooms]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryHKRooms]
	@SiteID int,
	@AreaID int,
	@BlockID int,
	@SubBlockID int,
	@RotaID int=0,
	@Floor int=0,
	@LinenChange int=0,
	@ShowDirtyOnly bit=0,
	@OrderByFloor bit=0,
	@EventID int=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @VIP varchar(255), @Disabled varchar(255), @Smoker varchar(255), @LastLet varchar(255), @Date datetime

SET @VIP = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.VIP.Text'), 'VIP')
SET @Disabled = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Disabled.Text'), 'Disabled')
SET @Smoker = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.Smoking.Text'), 'Smoking')
SET @LastLet = IsNull((SELECT Value FROM CustomSettings where CustomSetting = 'Kx.Room.Attribute.LastLet.Text'), 'LastLet')
SET @Date = CAST(FLOOR(CAST(getdate() as money)) as datetime)

SELECT	R.ResidentialRoomID,
		R.Name,
		R.FloorNumber,
		IsNull(CR.RotaName, '') as RotaName,
		R.ExtNo,
		R.MaidStatus,
		IsNull(R.MaidStatusNotes, '') as MaidStatusNotes,
		R.Smoking,
		R.VIP,
		R.Disabled,
		R.LastLet,
		@VIP as VIPText,
		@Disabled as DisabledText,
		@Smoker as SmokerText,
		@LastLet as LastLetText,
		RTrim(IsNull(SB.Name, '')) as BlockName,
		RTrim(IsNull(SSB.Name, '')) as SubBlockName,
		BT.Description as BedroomType,
		IsNull(EM.EventID, 0) as EventIDCurrent,
		IsNull(CAST(EM.EventID as varchar(10)) + ' ' + EM.Description, '') as EventDetails,
		CASE WHEN @Date = CAST(FLOOR(CAST(EMD.ArrivalDate as money)) as datetime) THEN 1 ELSE 0 END as GuestArriving,
		CASE WHEN @LinenChange = 0 THEN 0 WHEN ((Floor(CAST(RRO.Date as money)) - Floor(CAST(EM.ArrivalDate as money))) > 0) and ((Floor(CAST(RRO.Date as money)) - Floor(CAST(EM.ArrivalDate as money))) % @LinenChange = 0) THEN 1 ELSE 0 END as LinenChange,
		VP.PersonName
FROM	ResidentialRooms R 
JOIN	BedroomTypes BT ON BT.BedroomTypeID = R.BedroomTypeID
JOIN	SiteBlocks SB ON SB.BlockID = R.BlockID
LEFT JOIN SiteBlocks SSB ON SSB.BlockID = R.SubBlockID
LEFT JOIN ResidentialRoomCleaningRotas CR ON CR.CleaningRotaID = R.CleaningRotaID
LEFT JOIN ResidentialRoomOccupants RRO ON RRO.ResidentialRoomID = R.ResidentialRoomID AND RRO.Date = @Date
LEFT JOIN EventModules EM ON EM.EventModuleID = RRO.EventModuleID
LEFT JOIN EventModuleBlockBedroomAllocationDelegates EMBBAD ON EMBBAD.EventModuleID = RRO.EventModuleID and EMBBAD.PersonID = RRO.PersonID AND EMBBAD.PersonSequenceID =RRO.PersonSequenceID AND EMBBAD.Date = RRO.Date
LEFT JOIN EventModuleDelegates EMD ON EMD.EventModuleID = EMBBAD.EventModuleID AND EMD.PersonID = EMBBAD.PersonID AND EMBBAD.PersonSequenceID = EMD.PersonSequenceID
LEFT JOIN ViewPeople VP ON VP.PersonID = EMD.PersonID
WHERE	SB.SiteID = @SiteID
AND		SB.AreaID = @AreaID
AND		SB.BlockID = @BlockID
AND		(@SubBlockID = 0 OR @SubBlockID = R.SubBlockID)
AND		(@RotaID = 0 OR R.CleaningRotaID = @RotaID)
AND		(@Floor = -999 OR R.FloorNumber = @Floor)
AND		(@ShowDirtyOnly = 0 or R.MaidStatus = 'D')
AND		(@EventID = 0 OR @EventID = EM.EventID)
ORDER BY
	CASE WHEN @OrderByFloor = 1 THEN R.FloorNumber ELSE 0 END,
	R.DisplayOrder,
	R.Name


GO
/****** Object:  StoredProcedure [KxMobile].[qryLayouts]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryLayouts]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	*
FROM	Layouts L
WHERE	ISNull(L.Inactive, 0) = 0
ORDER BY L.LayoutID



GO
/****** Object:  StoredProcedure [KxMobile].[qryRoomImages]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryRoomImages]
	@ConferenceRoomID int,
	@LayoutID int=0,
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	D.LayoutID,
		D.Picture
FROM	ConferenceRoomLayoutImages D
WHERE	D.ConferenceRoomID = @ConferenceRoomID
AND		(D.LayoutID = @LayoutID OR (@LayoutID = -1 AND D.LayoutID <> 0))
ORDER BY [Default] DESC, D.ConferenceRoomLayoutImageID


GO
/****** Object:  StoredProcedure [KxMobile].[qrySetCheckUDFs]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qrySetCheckUDFs]
	@EventModuleDelegateID	int,
	@CustomFieldID int,
	@Value varchar(255),
	@ApplicationID int=0,
	@DeviceID int=0
AS

DECLARE @EventModuleID int

SELECT @EventModuleID = EMD.EventModuleID FROM EventModuleDelegates EMD WHERE EMD.EventModuleDelegateID = @EventModuleDelegateID

IF @EventModuleID > 0
BEGIN
	IF NOT EXISTS (SELECT NULL FROM DelegateCustomFields DCF WHERE DCF.EventModuleID = @EventModuleID AND DCF.CustomFieldID = @CustomFieldID)
	BEGIN
		INSERT INTO DelegateCustomFields (CustomFieldID, EventModuleID, OnReport, ShowInResidential)
			VALUES(@CustomFieldID, @EventModuleID, 0, 0)
	END

	IF NOT EXISTS (SELECT NULL FROM CustomFields2Values V WHERE V.CustomFieldID = @CustomFieldID AND V.EntityID = @EventModuleDelegateID AND V.EntityType = 'EventModuleDelegates')
	BEGIN
		INSERT INTO CustomFields2Values (CustomFieldID, EntityType, EntityID, FieldValue)
		  VALUES (@CustomFieldID, 'EventModuleDelegates', @EventModuleDelegateID, @Value)
	END
	ELSE
	BEGIN
		UPDATE CustomFields2Values SET FieldValue = @Value where CustomFieldID = @CustomFieldID AND EntityID = @EventModuleDelegateID AND EntityType = 'EventModuleDelegates'
	END
END

GO
/****** Object:  StoredProcedure [KxMobile].[qrySetDocument]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qrySetDocument]
	@Document image,
	@DocumentType varchar(255),
	@EntityID int,
	@EntityType varchar(255),
	@Compressed bit,
	@ApplicationID int=0,
	@DeviceID int=0
AS

INSERT INTO Documents (DocumentType, EntityID, EntityType, Compressed, Document) 
	VALUES(@DocumentType, @EntityID, @EntityType, @Compressed, @Document) 


GO
/****** Object:  StoredProcedure [KxMobile].[qrySiteBlocks]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qrySiteBlocks]
	@ApplicationID int=0,
	@DeviceID int=0
AS

SELECT	DISTINCT S.SiteID,
		RTrim(S.Name) as SiteName,
		A.AreaID,
		RTrim(A.Description) as AreaName,
		IsNull(SB.BlockID, 0) as BlockID,
		RTrim(IsNull(SB.Name, '')) as BlockName,
		0 as SubBlockID,
		CAST('' as varchar(255)) as SubBlockName,
		A.DisplayOrder,
		IsNull(SB.SequenceNo, 0) as SequenceNo,
		CASE WHEN EXISTS (SELECT NULL FROM BedroomTypeSiteLimits BTSL WHERE BTSL.BlockID = SB.BlockID) THEN 1 ELSE 0 END as BedroomsExist,
		CASE WHEN EXISTS (SELECT NULL FROM ConferenceRooms C WHERE C.AreaID = A.AreaID) THEN 1 ELSE 0 END as ConfRoomsExist,
		0 as ConferenceRoomGroup
FROM	Sites S 
JOIN	Areas A ON A.SiteID = S.SiteID
LEFT JOIN SiteBlocks SB ON SB.AreaID = A.AreaID
UNION
SELECT	DISTINCT S.SiteID,
		RTrim(S.Name) as SiteName,
		A.AreaID,
		RTrim(A.Description) as AreaName,
		SB.BlockID,
		RTrim(SB.Name) as BlockName,
		IsNull(SSB.BlockID, 0) as SubBlockID,
		RTrim(IsNull(SSB.Name, '')) as SubBlockName,
		A.DisplayOrder,
		SB.SequenceNo,
		CASE WHEN EXISTS (SELECT NULL FROM BedroomTypeSiteLimits BTSL WHERE BTSL.BlockID = SB.BlockID) THEN 1 ELSE 0 END as BedroomsExist,
		CASE WHEN EXISTS (SELECT NULL FROM ConferenceRooms C WHERE C.AreaID = SB.AreaID) THEN 1 ELSE 0 END as ConfRoomsExist,
		0 as ConferenceRoomGroup
FROM	Sites S 
JOIN	Areas A ON A.SiteID = S.SiteID
JOIN	SiteBlocks SB ON SB.AreaID = A.AreaID
LEFT JOIN SiteBlocks SSB ON SSB.MasterBlockID = SB.BlockID
UNION
SELECT	DISTINCT S.SiteID,
		RTrim(S.Name) as SiteName,
		0,
		'' as AreaName,
		SB.BlockID,
		RTrim(SB.Name) as BlockName,
		0 as SubBLockID,
		'' as SubBlockName,
		-1,
		SB.SequenceNo,
		0,
		0,
		1
FROM	Sites S 
JOIN	SiteBlocks SB ON SB.SiteID = S.SiteID
WHERE	SB.BlockType = 'C'
ORDER BY
	Rtrim(S.Name),
	S.SiteID,
	A.DisplayOrder,
	RTRim(A.Description),
	A.AreaID,
	IsNull(SB.SequenceNo, 0),
	RTrim(IsNull(SB.Name, '')),
	IsNull(SB.BlockID, 0)


GO
/****** Object:  StoredProcedure [KxMobile].[qryStatsBedroomInUse]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryStatsBedroomInUse]
	@Date Datetime,
	@SiteID int,
	@AreaID int,
	@BlockID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS


CREATE TABLE #Results  (ID int identity (1,1), BedroomTypeID int, Description varchar(255), SortOrder int, Booked int, BedroomCount int, OverbookPercentage float, OverbookCount int)

INSERT INTO #Results
SELECT	EMBBA.BedroomTypeID,
		BT.Description as BedroomType,
		BTSL.SortOrder,
		SUM(Allocation) as Booked,
		MAX(BTSL.BedroomCount) as BedroomCount,
		MAX(BTSL.OverbookingPercentage) as OverBookPercentage,
		MAX(FLOOR(CAST(BTSL.OverbookingPercentage as float) * (CAST(BTSL.BedroomCount as float) / 100) + 0.9999)) as OverbookCount
FROM	EventModuleBlockBedroomAllocation EMBBA
JOIN	EventModules EM ON EM.EventModuleID = EMBBA.EventModuleID
JOIN	Status S ON S.StatusID = EM.StatusID
JOIN	SiteBlocks SB ON SB.BlockID = EMBBA.BlockID
JOIN	BedroomTypeSiteLimits BTSL ON BTSL.BedroomTypeID = EMBBA.BedroomTypeID AND BTSL.BlockID = EMBBA.BlockID
JOIN	BedroomTypes BT ON BT.BedroomTypeID = EMBBA.BedroomTypeID
WHERE	EMBBA.Date = @Date
AND		(@SiteID = 0 OR @SiteID = SB.SiteID)
AND		(@AreaID = 0 OR @AreaID = SB.AreaID)
AND		(@BlockID = 0 OR @BlockID = SB.BlockID)
AND		S.EffectCapacity = 1
GROUP BY
	BT.Description,
	BTSL.SortOrder,
	EMBBA.BedroomTypeID
ORDER BY BTSL.SortOrder,
	BT.Description

SELECT	R.*,
		C.Colour
FROM	#Results R
JOIN	KxMobile.Colours C ON C.ColourID = R.ID

DROP TABLE #Results

GO
/****** Object:  StoredProcedure [KxMobile].[qryStatsCleanVsDirty]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryStatsCleanVsDirty]
	@SiteID int,
	@AreaID int,
	@BlockID int,
	@ApplicationID int=0,
	@DeviceID int=0
AS


CREATE TABLE #Results  (ID int identity (1,1), MaidStatus varchar(20), BedroomCount int)

INSERT INTO #Results
SELECT	IsNull(NullIf(CAST(rr.MaidStatus as varchar(20)), ''), '{Not Set}'),
		COUNT(*)
FROM	ResidentialRooms RR
JOIN	SiteBlocks SB ON SB.BlockID = RR.BlockID
WHERE	(@SiteID = 0 OR @SiteID = SB.SiteID)
AND		(@AreaID = 0 OR @AreaID = SB.AreaID)
AND		(@BlockID = 0 OR @BlockID = SB.BlockID)
GROUP BY
	RR.MaidStatus
ORDER BY RR.MaidStatus

SELECT	R.ID,
		CASE WHEN R.MaidStatus = 'C' THEN 'Clean' WHEN R.MaidStatus = 'D' THEN 'Dirty' WHEN R.MaidStatus = 'I' THEN 'Inspected' ELSE R.MaidStatus END as MaidStatus,
		R.BedroomCount,
		CASE WHEN R.MaidStatus = 'C' THEN 'clGreen' WHEN R.MaidStatus = 'D' THEN 'clRed' WHEN R.MaidStatus = 'I' THEN 'clYellow' ELSE 'clMoneyGreen' END as Colour
FROM	#Results R

DROP TABLE #Results

GO
/****** Object:  StoredProcedure [KxMobile].[qryStoreAudit]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryStoreAudit]
	@DeviceID				[int],
	@ApplicationID			[int] = 0,
	@Query					[varchar](max) = '',
	@StoredProc				[varchar](255) = '',
	@StoredProcParams		[varchar](max) = '',
	@AuditInfo				[varchar](max) = ''
	
AS	

INSERT INTO KxMobile.Audit(DeviceID, ApplicationID, Query, StoredProc, StoredProcParams, AuditInfo, RecordDateTime)
	VALUES (@DeviceID, @ApplicationID, @Query, @StoredProc, @StoredProcParams, @AuditInfo, getdate())

GO
/****** Object:  StoredProcedure [KxMobile].[qryStorePin]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[qryStorePin]
	@AppApprovalID			[int],
	@Pin					[varchar](255)
	
AS	

UPDATE AppApproval SET SecurityPin = @Pin WHERE AppApprovalID = @AppApprovalID

GO
/****** Object:  StoredProcedure [KxMobile].[SetApplications]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
 

CREATE PROCEDURE [KxMobile].[SetApplications]
	@ApplicationID int,
	@Application varchar(255),
	@Inactive bit,
	@LicenceInfo varchar(max),
	@FeatureAccess varchar(max),
	@FullDateFormat varchar(20),
	@LongDateFormat varchar(20),
	@ShortDateFormat varchar(20),
	@TimeFormat varchar(20),
	@CurrencyFormat varchar(20),
	@Settings varchar(max),
	@Lang int
AS

UPDATE
	KxMobile.Applications
SET
	Application = @Application,
	Inactive = @Inactive,
	LicenceInfo = @LicenceInfo,
	FeatureAccess = @FeatureAccess,
	FullDateFormat = @FullDateFormat,
	LongDateFormat = @LongDateFormat,
	ShortDateFormat = @ShortDateFormat,
	TimeFormat = @TimeFormat,
	CurrencyFormat = @CurrencyFormat,
	Settings = @Settings,
	Lang = @Lang
WHERE
	ApplicationID	= @ApplicationID
 
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [KxMobile].[SetMaidStatus]    Script Date: 6/11/21 4:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [KxMobile].[SetMaidStatus]
	@ResidentialRoomID int,
	@MaidStatus char(1),
	@Notes varchar(max),
	@UserID int,
	@ApplicationID int=0, -- Needed for KxMobile
	@DeviceID int=0 -- Needed for KxMobile
AS

INSERT INTO
	AuditLogs(TableName, Operation, KeyInfo, OldData, NewData, UserID, DateTime, Notes)
SELECT
	'ResidentialRooms',
	'Edit',
	'ResidentialRoomID=' + CAST(@ResidentialRoomID AS varchar(10)),
	'MaidStatus=' + MaidStatus,
	'MaidStatus=' + @MaidStatus,
	@UserID,
	GetDate(),
	'KxMobile.SetMaidStatus'
FROM
	ResidentialRooms
WHERE
	ResidentialRoomID = @ResidentialRoomID

DECLARE @OperationID int
DECLARE @Code varchar(15)

SELECT @Code  = 
	CASE	
	WHEN @MaidStatus = 'D' THEN 'DIRTY'
	WHEN @MaidStatus = 'C' THEN 'CLEAN'
	WHEN @MaidStatus = 'R' THEN 'REJECTED'
	WHEN @MaidStatus = 'I' THEN 'INSPECTED'
	END

SELECT @OperationID=RoomAuditLogOperationID FROM RoomAuditLogOperation WHERE Code = @Code

DECLARE @RoomAuditLogID int

INSERT INTO
	RoomAuditLog(ResidentialRoomID, CreationUserID, CreationDate,RoomAuditLogOperationID,Notes,Form)
VALUES(
	@ResidentialRoomID,
	@UserID,
	GetDate(),
	@OperationID, 
	@Notes,
	'SP-KxMobile.SetMaidStatus')

SET @RoomAuditLogID =  SCOPE_IDENTITY()

UPDATE
	ResidentialRooms
SET
	MaidStatus = @MaidStatus,
	MaidStatusNotes = ''
WHERE
	ResidentialRoomID = @ResidentialRoomID

SELECT @RoomAuditLogID as RoomAuditLogID

GO

/****** Object:  UserDefinedFunction [dbo].[Capitalise]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Function [dbo].[Capitalise]
(
    @Text varchar(1000)
)
returns varchar(1000)
AS
BEGIN 
	declare @ReturnString varchar(1000)
	set @ReturnString = ''
	declare @Word varchar(30)
	declare @Pointer int
	set @Pointer = 0
	if right( @Text, 1 ) <> ' '
	set @Text = @Text + ' '
	while charindex( ' ', @Text, @Pointer ) > 1
	begin
		set @Word = substring( @Text, @Pointer, charindex( ' ', @Text, @Pointer ) - @Pointer )
		--if len( @Word ) > 1
			set @Word = upper( left( @Word, 1 ) ) + lower( right( @Word, len( @Word ) - 1 ) )
		set @Pointer = charindex( ' ', @Text, @Pointer ) + 1
		set @ReturnString = @ReturnString + @Word + ' '
	end
	set @ReturnString = rtrim( ltrim( @ReturnString ) )
	return @ReturnString 
END
GO
/****** Object:  UserDefinedFunction [dbo].[DateToStr]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[DateToStr](@DateTime Datetime)
RETURNS varchar(255) AS  
BEGIN
	DECLARE @Lang int SET @Lang = dbo.GetLang()
	DECLARE
		@Result varchar(255)
	SET @Result = 
		CASE @Lang
			WHEN 2 THEN
				CONVERT(varchar, @DateTime, 101)
			WHEN 8 THEN
				CONVERT(varchar, @DateTime, 101)
			WHEN 7 THEN
				CONVERT(varchar, @DateTime, 105)
			ELSE
				CONVERT(varchar, @DateTime, 103)
			END
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_str_TO_BASE64]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_str_TO_BASE64]
(
    @STRING NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    RETURN (
        SELECT
            CAST(N'' AS XML).value(
                  'xs:base64Binary(xs:hexBinary(sql:column("bin")))'
                , 'NVARCHAR(MAX)'
            )   Base64Encoding
        FROM (
            SELECT CAST(@STRING AS VARBINARY(MAX)) AS bin
        ) AS bin_sql_server_temp
    )
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnAge]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnAge](@DOB datetime, @D datetime) RETURNS int AS
BEGIN
/*
	The following query illustrates where Tim's formula (Age1) does not work:
	SELECT * FROM (
	SELECT
		FLOOR(DATEDIFF(DAY, D0.Date, D1.Date) / CAST(365.25 AS money)) AS Age1,
		DATEDIFF(YEAR, D0.Date, D1.Date) + CASE WHEN MONTH(D0.Date) * 100 + DAY(D0.Date) > MONTH(D1.Date) * 100 + DAY(D1.Date) THEN -1 ELSE 0 END AS Age2,
		D0.Date AS DOB,
		D1.Date AS Date
	FROM
		Dates D0
		CROSS JOIN Dates D1
	WHERE
		D0.Date BETWEEN '19800101' AND '19811231'
	AND	D1.Date BETWEEN '20000101' AND '20011231'
	) A WHERE Age1 <> Age2
*/
	IF @D = 0 SET @D = CURRENT_TIMESTAMP
	RETURN DATEDIFF(YEAR, @DOB, @D) + CASE WHEN MONTH(@DOB) * 100 + DAY(@DOB) > MONTH(@D) * 100 + DAY(@D) THEN -1 ELSE 0 END
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnBlankIfInvalidDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnBlankIfInvalidDate](@DateString varchar(255), @Lang int) RETURNS varchar(255) AS
BEGIN
	IF dbo.fnValidDate(@DateString, @Lang) = 0
		SET @DateString = ''
	RETURN @DateString
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnCalcVAT]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnCalcVAT](@VATID int, @SourceAmount money, @NetOrGross char(1)) RETURNS money AS
BEGIN
	DECLARE
		@Sign int,
		@VAT money,
		@VATRate numeric(20,10),
		@RateType varchar(10),
		@AppliesIfGreaterThan money,
		@AppliesIfLessThan money
	SELECT
		@VATRate = Rate,
		@RateType = ISNULL(RateType, 'Percent'),
		@AppliesIfGreaterThan = ISNULL(AppliesIfGreaterThan, 0),
		@AppliesIfLessThan = ISNULL(AppliesIfLessThan, 0)
	FROM
		VAT
	WHERE
		VATID = @VATID
	SET @Sign = CASE WHEN @SourceAmount < 0 THEN -1 ELSE 1 END
	SET @SourceAmount = ABS(@SourceAmount)
	IF @SourceAmount <= @AppliesIfGreaterThan OR @AppliesIfLessThan > 0 AND @SourceAmount >= @AppliesIfLessThan
		SET @VAT = 0
	ELSE IF @RateType = 'Value'
		SET @VAT = @VATRate
	ELSE IF @NetOrGross = 'N'
		SET @VAT = dbo.fnRoundVAT(@SourceAmount * (@VATRate / 100))
	ELSE IF @NetOrGross = 'G'
		SET @VAT = dbo.fnRoundVAT(@SourceAmount * (1 - 1 / (1 + @VATRate / 100)))
	SET @VAT = @VAT * @Sign
	RETURN @VAT
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnCateringCodes]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnCateringCodes](@BookingID int) RETURNS varchar(8000) AS
BEGIN
	DECLARE @CateringCodes varchar(8000) SET @CateringCodes = NULL
	SELECT
		@CateringCodes = ISNULL(@CateringCodes + ', ', '') + ISNULL(NULLIF(CAST(RTRIM(F.Code) AS varchar(255)), ''), RTRIM(F.Description)) +
		CASE WHEN EMDCF.Quantity <> 0 AND EMDCF.Quantity <> EMDCA.Quantity THEN ' (' + CAST(EMDCF.Quantity AS varchar) + ')' ELSE '' END
	FROM
		EventModuleDayCateringAreas EMDCA
		JOIN EventModuleDayCateringFacilities EMDCF ON EMDCF.EventModuleDayCateringAreaID = EMDCA.EventModuleDayCateringAreaID
		JOIN Facilities F ON F.FacilityID = EMDCF.FacilityID
	WHERE
		EMDCF.EventModuleDayCateringAreaID = @BookingID
	RETURN @CateringCodes
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnChargesToCredit]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnChargesToCredit](@AccountChargeIDs varchar(max), @CreditQuantities varchar(max), @CreditAmounts varchar(max))
RETURNS @Table TABLE (AccountChargeID int, CreditQuantity float, CreditAmount money)
AS
BEGIN
	INSERT INTO
		@Table(AccountChargeID, CreditQuantity, CreditAmount)
	SELECT
		CAST(ID.String AS int),
		CAST(Q.String AS float),
		CAST(A.String AS money)
	FROM
		dbo.fnStringToTableEx(@AccountChargeIDs, ';') ID
		JOIN dbo.fnStringToTableEx(@CreditQuantities, ';') Q ON Q.N = ID.N
		JOIN dbo.fnStringToTableEx(@CreditAmounts, ';') A ON A.N = ID.N
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnChargeTypeNominal]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnChargeTypeNominal]
	(
	@EventModuleID int,
	@ChargeTypeID int,
	@HeaderChargeTypeID int,
	@MatrixID int,
	@CateringProviderID int
	)
RETURNS
	integer
AS
BEGIN
	SET @CateringProviderID = ISNULL(@CateringProviderID, 0)
	DECLARE @NominalID int
	SET @NominalID = ISNULL(@NominalID, 0)
	DECLARE
		@SalesTypeID int,
		@EventTypeID int,
		@EventBusinessTypeID int,
		@EventSiteID int,
		@VATExempt bit,
		@SiteID int
	DECLARE
		@RateBlockID		int,
		@RateBedroomTypeID	int,
		@RateFacilityID		int,
		@RateConferenceRoomID	int,
		@RateAreaID		int,
		@RateSiteID		int
	SET @RateBlockID = 0
	SET @RateBedroomTypeID = 0
	SET @RateFacilityID = 0
	SET @RateConferenceRoomID = 0
	SET @RateAreaID = 0
	SET @RateSiteID = 0
	IF @MatrixID > 0
	SELECT
		@RateBlockID = ISNULL(M.BlockID, 0),
		@RateBedroomTypeID = ISNULL(M.BedroomTypeID, 0),
		@RateFacilityID = ISNULL(M.FacilityID, 0),
		@RateConferenceRoomID = ISNULL(M.ConferenceRoomID, 0),
		@RateAreaID = COALESCE(SB.AreaID, CR.AreaID, 0),
		@RateSiteID = COALESCE(SB.SiteID, CR.SiteID, 0)
	FROM
		ChargeTypeRateMatrix M
		LEFT JOIN SiteBlocks SB ON SB.BlockID = M.BlockID
		LEFT JOIN ConferenceRooms CR ON CR.ConferenceRoomID = M.ConferenceRoomID
	WHERE
		M.MatrixID = @MatrixID
	SELECT
		@SalesTypeID = EM.SalesTypeID,
		@EventTypeID = E.EventTypeID,
		@EventBusinessTypeID = ET.EventBusinessTypeID,
		@EventSiteID = EM.SiteID,
		@VATExempt = EM.VATExempt
	FROM
		EventModules EM
		JOIN Events E ON E.EventID = EM.EventID
		JOIN EventTypes ET ON ET.EventTypeID = E.EventTypeID
	WHERE
		EM.EventModuleID = @EventModuleID
	SELECT TOP 1
		@NominalID = NominalID
	FROM
		ChargeTypeNominalMatrix
	WHERE
		ChargeTypeID = @ChargeTypeID
	AND	@HeaderChargeTypeID = ISNULL(HeaderChargeTypeID, @HeaderChargeTypeID)
	AND	@SalesTypeID = ISNULL(SalesTypeID, @SalesTypeID)
	AND	@EventBusinessTypeID = ISNULL(EventBusinessTypeID, @EventBusinessTypeID)
	AND	@EventTypeID = ISNULL(EventTypeID, @EventTypeID)
	AND	@EventSiteID = ISNULL(EventSiteID, @EventSiteID)
	AND	(VATExempt IS NULL OR VATExempt = @VATExempt)
	AND	@RateSiteID = ISNULL(RateSiteID, @RateSiteID)
	AND	@RateAreaID = ISNULL(RateAreaID, @RateAreaID)
	AND	@RateBlockID = ISNULL(RateBlockID, @RateBlockID)
	AND	@RateBedroomTypeID = ISNULL(RateBedroomTypeID, @RateBedroomTypeID)
	AND	@RateFacilityID = ISNULL(RateFacilityID, @RateFacilityID)
	AND @RateConferenceRoomID = ISNULL(RateConferenceRoomID, @RateConferenceRoomID)
	AND	@CateringProviderID = ISNULL(CateringProviderID, @CateringProviderID)
	ORDER BY
		ChargeTypeNominalMatrixID
	SET @NominalID = ISNULL(@NominalID, 0)
	IF @NominalID = 0
		SELECT @NominalID = NominalID FROM ChargeTypes WHERE ChargeTypeID = @ChargeTypeID
	SET @NominalID = ISNULL(@NominalID, 0)
	IF @NominalID = 0
		SELECT @NominalID = NominalID FROM ChargeTypes WHERE ChargeTypeID = @HeaderChargeTypeID
	IF (SELECT Value FROM CustomSettings WHERE CustomSetting = 'Apply Nominal Mappings') = 'T'
	BEGIN
		SET @SiteID = ISNULL(@RateSiteID, @EventSiteID)
		SELECT TOP 1
			@NominalID = NewNominalID
		FROM
			NominalMapping
		WHERE
			OriginalNominalID = @NominalID
		AND	(ISNULL(SalesTypeID, 0) = 0 OR SalesTypeID = @SalesTypeID)
		AND	(ISNULL(EventTypeID, 0) = 0 OR EventTypeID = @EventTypeID)
		AND	(ISNULL(SiteID, 0) = 0 OR SiteID = @SiteID)
		AND	(ISNULL(AreaID, 0) = 0 OR AreaID = @RateAreaID)
		AND	(ISNULL(BlockID, 0) = 0 OR BlockID = @RateBlockID)
	END
	RETURN @NominalID
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnCharIndex]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnCharIndex] (@KeyField int, @Searching varchar(MAX))
RETURNS int AS  
BEGIN	
	RETURN CHARINDEX(';' + CAST(@KeyField AS Varchar(20)) + ';', ';' + @Searching + ';')
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnCheckTaxModelForContinuity]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnCheckTaxModelForContinuity](@TaxModelID int)
RETURNS
	int
AS
BEGIN
	DECLARE @Result int SET @Result = 0;
	SELECT
		@Result = T.VATID
	FROM
		VAT T
	WHERE
		T.StartDate IS NOT NULL AND (SELECT COUNT(*) AS TaxCount FROM dbo.fnTaxesForDate(@TaxModelID, T.StartDate-1)) = 0
	OR  T.EndDate IS NOT NULL AND (SELECT COUNT(*) AS TaxCount FROM dbo.fnTaxesForDate(@TaxModelID, T.EndDate+1)) = 0
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnClientInvoiceSequenceType]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnClientInvoiceSequenceType](
	@InvoiceType varchar(255),
	@RaisedBySiteID int,
	@AmountToInvoice money
	) RETURNS varchar(255) AS
BEGIN
	DECLARE @SiteSequence bit
	IF EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'SITE_INVOICE_RANGES')
		SET @SiteSequence = 1
	ELSE
		SET @SiteSequence = 0
	DECLARE @CreditNoteSequence bit
	IF EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'CREDIT_INVOICE_RANGES')
		SET @CreditNoteSequence = 1
	ELSE
		SET @CreditNoteSequence = 0
	DECLARE @SiteCode varchar(255)
	IF @SiteSequence = 0
		SET @SiteCode = ''
	ELSE
		SELECT
			@SiteCode = ISNULL(SalesOrgCode, '')
		FROM
			Sites
		WHERE
			SiteID = @RaisedBySiteID
	DECLARE @SequenceType varchar(255)
	IF @InvoiceType = 'INVREQ' AND @AmountToInvoice < 0 AND @CreditNoteSequence = 1
		SET @SequenceType = 'CREDIT' + @SiteCode
	ELSE IF @InvoiceType = 'INTINV' AND @AmountToInvoice < 0 AND @CreditNoteSequence = 1
		SET @SequenceType = 'INTCRN' + @SiteCode
	ELSE
		SET @SequenceType = RTRIM(@InvoiceType) + @SiteCode
	RETURN @SequenceType
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnColourRTFFromColour]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnColourRTFFromColour]
( 
        @Colour varchar(10)
) 
RETURNS varchar(300)
AS 
BEGIN
DECLARE @Result varchar(300)
SET @Result = ''
IF LEN(RTRIM(@Colour)) = 9 AND @Colour LIKE '$00%' 
BEGIN
	SET @Result = '{\rtf1\ansi\deff0\nouicompat{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 MS Shell Dlg 2;}{\f2\fnil Segoe UI;}}' +
		'{\colortbl ;\red' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 255 ELSE 0 END as varchar) +
		'\green' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN (CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 65280) / 256 ELSE 0 END as varchar) +
		'\blue' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN (CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 16711680) / 65536 ELSE 0 END as varchar) +';}' +
		'{\*\generator Riched20 10.0.16299}\viewkind0\uc1\pard\cf1\fs26\lang2057\u9608?\u9608?\u9608?\u9608?\cf0\f1\fs17\f2\fs16\par}'
END
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnConferenceRoomDayNotes]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnConferenceRoomDayNotes](@EventModuleID int, @Date DateTime, @ConferenceRoomID int)
RETURNS varchar(MAX) AS  
BEGIN
DECLARE @Results varchar(max)
SELECT
	@Results = COALESCE(@Results,'') + CAST(N.Notes AS varchar(max)) + CHAR(13) + CHAR(10)
FROM
	EventModuleConferenceRooms EMCR
	JOIN ConferenceRoomNotes CRN ON CRN.EventModuleConferenceRoomID = EMCR.EventModuleConferenceRoomID
	JOIN ConferenceRoomNoteDays CRD ON CRD.ConferenceRoomNoteID = CRN.ConferenceRoomNoteID
	JOIN Notes N ON N.NotesID =  CRN.NotesID 
WHERE
	EMCR.EventModuleID = @EventModuleID
	AND EMCR.ConferenceRoomID = @ConferenceRoomID
	AND CAST(FLOOR(CAST(CRD.Date AS FLOAT)) AS Datetime) = @Date
SELECT
	@Results = LTRIM(RTRIM(ISNULL(@Results,'') + dbo.fnParseRTF(CAST(ISNULL(N.Notes,'') AS varchar(max)))))
FROM 
	EventModuleConferenceRooms EMCR
	JOIN Notes N ON N.NotesID = EMCR.LayoutNotesID 
	WHERE
	EventModuleID = @EventModuleID
	AND ConferenceRoomID = @ConferenceRoomID
SELECT @Results = SUBSTRING(@Results, 1, DATALENGTH(@Results) - 
	CASE 
		WHEN @Results LIKE '%' + char(13) + char(10) + char(13) THEN 3 
		WHEN @Results LIKE '%' + char(13) + char(10) THEN 2
	ELSE 0 END) 
RETURN @Results
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnConferenceRoomOccupancy]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnConferenceRoomOccupancy]
	(
	@DateFrom 		datetime,
	@DateTo 		datetime,
	@TimeFrom 		datetime,
	@TimeTo 		datetime,
	@SiteID 		int,
	@DaysOfWeek     varchar(255)='',
	@DailyOccupancy	bit,
	@ExcludeSetupAndTakedown bit,
	@ExcludeAdHoc bit=0,
	@IncludeUnavailableRooms bit=0,
	@IncludeOutOfServiceRooms bit=0,
	@ExcludeInternal bit=0
	)
RETURNS
	@OccupancyTable
TABLE
	(
	ConferenceRoomID int,
	BookingType char(1),
	ConferenceRoomTypeID int,
	Date datetime,
	Occupancy float
	)
AS
BEGIN
	DECLARE @Days TABLE(DayName varchar(255))
	INSERT INTO
		@Days
	SELECT
		DayName
	FROM
		dbo.fnDaysOfWeekTable(@DaysOfWeek)
	DECLARE @Bookings TABLE (RoomOccupantID int,ConferenceRoomID int, BookingType char(1), ConferenceRoomTypeID int, EventModuleID int, DateFrom datetime, DateTo datetime, DeleteMe bit)
	INSERT INTO
		@Bookings
	SELECT
		CRO.RoomOccupantID,
		CRO.ConferenceRoomID,
		CRO.BookingType,
		EMCR.ConferenceRoomTypeID,
		IsNull(CRO.EventModuleId, 0) as EventModuleID,
		ISNULL(EMCR_Times.StartDateTime, CRO.FromDate) AS DateFrom,
		ISNULL(EMCR_Times.EndDateTime, CRO.ToDate) AS DateTo,
		0
	FROM
		ConferenceRoomOccupants CRO
		JOIN ConferenceRooms CR ON CR.ConferenceRoomID = CRO.ConferenceRoomID
		LEFT JOIN EventModuleConferenceRooms EMCR_Times ON EMCR_Times.RoomOccupantID = CRO.RoomOccupantID AND @ExcludeSetupAndTakedown = 1
		LEFT JOIN EventModuleConferenceRooms EMCR ON EMCR.RoomOccupantID = CRO.RoomOccupantID
		LEFT JOIN EventModules EM ON EM.EventModuleID = CRO.EventModuleID
		LEFT JOIN Status S ON S.StatusID = EM.StatusID
		LEFT JOIN SalesType ST ON ST.SalesTypeID = EM.SalesTypeID
	WHERE
		(@SiteID = 0 OR CR.SiteID = @SiteID)
	AND	CAST(FLOOR(CAST(ISNULL(EMCR_Times.StartDateTime, CRO.FromDate) as money)) as DateTime)  <= @DateTo
	AND	CAST(FLOOR(CAST(ISNULL(EMCR_Times.EndDateTime, CRO.ToDate) as money)) as DateTime) >= @DateFrom
	AND (CRO.EventModuleID <= 0 OR S.EffectCapacity = 1)	
	AND (@ExcludeAdHoc = 0 OR CRO.BookingType <> 'M')
	AND (@IncludeOutOfServiceRooms = 1 OR CRO.BookingType <> 'O')
	AND (@IncludeUnavailableRooms = 1 OR CRO.BookingType <> 'X')
	AND (@ExcludeInternal = 0 OR ISNULL(ST.Internal,1) = 0)
	UPDATE B
	SET		DeleteMe = 1
	FROM	@Bookings B
	WHERE	B.EventModuleID > 0
	AND		Exists (SELECT NULL FROM @Bookings B1
					 WHERE B1.EventModuleID = B.EventModuleID
					  AND B1.ConferenceRoomID = B.ConferenceRoomID
					  AND  B1.RoomOccupantID <> B.RoomOccupantID
					  AND B.BookingType = 'C' AND B1.BookingType = 'N'
					  AND B1.DateFrom <= B.DateTo
					  AND B1.DateTo >= B.DateFrom)
	DELETE FROM @Bookings WHERE DeleteME = 1
	DECLARE curOccupants CURSOR LOCAL FOR
	SELECT
		CRO.ConferenceRoomID,
		CRO.BookingType,
		CRO.ConferenceRoomTypeID,
		CAST(FLOOR(CAST(CRO.DateFrom AS money)) AS DateTime),
		CAST(FLOOR(CAST(CRO.DateTo AS money)) AS DateTime),
		CAST(CAST(CRO.DateFrom AS money) - FLOOR(CAST(CRO.DateFrom AS money)) AS DateTime),
		CAST(ISNULL(NULLIF(CAST(CRO.DateTo AS money) - FLOOR(CAST(CRO.DateTo AS money)), 0), 0) AS DateTime)
	FROM
		@Bookings CRO
	ORDER BY
		CRO.ConferenceRoomID,
		CRO.DateFrom,
		CRO.DateTo
	DECLARE
		@ConferenceRoomID int,
		@BookingType char(1),
		@ConferenceRoomTypeID int,
		@StartDate datetime,
		@EndDate datetime,
		@StartTime datetime,
		@EndTime datetime,
		@Date datetime,
		@DayLength float,
		@DayStartTime datetime,
		@DayEndTime datetime,
		@Occupancy float,
		@OccupancyInc float,
		@OriginalStartDate Datetime,
		@OriginalEndDate DateTime
	SET @DayLength = CAST(@TimeTo AS Float) - CAST(@TimeFrom AS Float)
	SET @StartDate = @DateFrom
	SET @EndDate = @DateTo
	OPEN curOccupants
	FETCH curOccupants INTO @ConferenceRoomID, @BookingType, @ConferenceRoomTypeID, @StartDate, @EndDate, @StartTime, @EndTime
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @OriginalStartDate = @StartDate
		SET @OriginalEndDate = @EndDate
		IF @StartDate < @DateFrom SET @StartDate = @DateFrom
		IF @EndDate > @DateTo SET @EndDate = @DateTo
		SET @Date=@StartDate
		WHILE @Date <= @EndDate
		BEGIN
			IF DATENAME(dw, @Date) IN (SELECT DayName FROM @Days)
			BEGIN
				IF @Date = @OriginalStartDate
					SET @DayStartTime = @StartTime
				ELSE
					SET @DayStartTime = @TimeFrom
				IF @Date = @OriginalEndDate
					SET @DayEndTime = @EndTime
				ELSE
					SET @DayEndTime = @TimeTo
				IF @DayStartTime < @TimeFrom AND @DayEndTime >= @TimeFrom
					SET @DayStartTime = @TimeFrom
				IF @DayEndTime > @TimeTo AND @DayStartTime <= @TimeTo
					SET @DayEndTime = @TimeTo
				IF @DayStartTime <= @TimeTo AND @DayEndTime >= @TimeFrom
				begin
					SET @OccupancyInc = (cast(@DayEndTime as float) - cast(@DayStartTime as float)) / @DayLength
				end
				ELSE
					SET @OccupancyInc = 0
				IF @DailyOccupancy = 1
					SET @OccupancyInc = 1
				IF EXISTS(SELECT * FROM @OccupancyTable WHERE ConferenceRoomID=@ConferenceRoomID AND Date=@Date AND BookingType=@BookingType AND ISNULL(ConferenceRoomTypeID, '')=ISNULL(@ConferenceRoomTypeID, ''))
				BEGIN
					SELECT @Occupancy = Occupancy FROM @OccupancyTable WHERE ConferenceRoomID=@ConferenceRoomID AND Date=@Date AND BookingType=@BookingType AND ISNULL(ConferenceRoomTypeID, '')=ISNULL(@ConferenceRoomTypeID, '')
					UPDATE @OccupancyTable SET Occupancy = @Occupancy + @OccupancyInc WHERE ConferenceRoomID=@ConferenceRoomID AND Date=@Date AND BookingType=@BookingType AND ISNULL(ConferenceRoomTypeID, '')=ISNULL(@ConferenceRoomTypeID, '')
				END
				ELSE
					INSERT INTO @OccupancyTable VALUES(@ConferenceRoomID, @BookingType, @ConferenceRoomTypeID, @Date, @OccupancyInc)
			END
			SET @Date = @Date + 1
		END
		FETCH curOccupants INTO @ConferenceRoomID, @BookingType, @ConferenceRoomTypeID, @StartDate, @EndDate, @StartTime, @EndTime
	END
	CLOSE curOccupants
	DEALLOCATE curOccupants
	IF @DailyOccupancy = 1
		UPDATE @OccupancyTable SET Occupancy = 1 WHERE Occupancy > 1
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDayOfWeekNumber]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDayOfWeekNumber](@Date datetime) RETURNS int AS
BEGIN
	DECLARE @DayName varchar(255) SET @DayName = DATENAME(DW, @Date)
	DECLARE @DayNumber int
	SET @DayNumber = CASE @DayName
		WHEN 'Sunday' THEN 1
		WHEN 'Monday' THEN 2
		WHEN 'Tuesday' THEN 3
		WHEN 'Wednesday' THEN 4
		WHEN 'Thursday' THEN 5
		WHEN 'Friday' THEN 6
		WHEN 'Saturday' THEN 7
	END
	DECLARE @StartDay int SELECT @StartDay = StartDay FROM Control
	SET @DayNumber = ((@DayNumber - @StartDay + 7) % 7) + 1
	RETURN @DayNumber
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDaysOfWeekTable]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDaysOfWeekTable]
	(
	@DaysOfWeek varchar(255)
	)
RETURNS
	@Days
TABLE
	(
	DayName varchar(255)
	)
AS
BEGIN
	DECLARE @WeekDays TABLE (DayName varchar(255))
	INSERT INTO
		@WeekDays
	SELECT TOP 7
		DATENAME(dw, Date) AS DayName
	FROM
		Dates
	ORDER BY
		Date
	INSERT INTO
		@Days
	SELECT
		DayName
	FROM
		@WeekDays
	WHERE
		@DaysOfWeek = '' OR ';' + @DaysOfWeek + ';' LIKE '%;' + DayName + ';%'
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDefaultDueDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDefaultDueDate](@EventModuleID int) RETURNS datetime AS
BEGIN
	DECLARE @PaymentTermsID int
	SET @PaymentTermsID = dbo.fnPaymentTermsID(@EventModuleID)
	DECLARE @DateType varchar(255), @Period int
	SELECT
		@DateType = PeriodDateFrom,
		@Period = Period
FROM
		PaymentTerms
	WHERE
		PaymentTermsID = @PaymentTermsID
	DECLARE @Date datetime
	SELECT
		@Date = CASE @DateType
					WHEN 'Start Date' THEN EM.StartDate
					WHEN 'End Date' THEN EM.EndDate
					ELSE CURRENT_TIMESTAMP
				END
	FROM
		EventModules EM
	WHERE
		EM.EventModuleID = @EventModuleID
	DECLARE @DueDate datetime
	SET @DueDate = dbo.fnFloorDate(@Date) + @Period
	RETURN @DueDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDefaultPaymentTerms]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDefaultPaymentTerms](@EventTypeID int) RETURNS int AS
BEGIN
	DECLARE @PaymentTermsID int, @EventTypePaymentTermsID int
	SELECT
		@PaymentTermsID = PaymentTermsID
	FROM
		PaymentTerms
	WHERE
		DefaultTerms = 1
	SELECT
		@PaymentTermsID = DefaultPaymentTermsID
	FROM
		EventTypes
	WHERE
		EventTypeID = @EventTypeID
	AND	DefaultPaymentTermsID > 0
	RETURN @PaymentTermsID
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDelegateDietDifferences]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDelegateDietDifferences](@EventModuleID int)
RETURNS
	@Differences TABLE(DietID int, SpecialDiet varchar(255), Date datetime, Number int, Delegates int)
AS
BEGIN
	DECLARE @DelegateDiets TABLE (DietID int, Date datetime, Number int)
	INSERT INTO
		@DelegateDiets
	SELECT
		PD.DietID,
		D.Date,
		COUNT(DISTINCT EMD.PersonID)
	FROM
		EventModuleDelegates EMD
		JOIN PeopleDiets PD ON PD.PersonID = EMD.PersonID
		JOIN Dates D ON D.Date BETWEEN dbo.fnFloorDate(EMD.ArrivalDate) AND dbo.fnFloorDate(EMD.DepartureDate)
	WHERE
		EMD.EventModuleID = @EventModuleID
	GROUP BY
		PD.DietID,
		D.Date
	DECLARE @DelegateSpecialDiets TABLE (SpecialDiet varchar(255), Date datetime, Number int)
	INSERT INTO
		@DelegateSpecialDiets
	SELECT
		EMSDR.SpecialDiet,
		D.Date,
		COUNT(DISTINCT EMD.PersonID)
	FROM
		EventModuleDelegateSpecialDiets EMSD
		JOIN EventModuleSpecialDietRequirements EMSDR ON EMSDR.EMSpecialDietRequirementID = EMSD.SpecialDietID
		JOIN EventModuleDelegates EMD ON EMD.EventModuleDelegateID = EMSD.EventModuleDelegateID
		JOIN Dates D ON D.Date BETWEEN dbo.fnFloorDate(EMD.ArrivalDate) AND dbo.fnFloorDate(EMD.DepartureDate)
	WHERE
		EMSDR.EventModuleID = @EventModuleID
	GROUP BY
		EMSDR.SpecialDiet,
		D.Date
	DECLARE @EventModuleDiets TABLE (DietID int, Date datetime, Number int)
	INSERT INTO
		@EventModuleDiets
	SELECT
		EMDR.DietID,
		EMDR.Date,
		EMDR.Quantity
	FROM
		EventModuleDietRequirements EMDR
	WHERE
		EMDR.EventModuleID = @EventModuleID
	AND	EMDR.Date > 0
	INSERT INTO
		@EventModuleDiets
	SELECT
		EMDR.DietID,
		D.Date,
		EMDR.Quantity
	FROM
		EventModuleDietRequirements EMDR
		JOIN EventModules EM ON EM.EventModuleID = EMDR.EventModuleID
		JOIN Dates D ON D.Date BETWEEN dbo.fnFloorDate(EM.ArrivalDate) AND dbo.fnFloorDate(EM.EndDate)
	WHERE
		EMDR.EventModuleID = @EventModuleID
	AND	EMDR.Date <= 0
	AND	NOT EXISTS(SELECT * FROM @EventModuleDiets WHERE DietID = EMDR.DietID AND Date = D.Date)
	DECLARE @EventModuleSpecialDiets TABLE (SpecialDiet varchar(255), Date datetime, Number int)
	INSERT INTO
		@EventModuleSpecialDiets
	SELECT
		EMSDR.SpecialDiet,
		EMSDR.Date,
		EMSDR.Quantity
	FROM
		EventModuleSpecialDietRequirements EMSDR
	WHERE
		EMSDR.EventModuleID = @EventModuleID
	AND	IsNull(EMSDR.Date, 0) > 0
	INSERT INTO
		@EventModuleSpecialDiets
	SELECT
		EMSDR.SpecialDiet,
		D.Date,
		EMSDR.Quantity
	FROM
		EventModuleSpecialDietRequirements EMSDR
		JOIN EventModules EM ON EM.EventModuleID = EMSDR.EventModuleID
		JOIN Dates D ON D.Date BETWEEN dbo.fnFloorDate(EM.ArrivalDate) AND dbo.fnFloorDate(EM.EndDate)
	WHERE
		EMSDR.EventModuleID = @EventModuleID
	AND	IsNull(EMSDR.Date, 0) <= 0
	AND	NOT EXISTS(SELECT * FROM @EventModuleSpecialDiets WHERE SpecialDiet = EMSDR.SpecialDiet AND Date = D.Date)
	INSERT INTO
		@Differences
	SELECT
		ISNULL(DD.DietID, EMD.DietID),
		NULL,
		ISNULL(DD.Date, EMD.Date),
		EMD.Number,
		DD.Number
	FROM
		@EventModuleDiets EMD
		FULL JOIN @DelegateDiets DD ON DD.DietID = EMD.DietID AND DD.Date = EMD.Date
	WHERE
		ISNULL(EMD.Number, 0) <> ISNULL(DD.Number, 0)
	INSERT INTO
		@Differences
	SELECT
		NULL,
		ISNULL(DSD.SpecialDiet, EMSD.SpecialDiet),
		ISNULL(DSD.Date, EMSD.Date),
		EMSD.Number,
		DSD.Number
	FROM
		@EventModuleSpecialDiets EMSD
		FULL JOIN @DelegateSpecialDiets DSD ON DSD.SpecialDiet = EMSD.SpecialDiet AND DSD.Date = EMSD.Date
	WHERE
		ISNULL(EMSD.Number, 0) <> ISNULL(DSD.Number, 0)
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDelegateRoutingDescription]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnDelegateRoutingDescription]
	(
	@EventModuleID int,
	@PersonID int,
	@PersonSequenceID int
	)
RETURNS
	varchar(255)
AS
BEGIN
	DECLARE @RoutingStyle int -- 0= Default Routing  1=All to personal  2=All to main  3=Nightly postings to main  4=Route to other delegate 5=Custom routing
	DECLARE @RouteChargesToPersonID int
	DECLARE @RouteChargesToPersonName varchar(255)
	DECLARE @DelegateRoutingDescription varchar(255)
	SELECT
		@RoutingStyle = EMD.RoutingStyle,
		@RouteChargesToPersonID = P.PersonID,
		@RouteChargesToPersonName = LTRIM(RTRIM(P.Title) + ' ' + RTRIM(P.Forename) + ' ' + RTRIM(P.Surname))
	FROM
		EventModuleDelegates EMD
		LEFT JOIN People P ON P.PersonID = EMD.RouteChargesToPersonID
	WHERE
		EMD.EventModuleID = @EventModuleID
	AND EMD.PersonID = @PersonID
	AND ISNULL(EMD.PersonSequenceID, 0) = ISNULL(@PersonSequenceID, 0)
	IF @RoutingStyle = 4 AND @RouteChargesToPersonID > 0
		SET @DelegateRoutingDescription = 'To guest: ' + @RouteChargesToPersonName
	ELSE IF @RoutingStyle = 0
		SET @DelegateRoutingDescription = 'Default routing'
	ELSE IF @RoutingStyle = 1
		SET @DelegateRoutingDescription = 'Charges to personal account'
	ELSE IF @RoutingStyle = 2
		SET @DelegateRoutingDescription = 'Charges to main account'
	ELSE IF @RoutingStyle = 3
 		SET @DelegateRoutingDescription = 'Nightly postings to main a/c'
	ELSE IF @RoutingStyle = 5
 		SET @DelegateRoutingDescription = 'Custom routing'
	ELSE
		SET @DelegateRoutingDescription = 'Unknown routing'
	RETURN @DelegateRoutingDescription
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDepartmentNoteAllDates]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnDepartmentNoteAllDates]
( 
        @EventModuleDepartmentNoteID int
) 
RETURNS BIT
AS 
BEGIN
DECLARE @Result bit
SELECT DISTINCT
	@Result = CAST (
	CASE WHEN (
		SELECT COUNT(DISTINCT D.Date)
		FROM EventModuleDayDepartmentNotes N1
		JOIN EventModuleDays D ON D.EventModuleDayID = N1.EventModuleDayID
		JOIN EventModuleDepartmentNotes N2 ON N2.EventModuleDepartmentNoteID = N1.EventModuleDepartmentNoteID
		WHERE N1.EventModuleDepartmentNoteID = @EventModuleDepartmentNoteID 
		)
		-	
		(
		SELECT Count(DISTINCT D1.Date) 
		FROM EventModuleDayDepartmentNotes N1
		JOIN EventModuleDays D ON D.EventModuleDayID = N1.EventModuleDayID
		JOIN EventModules EM ON EM.EventModuleID = D.EventModuleID
		JOIN Dates D1 ON D1.Date BETWEEN CAST(EM.ArrivalDate as Date) AND CAST(EM.DepartureDate as Date)
		WHERE N1.EventModuleDepartmentNoteID = @EventModuleDepartmentNoteID 
	) <> 0 THEN 0 ELSE 1 END as bit)
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnEventChargeTypeRateDetails]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnEventChargeTypeRateDetails](@EventID int)
RETURNS
	@Details
TABLE
	(
	EventChargePeriodID int,
	ChargeTypeID int,
	MatrixID int,
	Rate money,
	DetailChargeTypeID int,
	DetailMatrixID int,
	DetailRate money,
	AbsorbsDiscount bit,
	AbsorbsVATRounding bit
	)
AS
BEGIN
	INSERT INTO
		@Details
	SELECT
		RD.EventChargePeriodID,
		RD.ChargeTypeID,
		ISNULL(ECTR.MatrixID, 0),
		ECTR.Rate,
		RD.DetailChargeTypeID,
		RD.DetailMatrixID,
		RD.Rate,
		ISNULL(M.AbsorbsDiscount, HD.AbsorbsDiscount),
		ISNULL(M.AbsorbsVATRounding, HD.AbsorbsVATRounding)
	FROM
		EventChargePeriods ECP
		JOIN EventChargeTypeRates ECTR ON ECTR.EventChargePeriodID = ECP.EventChargePeriodID
		JOIN EventChargeTypeRateDetails RD ON RD.EventChargePeriodID = ECP.EventChargePeriodID AND RD.ChargeTypeID = ECTR.ChargeTypeID
		LEFT JOIN ChargeTypeRateMatrix M ON M.MatrixID = RD.DetailMatrixID
		LEFT JOIN ChargeTypeHeaderDetails HD ON HD.HeaderChargeTypeID = RD.ChargeTypeID AND HD.DetailChargeTypeID = RD.DetailChargeTypeID
	WHERE
		ECP.EventID = @EventID
	INSERT INTO
		@Details
	SELECT
		ECP.EventChargePeriodID,
		ECTR.ChargeTypeID,
		ISNULL(ECTR.MatrixID, 0),
		ECTR.Rate,
		M.ChargeTypeID,
		M.MatrixID,
		ISNULL(ROUND(NULLIF(M.SplitPercent, 0) * ECTR.Rate / 100.0, 2), M.Rate),
		M.AbsorbsDiscount,
		M.AbsorbsVATRounding
	FROM
		EventChargePeriods ECP
		JOIN EventChargeTypeRates ECTR ON ECTR.EventChargePeriodID = ECP.EventChargePeriodID
		JOIN ChargeTypeRateMatrix M ON M.ParentMatrixID = ECTR.MatrixID AND ECTR.MatrixID > 0
	WHERE
		ECP.EventID = @EventID
	AND	NOT EXISTS(SELECT NULL FROM @Details WHERE EventChargePeriodID = ECP.EventChargePeriodID AND ChargeTypeID = ECTR.ChargeTypeID)
	INSERT INTO
		@Details
	SELECT
		ECP.EventChargePeriodID,
		HD.HeaderChargeTypeID,
		ISNULL(ECTR.MatrixID, 0),
		ECTR.Rate,
		HD.DetailChargeTypeID,
		0,
		ISNULL(ROUND(NULLIF(HD.SplitPercent, 0) * ECTR.Rate / 100.0, 2), HD.Amount),
		HD.AbsorbsDiscount,
		HD.AbsorbsVATRounding
	FROM
		EventChargePeriods ECP
		JOIN EventChargeTypeRates ECTR ON ECTR.EventChargePeriodID = ECP.EventChargePeriodID
		JOIN ChargeTypeHeaderDetails HD ON HD.HeaderChargeTypeID = ECTR.ChargeTypeID AND ISNULL(ECTR.MatrixID, 0) = 0
	WHERE
		ECP.EventID = @EventID
	AND	NOT EXISTS(SELECT NULL FROM @Details WHERE EventChargePeriodID = ECP.EventChargePeriodID AND ChargeTypeID = ECTR.ChargeTypeID)
	UPDATE
		D
	SET
		DetailRate = Rate - F.Fixed
	FROM
		@Details D
		JOIN (
		SELECT
			EventChargePeriodID,
			ChargeTypeID,
			SUM(DetailRate) AS Fixed
		FROM
			@Details
		WHERE
			AbsorbsDiscount = 0
		GROUP BY
			EventChargePeriodID,
			ChargeTypeID
		) F ON F.EventChargePeriodID = D.EventChargePeriodID AND F.ChargeTypeID = D.ChargeTypeID
	WHERE
		D.AbsorbsDiscount = 1
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnEventModuleChangesMadeSince]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnEventModuleChangesMadeSince](@EventModuleID int, @ChangesMadeSince datetime, @ReportName varchar(255), @ID int = 0) RETURNS bit AS
BEGIN
	RETURN
		CASE 
			WHEN @ReportName = 'Events Running' THEN
				CAST(CASE WHEN EXISTS(
					SELECT 
						1 
					FROM 
						EventModuleAuditLogs 
					WHERE 
						EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
						AND (
								(Category  = 'Details' AND Operation = 'Edit' AND (CHARINDEX('Date', AuditLog) > 0) OR (CHARINDEX('Time', AuditLog) > 0))
								OR (Category  = 'Meeting Rooms' AND TableName = 'EventModuleConferenceRooms')
							)	
					) THEN 1 ELSE 0 END AS bit) 
			WHEN @ReportName = 'Schedule of Room and Catering Bookings' THEN
				CAST(CASE WHEN EXISTS(
					SELECT 1 FROM EventModuleAuditLogs
					WHERE 
						Category  = 'Meeting Rooms' 
						AND TableName = 'EventModuleConferenceRooms'
						AND EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
					UNION
					SELECT 1 FROM CateringAuditLog
					WHERE 
						EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
					) THEN 1 ELSE 0 END AS bit)
			WHEN @ReportName = 'Whats On?' THEN
				CAST(CASE WHEN EXISTS(
					SELECT 
						1 
					FROM 
						EventModuleAuditLogs 
					WHERE 
						EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
						AND (
								(Category  = 'Details' AND Operation = 'Edit' AND CHARINDEX('Date', AuditLog) > 0)
								OR (Category  = 'Meeting Rooms' AND TableName = 'EventModuleConferenceRooms')
								OR (Category  = 'Bedrooms')
							)	
					UNION
					SELECT 1 FROM CateringAuditLog
					WHERE 
						EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
					) THEN 1 ELSE 0 END AS bit)
			WHEN @ReportName = 'Room Usage and Equipment Requirements' THEN
				CAST(CASE WHEN EXISTS(
					SELECT 
						1 
					FROM 
						EventModuleAuditLogs EMAL
					JOIN EventModuleConferenceRooms EMCR ON EMCR.EventModuleConferenceRoomID = EMAL.IDValue
					WHERE 
						EMAL.EventModuleID = @EventModuleID 
						AND EMAL.IDField = 'EventModuleConferenceRoomID'
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
						AND (Category  = 'Meeting Rooms')
						AND (@ID = 0 OR EMCR.ConferenceRoomID = @ID)
					)
					OR 
					EXISTS(
					SELECT 
						1 
					FROM 
						EventModuleAuditLogs EMAL
					JOIN EventModuleConferenceRoomFacilities EMCRF ON EMCRF.EventConferenceRoomFacilityID = EMAL.IDValue
					WHERE 
						EMAL.EventModuleID = @EventModuleID 
						AND EMAL.IDField = 'EventConferenceRoomFacilityID'
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
						AND (Category  = 'Meeting Rooms')
						AND (@ID = 0 OR EMCRF.ConferenceRoomID = @ID)
					)					
					 THEN 1 ELSE 0 END AS bit) 
			WHEN @ReportName IN ('Conference Room Usage', 'Setup and Take Down Report') THEN
				CAST(CASE WHEN EXISTS(
					SELECT 
						1 
					FROM 
						EventModuleAuditLogs 
					WHERE 
						EventModuleID = @EventModuleID 
						AND dbo.fnFloorDate(CreationDate) >= @ChangesMadeSince
						AND (Category  = 'Meeting Rooms' AND TableName = 'EventModuleConferenceRooms')
					) THEN 1 ELSE 0 END AS bit) 
		END
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnEventModuleChargeTypeNominal]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnEventModuleChargeTypeNominal]
	(
	@EventModuleID int,
	@ChargeTypeID int,
	@HeaderChargeTypeID int,
	@MatrixID int
	)
RETURNS
	integer
AS
BEGIN
	RETURN dbo.fnChargeTypeNominal(@EventModuleID, @ChargeTypeID, @HeaderChargeTypeID, @MatrixID, 0)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentAgeOfDebt]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentAgeOfDebt]
( 
    @KxStudentID int
) 
RETURNS int
AS
BEGIN
DECLARE
	@AgeOfDebt int
SELECT
	@AgeOfDebt = DATEDIFF(day, MIN(CreationDate), CURRENT_TIMESTAMP)
FROM
(
SELECT
	SC.StudentChargeID,
	SC.TotalAmount AS TotalAmount,
	SI.CreationDate,
	ISNULL(SUM(CASE WHEN SC.TotalAmount < 0 THEN -COALESCE(PA.AmountAllocated, PA2.AmountAllocated, 0) ELSE COALESCE(PA.AmountAllocated, PA2.AmountAllocated, 0) END), 0) AS AmountAllocated
FROM
	ST2StudentCharges SC
	JOIN ST2StudentInvoices SI ON SI.InvoiceNumber = SC.InvoiceNumber
	LEFT OUTER JOIN ST2PaymentAllocations PA ON SC.StudentChargeID = PA.FromStudentChargeID
	LEFT OUTER JOIN ST2PaymentAllocations PA2 ON SC.StudentChargeID = PA2.ToStudentChargeID
WHERE
	SC.KxStudentID = @KxStudentID
	AND SC.DueDate < CURRENT_TIMESTAMP
	--AND SC.DateFrom < CURRENT_TIMESTAMP
GROUP BY
	SC.StudentChargeID,
	SC.TotalAmount,
	SI.CreationDate
) D
WHERE
	D.TotalAmount - D.AmountAllocated <> 0
RETURN @AgeOfDebt
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentAllocationCheckIn]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentAllocationCheckIn]
( 
   @AllocationD int
) 
RETURNS datetime
AS
BEGIN
DECLARE
	@AllocDate datetime
SELECT
	@AllocDate = MIN(CheckedInDate)
FROM
	ST2StudentAllocationDates SP
WHERE
	SP.AllocationID = @AllocationD
RETURN @AllocDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentAllocationCheckOut]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentAllocationCheckOut]
( 
   @AllocationID int
) 
RETURNS datetime
AS
BEGIN
DECLARE
	@AllocDate datetime
SELECT
	@AllocDate = MAX(CheckedOutDate)
FROM
	ST2StudentAllocationDates SP
WHERE
	SP.AllocationID = @AllocationID
RETURN @AllocDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentAllocationEnd]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentAllocationEnd]
( 
   @AllocationID int
) 
RETURNS datetime
AS
BEGIN
DECLARE
	@AllocDate datetime
SELECT
	@AllocDate = MAX(DepartureDate)
FROM
	ST2StudentAllocationDates SP
WHERE
	SP.AllocationID = @AllocationID
RETURN @AllocDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentAllocationStart]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentAllocationStart]
( 
   @AllocationD int
) 
RETURNS datetime
AS
BEGIN
DECLARE
	@AllocDate datetime
SELECT
	@AllocDate = MIN(ArrivalDate)
FROM
	ST2StudentAllocationDates SP
WHERE
	SP.AllocationID = @AllocationD
RETURN @AllocDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentCurrentDebt]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentCurrentDebt]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE
	@CurrentDebt money,
	@UninvoicedCharges money,
	@Invoiced int
SET @Invoiced = ISNULL((SELECT Value FROM CustomSettings WHERE CustomSetting = 'Student.DebtInvoiced'), 1)
SELECT
	@CurrentDebt = ISNULL(SUM(TotalAmount - AmountAllocated), 0)
FROM
	(
	SELECT
		SC.StudentChargeID,
		SC.TotalAmount AS TotalAmount,
		ISNULL(SUM(CASE WHEN SC.TotalAmount < 0 THEN -COALESCE(PA.AmountAllocated, PA2.AmountAllocated, 0) ELSE COALESCE(PA.AmountAllocated, PA2.AmountAllocated, 0) END), 0) AS AmountAllocated
	FROM
		ST2StudentCharges SC
		JOIN ST2StudentInvoices SI ON SI.InvoiceNumber = SC.InvoiceNumber
		LEFT OUTER JOIN ST2PaymentAllocations PA ON SC.StudentChargeID = PA.FromStudentChargeID
		LEFT OUTER JOIN ST2PaymentAllocations PA2 ON SC.StudentChargeID = PA2.ToStudentChargeID
	WHERE
		SC.KxStudentID = @KxStudentID
		AND SC.DueDate < CURRENT_TIMESTAMP
	GROUP BY
		SC.StudentChargeID,
		SC.TotalAmount
	) D
SELECT
	@UninvoicedCharges = SUM(TotalAmount)
FROM
	ST2StudentCharges
WHERE
	DueDate < CURRENT_TIMESTAMP
	AND InvoiceNumber = 0
	AND KxStudentID = @KxStudentID
RETURN CASE WHEN @Invoiced = 1 THEN @CurrentDebt ELSE @CurrentDebt + @UninvoicedCharges END
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentDepositsUsed]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentDepositsUsed]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE
	@DepositsUsed money
SELECT
	@DepositsUsed = SUM (ABS(COALESCE(PA.AmountAllocated, PA2.AmountAllocated, 0)))
FROM
	ST2StudentPayments SP
	LEFT JOIN ST2PaymentAllocations PA ON SP.StudentPaymentID = PA.FromStudentPaymentID
	LEFT JOIN ST2PaymentAllocations PA2 ON SP.StudentPaymentID = PA2.ToStudentPaymentID
	JOIN StudentDepositTypes DT ON DT.PaymentReasonID = SP.PaymentReasonID
WHERE
	SP.KxStudentID = @KxStudentID
RETURN @DepositsUsed
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentLastPaymentAmount]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentLastPaymentAmount]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE @LastPaymentAmount money
DECLARE
	@LastPaymentDate datetime
SELECT
	@LastPaymentDate = MAX(CAST(FLOOR(CAST(ReferenceDate AS money)) AS datetime))
FROM
	ST2StudentPayments SP
WHERE
	SP.KxStudentID = @KxStudentID
SELECT TOP 1
	@LastPaymentAmount = Amount
FROM
	ST2StudentPayments
WHERE
	KxStudentID = @KxStudentID
	AND ReferenceDate = @LastPaymentDate
ORDER BY
	ReferenceDate DESC
RETURN @LastPaymentAmount
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentLastPaymentDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentLastPaymentDate]
( 
    @KxStudentID int
) 
RETURNS datetime
AS
BEGIN
DECLARE
	@LastPaymentDate datetime
SELECT
	@LastPaymentDate = MAX(CAST(FLOOR(CAST(ReferenceDate AS money)) AS datetime))
FROM
	ST2StudentPayments SP
WHERE
	SP.KxStudentID = @KxStudentID
RETURN @LastPaymentDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentTotalDeposits]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentTotalDeposits]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE
	@TotalDeposits money
SELECT
	@TotalDeposits = SUM (SP.Amount)
FROM
	ST2StudentPayments SP
	JOIN StudentDepositTypes DT ON DT.PaymentReasonID = SP.PaymentReasonID
WHERE
	SP.KxStudentID = @KxStudentID
RETURN @TotalDeposits
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentTotalExposure]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentTotalExposure]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE @TotalExposure money,
	@work_Amount money
SELECT
	@work_Amount = SUM(TotalAmount)
FROM
	ST2StudentCharges
WHERE
	KxStudentID = @KxStudentID
SET @TotalExposure = ISNULL(@work_Amount, 0)
SELECT
	@work_Amount = SUM(PM.Amount)
FROM
	ST2StudentPayments PM
	JOIN StudentPaymentReasons SPR ON SPR.PaymentReasonID = PM.PaymentReasonID
WHERE
	KxStudentID = @KxStudentID
	AND SPR.MemoPayment = 0
SET @TotalExposure = @TotalExposure - ISNULL(@work_Amount, 0)
RETURN @TotalExposure
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentUnallocatedMonies]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentUnallocatedMonies]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE @Unallocatedmonies money
SELECT
	@Unallocatedmonies = ABS(SP.Amount) - ISNULL(SUM(ABS(PA.AmountAllocated)), 0)
FROM
	ST2StudentPayments SP
	LEFT OUTER JOIN
	(
		SELECT
			FromStudentPaymentID,
			ToStudentPaymentID,
			SUM(ABS(AmountAllocated)) AS AmountAllocated
		FROM
			ST2PaymentAllocations
		GROUP BY
			FromStudentPaymentID, ToStudentPaymentID
	) PA ON SP.StudentPaymentID IN (PA.FromStudentPaymentID, PA.ToStudentPaymentID)
WHERE
	SP.KxStudentID = @KxStudentID
	AND	NOT EXISTS (SELECT * FROM StudentDepositTypes SDT WHERE SDT.PaymentReasonID = SP.PaymentReasonID)
GROUP BY SP.Amount
RETURN @Unallocatedmonies
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentUninvoicedCharges]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentUninvoicedCharges]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE
	@UninvoicedCharges money
SELECT
	@UninvoicedCharges = SUM(TotalAmount)
FROM
	ST2StudentCharges
WHERE
	DueDate < CURRENT_TIMESTAMP
	AND InvoiceNumber = 0
	AND KxStudentID = @KxStudentID
RETURN @UninvoicedCharges
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnExportStudentWrittenOff]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnExportStudentWrittenOff]
( 
    @KxStudentID int
) 
RETURNS money
AS
BEGIN
DECLARE @WrittenOff money
SELECT
	@WrittenOff = SUM(Amount)
FROM
	ST2DebtWriteOff
WHERE
	KxStudentID = @KxStudentID
RETURN @WrittenOff
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnFloorDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnFloorDate] (@DateTime DATETIME)  
RETURNS DATETIME AS  
BEGIN 
	RETURN CAST(CAST(@DateTime AS date) AS DateTime)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnFormatMultiCodes]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnFormatMultiCodes]
	(
	@Code varchar(255)
	)
RETURNS
	varchar(255)
AS
BEGIN
	DECLARE @Count int SET @Count = 0
	SELECT
		@Code = REPLACE(@Code, ShortPrefix + '=', ''),
		@Count = @Count + 1
	FROM
		ExternalAccountCodeMappings
	SELECT
		@Code = REPLACE(@Code, ';', NominalCodeSeparator)
	FROM
		Control
	WHERE
		@Count > 0
	RETURN @Code
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDateGroup]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnGetDateGroup](@DateTime Datetime)
RETURNS Varchar(15) AS  
BEGIN
  DECLARE
	  @ADaysBetween Int,
    @iDaysSinceStartOfWeek int,
    @iDaysSinceStartOfLastWeek int,
    @iDaysSinceStartOfMonth int,
    @iDaysSinceStartOfYear int,
    @StartOfWeek DateTime,
    @StartOfMonth DateTime,
    @StartOfYear DateTime
	DECLARE
		@Result Varchar(15)
  SELECT @StartOfWeek = dbo.fnGetStartOfGivenDate(GETDATE(), 1)
	SELECT @iDaysSinceStartOfWeek = DATEDIFF(day, @StartOfWeek, GETDATE())
  SELECT @iDaysSinceStartOfLastWeek = @iDaysSinceStartOfWeek + 7
  SELECT @StartOfMonth = dbo.fnGetStartOfGivenDate(GETDATE(), 2)
  SELECT @iDaysSinceStartOfMonth = DATEDIFF(day, @StartOfMonth, GETDATE())
  SELECT @StartOfYear = dbo.fnGetStartOfGivenDate(GETDATE(), 3)
  SELECT @iDaysSinceStartOfYear = DATEDIFF(day, @StartOfYear, GETDATE())
  SELECT @ADaysBetween = DATEDIFF(day, @DateTime, GETDATE())
  --IF @DateTime > GETDATE() 
   --SET @ADaysBetween = @ADaysBetween * -1
  IF @ADaysBetween < 0 
   SET @Result = 'Future'
  ELSE IF @ADaysBetween = 0 
   SET @Result = 'Today'
  ELSE IF @ADaysBetween = 1 
   SET @Result = 'Yesterday'
  ELSE IF @ADaysBetween <= @iDaysSinceStartOfWeek 
   SET @Result = 'This Week'
  ELSE IF @ADaysBetween <= @iDaysSinceStartOfLastWeek 
   SET @Result = 'Last Two Weeks'
  ELSE IF @ADaysBetween <= @iDaysSinceStartOfMonth 
   SET @Result = 'This Month'
  ELSE IF @ADaysBetween <= @iDaysSinceStartOfYear 
   SET @Result = 'This Year'
  ELSE
   SET @Result = 'Previous Years'
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDisabledAttributeString]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnGetDisabledAttributeString]
( 
        @Disabled bit
) 
RETURNS varchar(255)
AS 
BEGIN
DECLARE @Result varchar(255)
SET @Result = ''
IF @Disabled = 1
	SELECT @Result = IsNull((SELECT TOP 1 Value FROM CustomSettings WHERE CustomSetting = 'Kx.Room.Attribute.Disabled.Text'), 'Disabled')
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetFormattedInvoiceText]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetFormattedInvoiceText](@ChargeTypeID int, @MatrixID int, @PersonID int)
RETURNS @InvoiceText TABLE (InvoiceText1 varchar(255), InvoiceText2 varchar(255))
AS
BEGIN
	DECLARE
		@CT varchar(255), @ST varchar(255), @RN varchar(255),
		@Template1 varchar(255), @Template2 varchar(255),
		@InvoiceText1 varchar(255), @InvoiceText2 varchar(255),
		@ILCT varchar(255), @ILST varchar(255)
	SELECT
		@CT = CT.InvoiceText1,
		@ST = M.ExternalDescription,
		@RN = RTRIM(COALESCE(CR.Name, F.Description, RTRIM(SB.Name) + ': ' + BT.Description, ''))
	FROM
		ChargeTypes CT
		LEFT JOIN ChargeTypeRateMatrix M ON M.ChargeTypeID = CT.ChargeTypeID AND M.MatrixID = @MatrixID
		LEFT JOIN ConferenceRooms CR ON CR.ConferenceRoomID = M.ConferenceRoomID
		LEFT JOIN Facilities F ON F.FacilityID = M.FacilityID
		LEFT JOIN SiteBlocks SB ON SB.BlockID = M.BlockID
		LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = M.BedroomTypeID
	WHERE
		CT.ChargeTypeID = @ChargeTypeID
	SET @ILCT = @CT
	SET @ILST = @ST
	IF @PersonID > 0
	BEGIN
		SELECT
			@ILCT = ILTC.AlternateText,
			@ILST = ILTM.AlternateText
		FROM
			People P
			JOIN InvoiceLanguages IL ON IL.InvoiceLanguage = P.InvoiceLanguage
			LEFT JOIN InvoiceLanguageTexts ILTC ON ILTC.InvoiceLanguageID = IL.InvoiceLanguageID AND ILTC.ChargeTypeID = @ChargeTypeID AND @ChargeTypeID <> 0
			LEFT JOIN InvoiceLanguageTexts ILTM ON ILTM.InvoiceLanguageID = IL.InvoiceLanguageID AND ILTM.MatrixID = @MatrixID AND @MatrixID <> 0
		WHERE
			P.PersonID = @PersonID
		IF @ILCT <> '' SET @CT = @ILCT
		IF @ILST <> '' SET @ST = @ILST
	END
	IF (@ST = '') and (@RN = '')
		SET @Template1 = '[CT]'
	ELSE IF (@ST <> '') and (@RN= '') AND EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Price But No Resource')
		SET @Template1 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Price But No Resource')
	ELSE IF (@ST = '') and (@RN <> '') AND EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Resource But No Price')
		SET @Template1 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Resource But No Price')
	ELSE
		SET @Template1 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format')
	IF (@ST = '') and (@RN = '')
		SET @Template2 = '[CT]'
	ELSE IF (@ST <> '') and (@RN= '') AND EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'InvoiceText2 Format Where Price But No Resource')
		SET @Template2 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText2 Format Where Price But No Resource')
	ELSE IF (@ST = '') and (@RN <> '') AND EXISTS(SELECT * FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Resource But No Price')
		SET @Template2 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText Format Where Resource But No Price')
	ELSE
		SET @Template2 = (SELECT Value FROM CustomSettings WHERE CustomSetting = 'InvoiceText2 Format')
	SET @Template1 = REPLACE(@Template1, 'CT', '[CT]')
	SET @Template1 = REPLACE(@Template1, 'ST', '[ST]')
	SET @Template1 = REPLACE(@Template1, 'RN', '[RN]')
	SET @Template1 = REPLACE(@Template1, '[[CT]]', '[CT]')
	SET @Template1 = REPLACE(@Template1, '[[ST]]', '[ST]')
	SET @Template1 = REPLACE(@Template1, '[[RN]]', '[RN]')
	SET @Template2 = REPLACE(@Template2, 'CT', '[CT]')
	SET @Template2 = REPLACE(@Template2, 'ST', '[ST]')
	SET @Template2 = REPLACE(@Template2, 'RN', '[RN]')
	SET @Template2 = REPLACE(@Template2, '[[CT]]', '[CT]')
	SET @Template2 = REPLACE(@Template2, '[[ST]]', '[ST]')
	SET @Template2 = REPLACE(@Template2, '[[RN]]', '[RN]')
	SET @InvoiceText1 = @Template1
	SET @InvoiceText2 = @Template2
	IF @InvoiceText1 <> ''
	BEGIN
		SET @InvoiceText1 = REPLACE(@InvoiceText1, '[CT]', @CT)
		SET @InvoiceText1 = REPLACE(@InvoiceText1, '[ST]', @ST)
		SET @InvoiceText1 = REPLACE(@InvoiceText1, '[RN]', @RN)
	END
	IF @InvoiceText2 <> ''
	BEGIN
		SET @InvoiceText2 = REPLACE(@InvoiceText2, '[CT]', @CT)
		SET @InvoiceText2 = REPLACE(@InvoiceText2, '[ST]', @ST)
		SET @InvoiceText2 = REPLACE(@InvoiceText2, '[RN]', @RN)
	END
	INSERT INTO @InvoiceText VALUES (@InvoiceText1, @InvoiceText2)
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetGuestRoom]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnGetGuestRoom]
( 
        @EventModuleID int, @PersonID int, @PersonSequenceID int, @Type char(1) 
) 
RETURNS VARCHAR(MAX) 
AS 
BEGIN
DECLARE @Rooms varchar(max)
SET @Rooms = ''
DECLARE @Results Table (ResidentialRoomID int, Name varchar(max), Date Datetime)
INSERT INTO @Results
SELECT RRO.ResidentialRoomID, CAST(RR.Name as varchar(max)) as Name, MIN(Date) as Date
FROM ResidentialRoomOccupants RRO
JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
WHERE RRO.EventModuleID = @EventModuleID
AND RRO.PersonID = @PersonID
AND RRO.PersonSequenceID = @PersonSequenceID
AND
(
	(@Type = 'F' AND RRO.Date = (SELECT MIN(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = @EventModuleID AND RRO1.PersonID = @PersonID AND RRO1.PersonSequenceID = @PersonSequenceID))
OR	(@Type = 'L' AND RRO.Date = (SELECT MAX(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = @EventModuleID AND RRO1.PersonID = @PersonID AND RRO1.PersonSequenceID = @PersonSequenceID))
OR	(@Type = 'C' AND RRO.Date = (CAST(getdate() as Date)))
OR	(@Type = 'A')
)
GROUP BY RRO.ResidentialRoomID, CAST(RR.Name as varchar(max))
IF (@Type = 'A')
BEGIN
	SELECT @Rooms = @Rooms + Name + ','
	FROM @Results 			
	ORDER BY Date 
	IF SUBSTRING(@ROOMS, LEN(@ROOMS) ,1) = ','
		SET @ROOMS = SUBSTRING(@ROOMS, 1,  LEN(@ROOMS)-1)
END
ELSE 
	SELECT @Rooms = Name FROM @Results
RETURN  ISNull(@Rooms, 0)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetGuestRoomBlock]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnGetGuestRoomBlock]
( 
        @EventModuleID int, @PersonID int, @PersonSequenceID int, @Type char(1) 
) 
RETURNS VARCHAR(MAX) 
AS 
BEGIN
DECLARE @Rooms varchar(max)
SET @Rooms = ''
DECLARE @Results Table (ResidentialRoomID int, Name varchar(max), Date Datetime)
INSERT INTO @Results
SELECT RRO.ResidentialRoomID, CAST(RTRIM(SB.Name) as varchar(max)) as Name, MIN(Date) as Date
FROM ResidentialRoomOccupants RRO
JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RRO.ResidentialRoomID
JOIN SiteBlocks SB ON SB.BlockID = RR.BlockID
WHERE RRO.EventModuleID = @EventModuleID
AND RRO.PersonID = @PersonID
AND RRO.PersonSequenceID = @PersonSequenceID
AND
(
	(@Type = 'F' AND RRO.Date = (SELECT MIN(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = @EventModuleID AND RRO1.PersonID = @PersonID AND RRO1.PersonSequenceID = @PersonSequenceID))
OR	(@Type = 'L' AND RRO.Date = (SELECT MAX(DATE) FROM ResidentialRoomOccupants RRO1 WHERE RRO1.EventModuleID = @EventModuleID AND RRO1.PersonID = @PersonID AND RRO1.PersonSequenceID = @PersonSequenceID))
OR	(@Type = 'C' AND RRO.Date = (CAST(getdate() as Date)))
OR	(@Type = 'A')
)
GROUP BY RRO.ResidentialRoomID, CAST(RTRIM(SB.Name) as varchar(max))
IF (@Type = 'A')
BEGIN
	SELECT @Rooms = @Rooms + Name + ','
	FROM @Results 			
	ORDER BY Date 
	IF SUBSTRING(@ROOMS, LEN(@ROOMS) ,1) = ','
		SET @ROOMS = SUBSTRING(@ROOMS, 1,  LEN(@ROOMS)-1)
END
ELSE 
	SELECT @Rooms = Name FROM @Results
RETURN  ISNull(@Rooms, 0)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetSmokerAttributeString]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnGetSmokerAttributeString]
( 
        @Smoker bit
) 
RETURNS varchar(255)
AS 
BEGIN
DECLARE @Result varchar(255)
SET @Result = ''
IF @Smoker = 1
	SELECT @Result = IsNull((SELECT TOP 1 Value FROM CustomSettings WHERE CustomSetting = 'Kx.Room.Attribute.Smoking.Text'), 'Smoking')
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetStartOfGivenDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetStartOfGivenDate](@DateTime Datetime, @iSelect Int)
RETURNS DateTime AS  
BEGIN
  --function to return the beginning of the week
  --@iSelect (1=Week, 2=Month, 3=Year)
  DECLARE
	  @fnGetStartOfGivenDate DateTime
	IF @iSelect = 1
	  SET @fnGetStartOfGivenDate = DATEADD(DAY, -((@@DATEFIRST + DATEPART(dw, @DateTime) -2) % 7), @DateTime)
	ELSE IF @iSelect = 2
	  SET @fnGetStartOfGivenDate = DATEADD(DAY, -(DAY(@DateTime)-1), @DateTime)
	ELSE
	  SET @fnGetStartOfGivenDate = DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0)
	RETURN @fnGetStartOfGivenDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetVIPAttributeString]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnGetVIPAttributeString]
( 
        @Colour varchar(10)
) 
RETURNS varchar(300)
AS 
BEGIN
DECLARE @Result varchar(300)
SET @Result = ''
IF LEN(RTRIM(@Colour)) = 9 AND @Colour LIKE '$00%' 
BEGIN
	SET @Result = '{\rtf1\ansi\deff0\nouicompat{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 MS Shell Dlg 2;}{\f2\fnil Segoe UI;}}' +
		'{\colortbl ;\red' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 255 ELSE 0 END as varchar) +
		'\green' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN (CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 65280) / 255 ELSE 0 END as varchar) +
		'\blue' + CAST(CASE WHEN @Colour LIKE '$%' AND LEN(RTRIM(@Colour)) = 9 THEN (CONVERT(int, CONVERT(VARBINARY,SUBSTRING(RTrim(@Colour), 2, 8), 2)) & 16711680) / 65536 ELSE 0 END as varchar) +';}' +
		'{\*\generator Riched20 10.0.16299}\viewkind0\uc1\pard\cf1\fs26\lang2057\u9608?\u9608?\u9608?\u9608?\cf0\f1\fs17\f2\fs16\par}'
END
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnInvoiceBalance]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnInvoiceBalance](@InvoiceID int) RETURNS money AS
BEGIN
	DECLARE @Invoiced money
	DECLARE @Settled money
	DECLARE @Balance money
	SET @Balance = 0
	SELECT
		@Balance = @Balance + ISNULL(SUM(TotalAmountInvoiced), 0)
	FROM
		InvoiceHeaders IH
		JOIN InvoiceDetails ID ON ID.InvoiceID = IH.InvoiceID
	WHERE
		IH.InvoiceID = @InvoiceID
	SELECT
		@Balance = @Balance - ISNULL(SUM(Amount), 0)
	FROM
		CashTransactions CT
	WHERE
		InvoiceID = @InvoiceID
	SELECT
		@Balance = @Balance + dbo.fnInvoiceBalance(IH.InvoiceID)
	FROM
		InvoiceHeaders IH
	WHERE
		IH.CreditOfInvoiceID = @InvoiceID
	RETURN @Balance
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnInvoiceDueDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnInvoiceDueDate](@InvoiceID int) RETURNS datetime AS
BEGIN
	DECLARE @PaymentTermsID int, @InvoiceDate datetime, @StartDate datetime, @EndDate datetime, @EventTypeID int, @DueDate datetime
	SELECT
		@PaymentTermsID = ISNULL(IH.PaymentTermsID, 0),
		@InvoiceDate = IH.TaxPointDate,
		@StartDate = EM.StartDate,
		@EndDate = EM.EndDate,
		@EventTypeID = E.EventTypeID,
		@DueDate = IH.DueDate
	FROM
		InvoiceHeaders IH
		JOIN EventModules EM ON EM.EventModuleID = IH.EventModuleID
		JOIN Events E ON E.EventID = EM.EventID
	WHERE
		IH.InvoiceID = @InvoiceID
	AND	IH.InvoiceType IN ('INVREQ', 'INTINV')
	IF @PaymentTermsID = 0
		SET @PaymentTermsID = dbo.fnDefaultPaymentTerms(@EventTypeID)
	DECLARE @PeriodDateFrom varchar(255), @Period int, @EditableDueDate bit
	SELECT
		@PeriodDateFrom = PeriodDateFrom, @Period = Period, @EditableDueDate = EditableDueDate
	FROM
		PaymentTerms
	WHERE
		PaymentTermsID = @PaymentTermsID
	IF @EditableDueDate = 0
	BEGIN
		DECLARE @Date datetime
		SET @Date = CASE @PeriodDateFrom WHEN 'Invoice Date' THEN @InvoiceDate WHEN 'Start Date' THEN @StartDate WHEN 'End Date' THEN @EndDate ELSE NULL END
		SET @Date = dbo.fnFloorDate(@Date)
		SET @DueDate = DATEADD(day, @Period, @Date)
	END
	RETURN @DueDate
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnMaxAllocationSetDayQuantity]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnMaxAllocationSetDayQuantity](@AllocationSetDayQuantityID int) RETURNS int AS
BEGIN
	DECLARE
		@AllocationSetRoomTypeID int,
		@Date datetime
	SELECT
		@AllocationSetRoomTypeID = AllocationSetRoomTypeID,
		@Date = DayDate
	FROM
		AllocationSetDayQuantities
	WHERE
		AllocationSetDayQuantityID = @AllocationSetDayQuantityID
	DECLARE
		@Max int,
		@AllocationSetID int
	SELECT
		@Max = RT.InitialRoomQuantity,
		@AllocationSetID = AllocationSetID
	FROM
		AllocationSetRoomTypes RT
	WHERE
		RT.AllocationSetRoomTypeID = @AllocationSetRoomTypeID
	IF (select ReleaseStyleID from AllocationSets where AllocationSetID = @AllocationSetID) = 1
		SELECT
			@Date = MIN(DayDate)
		FROM
			AllocationSetDayQuantities
		WHERE
			AllocationSetRoomTypeID = @AllocationSetRoomTypeID
	DECLARE @DaysBefore int SET @DaysBefore = DATEDIFF(DAY, CURRENT_TIMESTAMP, @Date)
	SELECT
		@Max = ROUND(@Max * (1 - 0.01 * RS.ReleasePercent), 0)
	FROM
		AllocationReleaseSchedules RS
	WHERE
		RS.AllocationSetRoomTypeID = @AllocationSetRoomTypeID
	AND	@DaysBefore <= RS.DaysBefore
	ORDER BY
		RS.DaysBefore
	RETURN @Max
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnOriginalInvoiceID]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnOriginalInvoiceID](@InvoiceID int) RETURNS int AS
BEGIN
	DECLARE @CreditOfInvoiceID int
	SELECT
		@CreditOfInvoiceID = CreditOfInvoiceID
	FROM
		InvoiceHeaders
	WHERE
		InvoiceID = @InvoiceID
	IF @CreditOfInvoiceID > 0
		SET @CreditOfInvoiceID = dbo.fnOriginalInvoiceID(@CreditOfInvoiceID)
	ELSE
		SET @CreditOfInvoiceID = @InvoiceID
	RETURN @CreditOfInvoiceID
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnParseRTF]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnParseRTF]
( 
        @rtf VARCHAR(MAX) 
) 
RETURNS VARCHAR(MAX) 
AS 
BEGIN 
        IF @rtf IS NULL OR @rtf ='' 
                RETURN '' 
        DECLARE @Stage TABLE 
                ( 
                        Chr CHAR(1), 
                        Pos INT 
                ) 
        INSERT          @Stage 
                        ( 
                                Chr, 
                                Pos 
                        ) 
        SELECT          SUBSTRING(@rtf, Number, 1), 
                        Number 
        FROM            master..spt_values 
        WHERE           Type = 'p' 
                        AND SUBSTRING(@rtf, Number, 1) IN ('{', '}')
        DECLARE @Pos1 INT, 
                @Pos2 INT 
        SELECT  @Pos1 = MIN(Pos), 
                @Pos2 = MAX(Pos) 
        FROM    @Stage 
        DELETE 
        FROM    @Stage 
        WHERE   Pos IN (@Pos1, @Pos2) 
        WHILE 1 = 1 
                BEGIN 
                        SELECT TOP 1    @Pos1 = s1.Pos, 
                                        @Pos2 = s2.Pos 
                        FROM            @Stage AS s1 
                        INNER JOIN      @Stage AS s2 ON s2.Pos > s1.Pos 
                        WHERE           s1.Chr = '{' 
                                        AND s2.Chr = '}' 
                        ORDER BY        s2.Pos - s1.Pos 
                        IF @@ROWCOUNT = 0 
                                BREAK 
                        DELETE 
                        FROM    @Stage 
                        WHERE   Pos IN (@Pos1, @Pos2) 
                        UPDATE  @Stage 
                        SET     Pos = Pos - @Pos2 + @Pos1 - 1 
                        WHERE   Pos > @Pos2 
			IF CHARINDEX('\viewkind', SUBSTRING(@rtf, @Pos1, @Pos2 - @Pos1 + 1)) = 0
	                        SET @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, '') 
                END 
        SET     @Pos1 = PATINDEX('%\cf[0123456789][0123456789 ]%', @rtf) 
        WHILE @Pos1 > 0 
                SELECT  @Pos2 = CHARINDEX(' ', @rtf, @Pos1 + 1), 
                        @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, ''), 
                        @Pos1 = PATINDEX('%\cf[0123456789][0123456789 ]%', @rtf)
         SELECT @rtf = REPLACE(@rtf, '\pard', ''), 
                @rtf = REPLACE(@rtf, '\par', ''),               
                @rtf = REPLACE(@rtf, '\ansicpg1252', ''),
		@rtf = REPLACE(@rtf, '\lang2057', ''), 
                @rtf = REPLACE(@rtf, '\deff0', ''),
                @rtf = REPLACE(@rtf, '\viewkind4', ''),
                @rtf = REPLACE(@rtf, '\lang1033', ''),
                @rtf = REPLACE(@rtf, '\lang3081', ''),
                @rtf = REPLACE(@rtf, '\nouicompat', ''),
                @rtf = REPLACE(@rtf, '\ansi', ''),
                @rtf = REPLACE(@rtf, '\f0', ''),
                @rtf = REPLACE(@rtf, '\fs16', ''),
                @rtf = REPLACE(@rtf, '\uc1', ''),
                @rtf = REPLACE(@rtf, '\rtf1', ''),
		@rtf = REPLACE(@rtf, '\b0', ''), 
                @rtf = REPLACE(@rtf, '\bullet', ''),
                @rtf = REPLACE(@rtf, '\i0', ''),
                @rtf = REPLACE(@rtf, '\i', ''),
                @rtf = REPLACE(@rtf, '\b', ''),
                @rtf = REPLACE(@rtf, '\f1', ''),
		@rtf = REPLACE(@rtf, '\f2', ''),
                @rtf = REPLACE(@rtf, '}', ''), 
                @rtf = REPLACE(@rtf, '{', ''),
                @rtf = REPLACE(@rtf, '    ', ''),
		@rtf = REPLACE(@rtf, '\cf0', ''),
		@rtf = REPLACE(@rtf, '\fs22', ''), 
		@rtf = REPLACE(@rtf, '\ltrpar', '')
-- ** Please do not add any further RTF tags to script instead insert the tags into ValidatorTables with TableRef = RT (see Below) [Syed Rahman] ** --
	DECLARE 
		@Item varchar(255), 
		@ReplaceWith varchar(255) 
	DECLARE RTF	CURSOR FOR
		SELECT 
			KeyInfo, 
			ISNULL(LookUpInfo,'')
		FROM 
			ValidatorTables 
		WHERE 
			TableRef = 'RT' 
		ORDER BY 
			LEN(KeyInfo) DESC
	OPEN RTF
		FETCH NEXT FROM RTF INTO @Item, @ReplaceWith
		WHILE @@FETCH_STATUS = 0
			BEGIN
			SET @rtf = Replace(@rtf, @item, @ReplaceWith)
			FETCH NEXT FROM RTF INTO @Item, @ReplaceWith
			END
		CLOSE RTF
		DEALLOCATE RTF
--	SELECT	@rtf = LEFT(@rtf, LEN(@rtf) - 1) where len(@rtf) > 0
--	SELECT  @rtf = STUFF(@rtf, 1, CHARINDEX(' ', @rtf), '')
   RETURN  @rtf 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnParseStringTSQL]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnParseStringTSQL] (@string NVARCHAR(MAX),@separator NCHAR(1))
RETURNS @parsedString TABLE (string NVARCHAR(MAX))
AS 
BEGIN
   DECLARE @position int
   SET @position = 1
   SET @string = @string + @separator
   WHILE charindex(@separator,@string,@position) <> 0
      BEGIN
         INSERT into @parsedString
         SELECT substring(@string, @position, charindex(@separator,@string,@position) - @position)
         SET @position = charindex(@separator,@string,@position) + 1
      END
     RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnPaymentTermsID]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnPaymentTermsID](@EventModuleID int) RETURNS int AS
BEGIN
	DECLARE @PaymentTermsID int, @EventID int, @CompanyID int
	SELECT
		@PaymentTermsID = PaymentTermsID,
		@EventID = EventID,
		@CompanyID = CompanyID
	FROM
		EventModules
	WHERE
		EventModuleID = @EventModuleID
	IF ISNULL(@PaymentTermsID, 0) = 0
	BEGIN
		SELECT
			@PaymentTermsID = PaymentTermsID
		FROM
			Companies
		WHERE
			CompanyID = @CompanyID
	END
	IF ISNULL(@PaymentTermsID, 0) = 0
	BEGIN
		SELECT
			@PaymentTermsID = dbo.fnDefaultPaymentTerms(E.EventTypeID)
		FROM
			Events E
		WHERE
			E.EventID = @EventID
	END
	RETURN @PaymentTermsID
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnRemoveInvalidXMLChars]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION	[dbo].[fnRemoveInvalidXMLChars]
( 
   @string varchar(max)
) 
RETURNS varchar(max)
AS
BEGIN
SET @string = RTRIM(LTRIM(IsNull(@String, '')))
IF @string <> ''
BEGIN
         DECLARE @badStrings VARCHAR(100), @ascii int, @Result varchar(max);
         DECLARE @increment INT= 1;
         SET @Result = ''
         WHILE @increment <= DATALENGTH(@String)
         BEGIN
			SET @ascii = ASCII(SUBSTRING(@String, @increment, 1))
            IF not ((@ascii < 32) and (@ascii <> 10) and (@ascii <> 13) and (@ascii <> 9))
            BEGIN
                SET @badStrings = CHAR(@ascii);
                SET @Result = @Result + @badStrings -- REPLACE(@String, @badStrings, '');
            END;
            SET @increment = @increment + 1;
         END;
         SET @string = @Result
END
RETURN @string
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnReplaceAccentedCharacters]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnReplaceAccentedCharacters](@Text varchar(8000))
RETURNS varchar(8000) AS
BEGIN
	DECLARE @FindChars varchar(255), @ReplaceChars varchar(255)
	DECLARE @TextLength int SET @TextLength = LEN(@Text)
	SET @FindChars = 'ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½;ï¿½'
	SET @ReplaceChars = 'ae;ss;th;d;AE;TH;D;a;a;a;a;a;a;e;e;e;e;i;i;i;i;o;o;o;o;o;o;u;u;u;u;c;n;y;A;A;A;A;A;A;E;E;E;E;I;I;I;I;O;O;O;O;O;O;U;U;U;U;C;N;Y'
	DECLARE @Find TABLE (ID int IDENTITY(1,1), String varchar(255))
	INSERT INTO @Find(String) SELECT String FROM dbo.fnStringToTable(@FindChars)
	DECLARE @Replace TABLE (ID int IDENTITY(1,1), String varchar(255))
	INSERT INTO @Replace(String) SELECT	String FROM	dbo.fnStringToTable(@ReplaceChars)
	DECLARE @Word TABLE (ID int IDENTITY(1,1), String varchar(255))
	INSERT INTO
		@Word(String)
	SELECT
		SUBSTRING(@Text, CAST(Date AS int), 1)
	FROM
		Dates
	WHERE
		Date BETWEEN 1 AND @TextLength
	ORDER BY
		Date
	UPDATE
		W
	SET
		String = R.String
	FROM
		@Word W
		JOIN @Find F ON CAST(F.String AS varbinary) = CAST(W.String AS varbinary)
		JOIN @Replace R ON R.ID = F.ID
	DECLARE @Result varchar(8000) SET @Result = ''
	SELECT
		@Result = @Result + String
	FROM
		@Word
	ORDER BY
		ID
	RETURN @Result;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnReplaceASCIIRange]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnReplaceASCIIRange](@Text varchar(max), @ASCIIFrom int, @ASCIITo int, @ReplaceWith varchar(255))
RETURNS varchar(max) AS
BEGIN
	DECLARE @n int SET @n = LEN(@Text)
	DECLARE @p int SET @p = 1
	DECLARE @c char(1)
	DECLARE @Shift int SET @Shift = LEN(@ReplaceWith) - 1
	WHILE @p <= @n
	BEGIN
		SET @c = SUBSTRING(@Text, @p, 1)
		IF ASCII(@c) BETWEEN @ASCIIFrom AND @ASCIITo
		BEGIN
			SET @Text = STUFF(@Text, @p, 1, @ReplaceWith)
			SET @p = @p + @Shift
			SET @n = @n + @Shift
		END
		SET @p = @p + 1
	END
	RETURN @Text
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnReplaceLast]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnReplaceLast](
	@Source varchar(255),
	@Find varchar(255),
	@Replace varchar(255)
	)
RETURNS
	varchar(255)
AS
BEGIN
	DECLARE @LenSource int SET @LenSource = LEN(@Source)
	DECLARE @Pos int
	SELECT
		@Pos = MAX(CHARINDEX(@Find, @Source, N))
	FROM
		(SELECT CAST(Date AS int) AS N FROM Dates WHERE Date BETWEEN 1 AND @LenSource) Numbers
	WHERE
		CHARINDEX(@Find, @Source, N) > 0
	RETURN ISNULL(SUBSTRING(@Source, 1, @Pos-1) + @Replace + SUBSTRING(@Source, @Pos + LEN(@Find), @LenSource), @Source)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnResidentialRoomIsRestrictedByBusinessType]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnResidentialRoomIsRestrictedByBusinessType](
	@ResidentialRoomID int,
	@EventBusinessTypeID int,
	@DateFrom datetime,
	@DateTo datetime
) RETURNS bit
AS
BEGIN
	DECLARE @Restricted bit SET @Restricted = 0

	IF EXISTS(
		SELECT
			*
		FROM
			ViewResidentialRoomAvailabilityRestrictions
		WHERE
			EventBusinessTypeID = @EventBusinessTypeID
		AND	ResidentialRoomID = @ResidentialRoomID
		AND	FromDate <= @DateTo
		AND	ToDate >= @DateFrom
	)
		SET @Restricted = 1

	RETURN @Restricted
END

GO
/****** Object:  UserDefinedFunction [dbo].[fnRoundVAT]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnRoundVAT](@VAT numeric(20,10)) RETURNS money AS
BEGIN
	IF @VAT > 0 AND @VAT < 0.01
		SET @VAT = 0.01
	ELSE IF @VAT < 0 AND @VAT > -0.01
		SET @VAT = -0.01
	ELSE IF (SELECT DropVATPartPenny FROM Control) = 0
	BEGIN
		DECLARE @Precision money
		SET @Precision = ISNULL((SELECT Value FROM CustomSettings WHERE CustomSetting = 'Financial.Rounding.Tax.Precision' AND ISNUMERIC(Value) = 1), 2)
		SET @VAT = ROUND(@VAT, @Precision)
	END
	ELSE IF @VAT > 0
		SET @VAT = FLOOR(@VAT * 100) / 100
	ELSE
		SET @VAT = CEILING(@VAT * 100) / 100
	RETURN ROUND(@VAT, 2)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnST2ApplicationState]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnST2ApplicationState](@ApplicationID int) RETURNS varchar(255) AS
BEGIN
DECLARE @Summary TABLE
(
       ApplicationID int,
	   LastAllocationDateID int,
	   NextAllocationDateID int
)
INSERT INTO @Summary
SELECT
       Application.ApplicationID AS ApplicationID,
       (SELECT TOP 1 AllocationDateID FROM
              ST2StudentAllocationDates SAD
              JOIN ST2StudentAllocations SA ON SA.AllocationID = SAD.AllocationID
       WHERE
              SA.ApplicationID = Application.ApplicationID
              AND SAD.DepartureDate > GETDATE()
       ORDER BY
              SAD.DepartureDate ASC
       ) AS NextAllocationDateID,
       (SELECT TOP 1 AllocationDateID FROM
              ST2StudentAllocationDates SAD
              JOIN ST2StudentAllocations SA ON SA.AllocationID = SAD.AllocationID
       WHERE
              SA.ApplicationID = Application.ApplicationID
       ORDER BY
              SAD.DepartureDate DESC
       ) AS LastAllocationDateID
FROM
       ST2Students Student
       JOIN ST2StudentApplications Application ON Student.KxStudentId = Application.KxStudentId AND Application.ApplicationID = @ApplicationID
       LEFT JOIN ST2StudentAddresses A ON  A.StudentAddressID = Student.CurrentStudentAddressID
       LEFT JOIN ST2StudentAddresses CA ON  CA.StudentAddressID = Student.PreferredEmailAddressID
       LEFT OUTER JOIN StudentYears SY ON SY.StudentYearID = Application.StudentYearID
       LEFT OUTER JOIN CourseTypes CT ON CT.CourseTypeID = Application.CourseTypeID
       LEFT OUTER JOIN ST2ApplicationTypes AT ON Application.ApplicationTypeID = AT.ApplicationTypeID
       LEFT OUTER JOIN ST2StudentPhotos SP ON SP.KxStudentID = Student.KxStudentID
DECLARE @Result varchar(255)
SELECT
       @Result = CASE
              WHEN AL.RejectedDate > 0 THEN 'Rejected'
              WHEN ISNULL(AL.AllocationID, 0) <> 0 AND NOT EXISTS (SELECT * FROM ST2StudentAllocationDates SAD2 WHERE SAD2.AllocationID = AL.AllocationID AND SAD2.DepartureDate >= CURRENT_TIMESTAMP) THEN 'Departed'
              WHEN SAD.CheckedInDate > 0 AND SAD.CheckedOutDate < 0 AND SAD.LiableTo > 0 THEN 'Departing'
              WHEN SAD.CheckedInDate > 0 AND SAD.CheckedOutDate < 0 THEN 'In house'
              WHEN AL.AcceptedDate > 0 THEN 'Accepted'
              WHEN AL.OfferDate > 0 THEN 'Offered'
              WHEN AL.AllocationID IS NULL THEN 'Not Allocated'
              ELSE 'Allocated'
       END
FROM
       @Summary S
       LEFT OUTER JOIN ST2StudentAllocationDates SAD ON SAD.AllocationDateID = COALESCE(S.NextAllocationDateID, S.LastAllocationDateID)
       LEFT OUTER JOIN ST2StudentAllocations AL ON AL.AllocationID = SAD.AllocationID
       LEFT OUTER JOIN ST2StudentApplications AP ON AP.ApplicationID = S.ApplicationID
       LEFT OUTER JOIN ResidentialRooms RR ON AL.ResidentialRoomId = RR.ResidentialRoomID
       LEFT OUTER JOIN SiteBlocks B ON B.BlockID = RR.BlockID
       LEFT OUTER JOIN SiteBlocks SB ON SB.BlockID = RR.SubBlockID
       LEFT OUTER JOIN Sites ST ON ST.SiteID = COALESCE(SB.SiteID, B.SiteID)
RETURN @Result
END

GO
/****** Object:  UserDefinedFunction [dbo].[fnStartOfYear]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnStartOfYear] (@Date datetime) RETURNS datetime AS
BEGIN
	DECLARE @StartOfYear DateTime, @EndOfYear datetime, @StartOfNextYear datetime
	SET @StartOfYear = CAST(STR(YEAR(@Date), 4) + '0101' AS DateTime)
	SET @EndOfYear = CAST(STR(YEAR(@Date), 4) + '1231' AS DateTime)
	SET @StartOfNextYear = @EndOfYear + 1
	WHILE dbo.fnDayOfWeekNumber(@StartOfYear) <> 1
		SET @StartOfYear = @StartOfYear - 1
	WHILE dbo.fnDayOfWeekNumber(@StartOfNextYear) <> 1
		SET @StartOfNextYear = @StartOfNextYear - 1
	IF @Date >= @StartOfNextYear
		SET @StartOfYear = @StartOfNextYear
	RETURN @StartOfYear
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnStringIndexOf]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnStringIndexOf] (@String varchar(8000), @Search varchar(255), @InstanceNo int)
RETURNS int AS  
BEGIN 
	DECLARE @Count int, @StartPos int, @NextPos int
	SET @Count = 0
	SET @StartPos = 1
	SET @NextPos = 1
	WHILE @Count < @InstanceNo AND @NextPos > 0
	BEGIN
		SET @NextPos = CHARINDEX(@Search, @String, @StartPos)
		IF @NextPos > 0
		BEGIN
			SET @Count = @Count + 1
			SET @StartPos = @NextPos + LEN(@Search)
		END
	END
	RETURN @NextPos
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnStringPart]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnStringPart] (@String varchar(8000), @Separator varchar(255), @PartNo int)  
RETURNS varchar(255) AS  
BEGIN
--	RETURN (SELECT String FROM fnStringToTableEx(@String, @Separator) WHERE N = @PartNo)
	DECLARE @StartPos int, @EndPos int, @Result varchar(255), @SeparatorLength int
	SET @SeparatorLength = LEN(@Separator)
	DECLARE @NoOfParts int
	SET @NoOfParts = 0
	IF @String <> ''
	BEGIN
		WHILE dbo.fnStringIndexOf(@String, @Separator, @NoOfParts) > 0
		BEGIN
			SET @NoOfParts = @NoOfParts + 1
		END
	END
	IF @PartNo = 1
		SET @StartPos = 1
	ELSE
		SET @StartPos = dbo.fnStringIndexOf(@String, @Separator, @PartNo-1) + @SeparatorLength
	SET @EndPos = dbo.fnStringIndexOf(@String, @Separator, @PartNo) - 1
	IF @EndPos <= 0 AND @PartNo <= @NoOfParts
		SET @EndPos = LEN(@String)
	IF @StartPos = 1 AND SUBSTRING(@String, 1, @SeparatorLength) = @Separator OR @EndPos <= 0
		SET @Result = ''
	ELSE
		SET @Result = SUBSTRING(@String, @StartPos, @EndPos - @StartPos + 1)
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnStringToDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnStringToDate](@DateString varchar(255), @Lang int) RETURNS datetime AS
BEGIN
	DECLARE @D char(2), @M char(2), @Y char(4)
	SET @D = SUBSTRING(@DateString, 1, 2)
	SET @M = SUBSTRING(@DateString, 4, 2)
	SET @Y = SUBSTRING(@DateString, 7, 4)
	IF ISNULL(@Lang, 0) = 0
		SET @Lang = dbo.GetLang()
	IF @Lang IN (2,8)
	BEGIN
		SET @M = SUBSTRING(@DateString, 1, 2)
		SET @D = SUBSTRING(@DateString, 4, 2)
	END
	DECLARE @Date datetime
	IF ISDATE(@Y + @M + @D) = 0
		SET @Date = NULL
	ELSE
		SET @Date = CAST(@Y + @M + @D AS datetime)
	RETURN @Date
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnStringToTable]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnStringToTable](@String varchar(max))
RETURNS @Table TABLE (String varchar(max))
AS
BEGIN
	DECLARE @Del char(1) SET @Del = ';'
	DECLARE @List varchar(max) SET @List = @Del + @String + @Del
	DECLARE @Length int SET @Length = LEN(@List)
	DECLARE @Numbers TABLE (N integer)
	INSERT INTO @Numbers SELECT 1
	DECLARE @Max integer = 1
	WHILE @Max < @Length
	BEGIN
		INSERT INTO @Numbers SELECT @Max + N FROM @Numbers WHERE @Max + N <= @Length ORDER BY N
		SELECT @Max = MAX(N) FROM @Numbers
	END
	INSERT INTO
		@Table
	SELECT
		SUBSTRING(@List, N+1, CHARINDEX(@Del, @List, N+1) - N - 1)
	FROM
		@Numbers
	WHERE
		SUBSTRING(@List, N, 1) = @Del
	AND N < @Length
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnStringToTableEx]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnStringToTableEx](@String varchar(max), @Del varchar(255))
RETURNS @Table TABLE (N int IDENTITY(1,1), String varchar(max))
AS
BEGIN
	DECLARE @List varchar(max) SET @List = @Del + @String + @Del
	DECLARE @Length int SET @Length = LEN(@List)
	DECLARE @DelLength int SET @DelLength = LEN(@Del)
	DECLARE @Numbers TABLE (N integer)
	INSERT INTO @Numbers SELECT 1
	DECLARE @Max integer = 1
	WHILE @Max < @Length
	BEGIN
		INSERT INTO @Numbers SELECT @Max + N FROM @Numbers WHERE @Max + N <= @Length ORDER BY N
		SELECT @Max = MAX(N) FROM @Numbers
	END
	INSERT INTO
		@Table(String)
	SELECT
		SUBSTRING(@List, N + @DelLength, CHARINDEX(@Del, @List, N + @DelLength) - N - @DelLength)
	FROM
		@Numbers
	WHERE
		SUBSTRING(@List, N, @DelLength) = @Del
	AND N + @DelLength < @Length
	ORDER BY
		N
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnTaxesExemptForDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnTaxesExemptForDate](@TaxModelID int, @Date datetime, @ExemptTaxCodes varchar(max)) RETURNS bit
BEGIN
	DECLARE @ExemptTaxRates int
	SELECT
		@ExemptTaxRates = COUNT(*)
	FROM
		dbo.fnTaxesForDate(@TaxModelID, @Date) T
		JOIN VAT V ON V.VATID = T.VATID
	WHERE
		';' + @ExemptTaxCodes + ';' LIKE '%;' + RTRIM(V.Status) + ';%'
	RETURN @ExemptTaxRates
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnTaxesForDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnTaxesForDate](@TaxModelID int, @Date datetime)
RETURNS @Rates TABLE(VATID int) AS
BEGIN
	INSERT INTO
		@Rates
	SELECT
		VATID
	FROM
		VAT
	WHERE
		TaxModelID = @TaxModelID
	AND	(ISNULL(StartDate, 0) <= 0 OR StartDate <= @Date)
	AND	(ISNULL(EndDate, 0) <= 0 OR EndDate >= @Date)
	ORDER BY
		OrderOfApplication
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnText]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnText](@Source varchar(max)) RETURNS varchar(max) AS
BEGIN
	DECLARE @Result varchar(max)
	SET @Result = @Source
	SET @Result = REPLACE(@Result, CHAR(9), ' ')
	SET @Result = dbo.fnReplaceASCIIRange(@Result, 0, 31, '')
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnUpperFirst]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnUpperFirst] (@Input varchar(255))
RETURNS varchar(255) AS  
BEGIN
	DECLARE @Output varchar(255)
	DECLARE @Pos int
	SET @Output = LOWER(@Input)
	SET @Output = UPPER(LEFT(@Output, 1)) + SUBSTRING(@Output, 2, 255)
	SET @Output = REPLACE(@Output, ' ', '_')
	SET @Pos = CHARINDEX('_', @Output)
	WHILE @Pos > 0
	BEGIN
		SET @Output = LEFT(@Output, @Pos-1) + ' ' + UPPER(SUBSTRING(@Output, @Pos+1, 1)) + SUBSTRING(@Output, @Pos+2, 255)
		SET @Pos = CHARINDEX('_', @Output)
	END
	SET @Output = REPLACE(@Output, '-', '_')
	SET @Pos = CHARINDEX('_', @Output)
	WHILE @Pos > 0
	BEGIN
		SET @Output = LEFT(@Output, @Pos-1) + '-' + UPPER(SUBSTRING(@Output, @Pos+1, 1)) + SUBSTRING(@Output, @Pos+2, 255)
		SET @Pos = CHARINDEX('_', @Output)
	END
	RETURN @Output
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnValidateEventModuleCode]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnValidateEventModuleCode] (@EventModuleID int, @CodeType char(2))  
RETURNS bit AS  
BEGIN
	DECLARE @TableRef1 char(2) SET @TableRef1 = 'AC'
	DECLARE @TableRef2 char(2) SET @TableRef2 = 'AC'
	DECLARE @TableRef3 char(2) SET @TableRef3 = 'AC'
	SELECT TOP 1 @TableRef1 = Value FROM CustomSettings WHERE CustomSetting = 'Validation.TableRef.CA'
	SELECT TOP 1 @TableRef2 = Value FROM CustomSettings WHERE CustomSetting = 'Validation.TableRef.DC'
	SELECT TOP 1 @TableRef3 = Value FROM CustomSettings WHERE CustomSetting = 'Validation.TableRef.PO'
	DECLARE @InvalidCodeCount int
	IF @CodeType = 'CA'
	BEGIN
		DECLARE @InvoiceCompanyRoleID int
		SELECT
			@InvoiceCompanyRoleID = InvoiceCompanyRoleID
		FROM
			Control
		SELECT
			@InvalidCodeCount = COUNT(*)
		FROM
			EventModules EM
			JOIN EventCompanies EC ON EC.EventID = EM.EventID AND (EC.CompanyID = EM.CompanyID OR EC.CompanyRoleID = @InvoiceCompanyRoleID)
			JOIN CompanyAddresses CA ON CA.CompanyID = EC.CompanyID AND CA.AddressID = EC.AddressID
			LEFT JOIN ValidatorTables VT1 ON VT1.TableRef = @TableRef1 AND VT1.KeyInfo = CA.ExternalAccountCode AND CA.ExternalAccountCode <> ''
		WHERE
			EM.EventModuleID = @EventModuleID
		AND	VT1.ValidatorID IS NULL
	END
	ELSE IF @CodeType IN ('DC', 'PO')
	BEGIN
		SELECT
			@InvalidCodeCount = COUNT(*)
		FROM
			EventModules EM
			LEFT JOIN ValidatorTables VT2 ON @CodeType = 'DC' AND VT2.TableRef = @TableRef2 AND VT2.KeyInfo = EM.DebitCode AND EM.DebitCode <> ''
			LEFT JOIN ValidatorTables VT3 ON @CodeType = 'PO' AND VT3.TableRef = @TableRef3 AND VT3.KeyInfo = EM.PurchaseOrderNumber AND EM.PurchaseOrderNumber <> ''
		WHERE
			EM.EventModuleID = @EventModuleID
		AND	(@CodeType = 'DC' AND VT2.ValidatorID IS NULL OR @CodeType = 'PO' AND VT3.ValidatorID IS NULL)
	END
	DECLARE @Valid bit SET @Valid = 0
	IF @InvalidCodeCount = 0 SET @Valid = 1
	RETURN @Valid
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnValidDate]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnValidDate](@DateString char(10), @Lang int) RETURNS bit AS
BEGIN
	DECLARE @Result bit
	IF dbo.fnStringToDate(@DateString, @Lang) IS NULL
		SET @Result = 0
	ELSE
		SET @Result = 1
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnWeekNumber]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fnWeekNumber] (@Date datetime) RETURNS int AS
BEGIN
	DECLARE @YearStart datetime SET @YearStart = dbo.fnStartOfYear(@Date)
	DECLARE @WeekNumber int SET @WeekNumber = 1 + DATEDIFF(day, @YearStart, @Date) / 7
	RETURN @WeekNumber
END
GO
/****** Object:  UserDefinedFunction [dbo].[FormatDateTime]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[FormatDateTime](@Value DateTime, @Format varchar(255))
RETURNS varchar(255) AS
BEGIN
	DECLARE @Day varchar(255) SET @Day = DATENAME(DW, @Value)
	DECLARE @DayNo varchar(255) SET @DayNo = DATENAME(D, @Value)
	DECLARE @Month varchar(255) SET @Month = DATENAME(M, @Value)
	DECLARE @MonthNo varchar(255) SET @MonthNo = MONTH(@Value)
	DECLARE @Year varchar(255) SET @Year = YEAR(@Value)
	DECLARE @Hour int SET @Hour = DATEPART(HOUR, @Value)
	DECLARE @Minute int SET @Minute = DATEPART(MINUTE, @Value)
	DECLARE @Second int SET @Second = DATEPART(SECOND, @Value)
	DECLARE @PM char(2) SET @PM = CASE WHEN @Hour < 12 THEN 'AM' ELSE 'PM' END
	DECLARE @Date varchar(255) SET @Date = @Format
	SET @Date = REPLACE(@Date, 'DDDD', 'T01')
	SET @Date = REPLACE(@Date, 'DDD', 'T02')
	SET @Date = REPLACE(@Date, 'DD', 'T03')
	SET @Date = REPLACE(@Date, 'D', 'T04')
	SET @Date = REPLACE(@Date, 'MMMM', 'T05')
	SET @Date = REPLACE(@Date, 'MMM', 'T06')
	SET @Date = REPLACE(@Date, 'MM', 'T07')
	SET @Date = REPLACE(@Date, 'M', 'T08')
	SET @Date = REPLACE(@Date, 'HH', 'T09')
	SET @Date = REPLACE(@Date, 'H', 'T10')
	SET @Date = REPLACE(@Date, 'NN', 'T11')
	SET @Date = REPLACE(@Date, 'SS', 'T12')
	SET @Date = REPLACE(@Date, 'PP', 'T13')
	SET @Date = REPLACE(@Date, 'P', 'T14')
	SET @Date = REPLACE(@Date, 'T01', @Day)
	SET @Date = REPLACE(@Date, 'T02', LEFT(@Day, 3))
	SET @Date = REPLACE(@Date, 'T03', REPLACE(STR(@DayNo, 2), ' ', '0'))
	SET @Date = REPLACE(@Date, 'T04', @DayNo)
	SET @Date = REPLACE(@Date, 'T05', @Month)
	SET @Date = REPLACE(@Date, 'T06', LEFT(@Month, 3))
	SET @Date = REPLACE(@Date, 'T07', REPLACE(STR(@MonthNo, 2), ' ', '0'))
	SET @Date = REPLACE(@Date, 'T08', @MonthNo)
	SET @Date = REPLACE(@Date, 'T09', REPLACE(STR(@Hour, 2), ' ', '0'))
	SET @Date = REPLACE(@Date, 'T10', CASE WHEN @Hour%12 = 0 THEN 12 ELSE @Hour%12 END)
	SET @Date = REPLACE(@Date, 'T11', REPLACE(STR(@Minute, 2), ' ', '0'))
	SET @Date = REPLACE(@Date, 'T12', REPLACE(STR(@Second, 2), ' ', '0'))
	SET @Date = REPLACE(@Date, 'T13', @PM)
	SET @Date = REPLACE(@Date, 'T14', LEFT(@PM, 1))
	SET @Date = REPLACE(@Date, 'YYYY', @Year)
	SET @Date = REPLACE(@Date, 'YY', RIGHT(@Year, 2))
	RETURN @Date
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEventModuleDescription]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[GetEventModuleDescription]
(
	@EventModuleID int
)
RETURNS varchar(255)
AS
BEGIN
DECLARE @Result varchar(255)
SET @Result = (SELECT
		CASE WHEN C.CourseBusinessTypeID = ETY.EventBusinessTypeID THEN ISNULL(ET.Code, '') + ' : ' + EM.Description ELSE EM.Description END AS Description
		--ET.Code,
		--EM.Description,
		--ETY.EventBusinessTypeID
	FROM
		Control C,
		EventModules EM
		JOIN Events E ON E.EventID = EM.EventID
		LEFT OUTER JOIN EventTemplates ET ON ET.EventTemplateID = E.EventTemplateID
		JOIN EventTypes ETY ON ETY.EventTypeID = E.EventTypeID
	WHERE
		EM.EventModuleID = @EventModuleID)
RETURN CAST(IsNull(@Result, '') as varchar(255))
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetLang]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[GetLang]()
RETURNS int AS  
BEGIN 
	DECLARE @Lang int SET @Lang = 1
	SELECT TOP 1
		@Lang = ISNULL(Lang, 1)
	FROM
		UserCurrent
	WHERE
		Spid = @@SPID
	ORDER BY
		UserCurrentID DESC
	RETURN @Lang
END
GO
/****** Object:  UserDefinedFunction [dbo].[IsDayExcludedFromPromotion]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  FUNCTION [dbo].[IsDayExcludedFromPromotion] (@Date datetime, @PromotionID int)
RETURNS bit
AS
BEGIN
	-- Declarations
	DECLARE @Output bit
	DECLARE @Day int
	-- Set Values
	SET @Output = 0
	SET @Day = DATEPART(dw, @Date)
	-- Check if Excluded
	SELECT	@Output = COUNT(*)
	FROM	Promotions p
	WHERE	p.PromotionID = @PromotionID
	AND
	(	((p.ExcludingSun = 1) AND (@Day = 1))
        	OR ((p.ExcludingMon = 1) AND (@Day = 2))
		OR ((p.ExcludingTue = 1) AND (@Day = 3))
		OR ((p.ExcludingWed = 1) AND (@Day = 4))
		OR ((p.ExcludingThur = 1) AND (@Day = 5))
		OR ((p.ExcludingFri = 1) AND (@Day = 6))
		OR ((p.ExcludingSat = 1) AND (@Day = 7))
	)
	RETURN @Output
END
GO
/****** Object:  UserDefinedFunction [dbo].[MinPositiveValue]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[MinPositiveValue] (@Value1 int, @Value2 int)
RETURNS int
AS
BEGIN
	-- Declarations
	DECLARE @Output int
	DECLARE @NewValue1 int
	DECLARE @NewValue2 int
	-- Set Values
	SET @NewValue1 = ISNULL(@Value1, 0)
	SET @NewValue2 = ISNULL(@Value2, 0)
	IF (@NewValue1 > @NewValue2) AND (@NewValue2 <> 0)
		SET @Output = @Value2
	ELSE
    		SET @Output = @Value1
	RETURN @Output
END
GO
/****** Object:  UserDefinedFunction [dbo].[SMSStatusCode]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[SMSStatusCode](@StatusMessage varchar(255))
RETURNS varchar(255) AS  
BEGIN 
DECLARE @Result varchar(255),
	@Pos int
SET @Result = 'N'
SET @Pos = CHARINDEX(' : ', @StatusMessage)
IF @Pos > 0 SET @Result = SUBSTRING(@StatusMessage, 1, 1)
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[StudentForename]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StudentForename](@ForeName nvarchar(255), @AKAName nvarchar(255))
RETURNS nvarchar(255) AS  
BEGIN 
DECLARE @Result nvarchar(255)
SET @Result = CAST(RTRIM(@Forename) +
	(CASE WHEN IsNull(@AKAName, '') <> '' AND UPPER(RTRIM(@AKAName)) <> UPPER(RTRIM(@Forename))
	THEN ' (' + RTRIM(@AKAName) + ')' ELSE '' END) AS nvarchar(255))
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[StudentFullname]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StudentFullname](@Title nvarchar(255), @ForeName nvarchar(255), @Surname nvarchar(255), @AKAName nvarchar(255))
RETURNS nvarchar(255) AS  
BEGIN 
DECLARE @Result nvarchar(255)
SET @Result = CAST(RTRIM(LTRIM(RTRIM(@Title) + ' ' + RTRIM(@Forename) + ' ' +
		(CASE WHEN IsNull(@AKAName, '') <> '' AND UPPER(RTRIM(@AKAName)) <> UPPER(RTRIM(@Forename))
		 THEN '(' + RTRIM(@AKAName) + ') ' ELSE '' END) +
		RTRIM(@Surname))) AS nvarchar(255))
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[StudentFullname2]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StudentFullname2](@Title nvarchar(255), @ForeName nvarchar(255), @Middle nvarchar(255), @Surname nvarchar(255), @AKAName nvarchar(255))
RETURNS nvarchar(255) AS  
BEGIN 
DECLARE @Result nvarchar(255)
SET @Result = CAST(RTRIM(LTRIM(RTRIM(@Title) + ' ' + RTRIM(@Forename) + ' ' +
		(CASE WHEN IsNull(@AKAName, '') <> '' AND UPPER(RTRIM(@AKAName)) <> UPPER(RTRIM(@Forename))
		 THEN '(' + RTRIM(@AKAName) + ') ' ELSE '' END) +
		 CASE WHEN RTRIM(@Middle) <> '' THEN RTRIM(@Middle) + ' ' ELSE '' END +
		RTRIM(@Surname))) AS varchar(255))
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[StudentPrintFullname]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StudentPrintFullname](@ForeName nvarchar(255), @Surname nvarchar(255), @AKAName nvarchar(255))
RETURNS nvarchar(255) AS  
BEGIN 
DECLARE @Result nvarchar(255)
SET @Result = RTRIM(@Surname) + ', ' +
		RTRIM(@Forename) + (CASE WHEN IsNull(@AKAName, '') <> '' AND
		UPPER(RTRIM(@AKAName)) <> UPPER(RTRIM(@Forename)) THEN ' (' + RTRIM(@AKAName) + ')' ELSE '' END)
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[StudentRoomName]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StudentRoomName](@RoomName varchar(255),
	@BlockName varchar(255),
	@SubBlockName varchar(255),
	@SiteName varchar(255) = null,
	@AreaName varchar(255) = null)
RETURNS varchar(255) AS  
BEGIN 
DECLARE @Result varchar(255)
SET @Result = LTRIM(RTRIM(ISNULL(@RoomName, '')) + ':' + CASE WHEN RTRIM(ISNULL(@SubBlockName, '')) <> '' THEN ' ' + RTRIM(ISNULL(@SubBlockName, '')) ELSE '' END + ' ' + RTRIM(ISNULL(@BlockName, '')))
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[TimeHHMMString]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[TimeHHMMString](@DateTime Datetime, @LanguageCode int)
RETURNS varchar(255) AS  
BEGIN 
DECLARE @Result varchar(255),
	@ConvertNumber int
SET	@ConvertNumber = CASE @LanguageCode WHEN 1 THEN 113 WHEN 2 THEN 109 WHEN 8 THEN 109 ELSE 113 END
SET @Result = SUBSTRING(CONVERT(varchar(30), @DateTime, @ConvertNumber), 13,5) + SUBSTRING(CONVERT(varchar(30), @DateTime, @ConvertNumber), 25,2)
RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[TimeToStr]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[TimeToStr](@DateTime Datetime)
RETURNS varchar(255) AS  
BEGIN 
	DECLARE @Lang int SET @Lang = dbo.GetLang()
	DECLARE
		@Result varchar(255)
	SET @Result = CASE @Lang
	WHEN 2 THEN
		RIGHT(CONVERT(varchar, @DateTime, 0), 7)
	WHEN 8 THEN
		RIGHT(CONVERT(varchar, @DateTime, 0), 7)
	ELSE
		LEFT(CONVERT(varchar, @DateTime, 8), 5)
	END
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[TRIM]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[TRIM](@string VARCHAR(MAX))
RETURNS VARCHAR(MAX)
BEGIN
RETURN LTRIM(RTRIM(@string))
END
GO
/****** Object:  Table [dbo].[SiteBlocks]    Script Date: 6/11/21 4:25:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteBlocks](
	[BlockID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Name] [char](40) NOT NULL,
	[BlockType] [char](1) NOT NULL,
	[MasterBlockID] [int] NULL,
	[SequenceNo] [int] NOT NULL,
	[DefCatConfRoomID] [int] NULL,
	[AreaID] [int] NULL,
	[Allocatable] [bit] NULL,
	[AddressID] [int] NULL,
	[ExternalCode] [varchar](50) NULL,
	[DistribAdHocUserID] [int] NULL,
	[UserGroupID] [int] NULL,
	[ExtraChargeID] [int] NULL,
	[DefaultCateringProviderID] [float] NULL,
 CONSTRAINT [PK_SiteBlocks] PRIMARY KEY NONCLUSTERED 
(
	[BlockID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewBedroomBlocks]    Script Date: 6/11/21 4:25:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[ViewBedroomBlocks]
AS
SELECT
	BlockID,
	Name
FROM
	SiteBlocks SB
WHERE
	SB.BlockType = 'R'
GO
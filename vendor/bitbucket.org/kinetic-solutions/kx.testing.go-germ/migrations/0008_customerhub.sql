EXEC('CREATE SCHEMA customerhub')

CREATE TABLE customerhub.CustomerGroups
(
	CustomerGroupID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,GroupName NVARCHAR(200) NOT NULL UNIQUE
	,ModifiedOn DATETIME NOT NULL DEFAULT (GETDATE())
	,ModifiedBy INT NOT NULL
);

CREATE TABLE customerhub.BookingCreateAs
(
		BookingCreateAsID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,Name VARCHAR(50)  NOT NULL 
);
SET IDENTITY_INSERT [customerhub].[BookingCreateAs] ON 
INSERT [customerhub].[BookingCreateAs] ([BookingCreateAsID], [Name]) VALUES (1, N'Event')
INSERT [customerhub].[BookingCreateAs] ([BookingCreateAsID], [Name]) VALUES (2, N'Enquiry')
SET IDENTITY_INSERT [customerhub].[BookingCreateAs] OFF

CREATE TABLE customerhub.BookingNoticeDayType
(
	BookingNoticeDayTypeID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,Name VARCHAR(50)  NOT NULL 
);
SET IDENTITY_INSERT [customerhub].[BookingNoticeDayType] ON 
INSERT [customerhub].[BookingNoticeDayType] ([BookingNoticeDayTypeID], [Name]) VALUES (1, N'Working days')
INSERT [customerhub].[BookingNoticeDayType] ([BookingNoticeDayTypeID], [Name]) VALUES (2, N'Days')
INSERT [customerhub].[BookingNoticeDayType] ([BookingNoticeDayTypeID], [Name]) VALUES (3, N'Hours')
INSERT [customerhub].[BookingNoticeDayType] ([BookingNoticeDayTypeID], [Name]) VALUES (4, N'None')
SET IDENTITY_INSERT [customerhub].[BookingNoticeDayType] OFF

CREATE TABLE customerhub.BookingTypes
(
	BookingTypeID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,Description NVARCHAR(200) NOT NULL UNIQUE	
	,CreateAs INT NOT NULL FOREIGN KEY REFERENCES CustomerHub.BookingCreateAs(BookingCreateAsID)
	,EventTypeId INT NOT NULL FOREIGN KEY REFERENCES EventTypes(EventTypeID)
	,EventStatusId INT NULL  FOREIGN KEY REFERENCES Status(StatusID)
	,MinNotice INT  NOT NULL
	,MaxNotice INT NOT NULL
	,NoticeDayType INT NOT NULL	FOREIGN KEY REFERENCES CustomerHub.BookingNoticeDayType(BookingNoticeDayTypeID)
	,EnquirySourceId INT NOT NULL FOREIGN KEY REFERENCES EnquirySources(EnquirySourceID)
	,IsActive BIT NOT NULL	
);

CREATE TABLE customerhub.CustomerGroupsBookingTypes
(
	CustomerGroupBookingTypeID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,CustomerGroupID INT NOT NULL FOREIGN KEY REFERENCES customerhub.CustomerGroups(CustomerGroupID) 
	,BookingTypeID INT NOT NULL FOREIGN KEY REFERENCES customerhub.BookingTypes(BookingTypeID)
);

CREATE TABLE customerhub.ConferenceRoomsBookingTypes
(
	ConferenceRoomBookingTypeID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,BookingTypeId INT NOT NULL FOREIGN KEY REFERENCES CustomerHub.BookingTypes(BookingTypeID)
	,ConferenceRoomId INT NOT NULL FOREIGN KEY REFERENCES ConferenceRooms(ConferenceRoomID)
);

CREATE TABLE customerhub.CustomerInvites
(
	CustomerInviteID INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	,ContactID INT NOT NULL
	,Status VARCHAR(25) NOT NULL	
	,KRN VARCHAR(200) NOT NULL
	,SubjectID VARCHAR(100) NULL
	,Etag VARCHAR(25) NULL
	,UserID INT NOT NULL
	,CustomerGroupID INT NOT NULL FOREIGN KEY REFERENCES customerhub.CustomerGroups(CustomerGroupID)
);

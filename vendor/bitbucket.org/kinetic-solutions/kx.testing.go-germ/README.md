# GERM Snapshot testing
This project attempt to solve a the problem of running concurrent tests in GO while using a real instance of GERM.

## Concept
This library is attempting to solve:

1. Resting the GERM database back to a known state between tests.

1. Allowing a SQL server instance to be shared across multiple tests, processes, and hosts.

1. Simplify parallel tests that use a real MS-SQL server.

1. Not needing to pull a bespoke GERM Docker image.

## How to use
First create a database pool. The database pool allows you to "Lease" databases. When a Lease request is made
the pool will either generate a new GERM databases or reuse an existing database. The Leased database can be
modified in anyway required to run your tests. On completion of the test(s) the Leased database should be
returned to the pool for reuse and resetting to its original state.

``` go 
func MyTestCaseThatNeedsCleanData(t *testing.T) {
	r := require.New(t)
	ctx := context.Background()
	docker, err := Docker(ctx, LatestSQL())
	
    r.NoError(err)
    
    // limit the number of databases in the pool to force re-use of
    // the same database. Normally defaults to 10s of databases.
    options := NewPoolOptions(docker)
    options.MaxDatabases = 1
	pool, err := NewPool(ctx, t.Logf, options)

	r.NoError(err)

    defer func() {
        r.NoError(pool.Close(ctx))
    }()

	t.Run("database should be restored between tests", func(t *testing.T) {
		r := require.New(t)

		createBank := func() (dbName string, bankId int) {
            // lease a standard GERM database with all data reset
			leased, err := pool.Lease(ctx)

			r.NoError(err)
			r.NoError(leased.DB().QueryRowContext(ctx, "SELECT DB_NAME()").Scan(&dbName))
			r.NoError(leased.DB().QueryRowContext(ctx, "INSERT INTO dbo.Banks ([Description]) VALUES ('MoneyMoneyMoney'); select SCOPE_IDENTITY()").Scan(&bankId))

            // return the database back to the pool for another test to use
			r.NoError(pool.Return(ctx, leased))

			return
		}

		firstDBName, firstBankID := createBank()
		secondDBName, secondBankID := createBank()

		r.Equal(firstDBName, secondDBName, "expected to get same DB back")
		r.Equal(firstBankID, secondBankID, "expected to insert same ID")
	})
}
```

## FAQ
### Project status
The project is a POC of an idea. If it becomes popular and/or worthwhile going forward it will
likely need an amount of refactoring. Feel free to submit a PR in the meantime.

### SQL License
When using the Docker Server option the code will pull the mcr.microsoft.com/mssql/server:latest 
image and set environment variables to accept the EULA. See [microsoft's Docker Hub](https://hub.docker.com/_/microsoft-mssql-server)
page for more details.

### Performance Overhead
The slowest part of the process is creating the Pool of databases in the first place. Once the databases are
created restoring the original data creates an overhead of around 500ms. To avoid slow tests the same database should
be used for multiple tests when possible.

### I don't want to use Docker
You can specify an already running server using the "Existing" function, e.g.

``` GO
	server := Existing(Connection{
		Username: "SA",
		Password: "PW",
		Port:     1433,
		Server:   "localhost",
	})

	pool, err := NewPool(ctx, t.Logf, NewPoolOptions(server))
```

### Generating the initial GERM Schema
The initial database schema was generated using [mssql-scripter](https://github.com/Microsoft/mssql-scripter)
in Docker. To dump the schema yourself you can run the following:

``` bash 
GERM_DB="$(docker run -d --rm 129231881647.dkr.ecr.eu-west-1.amazonaws.com/data/baseline-germ)"
germ_pw="$(docker exec -it $GERM_DB sh -c 'echo $MSSQL_SA_PASSWORD')"
grep -q 'Recovery is complete.' <(docker logs -f $GERM_DB)

docker run -it -v ${PWD}/dumps:/dump --link $GERM_DB:sql python:3.6-slim-jessie bash -c '
    apt-get update && apt-get install -y --no-install-recommends freetds-bin sed libunwind8 libicu52 libssl1.0.0
    pip install --upgrade pip
    pip install mssql-scripter
    
    mssql-scripter -S sql -d Blank_Kx2019_Dev -U sa > /dump/germ.sql
'
```
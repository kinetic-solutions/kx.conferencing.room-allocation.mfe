// Package krn defines a Kinetic Resource Name.
// A KRN uniquely and permanently identifies resources across services on Pulse.
// more info:
// https://kineticsolutions.atlassian.net/wiki/spaces/KXA/pages/1243742209/KRN+Kinetic+Resource+Names
package krn

import (
	"bytes"
	"errors"
	"fmt"
)

// KRN is a Kinetic Resource Name.
// The format of a KRN is krn:partition:service:ISO3166:tenant-id:resource
// where the resource part is opaque to services other than the service that
// owns this resource.
type KRN struct {
	Partition    string
	Service      string
	Jurisdiction string
	TenantID     string
	Resource     string
}

// String implements the io.Stringer interface.
func (k KRN) String() string {
	return "krn" +
		":" + k.Partition +
		":" + k.Service +
		":" + k.Jurisdiction +
		":" + k.TenantID +
		":" + k.Resource
}

// MarshalJSON implements the json.Marshaler interface.
func (k KRN) MarshalJSON() ([]byte, error) {
	return []byte(`"` + k.String() + `"`), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (k *KRN) UnmarshalJSON(text []byte) error {
	if len(text) < 2 || text[0] != '"' || text[len(text)-1] != '"' {
		return errors.New("cannot unmarshal KRN: JSON value must be a string")
	}

	text = text[1 : len(text)-1]

	parts := bytes.SplitN(text, []byte{':'}, 6)
	if len(parts) != 6 {
		return fmt.Errorf("cannot unmarshal KRN: expected 6 parts, got %d",
			len(parts))
	}

	prefix := string(parts[0])
	if prefix != "krn" {
		return fmt.Errorf(`cannot unmarshal KRN: expected prefix "krn" ; got %q`,
			prefix)
	}

	if len(parts[1]) == 0 {
		return errors.New("cannot unmarshal KRN: partition is empty")
	}
	if len(parts[2]) == 0 {
		return errors.New("cannot unmarshal KRN: service is empty")
	}
	if len(parts[4]) == 0 {
		return errors.New("cannot unmarshal KRN: tenantID is empty")
	}
	if len(parts[5]) == 0 {
		return errors.New("cannot unmarshal KRN: resource is empty")
	}
	k.Partition = string(parts[1])
	k.Service = string(parts[2])
	k.Jurisdiction = string(parts[3])
	k.TenantID = string(parts[4])
	k.Resource = string(parts[5])
	return nil
}

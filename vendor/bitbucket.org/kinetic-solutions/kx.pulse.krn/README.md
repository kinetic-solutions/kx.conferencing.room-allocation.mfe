# kx.pulse.krn
This basic Go module for marshalling / unmarshalling a KRN in Go

## Installing

$ `go get bitbucket.org/kinetic-solutions/kx.pulse.krn`


## Usage
```go
import krn "bitbucket.org/kinetic-solutions/kx.pulse.krn"

type customer struct {
	KRN krn.KRN `json:"id"`
	Name string `json:"name"`
}
```

package krn

const (
	// DefaultPartion all KRNs will currently be the same default partion
	DefaultPartion Partition = "kx"
)

// Partition of the KRN, this should usually be DefaultPartion
type Partition string

func (p Partition) String() string {
	return string(p)
}

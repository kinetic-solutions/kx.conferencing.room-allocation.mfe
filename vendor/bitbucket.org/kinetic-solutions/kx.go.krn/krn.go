package krn

import (
	"fmt"
	"strings"
)

var partNames = [...]string{"identifier", "partition", "service", "jurisdiction", "tenant", "resource"}

type krnPart int

func (s krnPart) String() string {
	return partNames[s]
}

const (
	identifier krnPart = iota
	partition
	service
	jurisdiction
	tenant
	resource

	separator       = ':'
	identifierValue = "krn"
)

// ErrPartInvalid is returned if a part of the KRN isn't valid
type ErrPartInvalid struct {
	RawKRN string
	part   krnPart
	reason string
}

func (e ErrPartInvalid) Error() string {
	return fmt.Sprintf("%q of KRN %q is invalid: %s", e.part, e.RawKRN, e.reason)
}

/*
KRN represents a split apart KRN that can be used to inspect and interact with the KRN data. KRN can also be used as a KRN builder. E.G:

krn := KRN{ Partition: "kx", Service: "dataservices", Jurisdiction: "EU", Tenant: "42389023498", Resource: "contacts/V1/32849" }
s, err := krn.Construct()
(s == "krn:kx:dataservices:EU:42389023498:contacts/V1/32849") == true

KRN can also be used for equality:

first := KRN{ Partition: "kx", Service: "dataservices", Jurisdiction: "EU", Tenant: "42389023498", Resource: "contacts/V1/32849" }
second := krn := KRN{}
(first != second) == true
*/
type KRN struct {
	Partition    Partition
	Service      string
	Jurisdiction string
	Tenant       string
	Resource     string
}

// SetJurisdiction sets the Jurisdiction of the KRN
func (k *KRN) SetJurisdiction(s string) {
	// normalise the Jurisdiction to upper case
	k.Jurisdiction = strings.ToUpper(s)
}

// String returns the KRN in String format. No validation is performed on the fields. To validate
// and stringify the KRN use Construct.
func (k *KRN) String() string {
	return strings.Join([]string{
		identifierValue,
		k.Partition.String(),
		k.Service,
		strings.ToUpper(k.Jurisdiction),
		k.Tenant,
		k.Resource},
		string(separator))
}

// Construct validates the KRN and if valid returns a string representation
func (k *KRN) Construct() (s string, err error) {
	s = k.String()
	if _, err = Parse(s); err != nil {
		s = ""
	}
	return
}

// Parse decodes s into a KRN or returns an error. The KRN string must
// be in the format krn:partition:Service:Juristdiction(*optional*):Tenant:Resource
func Parse(s string) (KRN, error) {
	s = strings.TrimSpace(s)

	if s == "" {
		return KRN{}, &ErrPartInvalid{RawKRN: s, part: identifier, reason: "Cannot parse empty KRN"}
	}

	var (
		result      KRN
		part        krnPart
		fromPostion int
	)

	for i, c := range s {
		switch c {
		case '\'', '/', '\\', '?', '!', '<', '>', '"', '`':
			return KRN{}, &ErrPartInvalid{RawKRN: s, part: part, reason: "reserved char"}
		case separator:
			value := s[fromPostion:i]

			if part != jurisdiction && len(value) == 0 {
				return KRN{}, &ErrPartInvalid{RawKRN: s, part: part, reason: "empty"}
			}

			switch part {
			case identifier:
				if value != identifierValue {
					return KRN{}, &ErrPartInvalid{RawKRN: s, part: identifier, reason: "must be krn"}
				}
			case partition:
				result.Partition = Partition(value)
			case service:
				result.Service = value
			case jurisdiction:
				// TODO: ideally we don't want to convert to upper here.
				// one way to avoid this is to create getter/setters
				// for each resource and just store slices of bytes
				// for each doing the string conversions only when required.
				result.SetJurisdiction(value)
			case tenant:
				result.Tenant = value
			}

			fromPostion = i + 1
			part++

			if part == resource {
				if len(s) <= i+1 {
					return KRN{}, &ErrPartInvalid{RawKRN: s, part: resource, reason: "empty"}
				}

				result.Resource = s[i+1:]
				return result, nil
			}
		}
	}

	return KRN{}, &ErrPartInvalid{RawKRN: s, part: part, reason: "unknown KRN format"}
}

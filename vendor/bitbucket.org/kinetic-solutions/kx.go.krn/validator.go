package krn

import (
	"reflect"
)

// KRNValuer implements validator.CustomTypeFunc to convert KRN to string.
// In order to use this with validator first register custom type using this function and KRN. example,
// https://github.com/go-playground/validator/blob/master/_examples/custom/main.go
func KRNValuer(field reflect.Value) interface{} {

	if KRNField, ok := field.Interface().(KRN); ok {
		k, _ := KRNField.Construct()
		return k
	}
	return nil

}

# KRN #

The KRN package provides a common set of KRN functions such as Building, encoding to/from JSON, Equality, and Parsing.

### Install ###

`go get bitbucket.org/kinetic-solutions/kx.go.krn`

### Testing ###

`go test`

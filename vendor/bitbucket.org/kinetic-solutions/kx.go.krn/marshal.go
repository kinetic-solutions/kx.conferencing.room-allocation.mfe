package krn

import "encoding/json"

// MarshalJSON Converts KRN to string form
func (k KRN) MarshalJSON() ([]byte, error) {
	s, err := k.Construct()

	if err != nil {
		return nil, err
	}

	return json.Marshal(s)
}

// UnmarshalJSON Converts a KRN string to a KRN
func (k *KRN) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}

	parsed, err := Parse(s)

	if err != nil {
		return err
	}

	k.Partition = parsed.Partition
	k.Service = parsed.Service
	k.Jurisdiction = parsed.Jurisdiction
	k.Tenant = parsed.Tenant
	k.Resource = parsed.Resource

	return nil
}

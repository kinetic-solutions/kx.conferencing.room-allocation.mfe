package main

import (
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/getbedroomtypes"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/logging"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-xray-sdk-go/xray"
	"go.uber.org/zap"
)

func main() {
	log := logging.New()
	defer log.Sync()
	xray.SetLogger(logging.Xray(log))
	lambda.Start(newWrapper(log))
}

func newWrapper(log *zap.SugaredLogger) lambdawrapper.Lambda {
	return lambdawrapper.WrapFunc(log, getbedroomtypes.New)
}

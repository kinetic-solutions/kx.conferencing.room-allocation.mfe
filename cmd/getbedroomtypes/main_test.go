package main

import (
	"context"
	"encoding/base64"
	"net/http"
	"os"
	"testing"
	"time"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_bedroomtypes_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name            string
		path            map[string]string
		query           map[string]string
		initialDatabase *germdatabase.GermDatabase
		expectedStatus  int
		expectedBody    string
	}{
		{
			name:           "scope must be one of roominglist, reservations or all",
			query:          map[string]string{"scope": "not_a_scope", "eventID": "1"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "scope",
					"error": "scope must be either roominglist, reservations, or all"
				}]
			}`,
		},
		{
			name:           "if the scope query parameter is roominglist, then eventID is required",
			query:          map[string]string{"scope": "roominglist"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID is required if scope is set"
				}]
			}`,
		},
		{
			name:           "if the scope query parameter is reservations, then eventID is required",
			query:          map[string]string{"scope": "reservations"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID is required if scope is set"
				}]
			}`,
		},
		{
			name:           "eventID must be a number",
			query:          map[string]string{"eventID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID must be an integer"
				}]
			}`,
		},
		{
			name: "should return list of bedroomtypes by default",
			initialDatabase: germdatabase.New().
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"bedroomTypes": [
						{
							"id": 1,
							"name": "Single"
						},
						{
							"id": 2,
							"name": "Double"
						}
					]
				}`,
		},
		{
			name: "when scope is all, should return list of bedroomtypes",
			path: map[string]string{"scope": "all"},
			initialDatabase: germdatabase.New().
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"bedroomTypes": [
						{
							"id": 1,
							"name": "Single"
						},
						{
							"id": 2,
							"name": "Double"
						}
					]
				}`,
		},
		{
			name:  "when scope is roominglist, only show bedroomtypes from rooming list rooms",
			query: map[string]string{"scope": "roominglist", "eventID": "1"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 1, EventModuleId: 1, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 2, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 1, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 2, EventModuleID: 2, ResidentialRoomID: 1, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E02", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
					germdatabase.BedroomType{BedroomTypeID: 3, Description: "Triple"},
				).
				WithSites(
					germdatabase.Site{SiteID: 10, Name: "Site 1"},
					germdatabase.Site{SiteID: 11, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 10, AreaID: 1, BlockID: 1, Name: "Block 1"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 1, BlockID: 2, Name: "Block 2"},
					germdatabase.SiteBlock{SiteID: 11, AreaID: 1, BlockID: 3, Name: "Block 3"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 2, BlockID: 4, Name: "Block 4"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
			{
				"bedroomTypes": [
					{
						"id": 1,
						"name": "Single"
					}
				]
			}`,
		},
		{
			name:  "when scope is reservations, only show bedroomTypes from reserved rooms",
			query: map[string]string{"scope": "reservations", "eventID": "1"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 1, EventModuleId: 1, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 2, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 2, Allocation: 1, BedroomTypeID: 2, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 2, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
					germdatabase.BedroomType{BedroomTypeID: 3, Description: "Triple"},
				).
				WithSites(
					germdatabase.Site{SiteID: 10, Name: "Site 1"},
					germdatabase.Site{SiteID: 11, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 10, AreaID: 1, BlockID: 1, Name: "Block 1"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 1, BlockID: 2, Name: "Block 2"},
					germdatabase.SiteBlock{SiteID: 11, AreaID: 1, BlockID: 3, Name: "Block 3"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 2, BlockID: 4, Name: "Block 4"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
			{
				"bedroomTypes": [
					{
						"id": 2,
						"name": "Double"
					}
				]
			}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters:        tc.path,
				QueryStringParameters: tc.query,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

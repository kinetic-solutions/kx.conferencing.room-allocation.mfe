package main

import (
	"context"
	"encoding/base64"
	"net/http"
	"os"
	"testing"
	"time"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_rooms_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()
	r.NoError(err)

	t.Log("starting tests")

	tt := []struct {
		name            string
		path            map[string]string
		initialDatabase *germdatabase.GermDatabase
		expectedStatus  int
		expectedBody    string
		query           map[string]string
	}{
		//region parameter validation
		{
			name:           "if event id is missing, return 400",
			path:           map[string]string{},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id is required"
				}]
			}`,
		},
		{
			name: "if event id is a non-integer, return 400",
			path: map[string]string{
				"eventID": "{}",
			},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be an integer"
				}]
			}`,
		},
		{
			name:           "if event id is less than 1, return 400",
			path:           map[string]string{"eventID": "0"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be greater than 0"
				}]
			}`,
		},
		{
			name: "if event doesn't exist, return 404",
			path: map[string]string{
				"eventID":    "99999999",
				"attendeeID": "11",
			},
			query:          map[string]string{"arrival": "2019-01-01", "departure": "2019-01-06"},
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "event 99999999 not found"
			}`,
		},
		{
			name:           "if arrival date is missing, return 400",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "arrival",
					"error": "arrival is required"
				}]
			}`,
		},
		{
			name:           "if arrival date is not a date, return 400",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "not a date", "departure": "2019-01-06"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "arrival",
					"error": "arrival is not a date"
				}]
			}`,
		},
		{
			name:           "if departure date is missing, return 400",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-01"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "departure",
					"error": "departure is required"
				}]
			}`,
		},
		{
			name:           "if pageSize is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "pageSize": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "pageSize",
					"error": "pageSize must be an integer"
				}]
			}`,
		},
		{
			name:           "if pageSize is less than 1",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "pageSize": "0"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "pageSize",
					"error": "pageSize must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if includeOccupied is not a boolean",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "includeOccupied": "notABool"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "includeOccupied",
					"error": "includeOccupied must be a boolean"
				}]
			}`,
		},
		{
			name:           "if roomName is empty",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "roomName": ""},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "roomName",
					"error": "roomName must have length greater than 0"
				}]
			}`,
		},
		{
			name:           "if siteID is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "siteID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "siteID",
					"error": "siteID must be an integer"
				}]
			}`,
		},
		{
			name:           "if areaID is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "areaID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "areaID",
					"error": "areaID must be an integer"
				}]
			}`,
		},
		{
			name:           "if blockID is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "blockID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "blockID",
					"error": "blockID must be an integer"
				}]
			}`,
		},
		{
			name:           "if subBlockID is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "subBlockID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "subBlockID",
					"error": "subBlockID must be an integer"
				}]
			}`,
		},
		{
			name:           "if bedroomTypeID is not a number",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "bedroomTypeID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "bedroomTypeID",
					"error": "bedroomTypeID must be an integer"
				}]
			}`,
		},
		{
			name:           "if smoking is not a boolean",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "smoking": "notABoolean"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "smoking",
					"error": "smoking must be a boolean"
				}]
			}`,
		},
		{
			name:           "if vip is not a boolean",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "vip": "notABoolean"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "vip",
					"error": "vip must be a boolean"
				}]
			}`,
		},
		{
			name:           "if disabled is not a boolean",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "disabled": "notABoolean"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "disabled",
					"error": "disabled must be a boolean"
				}]
			}`,
		},
		{
			name:           "if lastLet is not a boolean",
			path:           map[string]string{"eventID": "1"},
			query:          map[string]string{"arrival": "2019-01-05", "departure": "2019-01-06", "lastLet": "notABoolean"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "lastLet",
					"error": "lastLet must be a boolean"
				}]
			}`,
		},
		//endregion
		{
			name:  "it should return all rooms",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "D", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "I", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "D",
							"attributes": []
						},
						{
							"bedroomID": 3,
							"bedroomName": "E03",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "I",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "it should default the room status to 'C'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				//This room doesn't have a maidstatus, so it should default to 'C'
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "RoomID 2 should appear occupied for an attendee staying two nights on 2019-01-01",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithPeople(germdatabase.Person{PersonID: 11, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 11, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 1,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "RoomID 1 and 2 should appear not occupied as the RRO records exist but the personID are -99999",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-01"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithPeople(germdatabase.Person{PersonID: -99999, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "if pageSize query parameter is set, then the number of rows returned is restricted",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "pageSize": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 13, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 13, EventModuleId: 2, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 1,
							"bedroomName": "E01",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						},
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					],
					"next": "eyJhcnJpdmFsIjoiMjAxOS0wMS0wMVQwMDowMDowMFoiLCJkZXBhcnR1cmUiOiIyMDE5LTAxLTA0VDAwOjAwOjAwWiIsImluY2x1ZGVPY2N1cGllZCI6dHJ1ZSwicGFnZVNpemUiOjIsInBhZ2UiOjJ9"
				}`,
		},
		{
			name:  "if cursor query parameter is set, the next page is returned",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"cursor": "eyJhcnJpdmFsIjoiMjAxOS0wMS0wMVQwMDowMDowMFoiLCJkZXBhcnR1cmUiOiIyMDE5LTAxLTA0VDAwOjAwOjAwWiIsImluY2x1ZGVPY2N1cGllZCI6dHJ1ZSwicGFnZVNpemUiOjIsInBhZ2UiOjJ9"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 13, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 13, EventModuleId: 2, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 3,
							"bedroomName": "E03",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						},
						{
							"bedroomID": 4,
							"bedroomName": "E04",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "should not include flats in the results",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Room in flat", RoomInFlat: true},
					germdatabase.BedroomType{BedroomTypeID: 3, Description: "Book by flat", BookByRoom: true}).
				WithPeople(germdatabase.Person{PersonID: -99999, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 3, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "should not include inactive in the results",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithPeople(germdatabase.Person{PersonID: -99999, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate(), Inactive: 0},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate(), Inactive: 1},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate(), Inactive: 1},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "should not include rooms that have their availability restricted by RABBT",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 9, BlockID: 2, Name: "West Wing", AreaID: 1},
				).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithControl(
					germdatabase.Control{StudentEventBusinessTypeID: 1},
				).
				WithAvailabilityRestrictions(
					germdatabase.AvailabilityRestriction{AvailabilityRestrictionID: 1, EventBusinessTypeID: 5, BlockID: 2, BedroomTypeID: 1, FromDate: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC), ToDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithAvailabilityResidentialRooms(
					germdatabase.AvailabilityRestrictionResidentialRoom{AvailabilityRestrictionID: 1, ResidentialRoomID: 3},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "should not include rooms that appear in the ResidentialRoomRestrictions table",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 9, BlockID: 2, Name: "West Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 5, Name: "E05", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomRestrictions(
					germdatabase.ResidentialRoomRestriction{ResidentialRoomRestrictionID: 1, ResidentialRoomID: 4, BedIndex: 0},
					germdatabase.ResidentialRoomRestriction{ResidentialRoomRestrictionID: 2, ResidentialRoomID: 5, BedIndex: 0},
				).
				WithResidentialRoomRestrictionHeader(
					germdatabase.ResidentialRoomRestrictionHeader{ResidentialRoomRestrictionID: 1, BlockID: 1, StartDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationUserID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomRestrictionHeader{ResidentialRoomRestrictionID: 2, BlockID: 1, StartDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationUserID: 1, CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						},
						{
							"bedroomID": 3,
							"bedroomName": "E03",
							"bedroomType": "Double",
							"bedroomTypeID": 2,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "should not include room E02 since it's been occupied by an attendee from another event",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 13, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 13, EventModuleId: 2, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 2, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 1,
							"bedroomName": "E01",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		//region filtering
		{
			name:  "filter by includeOccupied - E02 should not apear since it's occupied by someone",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "includeOccupied": "false"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithPeople(germdatabase.Person{PersonID: 11, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 11, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 3,
							"bedroomName": "E03",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by roomName - E02 should apear since it matches the filter exactly, E023 should appear since it matches partially",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "roomName": "E02"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithPeople(germdatabase.Person{PersonID: 11, ModifiedDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate()}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						},
						{
							"bedroomID": 4,
							"bedroomName": "E023",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by siteID - E02 should apear since it matches siteID filter",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "siteID": "9"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
					germdatabase.Site{SiteID: 10, Name: "Small Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 10, BlockID: 2, Name: "West Wing", AreaID: 1},
				).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 1, BlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by areaID - E03 should apear since it matches areaID filter",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "areaID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
					germdatabase.Site{SiteID: 10, Name: "Small Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 10, BlockID: 2, Name: "West Wing", AreaID: 2},
				).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 3,
							"bedroomName": "E03",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 2,
							"blockName": "West Wing",
							"siteID": 10,
							"areaID": 2,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by blockID - E02 and E023 should apear since they matches blockID filter",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "blockID": "1"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
					germdatabase.Site{SiteID: 10, Name: "Small Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 10, BlockID: 2, Name: "West Wing", AreaID: 2},
				).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						},
						{
							"bedroomID": 4,
							"bedroomName": "E023",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by subblockID - E02 should apear since it matches subblockID filter",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "subBlockID": "1"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
					germdatabase.Site{SiteID: 10, Name: "Small Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 10, BlockID: 2, Name: "West Wing", AreaID: 2},
				).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"}).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, SubBlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 2, SubBlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 1, BlockID: 1, SubBlockID: 2, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 1,
							"subBlockName": "East Wing",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by bedroomTypeID - E023 should apear since it matches bedroomTypeID filter",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "bedroomTypeID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
					germdatabase.Site{SiteID: 10, Name: "Small Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
					germdatabase.SiteBlock{SiteID: 10, BlockID: 2, Name: "West Wing", AreaID: 2},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
					germdatabase.BedroomType{BedroomTypeID: 2, Description: "Double"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 4,
							"bedroomName": "E023",
							"bedroomType": "Double",
							"bedroomTypeID": 2,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": []
						}
					]
				}`,
		},
		{
			name:  "filter by smoking - E02 should apear since it's a smoking room'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "smoking": "true"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", Smoking: true, BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": [{"key": "smoking", "value": "Smoking"}]
						}
					]
				}`,
		},
		{
			name:  "filter by disabled - E02 should apear since it's a disabled room'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "disabled": "true"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", Disabled: true, BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": [{"key": "disabled", "value": "Disabled"}]
						}
					]
				}`,
		},
		{
			name:  "filter by vip - E02 should apear since it's a vip room'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "vip": "true"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", VIP: true, BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": [{"key": "vip", "value": "VIP"}]
						}
					]
				}`,
		},
		{
			name:  "filter by lastLet - E02 should apear since it's a lastLet room'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "lastLet": "true"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", LastLet: true, BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "C",
							"attributes": [{"key": "lastLet", "value": "Last Let"}]
						}
					]
				}`,
		},
		{
			name:  "filter by maidStatus - E02 should apear since it matches the filter'",
			path:  map[string]string{"eventID": "12"},
			query: map[string]string{"arrival": "2019-01-01", "departure": "2019-01-04", "maidStatus": "D"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(
					germdatabase.Site{SiteID: 9, Name: "Big Site"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "D", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E023", BedroomTypeID: 2, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"rooms": [
						{
							"bedroomID": 2,
							"bedroomName": "E02",
							"bedroomType": "Single",
							"bedroomTypeID": 1,
							"blockID": 1,
							"blockName": "East Wing",
							"siteID": 9,
							"areaID": 1,
							"subBlockID": 0,
							"subBlockName": "",
							"actualCapacity": 1,
							"typicalCapacity": 1,
							"occupied": 0,
							"status": "D",
							"attributes": []
						}
					]
				}`,
		},
		//endregion
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}

			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters:        tc.path,
				QueryStringParameters: tc.query,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

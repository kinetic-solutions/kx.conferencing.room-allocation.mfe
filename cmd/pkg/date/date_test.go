package date

import (
	"encoding/json"
	"log"
	"testing"
	"time"
)

func TestStringer(t *testing.T) {
	in := "2021-09-01"
	got := Must(in).String()
	if got != in {
		t.Fatalf("want %s ; got %s", in, got)
	}
}

func TestMarshal(t *testing.T) {
	mountain, err := time.LoadLocation("Canada/Mountain")
	if err != nil {
		t.Fatal("failed to load location")
	}

	tt := []struct {
		name    string
		in      Date
		want    string
		wantErr bool
	}{
		{
			name: "zero value -> null",
			in:   Date{},
			want: `null`,
		},
		{
			name: "returns date as a string in yyyy-mm-dd format",
			in:   Date{time.Date(2020, 8, 31, 1, 2, 3, 4, time.UTC)},
			want: `"2020-08-31"`,
		},
		{
			name: "ignores timezone when marshalling",
			in:   Date{time.Date(2020, 8, 30, 23, 2, 3, 4, mountain)},
			want: `"2020-08-30"`,
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			got, err := tc.in.MarshalJSON()
			if err != nil {
				t.Fatalf("got unexpected error %s", err)
			}

			if string(got) != tc.want {
				t.Fatalf("got = %s ; want %s", got, tc.want)
			}
		})
	}
}

func TestUnmarshalJSON(t *testing.T) {
	tt := []struct {
		name    string
		in      string
		want    time.Time
		wantErr bool
	}{
		{
			name:    "null -> error",
			in:      `null`,
			wantErr: true,
		},
		{
			name:    "undefined -> error",
			in:      `undefined`,
			wantErr: true,
		},
		{
			name:    "object -> error",
			in:      `{}`,
			wantErr: true,
		},
		{
			name:    "array -> error",
			in:      `[]`,
			wantErr: true,
		},
		{
			name:    "empty string -> error",
			in:      `""`,
			wantErr: true,
		},
		{
			name:    "a date and time -> error",
			in:      `"1901-01-01T00:00:00Z"`,
			wantErr: true,
		},
		{
			name:    "a date in the wrong format -> error",
			in:      `"01-01-1901"`,
			wantErr: true,
		},
		{
			name: "a date in the correct format -> ok",
			in:   `"1901-01-01"`,
			want: time.Date(1901, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			got := new(Date)
			err := json.Unmarshal([]byte(tc.in), got)
			if tc.wantErr != (err != nil) {
				t.Fatalf("wanted error (%v) ; got: %v", tc.wantErr, err)
			}

			if got.Time != tc.want {
				t.Fatalf("got %s ; want %s", got, tc.want)
			}
		})
	}
}

func TestScan(t *testing.T) {
	mountain, err := time.LoadLocation("Canada/Mountain")
	if err != nil {
		t.Fatal(err)
	}

	tt := []struct {
		name    string
		in      interface{}
		wantErr bool
		want    Date
	}{
		{
			name:    "slice of bytes in the wrong format -> error",
			in:      []byte("0101-01"),
			wantErr: true,
		},
		{
			name:    "slice of bytes with excess precision -> error",
			in:      []byte("1901-01-01T00:00:01.000Z"),
			wantErr: true,
		},
		{
			name: "slice of bytes in the correct format -> ok",
			in:   []byte("1901-01-01"),
			want: Must("1901-01-01"),
		},
		{
			name: "string in the correct format -> ok",
			in:   "1901-01-01",
			want: Must("1901-01-01"),
		},
		{
			name:    "time.Time with hour set -> error",
			in:      time.Date(2021, 1, 1, 23, 0, 0, 0, time.UTC),
			wantErr: true,
		},
		{
			name:    "time.Time with no hour in UTC, but non-zero in its location -> error",
			in:      time.Date(2021, 7, 30, 14, 0, 0, 0, mountain),
			wantErr: true,
		},
		{
			name:    "time.Time with no hour in its location, but has offset -> error",
			in:      time.Date(2021, 8, 1, 0, 0, 0, 0, mountain),
			wantErr: true,
		},
		{
			name: "time.Time with no UTC hour, and zero offset -> ok",
			in:   time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
			want: Must("2021-01-01"),
		},
		{
			name:    "unix timestamp -> error",
			in:      time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC).Unix(),
			wantErr: true,
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			got := new(Date)
			err := got.Scan(tc.in)
			if tc.wantErr != (err != nil) {
				log.Printf("wantErr is %v ; got %q", tc.wantErr, err)
				log.Println("failed in location", tc.in.(time.Time).Location().String())
				log.Println()
				t.FailNow()
			}

			if *got != tc.want {
				t.Fatalf("got %s ; want %s", *got, tc.want)
			}
		})
	}
}

func TestValue(t *testing.T) {
	var (
		d    Date      = Must("2021-09-02")
		want time.Time = time.Date(2021, 9, 2, 0, 0, 0, 0, time.UTC)
	)

	got, err := d.Value()
	if err != nil {
		t.Fatalf("got unexpected error: %s", err)
	}

	if got != want {
		t.Fatalf("Value() = %v ; want %v", got, want)
	}
}

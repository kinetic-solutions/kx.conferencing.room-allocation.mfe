package main

import (
	"context"
	"database/sql"
	"encoding/base64"
	"net/http"
	"os"
	"testing"
	"time"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_attendee_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name            string
		path            map[string]string
		initialDatabase *germdatabase.GermDatabase
		expectedStatus  int
		expectedBody    string
	}{
		{
			name:           "if event id is missing, return 400",
			path:           map[string]string{},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id is required"
				}]
			}`,
		},
		{
			name: "if event id is a non-integer, return 400",
			path: map[string]string{
				"eventID": "{}",
			},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be an integer"
				}]
			}`,
		},
		{
			name:           "if event id is less than 1, return 400",
			path:           map[string]string{"eventID": "0"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be greater than 0"
				}]
			}`,
		},

		{
			name:           "if attendee id is missing, return 400",
			path:           map[string]string{"eventID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id is required"
				}]
			}`,
		},
		{
			name: "if attendee id is a non-integer, return 400",
			path: map[string]string{
				"eventID":    "11",
				"attendeeID": "{}",
			},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be an integer"
				}]
			}`,
		},
		{
			name:           "if attendee id is less than 1, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "0"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be greater than 0"
				}]
			}`,
		},
		{
			name: "if event doesn't exist, return 404",
			path: map[string]string{
				"eventID":    "99999999",
				"attendeeID": "11",
			},
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "event 99999999 not found"
			}`,
		},
		{
			name: "should return a 404 if there is no attendee",
			path: map[string]string{"eventID": "11", "attendeeID": "11111"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusNotFound,
			expectedBody:   `{"message": "attendee 11111 not found"}`,
		},
		{
			name: "should return event attendees",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Lisa", Surname: "MillsCorp", VIP: true, Disabled: true, Smoker: true, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithCustomSettings(
					germdatabase.CustomSetting{CustomSettingID: 1, CustomSetting: "Kx.Room.Attribute.VIP.Text", Value: "Very important person"},
					germdatabase.CustomSetting{CustomSettingID: 2, CustomSetting: "Kx.Room.Attribute.Disabled.Text", Value: "Accessible"},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, BlockID: sql.NullInt64{Int64: 11, Valid: true}, BedroomTypeID: sql.NullInt64{Int64: 11, Valid: true}, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Lisa",
					"surname": "MillsCorp",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [{"key": "smoking", "value": "Smoking"}, {"key": "disabled", "value": "Accessible"}, {"key": "vip", "value": "Very important person"}],
					"block": {
						"id": 11,
						"name": "East Wing",
						"areaId": 1,
						"siteId": 1
					},
					"bedroomType": {
						"id": 11,
						"name": "Single2"
					},
					"isCheckedIn": false,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					}
				}`,
		},
		{
			name: "should return null for block or bedroom type properties if they don't exist",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Sarah", Surname: "Bridge", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Sarah",
					"surname": "Bridge",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"isCheckedIn": false,
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					}
				}`,
		},
		{
			name: "if attendee is allocated a bedroom, should return bedroom along with the attendee",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Dean", Surname: "Mills", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), Valid: true}, DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Dean",
					"surname": "Mills",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					},
					"isCheckedIn": true,
					"roomAllocation": {
						"isFlat": false,
						"bedroomID": 1,
						"bedroomName": "E01",
						"bedroomType": "Single2",
						"bedroomTypeID": 11,
						"blockID": 11,
						"blockName": "East Wing",
						"siteID": 1,
						"areaID": 1,
						"subBlockID": 0,
						"subBlockName": "",
						"actualCapacity": 1,
						"typicalCapacity": 1,
						"occupied": 1,
						"status": "C",
						"attributes": []
					}
				}`,
		},
		{
			name: "if attendee is been allocated to a different room in the past, only show the most most recent room allocation",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Dean", Surname: "Mills", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), Valid: true}, DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Dean",
					"surname": "Mills",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					},
					"isCheckedIn": true,
					"roomAllocation": {
						"isFlat": false,
						"bedroomID": 2,
						"bedroomName": "E02",
						"bedroomType": "Single2",
						"bedroomTypeID": 11,
						"blockID": 11,
						"blockName": "East Wing",
						"siteID": 1,
						"areaID": 1,
						"subBlockID": 0,
						"subBlockName": "",
						"actualCapacity": 1,
						"typicalCapacity": 1,
						"occupied": 1,
						"status": "C",
						"attributes": []
					}
				}`,
		},
		{
			name: "if attendee has been allocated to a room in the past, but it's since been deleted, don't show a room allocation",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Dean", Surname: "Mills", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), Valid: true}, DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Dean",
					"surname": "Mills",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					},
					"isCheckedIn": true
				}`,
		},
		{
			name: "if attendee is allocated to a room in a different event, don't show a room allocation",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 11, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 22, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Dean", Surname: "Mills", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 2, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 2, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), Valid: true}, DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Dean",
					"surname": "Mills",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					},
					"isCheckedIn": true
				}`,
		},
		{
			name: "if attendee is allocated to a room that is a flat, the room allocation should have isFlat set to true",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Dean", Surname: "Mills", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2", RoomInFlat: true},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Dean",
					"surname": "Mills",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": null,
					"bedroomType": null,
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": false
					},
					"isCheckedIn": false,
					"roomAllocation": {
						"isFlat": true,
						"bedroomID": 1,
						"bedroomName": "E01",
						"bedroomType": "Single2",
						"bedroomTypeID": 11,
						"blockID": 11,
						"blockName": "East Wing",
						"siteID": 1,
						"areaID": 1,
						"subBlockID": 0,
						"subBlockName": "",
						"actualCapacity": 1,
						"typicalCapacity": 1,
						"occupied": 1,
						"status": "C",
						"attributes": []
					}
				}`,
		},
		{
			name: "if event has any ResidentialRoomAllocations aka rooming list, then hasRoomingList should be true",
			path: map[string]string{"eventID": "11", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 11, EventModuleId: 1, SiteID: 20, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 22, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 1, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 20, Name: "Site 20"},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 11, Description: "Single2", RoomInFlat: true},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 11, AreaID: 1, Name: "East Wing"},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 1, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 11, BlockID: 11, MaidStatus: "C", Capacity: 1, TypicalCapacity: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 1, PersonID: 1, PersonSequenceID: 0, BlockID: sql.NullInt64{Int64: 11, Valid: true}, BedroomTypeID: sql.NullInt64{Int64: 11, Valid: true}, ArrivalDate: time.Date(2019, 1, 1, 9, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 3, 17, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"id": 11,
					"forename": "Joe",
					"surname": "Bloggs",
					"arrival": "2019-01-01T09:00:00",
					"departure": "2019-01-03T17:00:00",
					"attributes": [],
					"block": {
						"areaId": 1,
						"id": 11,
						"name": "East Wing",
						"siteId": 1
					},
					"bedroomType": {
						"id": 11,
						"name": "Single2"
					},
					"event": {
						"id": 11,
						"siteId": 20,
						"hasRoomingList": true
					},
					"isCheckedIn": false
				}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters: tc.path,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

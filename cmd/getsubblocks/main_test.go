package main

import (
	"context"
	"encoding/base64"
	"net/http"
	"os"
	"testing"
	"time"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_subblocks_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name            string
		path            map[string]string
		query           map[string]string
		initialDatabase *germdatabase.GermDatabase
		expectedStatus  int
		expectedBody    string
	}{
		{
			name:           "scope must be one of roominglist, reservations or all",
			query:          map[string]string{"scope": "not_a_scope", "eventID": "1", "masterBlockId": "abc"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "scope",
					"error": "scope must be either roominglist, reservations, or all"
				}]
			}`,
		},
		{
			name:           "if the scope query parameter is roominglist, then eventID is required",
			query:          map[string]string{"scope": "roominglist"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID is required if scope is set"
				}]
			}`,
		},
		{
			name:           "if the scope query parameter is reservations, then eventID is required",
			query:          map[string]string{"scope": "reservations"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID is required if scope is set"
				}]
			}`,
		},
		{
			name:           "eventID must be a number",
			query:          map[string]string{"eventID": "notANumber"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "eventID must be an integer"
				}]
			}`,
		},
		{
			name:           "if masterBlockId is not provided, return 400",
			query:          map[string]string{},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "masterBlockId",
					"error": "masterBlockId is required"
				}]
			}`,
		},
		{
			name:           "if masterBlockId is not a number, return 400",
			query:          map[string]string{"masterBlockId": "abc"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "masterBlockId",
					"error": "masterBlockId must be a number"
				}]
			}`,
		},
		{
			name:  "should return list of subblocks for masterBlockID 1",
			query: map[string]string{"masterBlockId": "1"},
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 2, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 1, Name: "East Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 2, MasterBlockID: 1, Name: "West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 3, MasterBlockID: 1, Name: "South West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 4, MasterBlockID: 1, Name: "Lakeside Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 5, MasterBlockID: 2, Name: "Woodland Cabins"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"subblocks": [
						{
							"id": 2,
							"name": "West Wing"
						},
						{
							"id": 3,
							"name": "South West Wing"
						},
						{
							"id": 4,
							"name": "Lakeside Cabins"
						}
					]
				}`,
		},
		{
			name:  "should return list of subblocks for masterBlockID 2",
			query: map[string]string{"masterBlockId": "2"},
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 2, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 1, Name: "East Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 2, MasterBlockID: 1, Name: "West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 3, MasterBlockID: 1, Name: "South West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 4, MasterBlockID: 1, Name: "Lakeside Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 5, MasterBlockID: 2, Name: "Woodland Cabins"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"subblocks": [
						{
							"id": 5,
							"name": "Woodland Cabins"
						}
					]
				}`,
		},
		{
			name:  "when scope is rooming list, only show subblocks from rooming list rooms filtered by masterblockid and eventid",
			query: map[string]string{"masterBlockId": "2", "eventID": "2", "scope": "roominglist"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 1, EventModuleId: 1, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 2, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 1, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 2, EventModuleID: 2, ResidentialRoomID: 2, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 3, EventModuleID: 2, ResidentialRoomID: 3, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 4, EventModuleID: 2, ResidentialRoomID: 2, FirstNight: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 5, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 5, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 4, CreationDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 2, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 99, Name: "North Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 2, Name: "West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 3, Name: "South West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 4, MasterBlockID: 3, Name: "Lakeside Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 5, MasterBlockID: 2, Name: "Woodland Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 6, MasterBlockID: 2, Name: "Mountain Cabins"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"subblocks": [
						{
							"id": 5,
							"name": "Woodland Cabins"
						}
					]
				}`,
		},
		{
			name:  "when scope is reservations, only show subblocks from reservation rooms filtered by masterblockid and eventid",
			query: map[string]string{"masterBlockId": "2", "eventID": "2", "scope": "reservations"},
			initialDatabase: germdatabase.New().
				WithEventModules(
					germdatabase.EventModule{EventID: 1, EventModuleId: 1, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 2, EventModuleId: 2, SiteID: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithEvents(
					germdatabase.Event{EventID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 2, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 3, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 3, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 3, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 3, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC)},

					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 2, BlockID: 2, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 2, BlockID: 2, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 2, BlockID: 2, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 2, BlockID: 2, BedroomTypeID: 1, Allocation: 10, Date: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 5, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 5, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E02", BedroomTypeID: 1, BlockID: 2, SubBlockID: 4, CreationDate: germdatabase.DefaultDate()},
				).
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Site 1"},
					germdatabase.Site{SiteID: 2, Name: "Site 2"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 99, Name: "North Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 2, Name: "West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 3, Name: "South West Wing"},
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 4, MasterBlockID: 3, Name: "Lakeside Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 5, MasterBlockID: 2, Name: "Woodland Cabins"},
					germdatabase.SiteBlock{SiteID: 2, AreaID: 1, BlockID: 6, MasterBlockID: 3, Name: "Mountain Cabins"},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"subblocks": [
						{
							"id": 5,
							"name": "Woodland Cabins"
						}
					]
				}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()
			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters:        tc.path,
				QueryStringParameters: tc.query,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

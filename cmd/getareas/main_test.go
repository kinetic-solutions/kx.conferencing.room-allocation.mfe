package main

import (
	"context"
	"encoding/base64"
	"net/http"
	"os"
	"testing"
	"time"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_areas_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name            string
		initialDatabase *germdatabase.GermDatabase
		path            map[string]string
		query           map[string]string
		expectedStatus  int
		expectedBody    string
	}{
		{
			name:           "if siteId is not provided, return 400",
			query:          map[string]string{},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "siteId",
					"error": "siteId is required"
				}]
			}`,
		},
		{
			name:           "if siteId is not a number, return 400",
			query:          map[string]string{"siteId": "abc"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "siteId",
					"error": "siteId must be a number"
				}]
			}`,
		},
		{
			name:           "scope must be one of roominglist, reservations or all",
			query:          map[string]string{"scope": "not_a_scope", "siteId": "1"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "scope",
					"error": "scope must be either roominglist, reservations, or all"
				}]
			}`,
		},
		{
			name:  "should return list of rooming list areas filtered for site 10",
			query: map[string]string{"scope": "roominglist", "siteId": "10"},
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Kinetic Solutions"},
					germdatabase.Site{SiteID: 10, Name: "Site Two"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 1, Name: "Site Block 0"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 2, BlockID: 2, Name: "Site Block 1"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 3, BlockID: 3, Name: "Site Block 2"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 4, BlockID: 4, Name: "Site Block 3"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 2, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 3, CreationDate: germdatabase.DefaultDate()},
				).
				WithAreas(
					germdatabase.Area{AreaID: 1, Description: "Riverside Cabins", SiteID: 1},
					germdatabase.Area{AreaID: 2, Description: "Lakeside Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 3, Description: "Woodland Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 4, Description: "Mountain Cabins", SiteID: 10},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"areas": [
						{
							"id": 2,
							"name": "Lakeside Cabins"
						},
						{
							"id": 3,
							"name": "Woodland Cabins"
						}
					]
				}`,
		},
		{
			name:  "should return list of all areas filtered for site 10",
			query: map[string]string{"scope": "all", "siteId": "10"},
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Kinetic Solutions"},
					germdatabase.Site{SiteID: 10, Name: "Site Two"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 1, Name: "Site Block 0"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 2, BlockID: 2, Name: "Site Block 1"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 3, BlockID: 3, Name: "Site Block 2"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 4, BlockID: 4, Name: "Site Block 3"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 2, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 3, CreationDate: germdatabase.DefaultDate()},
				).
				WithAreas(
					germdatabase.Area{AreaID: 1, Description: "Riverside Cabins", SiteID: 1},
					germdatabase.Area{AreaID: 2, Description: "Lakeside Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 3, Description: "Woodland Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 4, Description: "Mountain Cabins", SiteID: 10},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"areas": [
						{
							"id": 2,
							"name": "Lakeside Cabins"
						},
						{
							"id": 4,
							"name": "Mountain Cabins"
						},
						{
							"id": 3,
							"name": "Woodland Cabins"
						}
					]
				}`,
		},
		{
			name:  "should return list of reservations areas filtered for site 10",
			query: map[string]string{"scope": "reservations", "siteId": "10"},
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Kinetic Solutions"},
					germdatabase.Site{SiteID: 10, Name: "Site Two"},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, AreaID: 1, BlockID: 1, Name: "Site Block 0"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 2, BlockID: 2, Name: "Site Block 1"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 3, BlockID: 3, Name: "Site Block 2"},
					germdatabase.SiteBlock{SiteID: 10, AreaID: 4, BlockID: 4, Name: "Site Block 3"},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 2, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 2, BlockID: 4, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 2, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, BlockID: 3, CreationDate: germdatabase.DefaultDate()},
				).
				WithAreas(
					germdatabase.Area{AreaID: 1, Description: "Riverside Cabins", SiteID: 1},
					germdatabase.Area{AreaID: 2, Description: "Lakeside Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 3, Description: "Woodland Cabins", SiteID: 10},
					germdatabase.Area{AreaID: 4, Description: "Mountain Cabins", SiteID: 10},
				),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"areas": [
						{
							"id": 2,
							"name": "Lakeside Cabins"
						},
						{
							"id": 4,
							"name": "Mountain Cabins"
						}
					]
				}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters:        tc.path,
				QueryStringParameters: tc.query,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

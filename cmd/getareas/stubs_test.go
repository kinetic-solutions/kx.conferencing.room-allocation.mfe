package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/docker"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/pool"
	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"
)

func createDBPool(ctx context.Context, t *testing.T, prefix string,
) (*germ.DatabasePool, error) {
	t.Helper()
	r := require.New(t)

	var server germ.Server

	germConn := os.Getenv("GERM_CONNECTION")
	if germConn != "" {
		t.Log("Using existing connection from the environment")
		var c germ.Connection
		err := json.Unmarshal([]byte(germConn), &c)
		r.NoError(err)
		server = germ.Existing(c)
	} else {
		t.Log("starting Docker")
		docker, err := docker.StartDocker(ctx,
			docker.WithName("GERM-pool"),
			docker.WithEnv(map[string]string{
				"SA_PASSWORD": "Aa1*xxxx",
			}),
		)
		r.NoError(err)
		server = docker
	}

	t.Log("setting up a pool of databases with prefix", prefix)
	return pool.NewPool(ctx, t.Logf, server,
		pool.WithNamePrefix(prefix))
}

func seedDB(ctx context.Context, db *sqlx.DB) error {
	tx, err := db.BeginTxx(ctx, &sql.TxOptions{Isolation: sql.LevelReadUncommitted})
	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}
	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, `
		DECLARE @date1 DATE =  CAST(N'2021-04-15T00:00:00.000' AS DateTime)

		SET IDENTITY_INSERT Sites ON
			INSERT INTO Sites (SiteID, Name, OffSite, DefaultBookingArrivalTime, DefaultBookingDepartureTime)
			VALUES
			(9, 'Site One', 0, '1899-12-30 09:00:00.000', '1899-12-30 14:00:00.000'),
			(10, 'Site Two', 0, '1899-12-30 09:00:00.000', '1899-12-30 14:00:00.000'),
			(11, 'Site Three', 0, '1899-12-30 09:00:00.000', '1899-12-30 14:00:00.000')
		SET IDENTITY_INSERT Sites OFF

		SET IDENTITY_INSERT Areas ON
			INSERT INTO Areas (AreaID, [Description], SiteID)
			VALUES
			(1, 'Main Area', 9),
			(2, 'Small Area', 9),
			(3, 'Lakeside Cabins', 10),
			(4, 'Woodland Cabins', 10)
		SET IDENTITY_INSERT Areas OFF

		SET IDENTITY_INSERT SiteBlocks ON
			INSERT INTO SiteBlocks
			(BlockID, SequenceNo, Name,              SiteID, AreaID, BlockType) VALUES
			(11,      2,          'East Wing',       9,       1,      'R'),
			(12,      1,          'West Wing',       9,       2,      'R'),
			(13,      99,         'Lakeside Cabins', 10,      3,      'R'),
			(14,      0,          'Woodland Cabins', 10,      4,      'R')
		SET IDENTITY_INSERT SiteBlocks OFF

		INSERT INTO ResidentialRooms
		(BedroomTypeID, BlockID, Name,  Capacity, Beds, SubBlockID, Smoking, Disabled, LastLet, CreationDate) VALUES
		/* singles - East (30) */
		(11,            11,      'E01', 1,        1,    0,          0,       0,        0,       @date1),
		(12,            12,      'E02', 1,        1,    0,          0,       0,        0,       @date1),
		(12,            13,      'E03', 1,        1,    0,          0,       0,        0,       @date1),
		(12,            14,      'E04', 1,        1,    0,          0,       0,        0,       @date1)
	`)

	if err != nil {
		return err
	}

	return tx.Commit()
}

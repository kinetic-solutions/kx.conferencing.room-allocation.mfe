package main

import (
	"context"
	"database/sql"
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	snspub "bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog/sns"
	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/smithy-go/ptr"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func TestDBHappyPath(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_allocate_room_db_happy_path_")
	r.NoError(err)

	t.Log("leasinga a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)

	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name              string
		path              map[string]string
		initialDatabase   *germdatabase.GermDatabase
		expectedStatus    int
		body              string
		expectedBody      string
		expectedDatabase  *germdatabase.GermDatabase
		expectedAudits    map[string]sns.PublishInput
		expectedHttpCalls map[string]stubHttpRequest
	}{
		{
			name:           "if event id is missing, return 400",
			path:           map[string]string{"attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id is required"
				}]
			}`,
		},
		{
			name:           "if event id is a non-integer, return 400",
			path:           map[string]string{"eventID": "{}", "attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be an integer"
				}]
			}`,
		},
		{
			name:           "if event id is less than 1, return 400",
			path:           map[string]string{"eventID": "0", "attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if attendee id is missing, return 400",
			path:           map[string]string{"eventID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id is required"
				}]
			}`,
		},
		{
			name:           "if attendee id is a non-integer, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "{}", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be an integer"
				}]
			}`,
		},
		{
			name:           "if attendee id is less than 1, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "0", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if room id is missing, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "roomID",
					"error": "room id is required"
				}]
			}`,
		},
		{
			name:           "if room id is a non-integer, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "11", "roomID": "{}"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "roomID",
					"error": "room id must be an integer"
				}]
			}`,
		},
		{
			name:           "if room id is less than 1, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "11", "roomID": "0"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "roomID",
					"error": "room id must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if event doesn't exist, return 404",
			path:           map[string]string{"eventID": "99999999", "attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "event 99999999 not found"
			}`,
		},
		{
			name: "if attendee doesn't exist, return 404",
			path: map[string]string{"eventID": "12", "attendeeID": "99999999", "roomID": "11"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 11, EventModuleId: 11, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 11, Forename: "Bob", Surname: "Smith", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "attendee 99999999 not found"
			}`,
		},
		{
			name: "when attendee exists but not associated with the given event, then return 404",
			path: map[string]string{"eventID": "12", "attendeeID": "13", "roomID": "11"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 11, EventModuleId: 11, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 11, Forename: "Bob", Surname: "Smith", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 11, PersonID: 11, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "attendee 13 not found"
			}`,
		},
		{
			name: "if room doesn't exist, return 404",
			path: map[string]string{"eventID": "12", "roomID": "99999999", "attendeeID": "11"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: 11, Forename: "Bob", Surname: "Smith", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 12, PersonID: 11, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "room 99999999 not found"
			}`,
		},
		{
			name: "when the room is full or already occupied, then return unprocessable entity",
			path: map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 11, Forename: "Bob", Surname: "Smith", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, PersonID: 12, EventModuleID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 12, PersonID: 11, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "this room is full or already occupied",
				"fields": [
					{
						"field": "roomId",
						"error": "1"
					}
				]
			}`,
		},
		{
			name:           "if params are valid, then the database is updated",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
			expectedAudits: map[string]sns.PublishInput{
				"1": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Allocated\",\"message\":[\"Reservation for James Lieu in room E02 Added\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
			},
		},
		{
			name:           "if params are valid and residential room occupants with -99999 exist, then update those to new person id",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
			expectedAudits: map[string]sns.PublishInput{
				"1": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Allocated\",\"message\":[\"Reservation for James Lieu in room E02 Added\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
			},
		},
		{
			name:           "if checked in attendee adds another allocation, allocate new room(s) starting from end of current room allocation",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), CheckedInDate: sql.NullTime{Valid: true, Time: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC)}},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 4, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 5, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name:           "if checked in attendee adds another allocation BUT provides the first night, then allocate new room(s) starting from the provided firstNight",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			body:           `{ "firstNight":"2021-02-02" }`,
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), CheckedInDate: sql.NullTime{Valid: true, Time: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC)}},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 4, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 5, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
				),
		},

		{
			name:           "if checked in is GERM Null date then ignore and override any existing room occupants with new allocation - edge case",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 3, Name: "E03", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 3, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 7, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), CheckedInDate: sql.NullTime{Valid: true, Time: time.Date(1899, 12, 30, 0, 0, 0, 0, time.UTC)}},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 4, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 5, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 6, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name:           "if attendee already has a room allocation, and that room is on the rooming list - those room occupant records should have their personID updated to -99999",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 13, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 13, EventModuleId: 13, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 1, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 13, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 13, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 13, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 1, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 2, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 13, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 13, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
			expectedAudits: map[string]sns.PublishInput{
				"1": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Allocated\",\"message\":[\"Reservation for James Lieu in room E02 Added\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
				"2": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Un-Allocated\",\"message\":[\"Reservation for James Lieu removed from room E01\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
			},
			expectedHttpCalls: map[string]stubHttpRequest{
				"1": {
					url:  "https://conferencing.api.kxcloud.net/abc-123/bedroom-event-handlers/handle-allocate-room",
					body: "{\"attendeeID\":12,\"eventID\":12,\"firstNight\":\"2021-02-01T00:00:00Z\",\"lastNight\":\"2021-02-02T00:00:00Z\",\"roomID\":2}"},
			},
		},
		{
			name:           "if attendee already has a room allocation, and that room is not on the rooming list - those room occupant records should be deleted",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name:           "if attendee already has a room allocation, and that room still has someone else in it - the room occupant records for our attendee should be deleted, but the other attendee should still remain",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 13, Forename: "John", Surname: "Smith", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 12, ResidentialRoomID: 2, FirstNight: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, Capacity: 2, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 13, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 13, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 13, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 12, PersonID: 13, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if a block/bedroom type reservations exist and are sufficient, the reservations are untouched",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Jane", Surname: "Doe", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(germdatabase.SiteBlock{SiteID: 1, BlockID: 1}).
				WithBedroomTypes(germdatabase.BedroomType{BedroomTypeID: 1}).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2023, 4, 14, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2023, 4, 16, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E01", BedroomTypeID: 1, BlockID: 1, Capacity: 1, CreationDate: germdatabase.DefaultDate()},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2023, 4, 14, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2023, 4, 17, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 14, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 15, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 3, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 16, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2023, 4, 14, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2023, 4, 15, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 0, Date: time.Date(2023, 4, 16, 0, 0, 0, 0, time.UTC)},
				),

			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 14, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 15, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2023, 4, 16, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if the room that appears in the ResidentialRoomRestrictions table, then return 422",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "4"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Jane", Surname: "Doe", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 3, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomRestrictions(
					germdatabase.ResidentialRoomRestriction{ResidentialRoomRestrictionID: 1, ResidentialRoomID: 4, BedIndex: 0},
				).
				WithResidentialRoomRestrictionHeader(
					germdatabase.ResidentialRoomRestrictionHeader{ResidentialRoomRestrictionID: 1, BlockID: 1, StartDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationUserID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "this room is out of service",
				"fields": [
					{
						"field": "roomId",
						"error": "2"
					}
				]
			}`,
		},
		{
			name: "if the room has their availability restricted by RABBT, then return 422",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "4"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Jane", Surname: "Doe", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 3, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithAvailabilityRestrictions(
					germdatabase.AvailabilityRestriction{AvailabilityRestrictionID: 1, EventBusinessTypeID: 5, BlockID: 1, BedroomTypeID: 1, FromDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), ToDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithAvailabilityResidentialRooms(
					germdatabase.AvailabilityRestrictionResidentialRoom{AvailabilityRestrictionID: 1, ResidentialRoomID: 4},
				).
				WithControl(
					germdatabase.Control{StudentEventBusinessTypeID: 1},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "this room is has its availability restricted",
				"fields": [
					{
						"field": "roomId",
						"error": "5"
					}
				]
			}`,
		},
		{
			name:           "if the room that appears in the ResidentialRoomRestrictions table but has custom settings AllowDifferentAllocationOverBooking, then return 422",
			path:           map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "4"},
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Jane", Surname: "Doe", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 3, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithResidentialRoomRestrictions(
					germdatabase.ResidentialRoomRestriction{ResidentialRoomRestrictionID: 1, ResidentialRoomID: 4, BedIndex: 0},
				).
				WithResidentialRoomRestrictionHeader(
					germdatabase.ResidentialRoomRestrictionHeader{ResidentialRoomRestrictionID: 1, BlockID: 1, StartDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC), CreationUserID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithCustomSettings(
					germdatabase.CustomSetting{CustomSettingID: 6635, CustomSetting: "AllowDifferentAllocationOverBooking", Value: "T"},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
		},
		{
			name:           "if the room falls under a full allocation, then return 422",
			path:           map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "4"},
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "this room's allocation is full",
				"fields": [
					{
						"field": "roomId",
						"error": "3"
					}
				]
			}`,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Jane", Surname: "Doe", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 4, Name: "E04", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleBlockBedroomAllocation(
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 1, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 2, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.EventModuleBlockBedroomAllocation{EMBBAID: 3, EventModuleID: 1, BlockID: 1, Allocation: 1, BedroomTypeID: 1, Date: time.Date(2019, 1, 3, 0, 0, 0, 0, time.UTC)},
				).
				WithControl(
					germdatabase.Control{StudentEventBusinessTypeID: 1},
				).
				WithAllocationReleaseStyles(
					germdatabase.AllocationReleaseStyle{ReleaseStyleID: 1, Description: "Allocation release style"},
				).
				WithAllocationSets(
					germdatabase.AllocationSet{AllocationSetID: 1, Name: "Test Allocation Set", SalesTypeID: 1, StartDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2019, 1, 4, 0, 0, 0, 0, time.UTC), CreationUserID: 1, CreationDate: germdatabase.DefaultDate(), StatusID: 1, ReleaseStyleID: 1},
				).
				WithAllocationSetDayQuantities(
					germdatabase.AllocationSetDayQuantity{AllocationSetDayQuantityID: 1, AllocationSetRoomTypeID: 1, DayDate: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC), InitialRoomQuantity: 1, CurrentRoomQuantity: 2},
				).
				WithAllocationSetRoomTypes(
					germdatabase.AllocationSetRoomType{AllocationSetRoomTypeID: 1, AllocationSetID: 1, BedroomTypeID: 1, SiteID: 1, AreaID: 1, BlockID: 1, InitialRoomQuantity: 1},
				).
				WithAllocationSetStatuses(
					germdatabase.AllocationSetStatus{StatusID: 1, Description: "Allocation set status", Active: true},
				).
				WithAllocationCompanies(
					germdatabase.AllocationCompany{AllocationCompanyID: 1, AllocationSetID: 1, CompanyID: 1},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
		},
		{
			name: "should not find room 2 since it's been occupied by an attendee from another event",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()},
					germdatabase.Event{EventID: 13, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.EventModule{EventID: 13, EventModuleId: 2, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 9}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 9, BlockID: 1, Name: "East Wing", AreaID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, Description: "Single"},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, Capacity: 1, TypicalCapacity: 1, MaidStatus: "C", CreationDate: germdatabase.DefaultDate()},
				).
				WithPeople(
					germdatabase.Person{PersonID: 12, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 13, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithControl(
					germdatabase.Control{StudentEventBusinessTypeID: 1},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 2, PersonID: 13, PersonSequenceID: 0, Date: time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 11, EventModuleID: 2, PersonID: 13, PersonSequenceID: 0, ArrivalDate: time.Date(2019, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2019, 1, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "this room is occupied by an attendee from another event",
				"fields": [
					{
						"field": "roomId",
						"error": "4"
					}
				]
			}`,
		},
		{
			name:           "If there's a firstnight parameter in body then update accordingly",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "5"},
			body:           `{ "firstNight":"2021-02-23"}`,
			expectedStatus: http.StatusNoContent,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 6, Name: "E06", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 5, Name: "E05", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 21, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 25, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 21, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 22, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 23, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 6, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 24, 0, 0, 0, 0, time.UTC)},
				),

			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 21, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 22, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 5, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 23, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 5, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 24, 0, 0, 0, 0, time.UTC)},
				),
			expectedAudits: map[string]sns.PublishInput{
				"1": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Allocated\",\"message\":[\"Reservation for James Lieu in room E05 Added\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
				"2": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Un-Allocated\",\"message\":[\"Reservation for James Lieu removed from room E02\",\"Reservation for James Lieu removed from room E06\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
			},
		},
		{
			name:           "if request body is invalid then return with 400 error ",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			body:           `{sadfsdfasdf}`,
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message":"Body is invalid JSON"
			}`,
		},
		{
			name:           "if request body date is invalid then return with 400 error ",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			body:           `{"firstNight":"asdasd"}`,
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "cannot marshal body: date format must be yyyy-mm-dd"
			}`,
		},
		{
			name:           "If there's a firstnight parameter is before arrival date then return 400",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "6"},
			body:           `{ "firstNight":"2021-01-02"}`,
			expectedStatus: http.StatusBadRequest,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 6, Name: "E06", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 3, 0, 0, 0, 0, time.UTC)},
				),
			expectedBody: `{
				"message": "firstNight parameter is before arrival date or todays date"
			}`,
		},
		{
			name:           "If there's a firstnight parameter is current date then return 400",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "6"},
			body:           fmt.Sprintf(`{ "firstNight":"%s"}`, time.Now().AddDate(0, 0, 1).Format("2006-01-02")),
			expectedStatus: http.StatusBadRequest,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 12, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 12, Forename: "James", Surname: "Lieu", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 6, Name: "E06", BedroomTypeID: 1, Capacity: 4, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithEventModuleDelegates(
					germdatabase.EventModuleDelegate{EventModuleDelegateID: 12, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, ArrivalDate: time.Date(2021, 2, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2021, 2, 4, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 12, PersonID: 12, PersonSequenceID: 0, Date: time.Date(2021, 2, 3, 0, 0, 0, 0, time.UTC)},
				),
			expectedBody: `{
				"message": "firstNight parameter is before arrival date or todays date"
			}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			stubSNS := newStubSNS(t, log)
			pub := snspub.New(stubSNS, "fake-sns-topic")

			stubHTTP := newStubHTTP(t, log)
			h := newHandler(log, pub, stubHTTP)

			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters: tc.path,
				Body:           tc.body,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					TimeEpoch: time.Date(2021, 10, 10, 9, 0, 0, 0, time.UTC).UnixNano() / int64(time.Millisecond), //Needs to be in milliseconds,
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user": base64.RawStdEncoding.EncodeToString([]byte(`{
								"sub": "user-sub-123",
								"https://kxcloud.net/germ/userids": {
									"xyz": ["42"]
								}
							}`)),
							"sub": "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			if tc.expectedBody != "" {
				a.JSONEq(tc.expectedBody, res.Body)
			} else {
				a.Empty(res.Body)
			}
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)

			if tc.expectedAudits != nil {
				a.Equal(tc.expectedAudits, stubSNS.messages)
			}

			if tc.expectedHttpCalls != nil {
				a.Equal(tc.expectedHttpCalls, stubHTTP.calls)
			}

			if tc.expectedDatabase != nil {
				tc.expectedDatabase.Assert(ctx, db, t)
			}
		})
	}
}

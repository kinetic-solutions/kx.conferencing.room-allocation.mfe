package main

import (
	"net/http"
	"os"

	"bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog"
	snspub "bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog/sns"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/allocateroom"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/httpclient"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
	"bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/logging"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-xray-sdk-go/xray"
	"go.uber.org/zap"
)

var (
	auditTopicArn string = os.Getenv("AUDIT_SNS_ARN") // arn:aws:sns:eu-west-1:129231881647:conferencing_audit_records
)

func main() {
	log := logging.New()
	defer log.Sync()
	xray.SetLogger(logging.Xray(log))

	sess := session.Must(session.NewSession())
	pub := snspub.New(sns.New(sess, &aws.Config{
		HTTPClient: xray.Client(http.DefaultClient),
	}), auditTopicArn)

	httpClient := xray.Client(http.DefaultClient)

	lambda.Start(newHandler(log, pub, httpClient))
}

func newHandler(log *zap.SugaredLogger, pub auditlog.Publisher, httpClient httpclient.HttpClient) lambdawrapper.Lambda {
	handler := allocateroom.Handler(pub, httpClient)
	return lambdawrapper.WrapFunc(log, handler)
}

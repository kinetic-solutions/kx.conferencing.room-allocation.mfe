package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/docker"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/pool"
	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

// stubSNS can be used instead of the real SNS client, for the purposes of
// publishing audit records.
type stubSNS struct {
	t     *testing.T
	log   *zap.SugaredLogger
	mu    *sync.Mutex
	count int
	// messages stores incoming sns messages, so we can observe them for testing
	messages map[string]sns.PublishInput
}

func newStubSNS(t *testing.T, log *zap.SugaredLogger) *stubSNS {
	return &stubSNS{
		t:        t,
		log:      log,
		mu:       new(sync.Mutex),
		messages: make(map[string]sns.PublishInput),
	}
}

// PublishWithContext is the minimal functionality that our fakeSNS requires.
// This implementation will write the sns.PublishInput to the logger.
func (s *stubSNS) PublishWithContext(_ context.Context, in *sns.PublishInput,
	_ ...request.Option) (*sns.PublishOutput, error) {
	s.t.Helper()

	s.log.Infow("fake publish to SNS", "sns input", in)

	time.Sleep(10 * time.Microsecond)

	s.mu.Lock()
	defer s.mu.Unlock()

	s.count++
	msgID := strconv.Itoa(s.count)
	s.messages[msgID] = *in

	return &sns.PublishOutput{
		MessageId: &msgID,
	}, nil
}

// stubHTTP can be used instead of the real HTTP client, for the purposes of
// making HTTP requests.
type stubHTTP struct {
	t     *testing.T
	log   *zap.SugaredLogger
	mu    *sync.Mutex
	count int
	// calls stores incoming http requests, so we can observe them for testing
	calls map[string]stubHttpRequest
}

func newStubHTTP(t *testing.T, log *zap.SugaredLogger) *stubHTTP {
	return &stubHTTP{
		t:     t,
		log:   log,
		mu:    new(sync.Mutex),
		calls: make(map[string]stubHttpRequest),
	}
}

type stubHttpRequest struct {
	body string
	url  string
}

// Do is the minimal functionality that our fakeHTTP requires.
// This implementation will write the httpRequest to the logger.
func (s *stubHTTP) Do(req *http.Request) (*http.Response, error) {
	s.t.Helper()

	s.log.Infow("fake HTTP request", "http request", req)

	time.Sleep(10 * time.Microsecond)

	s.mu.Lock()
	defer s.mu.Unlock()

	s.count++
	msgID := strconv.Itoa(s.count)
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	s.calls[msgID] = stubHttpRequest{
		body: string(body[:]),
		url:  req.URL.String(),
	}

	return &http.Response{
		StatusCode: http.StatusOK,
	}, nil
}

func createDBPool(ctx context.Context, t *testing.T, prefix string,
) (*germ.DatabasePool, error) {
	t.Helper()
	r := require.New(t)

	var server germ.Server

	germConn := os.Getenv("GERM_CONNECTION")
	if germConn != "" {
		t.Log("Using existing connection from the environment")
		var c germ.Connection
		err := json.Unmarshal([]byte(germConn), &c)
		r.NoError(err)
		server = germ.Existing(c)
	} else {
		t.Log("starting Docker")
		docker, err := docker.StartDocker(ctx,
			docker.WithName("GERM-pool"),
			docker.WithEnv(map[string]string{
				"SA_PASSWORD": "Aa1*xxxx",
			}),
		)
		r.NoError(err)
		server = docker
	}

	t.Log("setting up a pool of databases with prefix", prefix)
	return pool.NewPool(ctx, t.Logf, server,
		pool.WithNamePrefix(prefix))
}

package main

import (
	"context"
	"encoding/base64"
	"net/http"
	"os"
	"testing"

	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_sites_two_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("starting tests")

	tt := []struct {
		name            string
		initialDatabase *germdatabase.GermDatabase
		query           map[string]string
		expectedStatus  int
		expectedBody    string
	}{
		{
			name: "should return empty list if there are no sites",
			initialDatabase: germdatabase.New().
				WithSites().
				WithAreas(),
			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"sites": []
				}`,
		},
		{
			name: "should return list of all sites by default",
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Kinetic Solutions"},
					germdatabase.Site{SiteID: 2, Name: "Ashby House"},
				).
				WithAreas(
					germdatabase.Area{AreaID: 4, Description: "Student Union", SiteID: 1},
					germdatabase.Area{AreaID: 6, Description: "Sullivan Quad", SiteID: 2},
					germdatabase.Area{AreaID: 7, Description: "Holland House", SiteID: 2},
				),

			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"sites": [{
						"name": "Kinetic Solutions",
						"id": 1,
						"areas": [{
								"id": 4,
								"name": "Student Union"
							}]
					},
					{
						"name": "Ashby House",
						"id": 2,
						"areas": [
							{
								"id": 6,
								"name": "Sullivan Quad"
					     	},
							{
								"id": 7,
								"name": "Holland House"
							}
						]
					}]
				}`,
		},
		{
			name: "if a site doesn't have an area, then return empty array for the areas property",
			initialDatabase: germdatabase.New().
				WithSites(
					germdatabase.Site{SiteID: 1, Name: "Kinetic Solutions"},
				).
				WithAreas(),

			expectedStatus: http.StatusOK,
			expectedBody: `
				{
					"sites": [{
						"name": "Kinetic Solutions",
						"id": 1,
						"areas": []
					}]
				}`,
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			h := newWrapper(log)
			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}
			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				QueryStringParameters: tc.query,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
							"sub":                "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			a.JSONEq(tc.expectedBody, res.Body)

			a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)
		})
	}
}

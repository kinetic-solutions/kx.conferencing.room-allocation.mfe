package main

import (
	"context"
	"encoding/json"
	"os"
	"testing"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/docker"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/pool"
	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
	"github.com/stretchr/testify/require"
)

func createDBPool(ctx context.Context, t *testing.T, prefix string,
) (*germ.DatabasePool, error) {
	t.Helper()
	r := require.New(t)

	var server germ.Server

	germConn := os.Getenv("GERM_CONNECTION")
	if germConn != "" {
		t.Log("Using existing connection from the environment")
		var c germ.Connection
		err := json.Unmarshal([]byte(germConn), &c)
		r.NoError(err)
		server = germ.Existing(c)
	} else {
		t.Log("starting Docker")
		docker, err := docker.StartDocker(ctx,
			docker.WithName("GERM-pool"),
			docker.WithEnv(map[string]string{
				"SA_PASSWORD": "Aa1*xxxx",
			}),
		)
		r.NoError(err)
		server = docker
	}

	t.Log("setting up a pool of databases with prefix", prefix)
	return pool.NewPool(ctx, t.Logf, server,
		pool.WithNamePrefix(prefix))
}

package main

import (
	"context"
	"database/sql"
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	snspub "bitbucket.org/kinetic-solutions/kx.conferencing.audit-records/pkg/auditlog/sns"
	germdatabase "bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/database"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go/service/sns"

	"github.com/aws/smithy-go/ptr"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func TestDeAllocateRooms(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_deallocate_room_")
	r.NoError(err)
	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer db.Close()
	defer pool.Return(ctx, leasedDB)

	defer pool.Close(ctx)

	t.Log("starting tests")

	tt := []struct {
		name             string
		path             map[string]string
		query            map[string]string
		body             string
		initialDatabase  *germdatabase.GermDatabase
		expectedStatus   int
		expectedBody     string
		expectedDatabase *germdatabase.GermDatabase
		expectedAudits   map[string]sns.PublishInput
	}{
		{
			name:           "if event id is missing, return 400",
			path:           map[string]string{"attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id is required"
				}]
			}`,
		},
		{
			name:           "if event id is a non-integer, return 400",
			path:           map[string]string{"eventID": "{}", "attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be an integer"
				}]
			}`,
		},
		{
			name:           "if event id is less than 1, return 400",
			path:           map[string]string{"eventID": "0", "attendeeID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "eventID",
					"error": "event id must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if attendee id is missing, return 400",
			path:           map[string]string{"eventID": "11", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id is required"
				}]
			}`,
		},
		{
			name:           "if attendee id is a non-integer, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "{}", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be an integer"
				}]
			}`,
		},
		{
			name:           "if attendee id is less than 1, return 400",
			path:           map[string]string{"eventID": "11", "attendeeID": "0", "roomID": "11"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "Bad Request",
				"fields": [{
					"field": "attendeeID",
					"error": "attendee id must be greater than 0"
				}]
			}`,
		},
		{
			name:           "if request body is invalid then return with 400 error ",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			body:           `{sadfsdfasdf}`,
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message":"Body is invalid JSON"
			}`,
		},
		{
			name:           "if request body date is invalid then return with 400 error ",
			path:           map[string]string{"eventID": "12", "attendeeID": "12", "roomID": "2"},
			body:           `{"firstNight":"asdasd"}`,
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "cannot marshal body: date format must be yyyy-mm-dd"
			}`,
		},
		{
			name: "if requestBody has a firstNight parameter but it's before the attendee arrival date, then return an error",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			body: `{"firstNight":"2021-12-12"}`,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), Valid: true}, CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "firstNight parameter is before arrival date or todays date"
			}`,
		},
		{
			name: "if requestBody has a firstNight parameter but it's after todays date, then return an error",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			body: fmt.Sprintf(`{ "firstNight":"%s"}`, time.Now().AddDate(0, 0, 1).Format("2006-01-02")),
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), Valid: true}, CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusBadRequest,
			expectedBody: `{
				"message": "firstNight parameter is before arrival date or todays date"
			}`,
		},
		{
			name: "if event doesn't exist, return 404",
			path: map[string]string{"eventID": "99999999", "attendeeID": "11", "roomID": "11"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      1,
					CreationDate: germdatabase.DefaultDate(),
				}).
				WithEventModules(germdatabase.EventModule{
					EventID:       1,
					EventModuleId: 1,
					ArrivalDate:   germdatabase.DefaultDate(),
					DepartureDate: germdatabase.DefaultDate(),
					CreationDate:  germdatabase.DefaultDate(),
					ModifiedDate:  germdatabase.DefaultDate(),
				}),
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "event 99999999 not found"
			}`,
		},
		{
			name: "if attendee doesn't exist, return 404",
			path: map[string]string{"eventID": "11", "attendeeID": "99999999", "roomID": "11"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      1,
					CreationDate: germdatabase.DefaultDate(),
				}).
				WithEventModules(germdatabase.EventModule{
					EventID:       11,
					EventModuleId: 1,
					ArrivalDate:   germdatabase.DefaultDate(),
					DepartureDate: germdatabase.DefaultDate(),
					CreationDate:  germdatabase.DefaultDate(),
					ModifiedDate:  germdatabase.DefaultDate(),
				}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 11, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				),
			expectedStatus: http.StatusNotFound,
			expectedBody: `{
				"message": "attendee 99999999 not found"
			}`,
		},
		{
			name: "if the room is a flat, then return a 422",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithPeople(
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, BookByRoom: true},
				).WithResidentialRoomOccupants(
				germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
				germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
			).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "room 2 is a flat"
			}`,
		},
		{
			name: "if the room is a room in a flat, then return a 422",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithPeople(
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1, RoomInFlat: true},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).WithResidentialRoomOccupants(
				germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
				germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
			).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusUnprocessableEntity,
			expectedBody: `{
				"message": "room 2 is a room in a flat"
			}`,
		},
		{
			name: "completely removes allocations and creates an audit from roomID for attendeeID 10 since it's room isn't on the rooming list",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E02", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
			expectedAudits: map[string]sns.PublishInput{
				"1": {
					TopicArn: ptr.String("fake-sns-topic"),
					Message:  ptr.String("{\"records\":[{\"auditLogKRN\":\"krn:kx:audit-records::xyz:EventModuleAuditLogs/ResidentialRoomOccupants/0\",\"userId\":42,\"operation\":\"Room Un-Allocated\",\"message\":[\"Reservation for Joe Bloggs removed from room E02\"],\"meta\":null}],\"publisher\":\"kx.conferencing.room-allocation-mfe\",\"category\":\"Bedrooms\",\"eventId\":12,\"logType\":\"EventModuleAuditLogs\"}"),
					MessageAttributes: map[string]*sns.MessageAttributeValue{
						"category": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("Bedrooms"),
						},
						"tenantId": {
							DataType:    ptr.String("String"),
							StringValue: ptr.String("xyz"),
						},
					},
				},
			},
		},
		{
			name: "if the attendee has already checked in, we can still remove it's allocation",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CheckedInDate: sql.NullTime{Time: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), Valid: true}, CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "completely removes allocations from the request body's firstNight onwards, and creates an audit from roomID for attendeeID 10 since it's room isn't on the rooming list",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			body: `{"firstNight":"2022-01-02"}`,
			initialDatabase: germdatabase.New().
				WithEvents(germdatabase.Event{
					EventID:      12,
					CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, Forename: "Joe", Surname: "Bloggs", CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
				).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, Name: "E01", BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 3, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 4, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 5, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if the roomID 2 is on the rooming list , then it updates allocations for attendeeID 10 to have personID -99999",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 1, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 2, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if the roomID 2 is on the rooming list and the requestBody has a firstNight, then it updates allocations from the firstNight onwards for attendeeID 10 to have personID -99999",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			body: `{"firstNight":"2022-01-02"}`,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 1, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 1, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: -99999, PersonSequenceID: 1, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if the roomID 2 is on the rooming list, and it has more than 1 person inside it, then the person is deleted without creating -99999 records",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
		{
			name: "if the roomID 2 is on the rooming list and the requestBody has a firstNight, and it has more than 1 person inside it, then the person is deleted from the firstNight onwards without creating -99999 records",
			path: map[string]string{"eventID": "12", "attendeeID": "10", "roomID": "2"},
			body: `{"firstNight":"2022-01-02"}`,
			initialDatabase: germdatabase.New().
				WithEvents(
					germdatabase.Event{EventID: 12, CreationDate: germdatabase.DefaultDate()}).
				WithEventModules(
					germdatabase.EventModule{EventID: 12, EventModuleId: 1, ArrivalDate: germdatabase.DefaultDate(), DepartureDate: germdatabase.DefaultDate(), CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithPeople(
					germdatabase.Person{PersonID: -99999, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 9, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()},
					germdatabase.Person{PersonID: 10, CreationDate: germdatabase.DefaultDate(), ModifiedDate: germdatabase.DefaultDate()}).
				WithSites(germdatabase.Site{SiteID: 1}).
				WithSiteBlocks(
					germdatabase.SiteBlock{SiteID: 1, BlockID: 1},
				).
				WithBedroomTypes(
					germdatabase.BedroomType{BedroomTypeID: 1},
				).
				WithResidentialRooms(
					germdatabase.ResidentialRoom{ResidentialRoomID: 2, BedroomTypeID: 1, BlockID: 1, CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomAllocations(
					germdatabase.ResidentialRoomAllocation{EMRRAID: 1, EventModuleID: 1, ResidentialRoomID: 2, FirstNight: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), LastNight: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
				).
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				).WithEventModuleDelegates(
				germdatabase.EventModuleDelegate{EventModuleDelegateID: 10, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, ArrivalDate: time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC), DepartureDate: time.Date(2022, 1, 3, 10, 10, 0, 0, time.UTC), CreationDate: germdatabase.DefaultDate()},
			),
			expectedStatus: http.StatusNoContent,
			expectedDatabase: germdatabase.New().
				WithResidentialRoomOccupants(
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 10, PersonSequenceID: 0, Date: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)},
					germdatabase.ResidentialRoomOccupant{ResidentialRoomID: 2, EventModuleID: 1, PersonID: 9, PersonSequenceID: 0, Date: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC)},
				),
		},
	}
	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			r := require.New(t)
			log := zaptest.NewLogger(t).Sugar()

			stubSNS := newStubSNS(t, log)
			pub := snspub.New(stubSNS, "fake-sns-topic")
			h := newHandler(log, pub)

			ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})
			if tc.initialDatabase != nil {
				r.NoError(tc.initialDatabase.Persist(ctx, db))
			}

			res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
				PathParameters:        tc.path,
				QueryStringParameters: tc.query,
				Body:                  tc.body,
				RequestContext: events.APIGatewayV2HTTPRequestContext{
					TimeEpoch: time.Date(2021, 10, 10, 9, 0, 0, 0, time.UTC).UnixNano() / int64(time.Millisecond), //Needs to be in milliseconds,
					Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
						Lambda: map[string]interface{}{
							"tenantGermConn":     leasedDB.DSN(),
							"tenantCode":         "abc-123",
							"tenantId":           "xyz",
							"tenantJurisdiction": "UK",
							"tenantTimeZone":     "Europe/London",
							"user": base64.RawStdEncoding.EncodeToString([]byte(`{
								"sub": "user-sub-123",
								"https://kxcloud.net/germ/userids": {
									"xyz": ["42"]
								}
							}`)),
							"sub": "123-asd-vfd",
						},
					},
				},
			})
			r.NoError(err)

			a := assert.New(t)
			a.Equal(tc.expectedStatus, res.StatusCode)
			if tc.expectedBody != "" {
				a.JSONEq(tc.expectedBody, res.Body)
			}
			a.Equal(false, res.IsBase64Encoded)
			a.Empty(false, res.MultiValueHeaders)
			a.Empty(res.Cookies)

			if tc.expectedAudits != nil {
				a.Equal(tc.expectedAudits, stubSNS.messages)
			}

			if tc.expectedDatabase != nil {
				tc.expectedDatabase.Assert(ctx, db, t)
			}
		})
	}
}

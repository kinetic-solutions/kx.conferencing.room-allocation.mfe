package main

import (
	"context"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"

	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/docker"
	"bitbucket.org/kinetic-solutions/kx.conferencing.room-allocation.mfe/internal/germtest/pool"
	germ "bitbucket.org/kinetic-solutions/kx.testing.go-germ"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"
)

func Test_When_No_Custom_Settings(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_attributes_with_no_custom_settings")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	r.NoError(err)

	t.Log("starting tests")

	t.Run("when there are no custom settings for room attributes, then return list of default attributes", func(t *testing.T) {
		r := require.New(t)
		log := zaptest.NewLogger(t).Sugar()

		h := newWrapper(log)
		ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})

		res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
			RequestContext: events.APIGatewayV2HTTPRequestContext{
				Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
					Lambda: map[string]interface{}{
						"tenantGermConn":     leasedDB.DSN(),
						"tenantCode":         "abc-123",
						"tenantId":           "xyz",
						"tenantJurisdiction": "UK",
						"tenantTimeZone":     "Europe/London",
						"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
						"sub":                "123-asd-vfd",
					},
				},
			},
		})
		r.NoError(err)

		a := assert.New(t)
		a.Equal(http.StatusOK, res.StatusCode)
		a.JSONEq(`
			{
				"attributes": {
					"disabled": "Accessible",
					"smoking": "Smoking",
					"vip": "VIP",
					"lastLet": "Last Let"
				}
			}
		`, res.Body)

		a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
		a.Equal(false, res.IsBase64Encoded)
		a.Empty(false, res.MultiValueHeaders)
		a.Empty(res.Cookies)
	})
}

func Test_When_There_Are_Custom_Settings(t *testing.T) {
	os.Setenv("AWS_XRAY_SDK_DISABLED", "true")
	t.Parallel()
	r := require.New(t)
	ctx := context.Background()

	pool, err := createDBPool(ctx, t, "test_get_attributes_with_custom_settings_")
	r.NoError(err)

	t.Log("leasing a database for this set of test cases")
	leasedDB, err := pool.Lease(ctx)
	r.NoError(err)

	db, err := sqlx.Open("sqlserver", leasedDB.DSN())
	r.NoError(err)
	defer pool.Return(ctx, leasedDB)
	defer pool.Close(ctx)
	defer db.Close()

	t.Log("seeding the database")
	err = seedDB(ctx, db)
	r.NoError(err)

	t.Log("starting tests")
	t.Run("when there are no custom settings for room attributes, then return list of default attributes", func(t *testing.T) {
		r := require.New(t)
		log := zaptest.NewLogger(t).Sugar()

		h := newWrapper(log)
		ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})

		res, err := h(ctx, &events.APIGatewayV2HTTPRequest{
			RequestContext: events.APIGatewayV2HTTPRequestContext{
				Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
					Lambda: map[string]interface{}{
						"tenantGermConn":     leasedDB.DSN(),
						"tenantCode":         "abc-123",
						"tenantId":           "xyz",
						"tenantJurisdiction": "UK",
						"tenantTimeZone":     "Europe/London",
						"user":               base64.RawStdEncoding.EncodeToString([]byte("{}")),
						"sub":                "123-asd-vfd",
					},
				},
			},
		})
		r.NoError(err)

		a := assert.New(t)
		a.Equal(http.StatusOK, res.StatusCode)
		a.JSONEq(`
			{
				"attributes": {
					"disabled": "Handicapped",
					"smoking": "Searing",
					"vip": "Celebrity",
					"lastLet": "Some Random Option"
				}
			}
		`, res.Body)

		a.Equal(map[string]string{"Content-Type": "application/json"}, res.Headers)
		a.Equal(false, res.IsBase64Encoded)
		a.Empty(false, res.MultiValueHeaders)
		a.Empty(res.Cookies)
	})
}

func createDBPool(ctx context.Context, t *testing.T, prefix string,
) (*germ.DatabasePool, error) {
	t.Helper()
	r := require.New(t)

	var server germ.Server

	germConn := os.Getenv("GERM_CONNECTION")
	if germConn != "" {
		t.Log("Using existing connection from the environment")
		var c germ.Connection
		err := json.Unmarshal([]byte(germConn), &c)
		r.NoError(err)
		server = germ.Existing(c)
	} else {
		t.Log("starting Docker")
		docker, err := docker.StartDocker(ctx,
			docker.WithName("GERM-pool"),
			docker.WithEnv(map[string]string{
				"SA_PASSWORD": "Aa1*xxxx",
			}),
		)
		r.NoError(err)
		server = docker
	}

	t.Log("setting up a pool of databases with prefix", prefix)
	return pool.NewPool(ctx, t.Logf, server,
		pool.WithNamePrefix(prefix))
}

func seedDB(ctx context.Context, db *sqlx.DB) error {
	tx, err := db.BeginTxx(ctx, &sql.TxOptions{Isolation: sql.LevelReadUncommitted})
	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}
	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, `
		INSERT INTO CustomSettings
		(CustomSetting, 					Value				) VALUES
		('Kx.Room.Attribute.Disabled.Text', 'Handicapped'		),
		('Kx.Room.Attribute.LastLet.Text',  'Some Random Option'),
		('Kx.Room.Attribute.Smoking.Text',  'Searing' 			),
		('Kx.Room.Attribute.VIP.Text', 		'Celebrity'			)
	`)

	if err != nil {
		return err
	}

	return tx.Commit()
}

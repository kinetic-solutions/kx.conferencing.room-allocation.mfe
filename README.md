# MFE Template
This repo is intended to show how to structure a new Micro Front End (MFE).

## Starting a new MFE

1. Fork this template into a new Bitbucket repository
1. Configure Jenkins/Bitbucket
1. Configure `./public/webpack.config.js`
1. Change the GO module name in `./go.mod`
1. Implement the MFE front-end and API's
1. Update readme to better describe your MFE
1. push changes

## Directory Structure

The directory structure is as follows:

```bash
.
├── go.mod                            <-- Standard go mod file
├── go.sum                            <-- Standard go sum file
├── jenkins.go.dockerfile             <-- docker file used to build the go code within the Jenkins build process
├── Jenkinsfile                       <-- Jenkins build pipeline
├── Makefile                          <-- Make to automate build
├── README.md                         <-- This instructions file
├── cmd                               <-- Source code for all Lambda functions
│   └── {lambda-name}                 <-- Name of Lambda function
│       ├──── main_test.go            <-- e2e tests for the Lambda function
│       └──── main.go                 <-- Lambda function entry point
├── internal                          <-- Source code specific to this MFE
│   └── {package}                     <-- Name of package
│       └──── {package_code}.go       <-- Package code
├── public                            <-- React UI code
│   ├── webpack.config.js             <-- UI webpack config
│   └── src                           <-- UI code
│       ├──── core                    <-- standard UI components
│       ├──── data                    <-- Redux
│       ├──── features                <-- MFE components
│       ├──── locals                  <-- MFE specific translations
│       ├──── shared                  <-- shared components within the MFE
│       └──── kinetic-mfe-template.js <-- Single SPA entry point
└── template.yaml                     <-- SAM deployment file
```

## APIs

### Tenant Data Isolation
Our primary defence against Tenant data leakage is diligence. Everybody is responsible for ensuring their code is secure. 

The Lambda code we're writing is multi-tenant, meaning every request handed to the Lambda code could be for a different tenant. As such there are some rules that must be followed:

1. Keep it Stateless. It's better to be slow than leak data.
1. Never cache data/objects. By caching data or objects we greatly increase the chance exposing Tenants data.
1. Only use the GERM Database connection handed to the handler. Currently there is no reason to create your own database connections.
1. Use the Lambda Wrapper base package. This package simplifies the consumption of Lambda Events, ensuring that the correct Tenant database connection is used.
1. Don't leak secrets or private data in logging. Always be careful and purposeful with your log messages.
1. Remember, *keep it Stateless*. It's better to be slow than leak data.

### Sharing Endpoints across MFEs
Don't do it. By sharing an endpoint we create coupling, which reduces our ability to make changes fast. To quote Rob Pike:

> [A little copying is better than a little dependency.](https://www.youtube.com/watch?v=PAAkCSZUG1c&t=568s)

### Handler Example

Handlers are created by implementing the `lambdawrapper.Handler` interface. When the handler is invoked a new `lambdawrapper.LambdaContext` is generated and handed to your function. The LambdaContext exposes a `DB` field. The `DB` field can then be used to interact with the tenants GERM database. Below is an example of a handler.

``` go 
package helloworld

import (
  "fmt"
  "net/http"
  "time"

  "bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
)

// clock mimics time.Now function, allowing the developer to either pass
// the real time.Now function or a mocked time to the New function.
type clock func() time.Time

// helloHandler is the private request handler that implements lambdawrapper.Handler
type helloHandler struct{ now clock }

// response is the JSON response model for the handler. The `json` tags can be used
// to control the JSON encoding behaviour.
type response struct {
  Created time.Time `json:"created_at"`
  Hello   string    `json:"hello"`
}

// Serve receives as LambdaContext, which exposes a number of helpful fields,
// including access to structured logging, request details, and GERM.
func (h *helloHandler) Serve(lc *lambdawrapper.LambdaContext) error {
  // create the response object with the now dependency
  res := &response{Created: h.now()}

  // log offers structured, request specific, logging options
  lc.Log.Infow("selecting data from DB",
    "created", res.Created,
  )

  // run a database query, passing the result into res.Hello
  if err := lc.DB.GetContext(lc.Ctx, &res.Hello, `SELECT 'world'`); err != nil {
    // error handling is performed by returning an error directly
    return fmt.Errorf("cannot query DB: %w", err)
  }

  // return a 200 OK response with the JSON body from res
  return lc.JSON(http.StatusOK, res)
}

// New creates a helloWorldHandler ensuring all dependencies are passed
// in, returning the handler as the interface lambdawrapper.Handler
func New(now clock) lambdawrapper.Handler {
  if now == nil {
    panic("now is required")
  }

  return &helloHandler{now: now}
}

```

### Testing

All Lambda functions must have end two end (e2e) tests. To aid with this the [lambda-mfe](bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe) project exposes a `germ.Connect` function. The Connect function starts a local instance of GERM in Docker. The GERM database can then be used to create high level tests. Below is an example of testing the above `helloHandler`:

``` go
package helloworld

import (
  "context"
  "encoding/base64"
  "os"
  "testing"
  "time"

  "bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/lambdawrapper"
  "bitbucket.org/kinetic-solutions/kx.pulse.lambda-mfe/platform/test/germ"
  "github.com/aws/aws-lambda-go/events"
  "github.com/aws/aws-lambda-go/lambdacontext"
  "github.com/stretchr/testify/require"
  "go.uber.org/zap/zaptest"
)

func Test(t *testing.T) {
  // disable xray, otherwise we'll get panics
  os.Setenv("AWS_XRAY_SDK_DISABLED", "true")

  // create a context object for the tests
  ctx := context.Background()

  // start the test germ instance by either connecting to a already running instance or
  // using docker to start our own. Passing an empty germ.ConnectOptions means defaults
  // will be used.
  conn, err := germ.Connect(ctx, t, germ.ConnectOptions{})

  // make sure we didn't get an error while connecting
  require.NoError(t, err)

  // create testing table (tt) so that new tests can easily be added
  tt := []struct {
    name, expectedBody string
  }{
    {
      name: "should say hello: world",
      expectedBody: `{
        "created_at":"2010-10-01T20:23:00Z",
        "hello":"world"
      }`,
    },
  }

  // start each test, running them in parallel whenever possible
  for _, tc := range tt {
    t.Run(tc.name, func(t *testing.T) {
      t.Parallel()

      // mock the now function
      now := func() time.Time { return time.Date(2010, 10, 1, 20, 23, 0, 0, time.UTC) }

      // create test logger so we don't fill stdOut with junk
      log := zaptest.NewLogger(t).Sugar()

      // create lambda context object
      ctx := lambdacontext.NewContext(context.Background(), &lambdacontext.LambdaContext{AwsRequestID: "req-id"})

      // create our handler we're going to test
      handler := lambdawrapper.Wrap(log, New(now))

      // mock a users claims
      user := base64.RawStdEncoding.EncodeToString([]byte(`{"someClaim": true }`))

      // mock the lambda request object
      req := &events.APIGatewayV2HTTPRequest{
        PathParameters: map[string]string{},
        Body:           "{}",
        RequestContext: events.APIGatewayV2HTTPRequestContext{
          Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
            // Lambda is the properties that would normally be set by our authoriser
            Lambda: map[string]interface{}{
              "tenantGermConn": conn.DSN,         // tenant specific connection string
              "tenantCode":     "abc-123",        // tenant code
              "tenantId":       "xyz",            // tenant ID
              "user":           user,             // user claims
              "sub":            "user-sub-123",   // users sub
            },
          },
        },
      }

      // run the handler
      res, err := handler(ctx, req)

      // assert the handler response
      require.NoError(t, err)
      require.JSONEq(t, tc.expectedBody, res.Body)
    })
  }
}

```

## API Gateway
All API's are deployed behind AWS's API Gateway. There is one API Gateway per product, e.g. Conferencing's gateway is deployed as `https://conferencing.api.kxcloud.net`. Each MFE defines its own routes under their parent application.

### Authentication
Each Gateway uses our custom Authoriser. The Authoriser expects a `Authorization` header populated with a users JWT. See [kx.core.api-gateway-authenticator](https://bitbucket.org/kinetic-solutions/kx.core.api-gateway-authenticator) for the implementation.

The Authoriser is configured using [API Gateway Stage Variables](https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-stages.html#http-api-stages.stage-variables). The following Stage variables are required: 

| Key           | Value                                                         | Example                                       |
| ------------- | ------------------------------------------------------------- | --------------------------------------------- |
| oidcAudience  | The Audience that the token must include.                     | api://mykinetic.io/pulse/crm                  |
| oidcScopes    | Space separated list of Scopes that the token must include.   | subscriber-roles germ-user-details idp        |
| oidcIssuer    | Token issuer. Must be a URL.                                  | https://sso.mykinetic.io/auth/realms/__pulse  |
| appName       | Used to check the user has access to the application.         | crm                                           |

### Authorisation
The Gateway Authoriser will perform the following checks on the supplied JWT:

1. Is issued by the expected Issuer and is still valid
1. Has the correct Audience present
1. Has all the required Scopes
1. Has access to the requested Tenancy
1. Has at least one role granted for the requested App and Tenancy

Note, application specific role checks are extremely basic. The proxied Lambda function will still need to apply resource specific checks. If the Lambda code requires more details about the User the code can use the `LambdaContext.User` method:

```go
type myComplexUser struct {
  // embed the standard user claims
  users.User

  // custom claim we need to read
  IsAdmin bool `json:"https://kxcloud.net/admin"`
}

func (h *helloHandler) Serve(lc *lambdawrapper.LambdaContext) error {
  user := &myComplexUser{}

  if err := lc.User(user); err != nil {
    return err
  }

  lc.Log.Infow("user",
    "sub", user.Subject,
    "isAdmin", user.IsAdmin,
  )

  return lc.NoContent()
}
```

### API Namespacing

Each MFE must namespace their routes on the parent gateway. The URL structure is:

```bash
https://product.api.kxcloud.net/{tenantCode}/MFE-name/routes
```

Below is a explanation for each of the URL parts:

| Part          | Description                                                   |
| ------------- | ------------------------------------------------------------- |
| product       | The product that the MFE is being developed for.              |
| tenantCode    | Placeholder for the Tenant Code e.g. paytest1, this must match the value specified in the users JWT. The tenant code is used in the Authoriser and must be defined as `{tenantCode}` in the path.                   |
| MEF-name      | The name specified for this MFE.                              |
| routes        | Any routes that your MFE needs for its operation. These routes can define their own path parameters and be nested as deeply as you sensibly require.                                                                        |

### Sam

Each MFE is deployed using [SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html). The SAM `template.yml` is used to when when deploying the MFE. Below is a break down of each of the major sections.

#### Globals

The Globals section is boiler plate. It defines generic settings the Lambda will use, such as VPC settings, timeouts, and handlers. Generally a MFE shouldn't need to adjust these settings.

```yml
Globals:
  Function:
    Timeout: 5
    Runtime: go1.x
    Tracing: Active
    Handler: main
    VpcConfig:
      SecurityGroupIds:
        - sg-082435659b524896e
      SubnetIds:
        - subnet-0b2881ecf24570e8c
        - subnet-0b2c3bf62b0731948
        - subnet-039bc340db4f473e5
```

#### Parameters

Each MFE has the chance to uses a different API Gateway, hence you must first configure the `ApiId` and `AuthoriserId` in the globals section. e.g. for Conferencing the globals section would look like:

```yml
Parameters:
  ApiId:
    Type: String
    Default: "w5idth0vh3"
  AuthoriserId:
    Type: String
    Default: "82t088"
```

For other deployments, please speak to the Operations team.

#### Resources

The Resources sections defines each of the Lambdas that will be deployed. For each Lambda deployment there are four parts. You must update the resource name to something meaningful to your Lambda. The resource name in the below example is `exampleFunction`.

##### function definition

This section defines the Lambda function. You must change the `CodeUri` to your functions cmd path. You must also change the `FunctionName` to a logical unique name in the structure `<stack_name>-<function>` such as `example-mfe-exampleFunction` Unless your Lambda requires additional privileges the `Role` shouldn't be changed.

Every lambda function must have a corresponding Log Group resource (see Log Group definition).

```yml
exampleFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: ./cmd/hello-world
      Role: "arn:aws:iam::129231881647:role/PulseAppGenericApiFunctionRole"
      FunctionName: "example-mfe-exampleFunction"
```

##### Log Group Definition

This section defines the log group that your Lambda function will log in to. Not setting this log group definition will result in Lambda creating one for you, but with no log retention. You must change the `LogGroupName` to a specific structure `/aws/lambda/<your_function_name>`. Note, this resource has a dependency on the lambda function.

```yml
exampleFunctionLogGroup:
    Type: AWS::Logs::LogGroup
    DependsOn:
      - exampleFunction
    Properties:
      LogGroupName: '/aws/lambda/example-mfe-exampleFunction'
      RetentionInDays: 60
```

##### Integration

Integration defines the type of integration in API gateway. You must update the `IntegrationUri` to point at your new Lambda function ARN.

```yml
exampleFunctionIntegration:
    Type: "AWS::ApiGatewayV2::Integration"
    Properties:
      ApiId: !Ref ApiId
      IntegrationType: AWS_PROXY
      IntegrationUri: !Join
        - ""
        - - "arn:"
          - !Ref "AWS::Partition"
          - ":apigateway:"
          - !Ref "AWS::Region"
          - ":lambda:path/2015-03-31/functions/"
          - !GetAtt exampleFunction.Arn
          - /invocations
      IntegrationMethod: POST
      ConnectionType: INTERNET
      PayloadFormatVersion: 2.0
      TimeoutInMillis: 5000
```

##### Route

Route brings together the Lambda function and the Integration under a specific path. The Route also defines the Authoriser that is going to be used. You must updated the `DependsOn` and `Target` to the functions Integration. You must also update the `RouteKey` with the path/method you want to Lambda to respond to.

```yml
exampleFunctionRoute:
    Type: "AWS::ApiGatewayV2::Route"
    DependsOn:
      - exampleFunctionIntegration
    Properties:
      ApiId: !Ref ApiId
      RouteKey: "GET /{tenantCode}/example-mfe/path"
      AuthorizationType: "CUSTOM"
      AuthorizerId: !REF AuthoriserId
      Target: !Join
        - /
        - - integrations
          - !Ref exampleFunctionIntegration
```

##### Permissions

Permissions give API Gateway the ability to execute the Lambda function. Update the `FunctionName` to your Functions ARN and update the finial part of `SourceArn` to the same as the the Routes `RouteKey`.

```yml
exampleFunctionInvokePermissions:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt exampleFunction.Arn
      Principal: apigateway.amazonaws.com
      SourceAccount: 129231881647
      SourceArn: !Join
        - ""
        - - "arn:"
          - !Ref AWS::Partition
          - ":execute-api:"
          - !Ref "AWS::Region"
          - ":"
          - !Ref "AWS::AccountId"
          - ":"
          - !Ref ApiId
          - "/*/*/{tenantCode}/example-mfe/path"

```

#### Complete Example

```yml
Resources:
  exampleFunction:
    Type: AWS::Serverless::Function # More info about Function Resource: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md#awsserverlessfunction
    Properties:
      CodeUri: ./cmd/hello-world
      Role: "arn:aws:iam::129231881647:role/PulseAppGenericApiFunctionRole"
      FunctionName: "example-mfe-exampleFunction"
  
  exampleFunctionLogGroup:
    Type: AWS::Logs::LogGroup
    DependsOn:
      - exampleFunction
    Properties:
      LogGroupName: '/aws/lambda/example-mfe-exampleFunction'
      RetentionInDays: 60

  exampleFunctionIntegration:
    Type: "AWS::ApiGatewayV2::Integration"
    Properties:
      ApiId: !Ref ApiId
      IntegrationType: AWS_PROXY
      IntegrationUri: !Join
        - ""
        - - "arn:"
          - !Ref "AWS::Partition"
          - ":apigateway:"
          - !Ref "AWS::Region"
          - ":lambda:path/2015-03-31/functions/"
          - !GetAtt exampleFunction.Arn
          - /invocations
      IntegrationMethod: POST
      ConnectionType: INTERNET
      PayloadFormatVersion: 2.0
      TimeoutInMillis: 5000

  exampleFunctionRoute:
    Type: "AWS::ApiGatewayV2::Route"
    DependsOn:
      - exampleFunctionIntegration
    Properties:
      ApiId: !Ref ApiId
      RouteKey: "GET /{tenantCode}/example-mfe"
      AuthorizationType: "CUSTOM"
      AuthorizerId: "5c4vcc"
      Target: !Join
        - /
        - - integrations
          - !Ref exampleFunctionIntegration

  exampleFunctionInvokePermissions:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt exampleFunction.Arn
      Principal: apigateway.amazonaws.com
      SourceAccount: 129231881647

```

## Logging

Logging is performed by [Zap](go.uber.org/zap). Zap is a structured logger, meaning you can pass it complex objects and depending on the environment will either log in JSON or plain text.

## Build Process

Jenkins is used to perform the build. Each MFE should define a Jenkins file that outlines its specific build steps. The default Jenkins file has been configured to build and test both the Lambdas and the UI code.

### Setup

1. Configure environment variables at the start of the Jenkins file.
1. Configure the SAM template.yml file with the correct settings for your environment.
1. Configure Bitbucket to notify Jenkins on push.
1. Setup a new Jenkins project to listen to Bitbucket push events.

### Jenkins deployment key

```
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAxZvG2qIl7Q3QpJ20SV3+7OFSDQRUptVK6Mp2pUp7XHhibQWPFiGCzqsCSPo3E2UWk7dyALnfSjquYeaqm0u0IVtSn+d7zowvMCqge5VhHEbMGqZ4SE39TyFZpdqkhQ7wPaCKYzK7o/s6k1hTKFLDDihweVk93a/pRk5274yVGEhgGhhWLEA7AQS/VLI5epGDRMlVzlWb6Q2A4MGFccxxWfr/lmGToJJ1T46x1gY8VSxsbqRgqAd9Ps/dAgIBV9/6rA7WBKfLzAYD+6cB1tMin7GdUhn8VSHle7DR0Lsd13aXkMheLcEn7ctPEQPFaNh73l2pBoljLG+4QjIOnnbm7w== rsa-key-20160517
```

### Jenkins Webhook

When setting up the GIT repository you need to create a webhook that triggers Jenkins when new code is pushed. The Hook should be named `jenkins` with an address of https://jenkins.kxcloud.net/bitbucket-hook/

## FAQ - GO

### cmd vs internal directories

`cmd` represents an entry point for a Lambda function. `internal` is code that is required to make the Lambda run and do its work. The code within each Lambda's `./cmd/main.go` should be as minimal as possible, focusing purely on creating dependencies referenced within the `internal` directory. This creates a clear separation between initialisation code and logic.

### Where can I get help learning GO?

+ [https://tour.golang.org/welcome/1](Take the tour)
+ [https://quii.gitbook.io/learn-go-with-tests/](Learn GO with tests)
+ [https://gobyexample.com/](GO by example)

### Why is X like Y

First, [https://go-proverbs.github.io/](Refer to the proverbs). If not satisfied, raise it, maybe it's wrong.

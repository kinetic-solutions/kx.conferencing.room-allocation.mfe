#!/usr/bin/env groovy
def COLOR_MAP = [
    'SUCCESS': 'good', 
    'FAILURE': 'danger',
]

pipeline {
  options {
    disableConcurrentBuilds()
  }

  environment {
    awsCredentialID = "9a06251a-3905-4973-ac03-f29fa0c30d73"
    AWS_DEFAULT_REGION     = "eu-west-1"

    // These two values are project-specific and need to be changed
    // Don't forget to change your SAM parameters in the SAM build step
    FOLDER_NAME                = "kinetic-room-allocation"
    PROJECT                    = "@kinetic/room-allocation"
    BUILD_ROOT_FILE            = "/${FOLDER_NAME}/${VERSION}/kinetic-room-allocation.js"
    SLACK_NOTIFICATION_CHANNEL = "#a-team"
    SAM_STACK_NAME             = "room-allocation"

    S3_BUCKET = "static.kxcloud.net/applications/${FOLDER_NAME}"
  }

  agent { label 'linux-ec2' }

  stages {
    stage("Test") {
      parallel {
        stage("UI") {
          agent {
            docker {
              image "node:13.13-alpine"
              args "-v ${WORKSPACE}:/code -w /code"
              reuseNode true
            }
          }

          steps {
            sh "cd public && npm install"
            sh "cd public && npm run test:ci"
          }
        }
        stage("lambda") {
          environment {
            SQL_IMAGE="mcr.microsoft.com/mssql/server:latest"
            GERM_CONNECTION="{\"Username\": \"sa\", \"Password\": \"Aa1*xxxx\", \"Port\": 1433, \"Server\": \"sql\"}"
          }
          steps {
            script {
              docker.image(SQL_IMAGE).withRun('-e "SA_PASSWORD=Aa1*xxxx" -e "ACCEPT_EULA=Y" -e "MSSQL_PID=Developer" -p 1433') { c ->
                // wait for the database to finish restoring
                sh "bash -c \"grep -q 'Recovery is complete.' <(docker logs -f ${c.id})\""

                // pull the go docker image that we're going to run the tests in and
                // link the SQL database in
                docker.image('golang:1').inside("--link ${c.id}:sql") {
                  sh 'make test'
                }
              }
            }
          }
        }
      }
    }

    stage("UI Build") {
      agent {
        docker {
          image "node:13.13-alpine"
          args "-v ${WORKSPACE}:/code -w /code"
          reuseNode true
        }
      }

      steps {
        sh "cd public && npm run build"
      }
    }

    stage("SAM Deploy") {
      agent {
        dockerfile {
          filename "jenkins.go.dockerfile"
        }
      }

      steps {
        withAWS(credentials:awsCredentialID) {
          sh 'sam build'
          sh "sam deploy --no-fail-on-empty-changeset --stack-name ${SAM_STACK_NAME} --s3-bucket aws-sam-cli-managed-default-samclisourcebucket-1n7gwg473xnu --s3-prefix ${FOLDER_NAME} --region ${AWS_DEFAULT_REGION} --tags 'Orchestrator=SAM'"
        }
      }
    }

    stage("UI Deploy") {
      steps {
        script {
          withAWS(credentials:awsCredentialID) {
            sh "aws s3 sync public/dist/ s3://static.kxcloud.net/applications"
          }
        }
      }
    }
  }

  post {

    regression {
      script {
        env.GIT_COMMIT_MSG = sh (script: 'git log -1 --pretty=%B ${GIT_COMMIT}', returnStdout: true).trim()
        env.GIT_AUTHOR = sh (script: 'git log -1 --pretty=%cn ${GIT_COMMIT}', returnStdout: true).trim()
      }
      
      slackSend channel: "${SLACK_NOTIFICATION_CHANNEL}", color: COLOR_MAP[currentBuild.currentResult], message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.GIT_AUTHOR}\n Commit: ${env.GIT_COMMIT_MSG}\n More info at: ${env.BUILD_URL}"
    }

    fixed {
      script {
        env.GIT_COMMIT_MSG = sh (script: 'git log -1 --pretty=%B ${GIT_COMMIT}', returnStdout: true).trim()
        env.GIT_AUTHOR = sh (script: 'git log -1 --pretty=%cn ${GIT_COMMIT}', returnStdout: true).trim()
      }
      
      slackSend channel: "${SLACK_NOTIFICATION_CHANNEL}", color: COLOR_MAP[currentBuild.currentResult], message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.GIT_AUTHOR}\n Commit: ${env.GIT_COMMIT_MSG}\n More info at: ${env.BUILD_URL}"
    }
  }
}
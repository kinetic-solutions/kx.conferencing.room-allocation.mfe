.PHONY: build

export GO111MODULE=on
export GOOS=linux
export GOARCH=amd64

test:
	go test -race -count=1 ./...

build:
	CGO_ENABLED=0 sam build --parallel --cached
